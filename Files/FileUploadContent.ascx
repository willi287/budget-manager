﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileUploadContent.ascx.cs" Inherits="Eproc2.Web.Files.FakeFileUploadContent" %>

<%@ Register src="~/Controls/Silverlight/FileUpload.ascx" tagname="FileUpload" tagprefix="uc1" %>

<asp:FileUpload ID="fuFake" runat="server" />
    <asp:Button ID="btnUpload" runat="server" Text="Upload" 
        onclick="btnUpload_Click" />
<br />        
<asp:HyperLink ID="hlFileUrl" runat="server" />

<uc1:FileUpload SelectFileText="Upload PDF" FileUploadHandler="http://localhost:40000/receiver.ashx" MaxFileLength="4194304" ID="fileUpload" UploadedFileName="uploaded.file"  runat="server" FileFilter="All *.*|*.*|Pdf |*.pdf" />
