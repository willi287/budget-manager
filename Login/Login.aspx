﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Empty.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="Eproc2.Web.Login.Login" EnableViewState="false" %>

<%@ Register Src="LoginContent.ascx" TagName="LoginContent" TagPrefix="LoginContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="cntLogin" ContentPlaceHolderID="cphMainContent" runat="server">
    <div class="error">

        <script language="javascript">
            var validVersion = false;
            var inxStart;
            if ((inxStart = navigator.userAgent.indexOf("MSIE")) != -1) {
                inxStart += 5; var inxEnd = navigator.userAgent.indexOf(".", inxStart);
                var version = navigator.userAgent.substring(inxStart, inxEnd);
                if (version >= 7) validVersion = true;
            }
            if (validVersion == false) {
                document.write("<%= Resources.SR.IEWarning %>");
            }
        </script>

    </div>
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <LoginContent:LoginContent ID="LoginContent" runat="server" />
</asp:Content>
