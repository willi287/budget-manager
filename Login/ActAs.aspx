﻿<%@ Page Title="Act as..." Language="C#" AutoEventWireup="true" CodeBehind="ActAs.aspx.cs" MasterPageFile="~/Masters/Empty.Master"
    Inherits="Eproc2.Web.Login.ActAs" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ActAsContent.ascx" tagname="ActAsContent" tagprefix="actAs" %>

<asp:Content ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <actAs:ActAsContent ID="ActAsContent" runat="server"  />
</asp:Content>
