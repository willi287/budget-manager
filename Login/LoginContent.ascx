﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginContent.ascx.cs"
    Inherits="Eproc2.Web.Login.LoginContent" %>
<table>
    <tr>
        <td class="bold">
            <asp:Label ID="lblVW" runat="server" Text="<%$ Resources:SR,Valueworks %>" />
            &#174;
            <asp:Label ID="lblEprocTM" runat="server" Text="<%$ Resources:SR,EprocTM %>" />
            &#8482;
            <asp:Label ID="lblLogin" runat="server" Text="<%$ Resources:SR,Login %>" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Login ID="LoginForm" runat="server" OnAuthenticate="LoginForm_Authenticate" >
                <LayoutTemplate >
                            <table class="login">
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="UserNameLabel" runat="server" Text="<%$ Resources:SR, UserName %>"
                                            AssociatedControlID="UserName" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="UserName" runat="server" />
                                        <asp:RequiredFieldValidator ID="UserNameRequired" Display="Dynamic" runat="server" ControlToValidate="UserName"
                                            ErrorMessage="<%$ Resources:ValidationSR, UserNameRequiredMsg %>" ValidationGroup="ctl00$LoginForm" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="PasswordLabel" runat="server" Text="<%$ Resources:SR,Password %>"
                                            AssociatedControlID="Password" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" />
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" Display="Dynamic"
                                            ControlToValidate="Password" ErrorMessage="<%$ Resources:ValidationSR, PasswordRequiredMsg %>"
                                            ValidationGroup="ctl00$LoginForm" />
                                    </td>
                                </tr>
                                <tr>
                                <td>&nbsp;</td>
                                    <td>
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Login" ValidationGroup="ctl00$LoginForm" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ForeColor="Red"  ID="FailureText" runat="server" EnableViewState="False" />
                </LayoutTemplate>
            </asp:Login>
        </td>
    </tr>
</table>
