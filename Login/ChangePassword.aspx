﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Empty.Master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="Eproc2.Web.Login.ChangePassword" EnableViewState="false" %>

<%@ Register Src="ChangePasswordContent.ascx" TagName="ChangePasswordContent" TagPrefix="ChangePasswordContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="cntLogin" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ChangePasswordContent:ChangePasswordContent ID="ChangePasswordContent" runat="server" />
</asp:Content>
