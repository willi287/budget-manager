﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActAsContent.ascx.cs"
    Inherits="Eproc2.Web.Login.ActAsContent" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table cellpadding="0" border="0" style="width: 317px;">
    <tr>
        <td class="bold">
            <asp:Label ID="lblVW" runat="server" Text="<%$ Resources:SR,Valueworks %>" />
            &#174;
            <asp:Label ID="lblEprocTM" runat="server" Text="<%$ Resources:SR,EprocTM %>" />
            &#8482;
            <asp:Label ID="lblLogin" runat="server" Text="<%$ Resources:SR,Login %>" />
        </td>
    </tr>
    <tr>
        <td>
            <br/>
        </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="10" border="1" style="width: 417px;" cellspacing="0">
                <tr>
                    <td>
                        <table cellpadding="5" border="0" style="width: 417px;">
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblOrganisationType" runat="server" Text="<%$ Resources:SR,Type%>" />:
                                </td>
                                <td nowrap="nowrap">
                                    <qwc:DropDownList ID="ddlOrganisationType" runat="server" DataTextField="Value" DataValueField="Id"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" SortOrder="None"/>
                                </td>
                                <td style="width: 100px;">
                                </td>
                            </tr>
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblOrganisation" runat="server" Text="<%$ Resources:SR,Organisation%>" />:
                                </td>
                                <td>
                                    <qwc:DropDownList ID="ddlOrganisations" runat="server" AutoPostBack="true" DataTextField="Value"
                                        ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                                        DataValueField="Id" OnSelectedIndexChanged="ddlOrganisations_SelectedIndexChanged"  SortOrder="None"/>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblUser" runat="server" Text="<%$ Resources:SR,User%>" />:
                                </td>
                                <td>
                                    <qwc:DropDownList ID="ddlUsers" runat="server" AutoPostBack="false" DataTextField="Value"
                                        ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                                        DataValueField="Id" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged"  SortOrder="None"/>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnActAs" runat="server" Text="<%$ Resources:SR,ActAsSelectedUser %>"
                                        OnClick="btnActAs_Click" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
