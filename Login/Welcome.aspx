<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Login.Welcome" %>
<%@ Register src="WelcomeContent.ascx" tagname="WelcomeContent" tagprefix="uc1" %>
<%@ Register src="~/Controls/Core/NotificationArea.ascx" tagname="NotificationArea" tagprefix="uc1" %>

<asp:Content ID="Content0" ContentPlaceHolderID="NotificationAreaPlaceHolder" runat="server">
    <uc1:NotificationArea ID="NotificationArea1" runat="server" />    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">       
    <uc1:WelcomeContent ID="WelcomeContent1" runat="server" />       
</asp:Content>
