﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordContent.ascx.cs" Inherits="Eproc2.Web.Login.ChangePasswordContent" %>

<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<table>
    <tr>
        <td class="bold">
            <asp:Label ID="lblChangePassword" runat="server" Text="<%$ Resources:SR,PasswordChange %>" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblChangePasswordMsg" runat="server" Text="<%$ Resources:SR,ChangePasswordMsg %>" />
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="required" align="left">
            <asp:Label ID="lblOldPassword" runat="server" Text="<%$ Resources:SR,OldPassword %>"
                AssociatedControlID="OldPassword" />&nbsp;<span>*</span>
        </td>
        <td>
            <asp:TextBox ID="OldPassword" runat="server" TextMode="Password"/>
            <asp:RequiredFieldValidator ID="OldPasswordRequired" runat="server" Display="Dynamic"
                ControlToValidate="OldPassword" ErrorMessage="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                ValidationGroup="ctl00$LoginForm" />
            <asp:RegularExpressionValidator ID="revOldPassword" runat="server" Text="*" ControlToValidate="OldPassword" ValidationExpression=".{0,50}" />
        </td>
    </tr>
    <tr>
        <td class="required" align="left">
            <asp:Label ID="lblNewPassword" runat="server" Text="<%$ Resources:SR,NewPassword %>"
                AssociatedControlID="NewPassword" />&nbsp;<span>*</span>
        </td>
        <td>
            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"/>
            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" Display="Dynamic"
                ControlToValidate="NewPassword" ErrorMessage="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                ValidationGroup="ctl00$LoginForm" />
            <asp:RegularExpressionValidator ID="revNewPassword" runat="server" Text="*" ControlToValidate="NewPassword" ValidationExpression=".{0,50}" />
        </td>
    </tr>
    <tr>
        <td class="required" align="left">
            <asp:Label ID="lblConfirmNewPassword" runat="server" Text="<%$ Resources:SR,ConfirmNewPassword %>"
                AssociatedControlID="ConfirmNewPassword" />&nbsp;<span>*</span>
        </td>
        <td>
            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"/>
            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" Display="Dynamic"
                ControlToValidate="ConfirmNewPassword" ErrorMessage="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                ValidationGroup="ctl00$LoginForm" />
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="left">
            <asp:CompareValidator Display="Dynamic" ID="cvConfirmPassword" ControlToValidate="NewPassword"
                ControlToCompare="ConfirmNewPassword" runat="server" ErrorMessage="<%$ Resources:SR, EnterTheSamePassword %>" ValidationGroup="ctl00$LoginForm" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="left">
            <asp:Button ID="ChangePasswordButton" runat="server" OnCommand="OnChangePassword" Text="Submit" ValidationGroup="ctl00$LoginForm" />
        </td>
    </tr>

</table>