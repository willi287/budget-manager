﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeContent.ascx.cs" Inherits="Eproc2.Web.Login.WelcomeContent" %>
<%@ Register src="~/Controls/Core/Terms.ascx" tagname="Terms" tagprefix="uc3" %>

<uc3:Terms ID="terms" runat="server"/>

<div class="welcome">
    <h3>
        <asp:Literal ID="literal" runat="server" />
    </h3>
    <h4>
        <asp:LinkButton runat="server" ID="lbDetails" Text="<%$ Resources:SR,ConfirmDetails %>" OnClick="lbDetails_OnClick"/>
    </h4>
    <div>
        <asp:Literal runat="server" Text="<%$ Resources:SR,RecommendChangePassword %>" />
    </div>
    <div>
        <%=GetMoreInformationText() %>
    </div>
</div>
