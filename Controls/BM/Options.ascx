﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Options.ascx.cs" Inherits="Eproc2.Web.Controls.BM.Options" EnableViewState="true" %>

<table class="options">
    <tr>
        <td>
            <qwc:DropDownList ID="ddlPbm" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPbm_SelectedIndexChanged" ComparerType="Eproc2.Web.Controls.BM.EprocListItemComparer, Eproc2.Web" />
        </td>
        <td>
            <qwc:DropDownList ID="ddlPba" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPba_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>        
        <td>
            <qwc:DropDownList ID="ddlBcg" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBcg_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
    </tr>
</table>
