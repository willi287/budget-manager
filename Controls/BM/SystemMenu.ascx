﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemMenu.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.SystemMenu" %>
    
<asp:HyperLink ID="hlHome" runat="server" Text="<%$ Resources:SR,SM_HOME%>" />|
<asp:LinkButton ID="hlHelp" runat="server" Text="<%$ Resources:SR,SM_HELP%>" />
