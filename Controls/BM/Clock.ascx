﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Clock.ascx.cs" Inherits="Eproc2.Web.Controls.BM.Clock" %>
 <DIV>
        <script type="text/javascript">
            var minVal = $("input#<%=BindingMinuteControllId %>").val();
            var hourVal = $("input#<%=BindingHourControllId %>").val();
            
            minute = minVal;
            hour = hourVal;
            
            $("input#<%=BindingMinuteControllId %>").change(function() {
                minVal = $("input#<%=BindingMinuteControllId %>").val();
                minute = minVal;
                updateClock();
            });
            
            $("input#<%=BindingHourControllId %>").change(function() {
                hourVal = $("input#<%=BindingHourControllId %>").val();
                hour = hourVal;
                updateClock();
            });
        
	</script>
     <DIV id=clock_a></DIV>
     
 </DIV>
