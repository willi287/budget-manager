﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BVSelector.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.BVSelector" %>
<%@ Import Namespace="Qulix.Entities.Base" %>
<div class="bvSelectorLink">
    <asp:LinkButton ID="lbChangeBV" runat="server" OnCommand="OnCommand_ChangeBusinessView">
        <asp:Image ID="imgTitle" runat="server" SkinID="bvTitle" />&nbsp;<asp:Image ID="imgChange"
            runat="server" SkinID="bvChange" />
    </asp:LinkButton>    
</div>
