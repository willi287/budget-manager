﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasketItemsProcessedBox.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.BasketItemsProcessedBox" %>
<div style="width:100%" align="center">
    <div class="BasketItemsProcessedBox round20" align="center">
        <div class="BasketItemsProcessedBoxText">
            <asp:Literal runat=server Text="<%$ Resources: BM_SR, BasketItemsProcessedMessage %>"></asp:Literal>
        </div>
        <br />
        <div class="BasketItemsProcessedBoxLogo" >
        </div>
    </div>
</div>
