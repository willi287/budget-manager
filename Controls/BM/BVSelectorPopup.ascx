﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BVSelectorPopup.ascx.cs" Inherits="Eproc2.Web.Controls.BM.BVSelectorPopup" %>
<%@ Import Namespace="Qulix.Entities.Base"%>
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl" TagPrefix="e2w" %>
<e2w:PopupTemplatedControl runat="server" ID="pcSelectBV" Width="480px" IsSubmitVisible="true" IsCancelVisible="true" OnSubmit="OnSubmit" >
    <Title>
        <qwc:label text="<%$ Resources:BM_SR,BusinessView %>" runat="server" />        
        <h6>
            <qwc:label text="<%$ Resources:BM_SR,SelectRole %>" runat="server" />
        </h6>
    </Title>
    <Content>
        <div style="padding-left:110px; padding-top:25px; padding-bottom:40px">
            <table style="width: 250px;">
                <asp:Repeater runat="server">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <qwc:GroupRadioButton ID="rbView" 
                                    Text="<%# Eval(LookupEntity.PROP_VALUE) %>" 
                                    Key="<%# Eval(LookupEntity.PROP_ID) %>"
                                    Checked="<%# Eval(LookupEntity.PROP_ID).ToString() == BusinessViewId %>"
                                    GroupName="BusinessView" runat="server" />
                            </td>
                            <td>
                                <div style="width: 70px; height: 10px;
                                    background-color: <%# GetBusinessViewColor((Guid)Eval(LookupEntity.PROP_ID))%>;
                                    border-color: #C0C0C0">
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>            
        </div>
    </Content>
</e2w:PopupTemplatedControl>
