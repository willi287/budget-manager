﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserMenu.ascx.cs" Inherits="Eproc2.Web.Controls.BM.UserMenu" %>
<asp:HyperLink ID="hlActAs" Visible="false" runat="server" NavigateUrl="~/Login/ActAs.aspx"
    Text="<%$ Resources:SR,SM_ACTAS%>" />
<asp:HyperLink ID="hlActAsOriginalUser" Visible="false" runat="server" NavigateUrl="~/Login/ActAs.aspx?logout=1"
    Text="<%$ Resources:SR,SM_ACTAS_ORIGINAL_USER%>" />
<asp:Literal Text="&nbsp;|&nbsp;" runat="server" ID="actAsSeparator" />
<asp:HyperLink ID="hlLogout" Visible="false" runat="server" NavigateUrl="~/Login/Login.aspx?logout=1"
    Text="<%$ Resources:SR,SM_LOGOUT%>" />
