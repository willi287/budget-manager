﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EndDatePanel.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.DTS.EndDatePanel" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<asp:panel runat="server" id="pnlEndDate" class="endDate" groupingtext="<%$ Resources:BM_SR, EndDate %>">
    <table>
        <tr class="endCustom">
            <td>
                <qwc:RadioButton ID="rbEndDate" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndDate %>" />
            </td>
            <td>
                <e2c:Calendar ID="calEndDate" runat="server"/>
            </td>
        </tr>
        <tr class="endPeriod">
            <td>
                <qwc:RadioButton ID="rbEndAfter" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndAfter %>" />
            </td>
            <td> 
                <qwc:TextBox ID="tbEndPeriod" runat="server" MaxLength="3" CssClass="shortest" />
                <asp:RegularExpressionValidator id="rvEndPeriod" runat="server" text="*" controltovalidate="tbEndPeriod"
                    ValidationExpression="\d*" tooltip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>"
                    display="Dynamic" />
                <asp:Label ID="litPeriodDays" CssClass="daily" runat="server" Text="<%$ Resources:BM_SR, Days %>" />                            
                <asp:Label id="litPeriodWeeks" CssClass="weekly" runat="server" text="<%$ Resources:BM_SR, Weeks %>" />                            
                <asp:Label id="litPeriodMonths" CssClass="monthly" runat="server" text="<%$ Resources:BM_SR, Monthes%>" />                            
            </td>
        </tr>
        <tr class="noEnd">
            <td>
                <qwc:RadioButton ID="rbNoEndDate" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, NoEndDate %>" />
            </td>
        </tr>
    </table>
</asp:panel>
