﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeSelector.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.DTS.DateTimeSelector" %>
<%@ Register Src="StartDatePanel.ascx" TagName="StartDatePanel" TagPrefix="uc1" %>
<%@ Register Src="AppointementTimesPanel.ascx" TagName="AppointementTimesPanel" TagPrefix="uc2" %>
<%@ Register Src="RepeatingPatternPanel.ascx" TagName="RepeatingPatternPanel" TagPrefix="uc3" %>
<%@ Register Src="EndDatePanel.ascx" TagName="EndDatePanel" TagPrefix="uc4" %>

<script type="text/javascript">
    $(this).onPartialDocumentReady(onLoadDateTimeSelector);
    $('.popupControlOk').live('click', function (event) {
        DisableHiddenValidators();
    });
</script>

<div class="dateTimeSelector">
    <uc1:StartDatePanel ID="StartDatePanel1" runat="server" />
    <uc2:AppointementTimesPanel ID="AppointementTimesPanel1" runat="server" />
    <div class="selectors">
        <asp:CheckBox runat="server" ID="cbShowRepeatingOptions" Text="<%$ Resources:BM_SR,ShowRepeatingOptions %>"
            class="repeatingOptions" />
    </div>
    <uc3:RepeatingPatternPanel ID="RepeatingPatternPanel1" runat="server" />
    <uc4:EndDatePanel id="EndDatePanel1" runat="server" />
</div>
