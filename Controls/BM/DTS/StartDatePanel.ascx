﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StartDatePanel.ascx.cs" Inherits="Eproc2.Web.Controls.BM.DTS.StartDatePanel" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<asp:panel runat="server" id="pnlStartDate" groupingtext="<%$ Resources:SR, StartDate %>" class="startDate">
    <table>
        <tr>
            <td class="caption">
                <qwc:Label ID="lblStartDate" runat="server" Text="<%$ Resources:SR, StartDate %>" />
            </td>
            <td>
                <e2c:Calendar ID="calStartDate" runat="server" />
            </td>
        </tr>
    </table>
</asp:panel>

