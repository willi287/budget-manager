﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RepeatingPatternPanel.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.DTS.RepeatingPatternPanel" %>
<asp:panel runat="server" id="pnlRepeatingPattern" groupingtext = "<%$ Resources:BM_SR,RepeatingPattern %>"
    class="repeatingPattern">
    <table class="subform" style="border-collapse: collapse">
        <tr>
            <td class="selectors">
                <table>
                    <tr>
                        <td>
                            <qwc:GroupRadioButton ID="rbDaily" Text = "<%$ Resources:BM_SR,Daily %>" runat="server"
                                CssClass="daily" AutoPostBack="false" GroupName="selectors"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:GroupRadioButton ID="rbWeekly" Text = "<%$ Resources:BM_SR,Weekly %>" runat="server"
                                CssClass="weekly" AutoPostBack="false" GroupName="selectors"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:GroupRadioButton ID="rbMonthly" Text = "<%$ Resources:BM_SR,Monthly %>" runat="server"
                                CssClass="monthly" AutoPostBack="false" GroupName="selectors"/>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="gray panels">
                <asp:panel id="pDailyOptions" runat="server" CssClass="daily">
                    <div>
                        <qwc:GroupRadioButton id="rbRepeatEveryDay" text = "<%$ Resources:BM_SR,RepeatEvery %>" runat="server" GroupName="daily"/>
                        <qwc:TextBox id="tbDailyPeriod" runat="server" />
                        <asp:RegularExpressionValidator ID="rvDailyPeriod" runat="server" text = "*" controltovalidate="tbDailyPeriod"
                            tooltip="CommonRangeVEMsg" display="Dynamic" ValidationExpression="\d*" />
                        <asp:Literal id="litDays" text = "<%$ Resources:BM_SR,days %>" runat="server" />
                    </div>
                    <div>
                        <qwc:GroupRadioButton id="rbRepeatEveryWeekday" text = "<%$ Resources:BM_SR,RepeatEveryWeekday %>" runat="server" GroupName="daily"/>
                    </div>
                    <div>
                        <qwc:GroupRadioButton id="rbRepeatEveryWeekend" text = "<%$ Resources:BM_SR,RepeatEveryWeekend %>" runat="server" GroupName="daily"/>
                    </div>
                </asp:panel>
                <asp:panel id="pWeeklyOptions" runat="server" cssclass="weekly">
                    <div>
                        <asp:Literal id="litRepeatEvery" runat="server" text = "<%$ Resources:BM_SR,RepeatEvery %>" />
                        <qwc:TextBox ID="tbWeekPeriod" runat="server" CssClass="shortest" MaxLength="2" />
                        <asp:RegularExpressionValidator ID="rvWeekPeriod" runat="server" Text="*" ControlToValidate="tbWeekPeriod"
                            ValidationExpression="\d*" ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                        <asp:Literal id="litWeekOn" runat="server" text = "<%$ Resources:BM_SR,WeekOn %>" />
                        <asp:CheckBoxList id="cblWeeklyPatern" runat="server" repeatdirection="Vertical" />
                    </div>
                </asp:panel>
                <asp:panel id="pMonthlyOptions" runat="server" cssclass="monthly">
                    <div class="repeatEveryDayMonthly">
                        <qwc:GroupRadioButton id="rbRepeatEveryDayMonthly" CssClass="allDaysOfWeek" runat="server" text = "<%$ Resources:BM_SR,day %>" GroupName="monthly"/>
                            <qwc:TextBox ID="tbDayNumber" runat="server" CssClass="dayNumber shortest" MaxLength="2" />
                            <asp:RangeValidator id="rvRepeatFrequency" MinimumValue="1" MaximumValue="31" Type="Integer" runat="server"
                                text="*" ControlToValidate="tbDayNumber" ToolTip="<%$ Resources:BM_ValidationSR,DayNumberCommonRangeVEMsg %>" Display="Dynamic"/>
                            <asp:Literal id="litOfEvery1" runat="server" text = "<%$ Resources:BM_SR,ofEvery %>" />
                            <qwc:TextBox ID="tbMonthPeriod1" runat="server" CssClass="monthPeriod shortest" MaxLength="2" />

                            <asp:RangeValidator id="rvRepeatPattern" MinimumValue="1" MaximumValue="12" Type="Integer" runat="server"
                                text="*" ControlToValidate="tbMonthPeriod1" ToolTip="<%$ Resources:BM_ValidationSR,MonthNumberCommonRangeVEMsg %>" Display="Dynamic"/>

                            <asp:Literal id="litMonth" runat="server" text = "<%$ Resources:BM_SR, monthPart %>" />
                        </div>
                    <qwc:Label ID="lblMonthlyAlert" CssClass="red hidden" runat="server" Text="<%$ Resources:BM_SR, MonthlyAlert%>"/>                    
                    <asp:HiddenField ID="hfTemplate" value="<%$ Resources:BM_SR, MonthlyAlert%>" runat="server" />
                    <div>
                        <div class="row repeatEveryXDayMonthly">
                            <qwc:GroupRadioButton id="rbRepeatEveryXDayMonthly" text = "<%$ Resources:BM_SR,the %>" runat="server" GroupName="monthly" />
                                <qwc:DropDownListControllable id="ddlNumber" runat="server" CssClass="dayNumber short"/>
                                <qwc:DropDownListControllable id="ddlDayOfWeek" runat="server" CssClass="weekDay short"/>
                                <qwc:Label ID="litOfEvery" Text = "<%$ Resources:BM_SR,ofEvery %>" runat="server" />
                                <qwc:TextBox ID="tbMonthPeriod2" runat="server" CssClass="monthPeriod shortest"/>
                                <asp:RangeValidator id="rvMonthPeriod2" MinimumValue="1" MaximumValue="12" Type="Integer" runat="server"
                                    text="*" ControlToValidate="tbMonthPeriod2" ToolTip="<%$ Resources:BM_ValidationSR,MonthNumberCommonRangeVEMsg %>" Display="Dynamic"/>
                                <qwc:Label ID="litMonths" Text = "<%$ Resources:BM_SR,months %>" runat="server" />
                            </div>
                        </div>
                </asp:panel>
            </td>
        </tr>
    </table>
</asp:panel>