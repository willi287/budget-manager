﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AppointementTimesPanel.ascx.cs"
    Inherits="Eproc2.Web.Controls.BM.DTS.AppointementTimesPanel" %>

<asp:Panel runat="server" ID="pnlAppointmentTimes" GroupingText="<%$ Resources:BM_SR, AppointmentTimes %>" class="appointment">
    <table class="form">
        <tr>
            <td>
                <qwc:RadioButton ID="rbIsTimeSpecified" GroupName="SpecificTime" runat="server" CssClass="isTimeSpecified" Text="<%$ Resources:BM_SR, SpecificTimeIsRequired %>" />
            </td>
            <td>
                <qwc:RadioButton ID="rbAnytime" GroupName="SpecificTime" runat="server" CssClass="anytime" Text="<%$ Resources:BM_SR, Anytime %>" />
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:Label ID="lblStartTime" runat="server" Text="<%$ Resources:BM_SR,StartTime %>"
                    AssociatedControlID="stStartTime" />:
            </td>
            <td>
                <qwc:SelectTime ID="stStartTime" runat="server" CssClass="startTime" AllowEmptyValue="true"/>
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:Label ID="lblEndTime" runat="server" Text="<%$ Resources:BM_SR,EndTime %>" AssociatedControlID="stEndTime" />:
            </td>
            <td>
                <qwc:SelectTime ID="stEndTime" runat="server" CssClass="endTime" AllowEmptyValue="true"/>
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:Label ID="lblDuration" runat="server" Text="<%$ Resources:BM_SR,Duration %>" AssociatedControlID="stDuration" />:
            </td>
            <td>
                <qwc:SelectTime ID="stDuration" IsDurationMode="true" runat="server" CssClass="duration"/>
            </td>
        </tr>
        <tr>
            <td />
            <td>
                <qwc:QWCheckBox ID="cbAllDay" Text="<%$ Resources:BM_SR, AllDay %>" runat="server" CssClass="allDay"/>
            </td>
        </tr>
    </table>
</asp:Panel>
