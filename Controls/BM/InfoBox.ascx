﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InfoBox.ascx.cs" Inherits="Eproc2.Web.Controls.BM.InfoBox" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl"
    TagPrefix="e2w" %>
<%@ Register Src="~/Content/BM/BusinessCards/IndividualBusinessCard.ascx" TagName="IndividualBusinessCard"
    TagPrefix="bm" %>
<%@ Register Src="~/Content/BM/BusinessCards/CommonBusinessCard.ascx" TagName="CommonBusinessCard"
    TagPrefix="bm" %>
<table cellspacing="10px" class="form">
    <tr>
        <td width="50%">
            <table class="subForm grey">
                <tr>
                    <td rowspan="4" width="65px">
                        <qwc:Image ID="contactDetailsIndicatorImage" ImageUrl="~\App_Themes\Default\Images\default_icon_male.png"
                            runat="server" />
                    </td>
                    <td class="caption" width="40%">
                        <qwc:InfoLabel ID="individualNameLabel" Text="<%$ Resources:BM_SR,IndividualsName %>" runat="server" />:
                    </td>
                    <td width="60%">
                        <qwc:DropDownList ID="ddlIndividuals" DataTextField="Value" DataValueField="Id" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" />
                    </td>
                    <td width="25px">
                        <asp:ImageButton ID="showIndividualCardLink" OnCommand="ShowIndividualBusinessCard" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="buyerNameLabel" Text="<%$ Resources:BM_SR,BuyerName %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="buyerNameValue" runat="server" />
                    </td>
                    <td>
                        <asp:ImageButton ID="showBuyerCardLink" OnCommand="ShowBuyerCard" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="buyerOrganisationLabel" Text="<%$ Resources:BM_SR,BuyerOrganisation %>"
                            runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="buyerOrganisationValue" runat="server" />
                    </td>
                    <td>
                        <asp:ImageButton ID="showBuyerOrganisationCardLink" OnCommand="ShowBuyerOrganisationCard" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:BM_SR, BuyerReference %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyerReference" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="caption">
                        <qwc:InfoLabel ID="supplierOrganisationLabel" Text="<%$ Resources:BM_SR,ServiceProviderOrganisationUCase %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="supplierOrganisationValue" runat="server" />
                    </td>
                    <td>
                        <asp:ImageButton ID="showSupplierOrganisationCardLink" OnCommand="ShowSupplierOrganisationCard" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:SR, ProviderReference %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblProviderReference" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table class="subForm grey">
                <tr>
                    <td rowspan="4" width="65px">
                        <qwc:Image ID="documentDetailsIndicatorImage" ImageUrl="~\App_Themes\Default\Images\simpledocument.png"
                            runat="server" />
                    </td>
                    <td  class="caption" width="40%">
                        <qwc:InfoLabel ID="documentStatusLabel" Text="<%$ Resources:BM_SR,DocumentStatus %>"
                            runat="server"/>:
                    </td>
                    <td width="60%">
                        <qwc:Label ID="documentStatusValue" runat="server" />
                    </td>
                    <td width="25px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="documentNumberLabel" Text="<%$ Resources:BM_SR,DocumentNumber %>"
                            runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="documentNumberValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="dateSubmittedLabel" Text="<%$ Resources:BM_SR,DateSubmitted %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="dateSubmittedValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="dateAcceptedLabel" Text="<%$ Resources:BM_SR,DateAccepted %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="dateAcceptedValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:SR, BuyerOrderNumber %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyerOrderNumber" runat="server" />
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr>
            </table>
        </td>
    </tr>
</table>
<e2w:PopupTemplatedControl ID="individualCardPopup" runat="server" IsSubmitVisible="false" CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <title />
    <content>
        <bm:IndividualBusinessCard ID="individualCard" runat="server" />
    </content>
</e2w:PopupTemplatedControl>
<e2w:PopupTemplatedControl id="buyerOrganisationCardPopup" runat="server" IsSubmitVisible="false" CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <title />
    <content>
        <bm:CommonBusinessCard ID="buyerOrganisationCard" runat="server" />        
    </content>
</e2w:PopupTemplatedControl>
<e2w:PopupTemplatedControl id="supplierOrganisationCardPopup" runat="server" IsSubmitVisible="false" CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <title />
    <content>
        <bm:CommonBusinessCard ID="supplierOrganisationCard" runat="server" />        
    </content>
</e2w:PopupTemplatedControl>
<e2w:PopupTemplatedControl ID="buyerCardPopup" runat="server" IsSubmitVisible="false" CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <title />
    <content>        
        <bm:CommonBusinessCard ID="buyerCard" runat="server" />
    </content>
</e2w:PopupTemplatedControl>
