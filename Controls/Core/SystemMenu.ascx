﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemMenu.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.SystemMenu" EnableViewState="false" %>
<asp:HyperLink ID="hlHome" runat="server" Text="<%$ Resources:SR,SM_HOME%>" />|
<asp:LinkButton ID="hlHelp" runat="server" Text="<%$ Resources:SR,SM_HELP%>" />
<%if (ShouldDisplayActAsButton)
  { %>
|<asp:HyperLink ID="hlActAs" runat="server" NavigateUrl="~/Login/ActAs.aspx" Text="<%$ Resources:SR,SM_ACTAS%>" />
<asp:HyperLink ID="hlActAsOriginalUser" runat="server" NavigateUrl="~/Login/ActAs.aspx?logout=1"
    Text="<%$ Resources:SR,SM_ACTAS_ORIGINAL_USER%>" />
<%} %>
<% if (ShouldDisplayLogoutButton)
   { %>
|<asp:HyperLink ID="hlLogout" runat="server" NavigateUrl="~/Login/Login.aspx?logout=1"
    Text="<%$ Resources:SR,SM_LOGOUT%>" />
<%} %>
