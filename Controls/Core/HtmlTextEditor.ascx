﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HtmlTextEditor.ascx.cs" Inherits="Eproc2.Web.Controls.Core.HtmlTextEditor" %>

<script type="text/javascript">
    $(this).onPartialDocumentReady(function () {
        tinymce.init({
            selector: ".js-tinymce",
            theme: "modern",
            encoding: "xml",
            width: 800,
            height: 400,
            menubar: false,
            statusbar: false,
            max_chars: 5000,
            max_chars_indicator: "lengthBox",
            max_chars_text: "Characters left:",
            max_chars_maxText: "Maximum characters limit reached",
            plugins: [
                "link", "save", "maxchars"
            ],
            content_css: "css/content.css",
            toolbar: "bold | italic | underline | bullist | numlist | link ",
            setup: function (editor) {
                editor.on('SaveContent', function (e) {
                    e.content = e.content.replace(/&#39/g, '&apos');
                });
            },
            editor_deselector: "aspNetDisabled"
        });
    });
</script>
<%--toolbar: "bold | italic | underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
    plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor"
        ],
--%>
<div>
    <textarea id="textArea" class="js-tinymce" encoding="xml" runat="server"></textarea>
    <label class="js-charLength"></label>
    <span id="lengthBox"></span>
</div>
