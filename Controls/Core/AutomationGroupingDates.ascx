﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutomationGroupingDates.ascx.cs" Inherits="Eproc2.Web.Controls.Core.AutomationGroupingDates" EnableViewState="true" %>
<table class="subForm grey">
    <tr>
        <td colspan="2" class="caption">
            <qwc:Label ID="lblAutomationGroupingDates" runat="server" Text="<%$ Resources:SR,AutomationGroupingDates%>" />:
        </td>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblCurrentGroupingStartDate" runat="server" Text="<%$ Resources:SR,CurrentGroupingStartDate%>"
                AssociatedControlID="lblCurrentGroupingStartDateValue" />:
        </td>
        <td>
            <qwc:Label runat="server" ID="lblCurrentGroupingStartDateValue"></qwc:Label>
        </td>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblCurrentGroupingEndDate" runat="server" Text="<%$ Resources:SR,CurrentGroupingEndDate%>"
                AssociatedControlID="lblCurrentGroupingEndDateValue" />:
        </td>
        <td>
            <qwc:Label ID="lblCurrentGroupingEndDateValue" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblOrderCreatingDate" runat="server" Text="<%$ Resources:SR,OrderCreatingDate%>"
                AssociatedControlID="lblOrderCreatingDateValue" />:
        </td>
        <td>
            <qwc:Label ID="lblOrderCreatingDateValue" runat="server" />
        </td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblNextGroupingStartDate" runat="server" Text="<%$ Resources:SR,NextGroupingStartDate%>"
                AssociatedControlID="lblNextGroupingStartDateValue" />:
        </td>
        <td>
            <qwc:Label ID="lblNextGroupingStartDateValue" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblNextGroupingEndDate" runat="server" Text="<%$ Resources:SR,NextGroupingEndDate%>"
                AssociatedControlID="lblNextGroupingEndDateValue" />:
        </td>
        <td>
            <qwc:Label ID="lblNextGroupingEndDateValue" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="bold indent">
            <qwc:Label ID="lblNextOrderCreatingDate" runat="server" Text="<%$ Resources:SR,NextOrderCreatingDateValue%>"
                AssociatedControlID="lblNextOrderCreatingDateValue" />:
        </td>
        <td>
            <qwc:Label ID="lblNextOrderCreatingDateValue" runat="server" />
        </td>
    </tr>
</table>