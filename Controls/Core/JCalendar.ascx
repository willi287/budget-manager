﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JCalendar.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.JCalendar" %>
<div class="datepicker">
</div>

<script type="text/javascript">
    Sys.Application.add_load(WireEvents);
    function WireEvents() {
        $(document).ready(function() {

            var myDate = new Date();
            var textVal = $("input#<%=BindingControlId %>").val();
            myDate = Date.parse(textVal);
            $(".datepicker").datepicker({ showButtonPanel: true });
            $(".datepicker").datepicker("setDate", myDate);
            $("input#<%=BindingControlId %>").change(function() {
                textVal = $("input#<%=BindingControlId %>").val();
                myDate = Date.parse(textVal);
                $(".datepicker").datepicker("setDate", myDate);
            });
        }); }

</script>

