﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="Eproc2.Web.Controls.Core.Search" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="Eproc2.Web.Controls.Core" Assembly="Eproc2.Web" %>
<table>
    <tr>
        <td colspan="3" style="vertical-align: top;">
            <asp:LinkButton ID="lnkAdvanced" Visible="false" runat="server" Text="<%$ Resources:SR,AdvancedSearch %>" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">
            <qwc:TextBox ID="tbSearch" runat="server" />
            <% if (!string.IsNullOrEmpty(WatermarkText)) { %>
            <ajaxToolkit:TextBoxWatermarkExtender ID="watermarkExtender" runat="server" TargetControlID="tbSearch" WatermarkCssClass="watermarked" />
            <% } %>
        </td>
        <td style="vertical-align: top;" id="tdSearch">
            <asp:Button ID="btnSearch" OnClick="SearchClick" runat="server" Text="<%$ Resources:SR,Search %>" />
        </td>
        <td style="vertical-align: top;">
            <asp:Button ID="btnAdvanced" runat="server" Text="<%$ Resources:SR,Advanced %>" />
        </td>
    </tr>
</table>
