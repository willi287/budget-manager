﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Options.ascx.cs" Inherits="Eproc2.Web.Controls.Core.Options" EnableViewState="true" %>

<table class="options">
    <tr>
        <td>
            <qwc:DropDownList ID="ddlOrganisation" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOrganisation_SelectedIndexChanged" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
        <td>
            <qwc:DropDownList ID="ddlBusinessView" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBusinessView_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"/>
        </td>
        <td>
            <qwc:DropDownList ID="ddlBcg" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBcg_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
        <td>
            <qwc:DropDownList ID="ddlContract" DataValueField="Id" DataTextField="Value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>        
    </tr>
</table>
