﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicTreeView.ascx.cs" Inherits="Eproc2.Web.Controls.Core.DynamicTreeView" %>

<script type="text/javascript">
    function logEvent(event, data, msg) {
        msg = msg ? ": " + msg : "";
        $.ui.fancytree.info("Event('" + event.type + "', node=" + data.node + ")" + msg);
    }
    $(function () {

        // Create the tree inside the <div id="tree"> element.
        $("#tree").fancytree({
            source: $.ajax({
                url: $("#<%=hfUrl.ClientID%>").val(),
                dataType: "json",
                cache: false,
            }),
            checkbox: true,
            icons: false,
            debugLevel: 0,
            selectMode: parseInt($("#<%=hfIsMultiSelect.ClientID%>").val()), // 1:single, 2:multi, 3:multi-hier
            select: function (event, data) {
                //logEvent(event, data, "current state=" + data.node.isSelected());
                var selectedNodes = data.tree.getSelectedNodes();
                var s = '';
                if ($("#<%=hfIsMultiSelect.ClientID%>").val() == '2') {
                    var str = [];
                    for (var i = 0; i < selectedNodes.length; i++) {
                        str.push(selectedNodes[i].key);
                    }
                    s = str.join(",");
                    $("#<%=hfAspSelectedNodes.ClientID%>").val(s);
                }
                else if (selectedNodes.length > 0) {
                    s = selectedNodes[0].key;
                    $("#<%=hfAspSelectedNode.ClientID%>").val(s);
                }
                else if($("#<%=hfIsAttributeValueForm.ClientID%>").val() == 1) {
                    $("#<%=hfAspSelectedNode.ClientID%>").val(s);
                }
                
            },
            init: function (event, data, flag) {
                $('#tree').fancytree("getRootNode").visit(function (node) {
                    var activeNode = node;
                    var value = $("#<%=hfAspSelectedNode.ClientID%>").val();
                    if (value.search(activeNode.key) >= 0) {
                        activeNode.toggleSelected();
                        while (activeNode.parent != null) {
                            activeNode.setExpanded(true);
                            activeNode = activeNode.parent;
                        }
                    }
                });
            }
        });
    });
</script>
<div>
    <table width="70%">
        <tr>
            <div id="tree">
            </div>
        </tr>
        <tr>
            <asp:HiddenField ID="hfAspSelectedNode" Value="" runat="server" />
        </tr>
        <tr>
            <asp:HiddenField ID="hfAspSelectedNodes" Value="" runat="server" />
        </tr>
         <tr>
            <asp:HiddenField ID="hfIsMultiSelect" Value="" runat="server" />
        </tr>
        <tr>
            <asp:HiddenField ID="hfIsAttributeValueForm" Value="" runat="server" />
        </tr>
        <tr>
            <asp:HiddenField ID="hfUrl" Value="" runat="server" />
        </tr>
    </table>
</div>
