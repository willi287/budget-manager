﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calendar.ascx.cs" Inherits="Eproc2.Web.Controls.Core.Calendar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Panel ID="pContainer" CssClass="clearStyle" runat="server">
    <qwc:TextBox ID="tbValue" Width="115" runat="server" />
    <asp:ImageButton ID="ibtnSelect" runat="server" SkinID="calendar" /> <asp:RangeValidator CultureInvariantValues="true" ID="rngValidator" runat="server"
        Type="Date" ControlToValidate="tbValue" MinimumValue="1900/01/01" MaximumValue="2100/01/01"
        Text="*" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" EnableClientScript="true"/> <asp:RequiredFieldValidator ID="requiredValidator" EnableClientScript="true" runat="server" Enabled="True" 
        ControlToValidate="tbValue" Text="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>">
    </asp:RequiredFieldValidator>
    <act:CalendarExtender ID="ceCalendar" OnClientShowing="function(sender, args){var offset=getWindowHeight()-getElementPosition(sender._button.id).top-sender._height-60; if(offset<0){sender._popupBehavior._y=-sender._height-70;}else{sender._popupBehavior._y=0;}}" OnClientShown="function(sender, args){sender._popupBehavior._element.style.zIndex = 10002;}"  runat="server" PopupButtonID="ibtnSelect" TargetControlID="tbValue" />
</asp:Panel>
<asp:Label ID="lblDay" runat="server" AssociatedControlID="tbValue"/>
