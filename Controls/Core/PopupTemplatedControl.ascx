﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopupTemplatedControl.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.PopupTemplatedControl" %>
<qwc:ModalPopupExtender TargetControlID="TargetPopupControl" BackgroundCssClass="modalBackground"
    PopupControlID="PopupPanel" PopupControlPanelID="PopupControlDiv" ID="ModalPopupExtender_popup_templated" PopupDragHandleControlID="PopupPanel" 
    runat="server">
</qwc:ModalPopupExtender>
<asp:Panel ID="PopupControlDiv" runat="server" style="display: none; width: auto;">
    <asp:Panel ID="PopupPanel" runat="server" Style="cursor: move;">
        <div id="win" class="module_overlay" style="height:100%">
            <div class="infoIconBd" runat="server" style="height: 100%">
                <div style="display: block; margin: 0; padding: 0; width: 100%">
                    <asp:Panel ID="pLogo" CssClass="vwLogo" runat="server" />
                    <asp:Panel ID="pTitle" CssClass="infoIconTitle" runat="server">
                        <asp:PlaceHolder runat="server" ID="phTitle" />
                    </asp:Panel>
                    <div class="infoIconDescription" style="height: 100%; padding: 0px">
                        <asp:PlaceHolder runat="server" ID="phContent" />
                    </div>
                    <div class="infoIconOkButton" align="center" style="width: 100%; display: block;">
                        <asp:Button ID="btnOk" Width="75" Height="22" runat="server" Text="<%$ Resources:SR,Ok %>"
                            OnClick="btnOk_Click" CssClass="popupControlOk" />
                        <asp:Button ID="btnCancel" Width="75" Height="22" runat="server" Text="<%$ Resources:SR,Cancel %>"
                            OnClick="btnCancel_Click" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>
<div style="display: none;">
    <asp:Button ID="TargetPopupControl" runat="server" Text="Button" />
</div>