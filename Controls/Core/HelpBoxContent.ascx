﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HelpBoxContent.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.HelpBoxContent" %>
<table>
    <tr>
        <td>
            <asp:Image ID="imgImage" runat="server" Visible="false" />
        </td>
        <td valign="top" align="justify">
            <div class="helpbox round">
                <div class="heading">
                    <div class="tdleft">
                        <asp:Image ID="imgI" runat="server" />
                    </div>
                    <div class="headingtext tdleft">
                        <qwc:Label ID="lblHeading" runat="server" />
                    </div>
                    <div class="tdright">
                        <asp:HyperLink ID="hlClose" runat="server">
                            <asp:Image ID="imgClose" runat="server" />
                        </asp:HyperLink>
                    </div>
                </div>
                <div class="hlpBxContent">
                    <%=GetInformationContentHtml() %>
                    <br />
                </div>
                <div class="turnoff">
                    <asp:LinkButton ID="btTurnOff" runat="server" OnClick="OnTurnOff" Text="<%$ Resources:SR,ClickToTurnOff %>" />
                </div>
            </div>
        </td>
    </tr>
</table>
