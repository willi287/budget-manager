﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Address.ascx.cs" Inherits="Eproc2.Web.Controls.Core.Address" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<table class="subForm">
    <tr>
        <td class="caption required">
            <qwc:InfoLabel ID="lblAddress" runat="server" Text="Address" AssociatedControlID="tbAddress"/>
            <span>*</span>
        </td>
        <td>
            <qwc:TextBox ID="tbAddress" runat="server" MaxLength="150"  />
             <spring:ValidationError id="AddressError" runat="server" />
        </td>
    </tr>
    
    <tr>
        <td  class="caption required">
            <qwc:InfoLabel ID="lblCity" runat="server" Text="<%$ Resources:SR,TownCity %>" AssociatedControlID="tbCity" />
            <span>*</span>
        </td>
        <td>
            <qwc:TextBox ID="tbCity" runat="server"  MaxLength="30"  />
            <spring:ValidationError id="CityError" runat="server" />
            
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblCounty" runat="server" Text="<%$ Resources:SR,County %>" />
        </td>
        <td>
            <qwc:DropDownList id="ddlCounty" runat="server"   ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
              <spring:ValidationError id="CountyError" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption required">
            <qwc:InfoLabel ID="lblCountry" runat="server" Text="<%$ Resources:SR,Country %>" AssociatedControlID="ddlCountry" />
            <span>*</span>
        </td>
        <td>
            <qwc:DropDownList id="ddlCountry" runat="server"   ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
            <spring:ValidationError id="CountryError" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption required">
            <qwc:InfoLabel ID="lblPostcode" runat="server" Text="<%$ Resources:SR,Postcode %>" AssociatedControlID="tbPostcode" />
            <span>*</span>
        </td>
        <td>
            <qwc:TextBox ID="tbPostcode" runat="server" MaxLength="10"  />
            <spring:ValidationError id="PostcodeError" runat="server" />
        </td>
    </tr>
</table>
