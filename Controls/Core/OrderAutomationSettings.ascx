﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderAutomationSettings.ascx.cs" Inherits="Eproc2.Web.Controls.Core.OrderAutomationSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register TagPrefix="e2c" TagName="Calendar" Src="~/Controls/Core/Calendar.ascx" %>

<div id="<%= this.ClientID %>">
    <asp:Panel runat="server" CssClass="subForm grey bold">
        <table>
            <tr>
                <td>
                    <qwc:Label ID="lblGroupAppointments" runat="server" Text="<%$ Resources:SR, GroupAppointments %>" />
                </td>
                <td>
                    <qwc:DropDownList ID="ddlAutomationType" CssClass="short" runat="server" SortOrder="None" DataTextField="Value" DataValueField="Id" />
                </td>
                <td>
                    <qwc:Label ID="lblEvery" runat="server" Text="<%$ Resources:SR, Every %>" /></td>
                <td style="width: 50px">
                    <qwc:DropDownList ID="ddlWeeklyPeriod" CssClass="shortest" runat="server" SortOrder="None" />
                    <qwc:DropDownList ID="ddlMonthlyPeriod" CssClass="shortest" runat="server" SortOrder="None" />
                </td>
                <td>
                    <qwc:Label ID="lblPeriodNAText" runat="server" Text="<%$ Resources:SR, NA %>" />
                    <qwc:Label ID="lblWeekPeriodText" runat="server" Text="<%$ Resources:SR, Weeks %>" />
                    <qwc:Label ID="lblMonthPeriodText" runat="server" Text="<%$ Resources:SR, Months %>" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <qwc:Label ID="lblStartDateOfTheFirstGroupingPeriod" runat="server" Text="<%$ Resources:SR, StartDateOfTheFirstGroupingPeriod %>" />:
                </td>
                <td>
                    <e2c:Calendar ID="calStartDate" runat="server" />
                </td>
                <td>
                    <span id="calValidationIndicator" class="red" data-bind="visible: <%= this.ParentClientId %>.CalendarValidationVisible, attr: {title: <%= this.ParentClientId %>.CalendarValidationMessage}">*</span>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <qwc:Label ID="lblCreateOrders" runat="server" Text="<%$ Resources:SR, CreateOrders %>" />
                </td>
                <td style="width: 50px">
                    <qwc:Label ID="lblPrderSendPeriodNAText" runat="server" Text="<%$ Resources:SR, NA %>" />
                    <qwc:DropDownList ID="ddlWeekOrderSendPeriod" CssClass="shortest" runat="server" SortOrder="None" DataTextField="Value" DataValueField="Key" />
                    <qwc:DropDownList ID="ddlMonthOrderSendPeriod" CssClass="shortest" runat="server" SortOrder="None" DataTextField="Value" DataValueField="Key" />
                </td>
                <td>
                    <qwc:Label ID="lblWeekOrderSendPeriodText" runat="server" Text="<%$ Resources:SR, Weeks %>" />
                </td>
                <td>
                    <qwc:Label ID="lblBeforeFirstAppointment" runat="server" Text="<%$ Resources:SR, BeforeFirstAppointment %>" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <input type="hidden" runat="server" id="hViewStateStorage" />
</div>
