﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Terms.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.Terms" EnableViewState="false" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="cc1" %>
<cc1:ModalPopupExtender TargetControlID="TargetPopupControl" BackgroundCssClass="modalBackground"
    PopupControlID="PopupPanel" PopupControlPanelID="PopupControlDiv" ID="ModalPopupExtender1" PopupDragHandleControlID="PopupPanel"
    runat="server">
</cc1:ModalPopupExtender>
<asp:Panel ID="PopupControlDiv" style="display: none; width : auto;" runat="server">
    <asp:Panel ID="PopupPanel" runat="server" Style="cursor: move;">
        <div id="win" class="module_overlay">
            <div class="infoIconBd">
                <div style="display: block; margin: 0; padding: 0; width: 100%">
                    <div class="vwLogo"></div>
                    <div class="infoIconTitle"></div>
                    <div class="infoIconDescription">
                        <asp:TextBox ID="lblMessage" runat="server" TextMode="MultiLine" ReadOnly="true"></asp:TextBox></div>
                    <div class="infoIconHelpLink">
                        <input type="checkbox" id="chbAgree" onclick="if( document.getElementById('chbAgree').checked)  { document.getElementById('<%=btnOk.ClientID%>').style.visibility='visible' } else  { document.getElementById('<%=btnOk.ClientID%>').style.visibility='hidden' }"><%= Security.GetStringResource("AcceptTerms")%></input></div>
                    <div class="infoIconOkButton" lign="center" style="width: 100%; display: block;">
                        <asp:Button ID="btnOk" Width=75 Height=22 runat="server" Text="<%$ Resources:SR,Submit %>" OnClick="btnOk_Click"/>
                        <asp:Button ID="btnCancel" PostBackUrl="~/Login/Login.aspx?logout=1" Width=75 Height=22 runat="server" Text="<%$ Resources:SR,Decline %>" OnClick="btnCancel_Click"/></div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>
<div style="display: none;">
    <asp:Button ID="TargetPopupControl" runat="server" Text="Button" /></div>
