﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchWithFields.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.SearchWithFields" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="Eproc2.Web.Controls.Core" Assembly="Eproc2.Web" %>
<table>
    <tr>
        <td colspan="4" style="vertical-align: top;">
            <asp:LinkButton ID="lnkAdvanced" Visible="false" runat="server" Text="<%$ Resources:SR,AdvancedSearch %>" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">
            <asp:DropDownList ID="ddlField" DataTextField="LocalizedName" DataValueField="FieldName" AutoPostBack="true"
                runat="server">
            </asp:DropDownList>
        </td>
        <td id="tdPredictiveSearch">
             <qwc:TextBox ID="tbSearch" runat="server" />
             <asp:RegularExpressionValidator ID="revSearch" runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectSearchStringMsg %>"
                        ErrorMessage="*" ControlToValidate="tbSearch" EnableClientScript="true" Display="Dynamic"
                        ValidationExpression="[^\*\&amp;\-\&quot;]*" /> 
             <% if (!string.IsNullOrEmpty(WatermarkText)) { %>
            <ajaxToolkit:TextBoxWatermarkExtender ID="watermarkExtender" runat="server" TargetControlID="tbSearch" WatermarkCssClass="watermarked" />
            <% } %>
        </td>
        <td style="vertical-align: top;" id="tdSearch">
           <asp:Button ID="btnSearch" OnClick="SearchClick" runat="server" Text="<%$ Resources:SR,Search %>" />           
        </td>
        <td style="vertical-align: top;">
            <asp:Button ID="btnAdvanced" Visible="false" runat="server" Text="<%$ Resources:SR,Advanced %>" />
        </td>
    </tr>
</table>
