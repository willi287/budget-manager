﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduleAutomationOptions.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.SchedulerAutomationOptions" %>
<%@ Register Src="~/Controls/Core/AutomationGroupingDates.ascx" TagName="AutomationGroupingDates"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OrderAutomationSettings.ascx" TagName="OrderAutomationSettings"
    TagPrefix="e2o" %>
<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upSchedulerAutomationOptions">
    <ContentTemplate>
        <table class="form">
            <tr class="grey">
                <td>
                    <qwc:GroupRadioButton runat="server" AutoPostBack="true" OnCheckedChanged="OnDefaultOrderAutomationChanged"
                    ID="rbDefaultOrderAutomation" GroupName="scheduleAutomationOptions"
                    Text="<%$ Resources: SR, DefaultOrderAutomation %>" />
                </td>
                <td>
                    <qwc:GroupRadioButton runat="server" ID="rbCustomOrderAutomation"
                    GroupName="scheduleAutomationOptions" AutoPostBack="true" OnCheckedChanged="OnDefaultOrderAutomationChanged"
                    Text="<%$ Resources: SR, CustomOrderAutomation %>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <e2o:orderautomationsettings id="ctrlOrderAutomationSettings" runat="server" />
                    <uc:automationgroupingdates runat="server" id="ctrlAutomationGroupingDates" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>