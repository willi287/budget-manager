﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationArea.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.NotificationArea" %>
<asp:Repeater ID="notifications" runat="server">
    <ItemTemplate>
        <div class="<%# Eval("CssClass") %>">
            <table width="100%" runat="server" id="tNotifications">
                <tr>
                    <td align="right" class="notificationImageColumn">
                        <asp:Image ID="warningImg" runat="server" ImageUrl="<%# Controller.GetWarningImageUrl() %>"/>
                    </td>
                    <td style="padding-left: 20px">
                        <%# Eval("Html") %>
                    </td>
                </tr>
            </table>
        </div>
    </ItemTemplate>
</asp:Repeater>
