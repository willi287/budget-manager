﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopupProcessing.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.PopupProcessing" EnableViewState="false" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="cc1" %>
<cc1:ModalPopupExtender TargetControlID="TargetPopupControl" BackgroundCssClass="modalBackground"
    BehaviorID="popupProcessing" PopupControlID="PopupPanel" PopupControlPanelID="PopupControlDiv"
    ID="ModalPopupExtender1" PopupDragHandleControlID="PopupPanel" runat="server">
</cc1:ModalPopupExtender>
<asp:Panel ID="PopupControlDiv" runat="server" Style="display: none; width: auto;">
    <asp:Panel ID="PopupPanel" runat="server" Style="cursor: move;">
        <div id="processing" class="module overlay" style="visibility: inherit; width: 150px;
            height: 30px">
            Processing...
        </div>
    </asp:Panel>
</asp:Panel>
<div style="display: none;">
    <asp:Button ID="TargetPopupControl" runat="server" Text="Button" />
</div>
