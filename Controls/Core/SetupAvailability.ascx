﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetupAvailability.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.SetupAvailability" %>
<%@ Register TagPrefix="qwc" Namespace="Eproc2.Web.Framework.Controls.Grid" Assembly="Eproc2.Web.Framework" %>
<table class="subForm gray">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="InfoLabelAvailabilitySchedule" runat="server" Text="<%$ Resources:SR, AvailabilityScheduler %>" />:
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:GroupRadioButton runat="server" Checked="True" AutoPostBack="false" CssClass="noAvailabilityScheduler"
                ID="rbNoAvailabilitySchedule" GroupName="scheduleOptionsGroup" Text="<%$ Resources: SR, NoAvailabilitySchedule %>" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:GroupRadioButton runat="server" ID="rbSameTimesForAllDays" GroupName="scheduleOptionsGroup" AutoPostBack="false"
                Text="<%$ Resources: SR, SameTimesForAllDays %>" CssClass="sameTimesForAllDays" />
            <qwc:ControlledGrid ID="cgSameTimesForAllDays" runat="server" KeyField="Id" AllowEditing="True" CssClass="cgSameTimeForAllDays GridEx">
                <Columns>
                    <qwc:TextColumn AllowSorting="false" DataField="Day" HeaderTitle="<%$ Resources:SR, AvailabilityDay %>">
                        <HeaderCellStyle Width="300px" />
                    </qwc:TextColumn>
                    <qwc:CheckBoxColumn IsEditableDataField="IsEditableAllDayDuration" AllowSorting="false" DataField="IsAllDayDuration" HeaderTitle="<%$ Resources:SR, AllDayAvailability %>">
                        <ControlStyle CssClass="isAllDayCkeckbox" />
                        <HeaderCellStyle Width="110px" />
                    </qwc:CheckBoxColumn>
                    <qwc:TimeColumn HeaderTitle="<%$ Resources:SR, From %>" DataTextField="Text" DataValueField="Value"
                        DataField="FromTime" DataSourceDataField="FromTimeDataSource" IsEditableDataField="IsEditableTimes">
                        <HeaderCellStyle Width="120px" />
                        <Validators>
                            <qwc:ColumnCustomValidator ID="cvSameTimesForAllDaysFromTime" ClientValidationFunction="customValidateTime" ToolTip="<%$ Resources:ValidationSR, AvailabilityScheduleInvalidTime %>" />
                        </Validators>
                        <ItemCellStyle CssClass="columnCell columnFrom" />
                    </qwc:TimeColumn>
                    <qwc:TimeColumn HeaderTitle="<%$ Resources:SR, To %>" DataTextField="Text" DataValueField="Value"
                        DataField="ToTime" DataSourceDataField="ToTimeDataSource" IsEditableDataField="IsEditableTimes">
                        <HeaderCellStyle Width="120px" />
                        <Validators>
                            <qwc:ColumnCustomValidator ID="cvSameTimesForAllDaysToTime" ClientValidationFunction="customValidateTime" ToolTip="<%$ Resources:ValidationSR, AvailabilityScheduleInvalidTime %>" />
                        </Validators>
                        <ItemCellStyle CssClass="columnCell columnTo" />
                    </qwc:TimeColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:TextBox runat="server" ID="txtValidatorHelper" CssClass="hidden"/>
            <qwc:GroupRadioButton CssClass="differentTimesForEachDay" runat="server" ID="rbDifferentTimeForEachDay" GroupName="scheduleOptionsGroup"
                AutoPostBack="false" Text="<%$ Resources: SR, DifferentTimeForEachDay %>" />
            <asp:CustomValidator ControlToValidate="txtValidatorHelper" EnableClientScript="True" runat="server" ValidateEmptyText="True" ID="selectionValidator" ClientValidationFunction="validateDaysSelection" Display="Dynamic" Text="*" ToolTip="<%$ Resources:ValidationSR, RequiredSelectDayMsg %>"></asp:CustomValidator>
            <qwc:ControlledGrid ID="cgDifferentTimesForEachDay" runat="server" KeyField="Id" AllowEditing="True" CssClass="cgDifferentTimesForEachDay GridEx">
                <Columns>
                    <qwc:CheckBoxColumn IsEditable="true" DataField="IsSelectedDay" UseHeaderCheckBox="true">
                        <ControlStyle CssClass="selectLineCkeckbox" />
                        <HeaderCellStyle Width="80px" CssClass="checkBoxColumnHeaderCell checkAllCell" />
                    </qwc:CheckBoxColumn>
                    <qwc:TextColumn AllowSorting="false" DataField="Day" HeaderTitle="<%$ Resources:SR, AvailabilityDay %>">
                        <HeaderCellStyle Width="300px" />
                    </qwc:TextColumn>
                    <qwc:CheckBoxColumn IsEditableDataField="IsEditableAllDayDuration" AllowSorting="false" DataField="IsAllDayDuration" HeaderTitle="<%$ Resources:SR, AllDayAvailability %>">
                        <ControlStyle CssClass="isAllDayCkeckbox" />
                        <HeaderCellStyle Width="110px" />
                    </qwc:CheckBoxColumn>
                    <qwc:TimeColumn HeaderTitle="<%$ Resources:SR, From %>" DataTextField="Text" DataValueField="Value" 
                        DataField="FromTime" DataSourceDataField="FromTimeDataSource" IsEditableDataField="IsEditableTimes">
                        <Validators>
                            <qwc:ColumnCustomValidator ID="cvDifferentTimesForEachDayFromTime" ClientValidationFunction="customValidateTime" ToolTip="<%$ Resources:ValidationSR, AvailabilityScheduleInvalidTime %>" />
                        </Validators>
                        <HeaderCellStyle Width="120px" />
                        <ItemCellStyle CssClass="columnCell columnFrom" />
                    </qwc:TimeColumn>
                    <qwc:TimeColumn HeaderTitle="<%$ Resources:SR, To %>" DataTextField="Text" DataValueField="Value"
                        DataField="ToTime" DataSourceDataField="ToTimeDataSource" IsEditableDataField="IsEditableTimes">
                        <Validators>
                            <qwc:ColumnCustomValidator ID="cvDifferentTimesForEachDayToTime" ClientValidationFunction="customValidateTime" ToolTip="<%$ Resources:ValidationSR, AvailabilityScheduleInvalidTime %>" />
                        </Validators>
                        <HeaderCellStyle Width="120px" />
                        <ItemCellStyle CssClass="columnCell columnTo" />
                    </qwc:TimeColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
