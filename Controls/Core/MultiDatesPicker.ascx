﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiDatesPicker.ascx.cs" Inherits="Eproc2.Web.Controls.Core.MultiDatesPicker" %>

<div class="multidatespicker-container">
    <div class="multidatespicker" data-dateFormat="<%=DATE_FORMAT %>" />

    <asp:HiddenField ID="hfSelectedDates" Value="" runat="server" />
    <asp:HiddenField ID="hfMinDate" Value="" runat="server" />
    <asp:HiddenField ID="hfMaxDate" Value="" runat="server" />
    <asp:HiddenField ID="hfMarkedDates" Value="" runat="server" />
</div>