﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetupDuration.ascx.cs" Inherits="Eproc2.Web.Controls.Core.SetupDurationControl" %>
<%@ Register Src="~/Controls/Core/CommonSetupDurationControl.ascx" TagName="CommonSetupDuration" TagPrefix="uc" %>

<table>
     <tbody>
         <tr>
             <td>
                <qwc:InfoLabel ID="InfoLabelForDuration" runat="server" Text="<%$ Resources:SR, TheProvidedServiceWillLstForLabel %>" CssClass="bold"/>:
                <br/>
                <uc:CommonSetupDuration ID="ctrlCommonSetupDurationControl" runat="server" />
             </td>
         </tr>
     </tbody>
</table>
<br/>
<table width="70%">
     <tbody>
         <tr>
             <td>
                 <qwc:InfoLabel ID="lblTotalServiceDurationInfo" runat="server" Text="<%$ Resources:SR, TotalServiceDurationInfoLabel %>"  CssClass="bold"/>:
             </td>
             <td>
                  <asp:Label runat="server" ID="lblTotal"/>
                  <asp:Button runat="server" ID="btnRefresh" Text=" <%$ Resources:SR, Refresh %>"/>
             </td>
         </tr>
     </tbody>
</table>
<br />
<table>
    <tbody>
        <tr>
            <td class="caption">
                <qwc:InfoLabel runat="server" ID="lblMinutesConversion" Text="<%$ Resources:SR, MinutesConversion %>"
                               AssociatedControlID="lblMinutesConversionValue" />:
            </td>
            <td>
                <qwc:Label runat="server" ID="lblMinutesConversionValue" />
                <qwc:Label runat="server" ID="lblMinutesConversionMinutes" Font-Bold="True" Text="<%$ Resources:SR, Minutes %>" />
            </td>
        </tr>
    </tbody>
</table>

