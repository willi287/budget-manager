﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetupMinIncrements.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.SetupMinIncrements" %>
<%@ Register Src="~/Controls/Core/CommonSetupDurationControl.ascx" TagName="CommonSetupDuration"
    TagPrefix="uc" %>
<div>
    <br />
    <qwc:InfoLabel ID="InfoLabelForMinIncrements" runat="server" Text="<%$ Resources:SR, TheServiceMayBeOrderedInIncrementsOfLabel %>" CssClass="bold"/>:
    <br />
    <uc:CommonSetupDuration ID="ctrlCommonSetupDurationControl" runat="server" />
    <br />
</div>
