﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HelpBox.ascx.cs" Inherits="Eproc2.Web.Controls.Core.HelpBox" %>
<%@ Register src="HelpBoxContent.ascx" tagname="HelpBoxContent" tagprefix="uc1" %>
<asp:UpdatePanel ID="upHelpBox" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="hfClosed" runat="server" Value="false" />
        <uc1:HelpBoxContent ID="helpBoxContent" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
