﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetupInvoicePrice.ascx.cs" Inherits="Eproc2.Web.Controls.Core.SetupInvoicePrice" %>
<table class="form">
    <tbody>
        <tr>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblStandardPrice" runat="server" Text="<%$ Resources:SR, StandardPrice %>"
                                    AssociatedControlID="lblStandardPriceValue" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblStandardPriceValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblInvoicePrice" runat="server" Text="<%$ Resources:SR, InvoicePrice %>"
                                    AssociatedControlID="lblStandardPriceValue" />:
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="PriceUpdateTypeGroupChanged" AutoPostBack="True" runat="server" ID="rbDirectPrice" Checked="True" GroupName="priceUpdateTypeGroup" />
                                                <qwc:Label runat="server" ID="lblPoundSign" Text="<%$ Resources:SR, PoundSign %>"/>
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="txtDirectPrice" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvDirectPrice" ControlToValidate="txtDirectPrice" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvDirectPrice" MinimumValue="0.00" MaximumValue="999999.99"
                                                    Type="Currency" EnableClientScript="True" runat="server" ErrorMessage="*" ControlToValidate="txtDirectPrice" ToolTip="<%$ Resources:ValidationSR,CurrencyVEMsg %>"
                                                    Display="Dynamic" />
                                                <asp:Button OnClick="RefreshDirectPrice" runat="server" ID="btnRefresh" Text="<%$ Resources:SR, Refresh %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="PriceUpdateTypeGroupChanged" AutoPostBack="True" runat="server" ID="rbPercentValue" GroupName="priceUpdateTypeGroup" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="txtPercentValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPercentValue" ControlToValidate="txtPercentValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPercentValue" MinimumValue="-1000.00" MaximumValue="1000.00"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="txtPercentValue" ToolTip="<%$ Resources:ValidationSR,PercentVEMsg%>"
                                                    Display="Dynamic" />
                                                <qwc:Label runat="server" ID="txtPercentValueAdditionalText" Text="<%$ Resources:SR, PercentValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="PriceUpdateTypeGroupChanged" AutoPostBack="True" runat="server" ID="rbPoundValue" GroupName="priceUpdateTypeGroup" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="txtPoundValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPoundValue" ControlToValidate="txtPoundValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPoundValue" MinimumValue="-999999.99" MaximumValue="999999.99"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="txtPoundValue" ToolTip="<%$ Resources:ValidationSR,CurrencyPoundsVEMsg %>"
                                                    Display="Dynamic" />
                                                <qwc:Label runat="server" ID="lblPoundValueAdditionalText" Text="<%$ Resources:SR, PoundValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>