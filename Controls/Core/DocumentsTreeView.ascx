﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentsTreeView.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.DocumentsTreeView" %>

<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:TreeView ID="tvMainLeft" runat="server" ExpandDepth="0" ShowLines="True" LeafNodeStyle-VerticalPadding="4" 
            SelectedNodeStyle-CssClass="selected" />
    </ContentTemplate>
</asp:UpdatePanel>

