﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.UserControl" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>

<table cellspacing="1" cellpadding="1" width="100%" border="0" class="main">
    <tr>
        <td colspan="4">         
            <asp:RadioButton ID="rbUseProperty" runat="server" Text="<%$ Resources:SR,UseProperty %>" GroupName="UseProperty" />        
        </td>        
    </tr>
    <tr>
        <td class="required">
            <qwc:InfoLabel ID="lblProperty" runat="server" Text="<%$ Resources:SR,Property %>" AssociatedControlID="tbPropertyName"/>
            <span>*</span>
        </td>
        <td>
            <qwc:TextBox ID="tbPropertyName" runat="server" MaxLength="150" />
        </td>
        <td>            
            <asp:Button ID="btnBrowse" runat="server" Text="..." />            
        </td>
        <td>            
            <asp:Button ID="btnNew" runat="server" Text="<%$ Resources:SR,New %>" />            
        </td>
    </tr>
    
    <tr>
        <td>
            <qwc:InfoLabel ID="lblPropertyAddress" runat="server" Text="<%$ Resources:SR,PropertyAddress %>" AssociatedControlID="tbPropertyAddress" />            
        </td>
        <td colspan="3">
            <qwc:TextBox ID="tbPropertyAddress" runat="server"  MaxLength="30" />
        </td>
    </tr>    
    
    <tr>
        <td>
            <qwc:InfoLabel ID="lblPropertyType" runat="server" Text="<%$ Resources:SR,PropertyType %>" AssociatedControlID="tbPropertyType" />            
        </td>
        <td colspan="3">
            <qwc:TextBox ID="tbPropertyType" runat="server"  MaxLength="30" />
        </td>
    </tr>    
    
    <tr>
        <td colspan="4">        
            <asp:RadioButton ID="rbNoProperty" runat="server" Text="<%$ Resources:SR,NoProperty %>" GroupName="UseProperty" />        
        </td>        
    </tr>
</table>
