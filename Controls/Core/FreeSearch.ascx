﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeSearch.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.FreeSearch" %>
<table>
    <tr>
        <td colspan="4" style="vertical-align: top;">
            <asp:LinkButton ID="lnkAdvanced" Visible="false" runat="server" Text="<%$ Resources:SR,AdvancedSearch %>" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">
            <asp:DropDownList ID="ddlField" DataTextField="LocalizedName" AutoPostBack="true" DataValueField="FieldName"
                runat="server">
            </asp:DropDownList>
        </td>
        <td id="tdPredictiveSearch">
             <qwc:PredictiveSearchTextBox Width="215px" runat="server" ID="tbSearch" PostBackOnSelect="true" OnValueSelected="OnValueSelected" />
        </td>
        <td style="vertical-align: top;" id="tdSearch">
           <asp:Button ID="btnSearch" OnClick="SearchClick" runat="server" Text="<%$ Resources:SR,Search %>" />           
        </td>
        <td style="vertical-align: top;">
            <asp:Button ID="btnAdvanced" Visible="false" runat="server" Text="<%$ Resources:SR,Advanced %>" />
        </td>
    </tr>
</table>
