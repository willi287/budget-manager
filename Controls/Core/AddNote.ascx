﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNote.ascx.cs" Inherits="Eproc2.Web.Controls.Core.AddNote" %>
<table>
    <tr>
        <td nowrap="nowrap" class="caption">
            <qwc:InfoLabel ID="lblSelectNoteTemplate" runat="server" Text="<%$ Resources:SR,SelectNoteTemplate %>" />:
        </td>
        <td>
            <qwc:DropDownList ID="ddlSelectNoteTemplate" DataTextField="Value" DataValueField="Id" runat="server" />
        </td>
        <td width="100%">
            <asp:Button ID="btnInsert" runat="server" Text="<%$ Resources:SR,Insert %>" OnClick="btnInsert_Click" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblNewNote" runat="server" Text="<%$ Resources:SR,NewNote %>" />:
        </td>
        <td colspan="2" width="100%">
            <asp:Button ID="btnSaveTemplate" runat="server" Text="<%$ Resources:SR,SaveNoteTemplate %>"
                OnClick="btnSaveTemplate_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="3" valign="middle">
            <table>
                <tr>
                    <td width="100%">
                        <%--use custom control TextArea (ingerited from asp:TextBox), which allows maxlength in multiline texboxes. So, input is disabled, when maxlength is reached.
                        Don't need validation using regex. This fixes a bug when validation denies saving when we try to save string with a line folding--%>
                        <qwc:TextArea ID="tbNewNote" runat="server" MaxLength="3000" TextMode="MultiLine"
                            Width="100%" Height="100" />
                    </td>
                    <td valign="middle">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
