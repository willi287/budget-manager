﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopupControl.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.PopupControl" EnableViewState="false" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="cc1" %>
<cc1:ModalPopupExtender TargetControlID="TargetPopupControl" BackgroundCssClass="modalBackground"
    PopupControlID="PopupPanel" PopupControlPanelID="PopupControlDiv" OkControlID="btnOk" ClientCloseControlID="btnClose" ID="ModalPopupExtender1" PopupDragHandleControlID="PopupPanel"
    runat="server">
</cc1:ModalPopupExtender>
<asp:Panel ID="PopupControlDiv" runat="server" style="display: none; width : auto;">
    <asp:Panel ID="PopupPanel" runat="server" Style="cursor: move;">
        <div id="win" class="module overlay panel" style="visibility: inherit; width : 50em;">
            <div class="hd">
                <div class="tl">
                </div>
                <div id="btnClose" class="close nonsecure">
                </div>
                <div class="tr">
                </div>
            </div>
            <div class="bd">
                <div style="display: block; margin: 0; padding: 0;">
                    <div class="exclamationIcon">
                    </div>
                    <div align="justify" style="display: block; margin-left: 50px; padding: 0;">
                        <asp:Literal ID="lblMessage" runat="server"></asp:Literal></div>
                    <div align="center" style="width: 100%; display: block; margin: 0; padding: 0;">
                        <asp:Button ID="btnOk" Width=75 Height=22 runat="server" Text="Ok" /></div>
                </div>
            </div>
            <div class="ft">
                <div class="bl">
                </div>
                <div class="br">
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>
<div style="display: none;">
    <asp:Button ID="TargetPopupControl" runat="server" Text="Button" />
</div>
