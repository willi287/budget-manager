﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommonSetupDurationControl.ascx.cs"
    Inherits="Eproc2.Web.Controls.Core.CommonSetupDurationControl" %>
<%@ Register TagPrefix="qws" Namespace="Qulix.Web.Controls" Assembly="Qulix.Web" %>

<asp:CheckBox runat="server" ID="cbAllDay" Text="<%$ Resources:SR, AllDayLabel %>" CssClass="js-IsAllDayCkeckbox" />/
<qws:HourDropDown DataTextField="Text" DataValueField="Value" ID="ddlHours" CssClass="js-ddlHours" runat="server">
</qws:HourDropDown> hour(s)
<qws:HourDropDown DataTextField="Text" DataValueField="Value" ID="ddlMins" CssClass="js-ddlMins" runat="server"></qws:HourDropDown> minutes per day and for
<qws:HourDropDown DataTextField="Text" DataValueField="Value" ID="ddlDays" runat="server"></qws:HourDropDown>
day(s) per week, for <qwc:TextBox runat="server" ID="tbWeeks" Width="30px"></qwc:TextBox> week(s)
<asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
    EnableClientScript="true" ID="rfvWeeks" ControlToValidate="tbWeeks" Display="Dynamic"
    runat="server" ErrorMessage="*" />
<asp:RangeValidator ID="rvWeeks" MinimumValue="1" MaximumValue="52" Type="Integer"
    runat="server" Text="*" ControlToValidate="tbWeeks" ToolTip="<%$ Resources:ValidationSR, WeekRangeVEMsg %>"
    Display="Dynamic" />