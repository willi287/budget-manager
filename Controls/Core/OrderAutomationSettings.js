﻿function CreateOrderAutomationSettingsViewModel(viewModel) {
    CreateAutomationViewModel(viewModel);
    addAutomationGroupingDates(viewModel);
}

function CreateAutomationViewModel(viewModel) {
    viewModel.IsWeeklyPeriodSelected = ko.computed(function () {
        return viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId();
    }, this);
    viewModel.IsMonthlyPeriodSelected = ko.computed(function () {
        return viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId();
    }, this);
    viewModel.IsPeriodNotSelected = ko.computed(function () {
        return !viewModel.IsWeeklyPeriodSelected() && !viewModel.IsMonthlyPeriodSelected();
    }, this);
    /*
    There is a special algorithm to calculate this field. This field depends on the following data:
    1) - Default or Custom methods are used;
    2) - Is it used a specific set RequestDate field;
    */
    viewModel.FirstGroupingStartDate = ko.dependentObservable({
        read: function () {
            var requestedDateStr = viewModel.RequestedDate();
            var result = null;
            var requestedDate;

            try {
                // The logic to calculate the date for the Custom Automation type
                if (viewModel.IsCustomAutomation()) {
                    if (requestedDateStr === null) {
                        alert('Request Date is not set in the binding model');
                    }

                    if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                        result = requestedDateStr;
                    } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                        requestedDate = getDate(requestedDateStr);
                        result = dateToString(new Date(requestedDate.getFullYear(), requestedDate.getMonth(), 1));
                    }
                    // The logic to calculate the date for the Default Automation type
                } else {
                    if (requestedDateStr !== null) {
                        requestedDate = getDate(requestedDateStr);
                        result = GetGroupingStartDate(viewModel, requestedDate);
                    } else {
                        result = viewModel.FirstStartDate();
                    }
                }
            } finally {
                if (result !== null) {
                    var dateControl = $('#' + viewModel.FirsGroupingStartDateControlId());
                    var parentControl = dateControl.parent();
                    var actualDateHiddenField = $('#' + viewModel.FirsGroupingStartDateHiddenControlId(), parentControl).get(0);

                    if (!actualDateHiddenField) {
                        actualDateHiddenField = $("<input>").attr({
                            id: viewModel.FirsGroupingStartDateHiddenControlId(),
                            name: viewModel.FirsGroupingStartDateHiddenControlId(),
                            type: "hidden",
                            value: result
                        });
                        actualDateHiddenField.appendTo(parentControl);
                    }
                    actualDateHiddenField.val(result);
                }
            }
            return result;
        },
        write: function (value) {
            viewModel.FirstStartDate(value);
        }
    }, this);
    viewModel.FirstGroupingStartDateToDisplay = ko.dependentObservable({
        read: function () {
            var result = viewModel.FirstGroupingStartDate();

            return result == null ? NA : result;
        },
        write: function (value) {
            if (value == NA) {
                viewModel.FirstGroupingStartDate(null);
            } else {
                viewModel.FirstGroupingStartDate(value);
            }
        }
    }, this);
    viewModel.CalendarValidationMessage = ko.computed(function () {
        if (viewModel.FirstGroupingStartDate) {
            var date = Date.parseExact(viewModel.FirstGroupingStartDate(), "d/M/yyyy");
            if (date) {
                if (Date.today().addDays(365) <= date || Date.today().addDays(-365) >= date) {
                    return viewModel.RangeValidationMsg();
                }
                if (viewModel.IsMonthlyPeriodSelected()) {
                    if (date.getDate() !== 1) {
                        return viewModel.DateValidationMsg();
                    }
                }
            }
        }
        return "";
    }, this);
    viewModel.CalendarValidationVisible = ko.computed(function () {
        return viewModel.CalendarValidationMessage() !== "";
    }, this);
}

function addAutomationGroupingDates(viewModel) {
    viewModel.CurrentGroupingPeriodStartDate = ko.computed(function () {
        var requestedDate = getCurrentDate();
        return GetGroupingStartDate(viewModel, requestedDate);
    }, this);

    viewModel.CurrentGroupingPeriodEndDate = ko.computed(function () {
        var currentGroupingPeriodStartDate = viewModel.CurrentGroupingPeriodStartDate();

        if (currentGroupingPeriodStartDate && currentGroupingPeriodStartDate !== NA) {
            var currentGroupingPeriodEndDate = getDate(currentGroupingPeriodStartDate);
            if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                currentGroupingPeriodEndDate.setDate(currentGroupingPeriodEndDate.getDate() + viewModel.SelectedWeeklyPeriod() * 7 - 1);
            } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                var orderGroupPeriod = viewModel.SelectedMonthlyPeriod();
                currentGroupingPeriodEndDate.setMonth(currentGroupingPeriodEndDate.getMonth() + parseInt(orderGroupPeriod));
                currentGroupingPeriodEndDate.setDate(currentGroupingPeriodEndDate.getDate() - 1);
            }
            return dateToString(currentGroupingPeriodEndDate);
        }
        return NA;
    }, this);

    viewModel.CurrentGroupingPeriodOrderCreatingDate = ko.computed(function () {
        var currentGroupingPeriodStartDate = viewModel.CurrentGroupingPeriodStartDate();

        if (currentGroupingPeriodStartDate && currentGroupingPeriodStartDate !== NA) {
            var currentGroupingPeriodOrderCreatingDate = getDate(currentGroupingPeriodStartDate);
            var orderSendPeriod;
            if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                orderSendPeriod = viewModel.SelectedWeeklySendPeriod();
                currentGroupingPeriodOrderCreatingDate.setDate(currentGroupingPeriodOrderCreatingDate.getDate() - orderSendPeriod * 7);
            } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                orderSendPeriod = viewModel.SelectedMonthlySendPeriod();
                currentGroupingPeriodOrderCreatingDate.setDate(currentGroupingPeriodOrderCreatingDate.getDate() - orderSendPeriod * 7);
            }
            return dateToString(currentGroupingPeriodOrderCreatingDate);
        }
        return NA;
    }, this);

    viewModel.NextGroupingPeriodStartDate = ko.computed(function () {
        var currentGroupingPeriodStartDate = viewModel.CurrentGroupingPeriodStartDate();

        if (currentGroupingPeriodStartDate && currentGroupingPeriodStartDate !== NA) {
            var nextGroupingPeriodStartDate = getDate(currentGroupingPeriodStartDate);
            var orderGroupPeriod;
            if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                orderGroupPeriod = viewModel.SelectedWeeklyPeriod();
                nextGroupingPeriodStartDate.setDate(nextGroupingPeriodStartDate.getDate() + orderGroupPeriod * 7);
            } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                orderGroupPeriod = viewModel.SelectedMonthlyPeriod();
                nextGroupingPeriodStartDate.setMonth(nextGroupingPeriodStartDate.getMonth() + parseInt(orderGroupPeriod));
            }
            return dateToString(nextGroupingPeriodStartDate);
        }
        return NA;
    }, this);

    viewModel.NextGroupingPeriodEndDate = ko.computed(function () {
        var nextGroupingPeriodStartDate = viewModel.NextGroupingPeriodStartDate();

        if (nextGroupingPeriodStartDate && nextGroupingPeriodStartDate !== NA) {
            var nextGroupingPeriodEndDate = getDate(nextGroupingPeriodStartDate);
            var orderGroupPeriod;
            if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                orderGroupPeriod = viewModel.SelectedWeeklyPeriod();
                nextGroupingPeriodEndDate.setDate(nextGroupingPeriodEndDate.getDate() + orderGroupPeriod * 7 - 1);
            } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                orderGroupPeriod = viewModel.SelectedMonthlyPeriod();
                nextGroupingPeriodEndDate.setMonth(nextGroupingPeriodEndDate.getMonth() + parseInt(orderGroupPeriod));
                nextGroupingPeriodEndDate.setDate(nextGroupingPeriodEndDate.getDate() - 1);
            }
            return dateToString(nextGroupingPeriodEndDate);
        }
        return NA;
    }, this);

    viewModel.NextGroupingPeriodOrderCreatingDate = ko.computed(function () {
        var nextGroupingPeriodStartDate = viewModel.NextGroupingPeriodStartDate();

        if (nextGroupingPeriodStartDate && nextGroupingPeriodStartDate !== NA) {
            var nextGroupingPeriodOrderCreatingDate = getDate(nextGroupingPeriodStartDate);
            var orderSendPeriod;
            if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
                orderSendPeriod = viewModel.SelectedWeeklySendPeriod();
                nextGroupingPeriodOrderCreatingDate.setDate(nextGroupingPeriodOrderCreatingDate.getDate() - orderSendPeriod * 7);
            } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
                orderSendPeriod = viewModel.SelectedMonthlySendPeriod();
                nextGroupingPeriodOrderCreatingDate.setDate(nextGroupingPeriodOrderCreatingDate.getDate() - orderSendPeriod * 7);
            }
            return dateToString(nextGroupingPeriodOrderCreatingDate);
        }
        return NA;
    }, this);
}

// Function to calculate grouping start date. The function is used in the following cases: 1) - Calcualte first grouping data and 2) - Calcualte current grouping start date
function GetGroupingStartDate(viewModel, requestedDate) {
    var groupingStartDate = getDate(viewModel.FirstStartDate());

    if (groupingStartDate) {
        var currentGroupingPeriodStartDate = groupingStartDate;
        if (viewModel.SelectedAutomationType() == viewModel.WeeklyPeriodId()) {
            var msInOneDay = 1000 * 60 * 60 * 24;
            var delta = (requestedDate - groupingStartDate) / msInOneDay;
            var differencesOfDays = Math.floor(delta / (viewModel.SelectedWeeklyPeriod() * 7)) * viewModel.SelectedWeeklyPeriod() * 7;
            currentGroupingPeriodStartDate.setDate(currentGroupingPeriodStartDate.getDate() + differencesOfDays);
        } else if (viewModel.SelectedAutomationType() == viewModel.MonthlyPeriodId()) {
            var completedMonthsCount = monthDifferenceBetweenDates(groupingStartDate, requestedDate);
            var orderGroupPeriod = viewModel.SelectedMonthlyPeriod();
            currentGroupingPeriodStartDate.setMonth(currentGroupingPeriodStartDate.getMonth() + Math.floor(completedMonthsCount / orderGroupPeriod) * orderGroupPeriod);
        }
        return dateToString(currentGroupingPeriodStartDate);
    }
    return NA;
}

function getDate(dateStr) {
    if (dateStr) {
        var dateParts = dateStr.split("/");
        return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
    }
    return null;
}

// The function calculates difference between 2 dates
function monthDifferenceBetweenDates(startDate, endDate) {
    var sy = startDate.getFullYear();
    var sm = startDate.getMonth();
    var ey = endDate.getFullYear();
    var em = endDate.getMonth();

    return (ey - sy) * 12 + em - sm;
}

function getCurrentDate() {
    var currentDate = new Date();
    currentDate.setFullYear(currentDate.getYear(), currentDate.getMonth(), currentDate.getDate());
    return currentDate;
}

function dateToString(date) {
    if (date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var dayFullFormat = (day < 10) ? '0' + day : day;
        var monthFullFormat = (month < 10) ? '0' + month : month;
        return dayFullFormat + "/" + monthFullFormat + "/" + date.getFullYear();
    }
    return 'N/A';
}