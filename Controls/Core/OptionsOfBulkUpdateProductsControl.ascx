﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OptionsOfBulkUpdateProductsControl.ascx.cs" Inherits="Eproc2.Web.Controls.Core.OptionsOfBulkUpdateProductsControl" %>
<table>
    <tbody>
        <tr class="clearStyle">
            <td class="caption" colspan="2">
                <qwc:InfoLabel ID="lblApplyToCatalogueProducts" runat="server" Text="<%$ Resources:SR, ApplyToCatalogueProducts %>" />:</td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="clearStyle">
                    <tbody>
                        <tr>
                            <td>
                                <qwc:GroupRadioButton Checked="True" Text="<%$ Resources:SR, ApplyToSelectedProducts %>" runat="server" AutoPostBack="False" runat="server" ID="rbApplyToSelectedProducts" GroupName="ApplyToCatalogueProducts" />
                            </td>
                        </tr>
<%--                        <tr>
                            <td>
                                <qwc:GroupRadioButton Text="<%$ Resources:SR, ApplyToAllFilteringProducts %>" runat="server" AutoPostBack="False" runat="server" ID="rbApplyToAllFilteringProducts" GroupName="ApplyToCatalogueProducts" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <qwc:GroupRadioButton Text="<%$ Resources:SR, ApplyToAllProviderCatalogueProducts %>" runat="server" AutoPostBack="False" runat="server" ID="rbApplyToAllProviderCatalogueProducts" GroupName="ApplyToCatalogueProducts" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
