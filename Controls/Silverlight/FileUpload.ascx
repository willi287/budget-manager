﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileUpload.ascx.cs" Inherits="Eproc2.Web.Controls.Silverlight.FileUpload" %>

<%@ Register Assembly="System.Web.Silverlight" Namespace="System.Web.UI.SilverlightControls"
    TagPrefix="qsc" %>

<table style="width:100%">
    <tr>
        <%if (SelectFileText != string.Empty)
          { %>
        <td></td>
        <%} %>
        <td>
            <%--<asp:TextBox ID="tbUploadedFile"  BorderWidth="0" runat="server" />--%>
            <%--<asp:Panel ID="tbUploadedFile" runat="server"></asp:Panel>--%>
            <div ID="tbUploadedFile" runat="server"></div>
        </td>
    </tr>
    <tr>
        <%if (SelectFileText != string.Empty)
          { %>
        <td class="caption"><qwc:InfoLabel ID="lbFileToUpload" runat="server" />:</td>
        <%} %>
        <td>
        <%--NOTE: KKI: To bring FileUpload control work at FireFox set Windowless="false", but design will be broken. So use it only in test goals! --%>
            <qsc:Silverlight ID="ctrFileUpload" runat="server" 
                Source="~/ClientBin/Eproc2.Silverlight.Applications.FileUpload.xap" 
                MinimumVersion="2.0.31005.0" Height="24px" Width="500px"  Windowless="true" /> 
            <%--<div ID="hfHasFile" runat="server" visible="false"></div>--%>
            <input type="hidden" ID="hfHasFile" runat="server" ></input>
            <input type="hidden" ID="hfFileId" runat="server" ></input>
            <%--<asp:HiddenField ID="hfHasFile" runat="server" />    --%>
        </td>
    </tr>
</table>
