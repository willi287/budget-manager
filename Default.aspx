﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Home" Title="Untitled Page" %>
<%@ Register src="Content/Core/Administration/News/News.ascx" tagname="MainPageNews" tagprefix="uc1" %>
<%@ Register src="Controls/Core/NotificationArea.ascx" tagname="NotificationArea" tagprefix="uc1" %>

<asp:Content ID="Content0" ContentPlaceHolderID="NotificationAreaPlaceHolder" runat="server">
    <uc1:NotificationArea ID="NotificationArea1" runat="server" />    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:MainPageNews ID="MainPageNewsList" runat="server" />
</asp:Content>
