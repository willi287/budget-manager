﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true"
    CodeBehind="ObjectNotFoundedPage.aspx.cs" Inherits="Eproc2.Web.Errors.ObjectNotFoundedPage" %>

<asp:Content ID="cntError" ContentPlaceHolderID="cphMainContent" runat="server">
    <table class="errorContent">        
        <tr>
            <td>
                <%= Resources.SR.ObjectNotFound %>
            </td>
        </tr>
        <tr>
            <td>
                <%= Resources.SR.Click %>
                <asp:LinkButton ID="btnContinue" Text='<%$ Resources:SR, Continue %>' runat="server"
                    OnClick="btnContinue_Click" />
                to return to the previous screen and try again...<br />
            </td>
        </tr>
    </table>
</asp:Content>
