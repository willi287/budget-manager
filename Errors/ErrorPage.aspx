﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true"
    CodeBehind="ErrorPage.aspx.cs" Inherits="Eproc2.Web.Errors.ErrorPage" %>

<asp:Content ID="cntError" ContentPlaceHolderID="cphMainContent" runat="server">

<table class="errorContent">
    <tr>
        <td>
            <asp:Label ID="lblErrorHeader" runat="server" Text="<%$ Resources:SR,ErrorHeader%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="lblErrorText" runat="server" Text="<%$ Resources:SR,ErrorText%>"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="lblErrorFooter" runat="server" Text="<%$ Resources:SR,ErrorFooter%>"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="tbUserMessage" runat="server" TextMode="MultiLine" />
        </td>
    </tr>
    <tr>
        <td>
            <%= Resources.SR.Click %>
            <asp:LinkButton ID="btnContinue" Text='<%$ Resources:SR, Continue %>' runat="server"
                OnClick="btnContinue_Click" />
            to return to the previous screen and try again...<br />
            <div id="divMoreDetails" runat="server">
            To see more details about an error click <a onclick="javascript:$('#errorDetails').toggleClass('hidden');return false;"
                href="#">
                <%=Resources.SR.MoreDetails %></a>
            </div>
        </td>
    </tr>
    <tr>
        <td id="errorDetails" class="hidden" >
            <asp:TextBox ID="tbErrorMessage" runat="server" ReadOnly="true" TextMode="MultiLine" />
        </td>
    </tr>
</table>

</asp:Content>
