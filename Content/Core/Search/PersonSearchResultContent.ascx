<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Search.PersonSearchResultListContent"  
CodeBehind="PersonSearchResultListContent.ascx.cs" %>
<%@ Register src="~/Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="57b2cee7-c44d-48b3-b008-d6ed375ba6f7" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id"
       OnViewing="OnViewing" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Position %>" DataField="Position" SortExpression="None" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,MobilePhone %>" DataField="MobilePhone" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Email %>" DataField="Email" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Organisation %>" DataField="Organisation" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Phone %>" DataField="Phone" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Fax %>" DataField="Fax" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
