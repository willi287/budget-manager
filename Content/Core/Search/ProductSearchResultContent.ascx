<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Search.SearchResultsListContent" %>
<%@ Register src="../../../Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f7b1fb6c-768a-499b-bc73-57a0a7407ce7" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductDescription %>" DataField="Description" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NetInvoicePrice %>" DataField="NetInvoicePrice" SortExpression="tPriceListItem.NetInvoicePrice"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Supplier %>" DataField="SupplierName" SortExpression="tOrganisationSupplier.Name"/>
    </Columns>
</qwc:ControlledGrid>
