﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Search.SearchResultPageBase" %>
<%@ Register src="PersonSearchResultContent.ascx" tagname="PersonSearchResultContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:PersonSearchResultContent ID="PersonSearchResultContent1" runat="server" />
</asp:Content>
