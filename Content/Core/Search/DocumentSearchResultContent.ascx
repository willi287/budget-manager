<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Search.SearchResultsListContent" %>
<%@ Register src="~/Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="31e25163-e278-436b-8654-1e22ddc5dd30" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="DocumentNo" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"    />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CreationDate %>" DataField="CreateTime" SortExpression="CreateTime" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,VWID %>" DataField="VWID" SortExpression="None" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Supplier %>" DataField="SupplierName" SortExpression="supplier.Name" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" DataField="Type" AllowSorting="false"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,DocumentNo %>" DataField="DocumentNo" SortExpression="DocumentNo" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CustomCode %>" DataField="CustomCode" SortExpression="CustomCode" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Buyer %>" DataField="BuyerName" SortExpression="buyer.Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ContractName %>" DataField="ContractName" SortExpression="ContractName" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="State" AllowSorting="false"/>
    </Columns>
</qwc:ControlledGrid>
