<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Search.SearchResultsListContent" %>
<%@ Register src="../../../Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="d99a31be-0dab-4a76-9e08-ba1f0157809a" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,VATNumber %>" DataField="VATNumber" SortExpression="VATNumber" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FlatNo %>" DataField="Flat" SortExpression="tAddress.Flat"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,HouseName %>" DataField="HouseName" SortExpression="tAddress.HouseName"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,HouseNumber %>" DataField="HouseNumber" SortExpression="tAddress.HouseNumber"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Street %>" DataField="Street" SortExpression="tAddress.Street"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Town %>" DataField="Town" SortExpression="tAddress.Town"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,County %>" DataField="County" SortExpression="tAddress.County"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PostCode %>" DataField="Postcode" SortExpression="tAddress.Postcode"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,VWId %>" DataField="VWId" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Fax %>" DataField="Fax" SortExpression="Fax"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Phone %>" DataField="Phone" SortExpression="Phone"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Email %>" DataField="Email" SortExpression="Email"/>
    </Columns>
</qwc:ControlledGrid>
