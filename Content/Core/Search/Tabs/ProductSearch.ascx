<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Search.Tabs.ProductSearch" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="433e2eae-aca1-47d4-850c-ccef26f1cae3" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblCode" runat="server" Text="<%$ Resources:SR,ProductCode%>" AssociatedControlID="tbCode" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDescription" runat="server" Text="<%$ Resources:SR,ProductDescription%>"
                            AssociatedControlID="tbDescription" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblNetInvoicePrice" runat="server" Text="<%$ Resources:SR,NetInvoicePrice%>"
                            AssociatedControlID="tbNetInvoicePrice" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbNetInvoicePrice" runat="server" />
                        <asp:CompareValidator ID="valNetInvoicePrice" ErrorMessage="*" ControlToValidate="tbNetInvoicePrice"
                            Display="Dynamic" runat="server" ValueToCompare="0" Operator="GreaterThanEqual"
                            Type="Double" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblName" runat="server" Text="<%$ Resources:SR,ProductName%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblSupplierName" runat="server" Text="<%$ Resources:SR,Supplier%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSupplierName" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td />
        <td class="caption">
            <qwc:RadioButton ID="rbAnd" GroupName="Junction" Text="<%$ Resources:SR,And %>" runat="server" Checked="True" />
            &nbsp;&nbsp;
            <qwc:RadioButton ID="rbOr" GroupName="Junction" Text="<%$ Resources:SR,Or %>" runat="server" />                
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <asp:Button ID="btnSearch" Text="<%$ Resources:SR,Search %>" runat="server" OnClick="btnSearch_Click" /><asp:Button
                ID="btnReset" Text="<%$ Resources:SR,Reset %>" CausesValidation=false runat="server" OnClick="btnReset_Click" />
        </td>
    </tr>
</table>
