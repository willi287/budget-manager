<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserSearch.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Search.Tabs.UserSearch" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="112c3bc5-bb00-4ec2-b86d-e9ef641a9a8c" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblName" runat="server" Text="<%$ Resources:SR,Name%>" AssociatedControlID="tbName" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPosition" runat="server" Text="<%$ Resources:SR,Position%>" AssociatedControlID="tbPosition" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPosition" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblMobilePhone" runat="server" Text="<%$ Resources:SR,MobilePhone%>"
                            AssociatedControlID="tbMobilePhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbMobilePhone" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblEmail" runat="server" Text="<%$ Resources:SR,Email%>" AssociatedControlID="tbEmail" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblOrganisation" runat="server" Text="<%$ Resources:SR,Organisation%>"
                            AssociatedControlID="tbOrganisation" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbOrganisation" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPhone" runat="server" Text="<%$ Resources:SR,Phone%>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblFax" runat="server" Text="<%$ Resources:SR,Fax%>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td />
        <td class="caption">
            <qwc:RadioButton ID="rbAnd" GroupName="Junction" Text="<%$ Resources:SR,And %>" runat="server" Checked="True" />
            &nbsp;&nbsp;
            <qwc:RadioButton ID="rbOr" GroupName="Junction" Text="<%$ Resources:SR,Or %>" runat="server" />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <asp:Button ID="btnSearch" Text="<%$ Resources:SR,Search %>" runat="server" OnClick="btnSearch_Click" /><asp:Button
                ID="btnReset" CausesValidation=false Text="<%$ Resources:SR,Reset %>" runat="server" OnClick="btnReset_Click" />
        </td>
    </tr>
</table>
