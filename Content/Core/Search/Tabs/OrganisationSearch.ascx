<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganisationSearch.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Search.Tabs.OrganisationSearch" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="46e00d54-816d-4a99-a875-5ccee21673cb" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblName" runat="server" Text="<%$ Resources:SR,Name%>" AssociatedControlID="tbName" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblVATNumber" runat="server" Text="<%$ Resources:SR,VATNumber%>" AssociatedControlID="tbVATNumber" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVATNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblFlat" runat="server" Text="<%$ Resources:SR,Flat%>" AssociatedControlID="tbFlat" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFlat" runat="server" />
                        <asp:CompareValidator ID="valFlat" ErrorMessage="*" ControlToValidate="tbFlat" Display="Dynamic"
                            runat="server" ValueToCompare="0" Operator="GreaterThanEqual" Type="Integer" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblHouseName" runat="server" Text="<%$ Resources:SR,HouseName%>" AssociatedControlID="tbHouseName" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbHouseName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblHouseNumber" runat="server" Text="<%$ Resources:SR,HouseNumber%>"
                            AssociatedControlID="tbHouseNumber" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbHouseNumber" runat="server" />
                        <asp:CompareValidator ID="valHouseNumber" ErrorMessage="*" ControlToValidate="tbHouseNumber"
                            Display="Dynamic" runat="server" ValueToCompare="0" Operator="GreaterThanEqual"
                            Type="Integer" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblStreet" runat="server" Text="<%$ Resources:SR,Street%>" AssociatedControlID="tbStreet" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbStreet" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblTown" runat="server" Text="<%$ Resources:SR,Town%>" AssociatedControlID="tbTown" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTown" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblCounty" runat="server" Text="<%$ Resources:SR,Country%>" AssociatedControlID="tbCounty" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCounty" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPostCode" runat="server" Text="<%$ Resources:SR,PostCode%>" AssociatedControlID="tbPostCode" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPostCode" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId%>" AssociatedControlID="tbVWId" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" runat="server" />
                        <asp:CompareValidator ID="valVWId" ErrorMessage="*" ControlToValidate="tbVWId" Display="Dynamic"
                            runat="server" ValueToCompare="0" Operator="GreaterThanEqual" Type="Integer" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblFax" runat="server" Text="<%$ Resources:SR,Fax%>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPhone" runat="server" Text="<%$ Resources:SR,Phone%>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblEmail" runat="server" Text="<%$ Resources:SR,Email%>" AssociatedControlID="tbEmail" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="caption">
                        <qwc:RadioButton ID="rbAnd" GroupName="Junction" Text="<%$ Resources:SR,And %>" runat="server" Checked="True" />
                        &nbsp;&nbsp;
                        <qwc:RadioButton ID="rbOr" GroupName="Junction" Text="<%$ Resources:SR,Or %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btSearch" Text="<%$ Resources:SR,Search %>" runat="server" />
                        <asp:Button ID="btReset" CausesValidation=false Text="<%$ Resources:SR,Reset %>" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>   
</table>
