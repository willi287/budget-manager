﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.CataloguePageBase" %>
<%@ Register Src="ProductItemViewContent.ascx" TagName="ProductItemViewContent" TagPrefix="uc1" %>

<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:ProductItemViewContent ID="ContentControl" runat="server" />
</asp:Content>
