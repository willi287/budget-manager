<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultCatalogueContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Catalog.DefaultCatalogueContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3d216d1f-e9a7-4e7f-b5e8-a4fb65a86520" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<div class="bordered" style="padding: 20px; float:left;">
    <table>
        <% if (IsSag)
           {%>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litOrganisation" runat="server" Text="<%$ Resources:SR,Organisation %>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlOrganisations" DataValueField="Id" DataTextField="Value"
                    runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlOrganisations_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
        <%} %>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litBCG" runat="server" Text="<%$ Resources:SR,BuyerCatalogueGroup %>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlBcgs" DataValueField="Id" DataTextField="Value" runat="server"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlBcgs_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litContracts" runat="server" Text="<%$ Resources:SR,Contract%>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlContracts" DataValueField="Id" DataTextField="Value" runat="server"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlContracts_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
    </table>
</div>
