<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.CatalogueTreeView" %>
<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:TreeView ID="tvContent" runat="server" ExpandDepth="0" ShowLines="True" SelectedNodeStyle-CssClass="selected">
        </asp:TreeView>
    </ContentTemplate>
</asp:UpdatePanel>
