<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.ProductsListContent" %>
<%@ Register src="~/Controls/Core/FreeSearch.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="173b9090-4832-4f2d-9769-1bb46dc4947d" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="ListItemId" AllowDeleting="false" AllowEditing="false" AllowPaging="true" SortExpression="Name">
    <Columns>
        <qwc:SelectColumn IsEditable=true DataField="Selected"/>
        <qwc:GroupRadioButtonColumn IsEditable=true DataField="Selected"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
        <qwc:ImageColumn AllowPreview=true HeaderTitle="<%$ Resources:SR,Image %>" DataField="ImageUrl" SortExpression="None" AllowSorting="false" Width="75" AlternativeImageUrl="LogoUrl" BlankImageUrl="BlankUrl" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>" DataField="SupplierName" SortExpression="tSupplierOrg.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Manufacturer %>" DataField="Mfr" SortExpression="Manufacturer" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetInvoicePrice %>" DataField="NetInvoicePrice" SortExpression="tPriceListItem.NetInvoicePrice"/>
    </Columns>
</qwc:ControlledGrid>
