<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractProductsContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Catalog.ContractProductsContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5360b4aa-75ef-4a5b-a404-f879deced6cb" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ControlledGrid ID="cgProductsList" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false">
    <Columns>
        <qwc:SelectColumn IsEditable=true DataField="Selected"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Catalog/ProductItemViewWithoutTree.aspx" />
        <qwc:ImageColumn HeaderTitle="<%$ Resources:SR,Image %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>" DataField="SupplierName" SortExpression="tSupplierOrg.Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetPrice %>" DataField="Price" SortExpression="Price" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetInvoicePrice %>" DataField="NetInvoicePrice" SortExpression="tPriceListItem.NetInvoicePrice"/>
    </Columns>
</qwc:ControlledGrid>
