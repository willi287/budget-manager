﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulkAddToPriceListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Catalog.BulkAddToPriceListContent" %>
<%@ Register TagPrefix="e2c" TagName="Calendar" Src="~/Controls/Core/Calendar.ascx" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OptionsOfBulkUpdateProductsControl.ascx" TagName="OptionsOfBulkUpdateProducts" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="08408ab1-8b25-48e7-b7b2-a364012132cd" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tbody>
        <tr>
            <td>
                <table class="subForm" style="width: 500px;">
                    <tbody>
                        <tr>
                            <td>
                                <uc:OptionsOfBulkUpdateProducts ID="ctrlOptionsOfBulkUpdateProducts" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel runat="server" ID="lblChoosePriceList" Text="<%$ Resources:SR, BulkAddToPriceList_ChoosePriceList %>" />:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:InfoLabel runat="server" ID="lblCatalogueGroup" Text="<%$ Resources:SR, BulkAddToPriceList_CatalogueGroup %>" AssociatedControlID="ddlCatalogueGroup" />:
                                            </td>
                                            <td>
                                                <qwc:DropDownList ID="ddlCatalogueGroup" runat="server" DataValueField="ID" DataTextField="Value" SortOrder="Asc" AutoPostBack="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:InfoLabel runat="server" runat="server" ID="lblPriceList" Text="<%$ Resources:SR, BulkAddToPriceList_PriceList %>" AssociatedControlID="ddlPriceList" />:
                                            </td>
                                            <td>
                                                <qwc:DropDownList ID="ddlPriceList" runat="server" DataValueField="ID" DataTextField="Value" SortOrder="None" AutoPostBack="True" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel runat="server" runat="server" ID="lblChooseUpdateTime" Text="<%$ Resources:SR, BulkAddToPriceList_ChooseUpdateTime %>" />:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton GroupName="grUpdateTime" AutoPostBack="True" runat="server" ID="rbImmediateUpdate" Checked="True" Text="<%$ Resources:SR, BulkAddToPriceList_ImmediateUpdate %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton GroupName="grUpdateTime" AutoPostBack="True" runat="server" ID="rbDelayedUpdate" Text="<%$ Resources:SR, BulkAddToPriceList_DelayedUpdate %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td class="captionNowrap">
                                                                <qwc:InfoLabel ID="lblPublishDate" runat="server" Text="<%$ Resources:SR, PublishDate %>" AssociatedControlID="calPublishDate" />:
                                                            </td>
                                                            <td class="formData">
                                                                <e2c:Calendar ID="calPublishDate" runat="server" AutoPostBack="True" />    
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblChooseAppliedPrice" runat="server" Text="<%$ Resources:SR, BulkAddToPriceList_ChooseAppliedPrice %>" />:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:InfoLabel ID="lblInvoicePrice" runat="server" Text="<%$ Resources:SR, InvoicePrice %>" />:
                                            </td>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="AppliedPriceTypeChanged" AutoPostBack="True" runat="server" ID="rbStandardPrice" GroupName="grAppliedPriceType" Checked="True" />
                                            </td>
                                            <td>
                                                <qwc:Label runat="server" ID="lblStandardPrice" Text="<%$ Resources:SR, BulkAddToPriceList_StandardPrice %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="AppliedPriceTypeChanged" AutoPostBack="True" runat="server" ID="rbPercentValue" GroupName="grAppliedPriceType" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="tbPercentValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPercentValue" ControlToValidate="tbPercentValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPercentValue" MinimumValue="-1000.00" MaximumValue="1000.00"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="tbPercentValue"
                                                    ToolTip="<%$ Resources:ValidationSR, PercentVEMsg%>" Display="Dynamic" />
                                                <qwc:Label runat="server" ID="lblPercentValue" Text="<%$ Resources:SR, PercentValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="AppliedPriceTypeChanged" AutoPostBack="True" runat="server" ID="rbPoundValue" GroupName="grAppliedPriceType" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="tbPoundValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPoundValue" ControlToValidate="tbPoundValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPoundValue" MinimumValue="-999999.99" MaximumValue="999999.99"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="tbPoundValue"
                                                    ToolTip="<%$ Resources:ValidationSR,CurrencyPoundsVEMsg %>" Display="Dynamic" />
                                                <qwc:Label runat="server" ID="lblPoundValue" Text="<%$ Resources:SR, PoundValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>                        
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
