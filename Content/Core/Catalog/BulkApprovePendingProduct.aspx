﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="BulkApprovePendingProductContent.ascx" TagName="BulkApprovePendingProductContent" TagPrefix="uc" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:BulkApprovePendingProductContent ID="Content" runat="server" />
</asp:Content>
