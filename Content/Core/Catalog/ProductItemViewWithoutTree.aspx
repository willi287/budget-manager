﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.CataloguePageBase" %>
<%@ Register Src="ProductItemViewContent.ascx" TagName="ProductItemViewContent" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:ProductItemViewContent ID="ContentControl" runat="server" />
</asp:Content>
