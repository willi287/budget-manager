﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulkUpdateStandardPriceContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Catalog.BulkUpdateStandardPriceContent" %>
<%@ Register TagPrefix="e2c" TagName="Calendar" Src="~/Controls/Core/Calendar.ascx" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OptionsOfBulkUpdateProductsControl.ascx" TagName="OptionsOfBulkUpdateProducts" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="B2134361-3F9E-4C75-B8AA-A323009BA55D" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tbody>
        <tr>
            <td>
                <table class="subForm" style="width: 500px;">
                    <tbody>
                        <tr>
                            <td>
                                <uc:OptionsOfBulkUpdateProducts ID="ctrlOptionsOfBulkUpdateProducts" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblChooseOptionToPriceUpdate" runat="server" Text="<%$ Resources:SR, ChooseOptionToPriceUpdate %>" />:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="PriceUpdateTypeGroupChanged"  AutoPostBack="True" runat="server" ID="rbPercentValue" GroupName="priceUpdateTypeGroup" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="txtPercentValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPercentValue" ControlToValidate="txtPercentValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPercentValue" MinimumValue="-1000.00" MaximumValue="1000.00"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="txtPercentValue" ToolTip="<%$ Resources:ValidationSR,PercentVEMsg%>"
                                                    Display="Dynamic" />
                                                <qwc:Label runat="server" ID="txtPercentValueAdditionalText" Text="<%$ Resources:SR, PercentValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="PriceUpdateTypeGroupChanged" AutoPostBack="True" runat="server" ID="rbPoundValue" GroupName="priceUpdateTypeGroup" />
                                            </td>
                                            <td>
                                                <qwc:TextBox runat="server" ID="txtPoundValue" CssClass="short"/>
                                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                                    EnableClientScript="true" ID="rfvPoundValue" ControlToValidate="txtPoundValue" Display="Dynamic"
                                                    runat="server" ErrorMessage="*" />
                                                <asp:RangeValidator ID="rvPoundValue" MinimumValue="-999999.99" MaximumValue="999999.99"
                                                    Type="Currency" runat="server" ErrorMessage="*" EnableClientScript="True" ControlToValidate="txtPoundValue" ToolTip="<%$ Resources:ValidationSR,CurrencyPoundsVEMsg %>"
                                                    Display="Dynamic" />
                                                <qwc:Label runat="server" ID="lblPoundValueAdditionalText" Text="<%$ Resources:SR, PoundValueAdditionalText %>"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel runat="server" runat="server" ID="lblWhenPriceAvailable" Text="<%$ Resources:SR, WhenPriceAvailable %>"></qwc:InfoLabel>:
                            </td>
                            </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton Checked="True" OnCheckedChanged="OptionOfApplyingPriceGroupChanged" GroupName="OptionsOfApplyingPriceGroup" AutoPostBack="True" runat="server" ID="rbApplyImmediatePrice" Text="<%$ Resources:SR, ApplyImmediatePrice %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <qwc:GroupRadioButton OnCheckedChanged="OptionOfApplyingPriceGroupChanged" GroupName="OptionsOfApplyingPriceGroup" AutoPostBack="True" runat="server" ID="rbApplyDelayedPrice" Text="<%$ Resources:SR, ApplyDelayedPrice %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td class="captionNowrap">
                                                                <qwc:InfoLabel ID="lblPublishDate" runat="server" Text="<%$ Resources:SR, PublishDate %>" AssociatedControlID="calPublishDate" />:
                                                            </td>
                                                            <td class="formData">
                                                                <e2c:Calendar ID="calPublishDate" runat="server" AutoPostBack="True" />    
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
