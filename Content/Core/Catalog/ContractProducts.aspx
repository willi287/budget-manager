﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.ContractProducts" %>
<%@ Register src="ContractProductsContent.ascx" tagname="ContractProductsContent" tagprefix="uc1" %>
<%@ Register src="POProductsCategoriesTree.ascx" tagname="POProductsCategoriesTree" tagprefix="uc2" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:ContractProductsContent ID="ContentControl" runat="server" />
</asp:Content>
