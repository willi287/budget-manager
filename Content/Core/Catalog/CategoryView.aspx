﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Catalog.CatalogueListPageBase" %>
<%@ Register src="~/Content/Core/Catalog/CategoryList.ascx" tagname="CategoryList" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
	<uc1:CategoryList ID="ContentControl" runat="server" />
</asp:Content>
