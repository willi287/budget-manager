﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="BulkAddToPriceListContent.ascx" TagName="BulkAddToPriceListContent" TagPrefix="uc" %>

<asp:Content ID="BulkAddToPriceListContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:BulkAddToPriceListContent ID="Content" runat="server" />
</asp:Content>
