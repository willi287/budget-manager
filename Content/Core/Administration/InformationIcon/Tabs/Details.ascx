<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.InformationIcon.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="d9db16e5-b5c6-4d6b-9705-b67ef7773d43" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFieldReference" runat="server" Text="<%$ Resources:SR,FieldReference%>"
                            AssociatedControlID="tbFieldReference" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFieldReference" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFullDescription" runat="server" Text="<%$ Resources:SR,FullDescription%>"
                            AssociatedControlID="tbFullDescription" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFullDescription" runat="server" MaxLength="2000" TextMode="MultiLine"
                            Height="100px" Width="80%" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblHelpLink" runat="server" Text="<%$ Resources:SR,HelpLink%>"
                            AssociatedControlID="tbHelpLink" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbHelpLink" runat="server" MaxLength="100" />
                    </td>
                </tr>
                
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblStatus" runat="server" Text="<%$ Resources:SR,Status%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblStatusContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
