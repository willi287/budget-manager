<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="18d1d04b-2acf-42f9-a3e7-46e4f505d148" />    
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="FieldReference" ConfirmationMsg="<%$ Resources:SR, MsgConfirmDeleteActionForInfo %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="FieldReference" SortExpression="FieldReference"/>
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="Status" 
            FalseValue="<%$ Resources:SR,Disabled %>"
            TrueValue="<%$ Resources:SR,Enabled %>"
            AllowSorting="false" />
    </Columns>
</qwc:ControlledGrid>
