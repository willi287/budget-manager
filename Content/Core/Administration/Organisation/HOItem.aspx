﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="HOItem.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.HOItem" Title="Untitled Page" %>
<%@ Register src="HOItemContent.ascx" tagname="HOItemContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="cntHOItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:HOItemContent ID="HOItemContent" runat="server" />
</asp:Content>
