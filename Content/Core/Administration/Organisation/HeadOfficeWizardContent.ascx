<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeadOfficeWizardContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.HeadOfficeWizardContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
