<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganisationWizardContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.OrganisationWizardContent"%>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
