<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="822ce174-56dc-4f56-ba73-b4593f670f68" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Administration/Organisation/HOItem.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,HeadOfficeName %>" DataField="Name" SortExpression="Name"/>
    </Columns>
</qwc:ControlledGrid>
