﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register TagPrefix="ctl" TagName="OrganisationList" Src="~/Content/Core/Administration/Organisation/OrganisationListContent.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:OrganisationList ID="ContentControl" runat="server" />
</asp:Content>

