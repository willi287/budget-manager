﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="ProviderListContent.ascx" tagname="ProviderListContent" tagprefix="uc" %>
<asp:Content ID="ctnProvider" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:ProviderListContent ID="ContentControl" runat="server" />
</asp:Content>
