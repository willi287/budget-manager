<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="54bf9cf3-a077-40bd-8b0e-994b9cc6fdd5" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <%--qwc:SelectColumn /--%>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ConsortiumName %>" DataField="Name" SortExpression="Name" />
    </Columns>
</qwc:ControlledGrid>
