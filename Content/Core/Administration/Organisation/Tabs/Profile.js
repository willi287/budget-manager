﻿function CreateOrganisationProfileViewModel(viewModel) {
    viewModel.AddressChecked = ko.observable(viewModel.AddressChecked());
    viewModel.IsNewAddress = ko.computed(function () {
        return (viewModel.AddressChecked() == "rbRegAddrNewAddr");
    }
    );
    viewModel.SelectedAddress = ko.computed(function () {
        if (viewModel.IsNewAddress()) { return viewModel.RegisteredAddress(); }
            else { return viewModel.OrganisationAddress(); }
        }, this);
}