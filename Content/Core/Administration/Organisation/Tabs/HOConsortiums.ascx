<%@ Import Namespace="Resources"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HOConsortiums.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.HOConsortiums" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="88b7eb1a-eb0e-4631-b9ae-85e7f2235911" />
<qwc:ControlledGrid ID="cgConsortiums" runat="server" KeyField="Id" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn DataField="InHO" UseHeaderCheckBox="true" EnableHighlight="true"/>                     
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ConsortiumName %>" DataField="Name"  SortExpression="None"/>         
    </Columns>
</qwc:ControlledGrid>

