﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleOptions.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ModuleOptions" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="ac9f1b54-4f6b-4e8c-9688-eed0017dcd0a" />

<table width="40%">
    <tr>
        <td>
            <br/>
            <table class="subForm">
                <tr>
                      <td class="caption">
                          <qwc:InfoLabel ID="InfoLabel1" CssClass="bold" runat="server" Text="<%$ Resources:SR, MarketplaceName %>" 
                                    AssociatedControlID="tbMarketplaceName" />:
                      </td>
                      <td>
                          <qwc:TextBox ID="tbMarketplaceName" runat="server" MaxLength="50" />
                       </td>
                </tr>
            </table>
        </td>
   </tr>
</table>
<table class="form">
   <tr>
        <td>
            <table class="subForm grey">
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblModuleOptions" runat="server" Text="<%$ Resources:SR, ModuleOptions %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:TreeView ID="tvModule" ShowLines="true" runat="server" AutoCheckUnchekChild="true"
                            ShowCheckBoxes="All" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%if (IsLocalGroupsAndActivitiesOptionVisible)
      { %> 
    <tr>
        <td>
            <qwc:InfoLabel ID="lblEnableLocalGroupsAndActivities" CssClass="bold" runat="server" Text="<%$ Resources:SR, LocalGroupsAndActivities %>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbEnableLocalGroupsAndActivities" runat="server" Text="<%$ Resources:SR, EnableLocalGroupsAndActivities  %>" />
        </td>
    </tr>
    <% } %>

    <%if (IsWhereILiveOptionVisible)
      { %> 
    <tr>
        <td>
            <qwc:InfoLabel ID="lblWhereILiveOptions" CssClass="bold" runat="server" Text="<%$ Resources:SR, WhereILiveOptions %>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbBrandedMarketplace" runat="server" Text="<%$ Resources:SR, BrandedMarketplace  %>" />
        </td>
    </tr>
    <% } %>
</table>
