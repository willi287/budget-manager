<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Consortiums.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.Consortiums" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="6d7e96fa-3b3f-4dd4-9c9f-41d0ad031a17" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgConsortiums"
    runat="server" KeyField="Id" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn  DataField="HasOrganisation" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ConsortiumName %>" DataField="Name" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>

