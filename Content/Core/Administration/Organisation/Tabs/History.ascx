<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="History.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.History" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="08495346-4f05-448f-a127-c3d2146302f1" />
<qwc:ControlledGrid ID="cgHistoryList" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false" SortExpression="None">
    <columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:DateTimeColumn DataField="ActionTime" HeaderTitle="<%$ Resources:SR,DateTime %>" ShowTime="true" SortExpression="None"/>
         <qwc:TextColumn DataField="ActorNameFull"    HeaderTitle="<%$ Resources:SR,User %>" SortExpression="None"/>
         <qwc:ResourceColumn DataField="ActionId"   HeaderTitle="<%$ Resources:SR,Changes %>" />         
    </columns>
</qwc:ControlledGrid>
