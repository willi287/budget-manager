<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsortiumUsersWizard.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ConsortiumUsersWizard" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="b9e065b6-9347-4b88-bab3-b89ba8a0637a" />
<table class="borderedOneRow">
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbUsersWizard" Checked="true" runat="server" Text="<%$ Resources:SR, NeedConsUsersWizard %>" />
        </td>
    </tr>
</table>
