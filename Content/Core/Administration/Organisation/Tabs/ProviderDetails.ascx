<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ProviderDetails" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="12a12fac-fad1-455d-82ad-22ca30e7abe7" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, OrganisationName %>"
                            AssociatedControlID="tbName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" MaxLength="100" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" CssClass="bold" Text="<%$ Resources:SR, Phone %>"
                            AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"/>
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" CssClass="bold" Text="<%$ Resources:SR, Email %>"
                            AssociatedControlID="tbEmail" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <asp:RegularExpressionValidator ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                            EnableClientScript="true" ControlToValidate="tbEmail" runat="server"
                            ErrorMessage="*" ValidationExpression="[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" CssClass="bold" Text="<%$ Resources:SR, Fax %>"
                            AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                        <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <qwc:Image ID="imgLogo" runat="server" PanelID="panelId" />
        </td>
    </tr>
</table>
