<%@ Import Namespace="Eproc2.Core.Logic.Security" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.SettingsWizard" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/AutomationGroupingDates.ascx" TagName="AutomationGroupingDates"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OrderAutomationSettings.ascx" TagName="OrderAutomationSettings"
    TagPrefix="e2o" %>
<uc:HelpBox runat="server" HelpBoxId="dbc0b0e6-ea3c-4583-b933-3f70ef6afb87" />
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<table class="form">
    <tr>
        <td colspan="2">
            <br />
            <asp:Table ID="tbEditablePONo" runat="server" Visible="false" CssClass="subForm grey">
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbEditablePONo" CssClass="bold" runat="server" Text="<%$ Resources:SR, EnablePONumber %>" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <% if (IsBuyerOptionsVisible)
               { %>
            <asp:Table ID="tbBuerOptions" runat="server" CssClass="subForm grey">
                <asp:TableRow>
                    <asp:TableHeaderCell HorizontalAlign="Left">
                        <qwc:InfoLabel ID="lblBuyerOptions" runat="server" Text="<%$ Resources:SR, BuyerOptions %>" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbBuyerAutoConfirmSO" CssClass="bold" runat="server" Text="<%$ Resources:BM_SR, AutoConfirmSO %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbAutoConfirmDN" CssClass="bold" runat="server" Text="<%$ Resources:SR, AutoConfirmDN %>" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <% } %>
        </td>
        <td valign="top" rowspan="2">
            <% if (IsDefaultInvoiceSettingsVisible)
               { %>
            <table class="subForm grey">
                <tr>
                    <td class="caption" colspan="2">
                        <qwc:InfoLabel ID="lblDefaultInvoiceSettings" runat="server" Text="<%$ Resources:SR, DefaultInvoiceSettings %>" /></td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel runat="server" ID="lblPaymentTerms" Text="<%$ Resources:SR, PaymentTerms %>" AssociatedControlID="tbPaymentTerms" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPaymentTerms" runat="server" MaxLength="3" />&nbsp;day(s) from invoice date
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvPaymentTerms" ControlToValidate="tbPaymentTerms" Display="Dynamic"
                            runat="server" ErrorMessage="*" />

                        <asp:RangeValidator runat="server" MinimumValue="1" MaximumValue="365" Type="Integer" ControlToValidate="tbPaymentTerms" Display="Dynamic" ErrorMessage="*"
                            ToolTip="<%$ Resources:ValidationSR, CommonRangeVEMsg %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" ID="lblBankName" Text="<%$ Resources:SR, BankName %>" AssociatedControlID="tbBankName" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBankName" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" ID="lblBankAccountNumber" Text="<%$ Resources:SR, BankAccountNumber %>" AssociatedControlID="tbBankAccountNumber" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBankAccountNumber" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" ID="lblBankSortCode" Text="<%$ Resources:SR, BankSortCode %>" AssociatedControlID="tbBankSortCode" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBankSortCode" runat="server" MaxLength="8" />
                        <asp:RegularExpressionValidator ID="revBankSortCode" runat="server" ControlToValidate="tbBankSortCode"
                            EnableClientScript="true" Style="white-space: nowrap" Display="Dynamic" ValidationExpression="\w\w-\w\w-\w\w"
                            ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" Text="*" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" ID="lblBankDetailsNote" Text="<%$ Resources:SR, BankDetailsNote %>" AssociatedControlID="tbBankDetailsNote" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBankDetailsNote" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" ID="lblProviderReference" Text="<%$ Resources:SR, ProviderReference %>" AssociatedControlID="ddlProviderReference" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlProviderReference" DataValueField="Id" DataTextField="Name" runat="server"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
            <br />
            <% } %>
            <% if (IsDocumentsSettingsVisible)
               { %>
            <table class="subForm grey">
                <tr>
                    <td class="caption" colspan="2">
                        <qwc:InfoLabel ID="lblDocumentSettings" runat="server" Text="<%$ Resources:SR, DocumentSettings %>" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel runat="server" ID="lblOrganisationReference" Text="<%$ Resources:SR, OrganisationReference %>"
                            AssociatedControlID="tbOrganisationReference" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox CssClass="short" ID="tbOrganisationReference" runat="server" MaxLength="5" />&nbsp;max
                        5 characters
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvOrganisationReference" ControlToValidate="tbOrganisationReference"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                        <asp:RegularExpressionValidator EnableClientScript="true" ControlToValidate="tbOrganisationReference"
                            ID="revOrganisationReference" runat="server" ToolTip="<%$ Resources:ValidationSR, OrganisationReferenceAlphanumericValidation %>"
                            ErrorMessage="*" ValidationExpression="\w{1,5}" />
                    </td>
                </tr>
            </table>
            <br />
            <% } %>
            <% if (IsCatalogueSettingsVisible)
               { %>
            <table class="subForm grey">
                <tr>
                    <td class="caption" colspan="2">
                        <qwc:InfoLabel ID="lblCatalogueSettings" runat="server" Text="<%$ Resources:SR, CatalogueSettings %>" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel runat="server" ID="lblCustomFieldName" Text="<%$ Resources:SR, CustomFieldName %>"
                            AssociatedControlID="tbCustomFieldName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox CssClass="short" ID="tbCustomFieldName" runat="server" MaxLength="30" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvCustomFieldName" ControlToValidate="tbCustomFieldName"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                    </td>
                </tr>
            </table>
            <br />
            <% } %>
            <% if (IsPasswordSecurityVisible)
               { %>
            <table class="subForm grey">
                <tr>
                    <td class="caption" colspan="4">
                        <qwc:InfoLabel ID="lblPasswordSecurity" runat="server" Text="<%$ Resources:SR, PasswordSecurity %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <qwc:Label runat="server" Text="<%$ Resources:SR, ApplyTo %>"></qwc:Label>:
                        <qwc:RadioButton ID="rbStaff" runat="server" GroupName="PasswordSecurityApplication" Text="<%$ Resources:SR, Staff %>" />
                        <qwc:RadioButton ID="rbAllUsers" runat="server" GroupName="PasswordSecurityApplication" Text="<%$ Resources:SR, AllUsers %>" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px">
                        <qwc:QWCheckBox ID="chkUppercaseCharacters" runat="server" Text="<%$ Resources:SR, UppercaseCharacters %>" />
                    </td>
                    <td colspan="2" style="vertical-align: bottom">
                        <qwc:Label runat="server" CssClass="bottomSpan" Text="<%$ Resources:SR, MinimumLengthOfPassword %>" />:
                    </td>
                    <td style="width: 60px">
                        <qwc:DropDownList runat="server" ID="ddlMinimumLengthOfPassword" CssClass="shortest" DataTextField="Value" DataValueField="Key" SortOrder="None"></qwc:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:QWCheckBox ID="chkLowercaseCharacters" runat="server" Text="<%$ Resources:SR, LowercaseCharacters %>" />
                    </td>
                    <td colspan="2" style="vertical-align: bottom">
                        <qwc:Label runat="server" Text="<%$ Resources:SR, NumberOfIncorrectAttemptsBeforeLock %>"></qwc:Label>:
                    </td>
                    <td>
                        <qwc:DropDownList runat="server" ID="ddlNumberOfIncorrectAttemptsBeforeLock" CssClass="shortest" DataTextField="Value" DataValueField="Key" SortOrder="None" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:QWCheckBox ID="chkNumbers" runat="server" Text="<%$ Resources:SR, Numbers %>" />
                    </td>
                    <td style="vertical-align: bottom">
                        <qwc:Label runat="server" Text="<%$ Resources:SR, PasswordExpiry %>"></qwc:Label>:
                    </td>
                    <td style="vertical-align: bottom">
                        <qwc:TextBox runat="server" ID="tbPasswordExpiry" Style="width: auto" />
                        <act:FilteredTextBoxExtender ID="tbPasswordExpiryLimiter" runat="server" TargetControlID="tbPasswordExpiry" FilterMode="ValidChars" FilterType="Numbers" />
                        <asp:RangeValidator ID="rvExpiryRangeValidator" MinimumValue="30" MaximumValue="999"
                            Type="Integer" runat="server" Text="*" ControlToValidate="tbPasswordExpiry"
                            ToolTip="<%$ Resources:ValidationSR,PasswordExpiryRangeVEMsg %>" Display="Dynamic" />
                    </td>
                    <td style="vertical-align: bottom">
                        <qwc:Label runat="server" Text="<%$ Resources:BM_SR, days %>"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:QWCheckBox ID="chkSpecialCharacters" runat="server" Text="<%$ Resources:SR, SpecialCharacters %>" />
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <br />
            <% } %>
            <table class="subForm">
                <%if (cbEnableSAGE.Checked)
                  {%>
                <tr>
                    <td>
                        <table id="sageDiv" class="subForm">
                            <tr>
                                <td class="caption">
                                    <qwc:InfoLabel ID="lblSageNominalCode" runat="server" Text="<%$ Resources:SR, SageNominalCode %>" />:
                                </td>
                                <td>
                                    <qwc:TextBox name="tbSageNominalCode" ID="tbSageNominalCode" runat="server" MaxLength="8" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%} if (cbEnableBuyerCodes.Checked && (Controller.IsVisibleControl(ContractEntity.BUYER_CODES) || SecurityHelper.IsSag()))
                  {%>
                <tr>
                    <td>
                        <hr />
                        <table id="tbBuyerCodes" class="subForm">
                            <tr>
                                <td class="caption required">
                                    <qwc:InfoLabel ID="lblBuyerCode1" runat="server" Text="<%$ Resources:SR, OrganisationCode1 %>"
                                        AssociatedControlID="tbBuyerCode1" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbBuyerCode1" runat="server" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption required">
                                    <qwc:InfoLabel ID="lblBuyerCode2" runat="server" Text="<%$ Resources:SR, OrganisationCode2 %>"
                                        AssociatedControlID="tbBuyerCode2" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbBuyerCode2" runat="server" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption required">
                                    <qwc:InfoLabel ID="lblBuyerCode3" runat="server" Text="<%$ Resources:SR, OrganisationCode3 %>"
                                        AssociatedControlID="tbBuyerCode3" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbBuyerCode3" runat="server" MaxLength="50" />
                                </td>
                            </tr>
                        </table>
                        <hr />
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td>
                        <% if (IsWizard) { %>
                        <qwc:QWCheckBox ID="cbUsersWizard" runat="server" Checked="true" Text="<%$ Resources:SR, NeedUsersWizard %>" />
                        <% } %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <% if (IsSupplierOptionsVisible)
               { %>
            <asp:Table ID="tbSupplierOptions" runat="server" CssClass="subForm grey">
                <asp:TableRow>
                    <asp:TableHeaderCell HorizontalAlign="Left">
                        <qwc:InfoLabel ID="lblSupplierOptions" runat="server" Text="<%$ Resources:SR, SupplierOptions %>" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbSupplierAutoConfirmSO" CssClass="bold" runat="server" Text="<%$ Resources:BM_SR, AutoConfirmSO %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbAutoConfirmPO" CssClass="bold" runat="server" Text="<%$ Resources:SR, AutoConfirmPO %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbEnableOrderUpload" CssClass="bold" runat="server" Text="<%$ Resources:SR, EnableOrderUpload %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbEnableUniqueInvoiceNo" CssClass="bold" runat="server" Text="<%$ Resources:SR, EnableUniqueInvoiceNo %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <br/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <% } %>

            <%if (IsAutomationSettingControlVisible)
              {%>
            <asp:Panel runat="server" CssClass="subForm grey bold">
                <table>
                    <tr>
                        <td>
                            <qwc:Label CssClass="subForm grey bold" ID="lblDefaultOrderAutomationSettings" runat="server" Text="<%$ Resources:SR, DefaultOrderAutomationSettings %>" />:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <e2o:OrderAutomationSettings ID="ctrlOrderAutomationSettings" runat="server" />
                            <uc:AutomationGroupingDates runat="server" ID="ctrlAutomationGroupingDates" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <br />
            <%
              }
            %>
            <% if (IsIntegrationCostCodesVisible)
               { %>
            <asp:Table ID="tbIntegrationCostCodes" runat="server" CssClass="subForm grey">
                <asp:TableRow>
                    <asp:TableHeaderCell ColumnSpan="2" HorizontalAlign="Left">
                        <qwc:InfoLabel ID="lblIntegrationCostCodes" runat="server" Text="<%$ Resources:SR, IntegrationCostCodes %>" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow ID="rowEnableSage">
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbEnableSAGE" CssClass="bold" runat="server" Text="<%$ Resources:SR, EnableSAGE %>" AutoPostBack="true" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="rowEnableBuyerCodes">
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="cbEnableBuyerCodes" CssClass="bold" runat="server" Text="<%$ Resources:SR, EnableOrganisationCodes %>" AutoPostBack="true" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <% } %>
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgCodes" runat="server" SortExpression="None" KeyField="OrganisationId"
    Visible="false">
    <Columns>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Organisation %>" DataField="OrganisationName"
            SortExpression="None" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, SageAccountCode %>" DataField="SageCode"
            SortExpression="None" ColumnType="String" MaxLength="50" />
    </Columns>
</qwc:ControlledGrid>
