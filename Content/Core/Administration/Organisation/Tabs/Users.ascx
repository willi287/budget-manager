<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Users.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.Users" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="e2w" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
    <%@ Register Src="../../../../../Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e32d23b9-2ad9-45c0-8504-832f574964be" />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<uc1:PopupControl ID="pcMessage" runat="server" Text="<%$ Resources:ValidationSR, PrincipalDeleteWarning %>" />
<table class="form">
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="lbtnAdd_Click" Caption="<%$ Resources:SR,Add %>"
                Visible="false" />
        </td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPrincipals"
    runat="server" KeyField="Id" OnDeleting="cgPrincipals_OnDeleting" SortExpression="None">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserId %>" DataField="Login" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserName %>" DataField="Name" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="RoleName" SortExpression="None"/>
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,AccountStatus %>" DataField="IsDisabled"
            FalseValue="<%$ Resources:SR,Active %>" TrueValue="<%$ Resources:SR,Disabled %>" />
    </Columns>
</qwc:ControlledGrid>
