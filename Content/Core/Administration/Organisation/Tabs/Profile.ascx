<%@ Import Namespace="Eproc2.Web.Framework.Tabs" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.Profile" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="355064c6-32d4-4890-b7a0-e6cc0866dfd6" />
<div id="<%= this.ClientID %>">
    <table class="form">
        <tr>
            <td>
                <table class="subForm">
                    <tr runat="server" id="vwIdRow" visible="false">
                        <td class="caption">
                            <qwc:InfoLabel ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbVWId" MaxLength="50" runat="server" Format="{0:D}" />
                            <asp:RegularExpressionValidator ID="rgxpVwIdValidator" runat="server" ControlToValidate="tbVWId"
                                EnableClientScript="true" Style="white-space: nowrap" Display="Dynamic" ValidationExpression="\d{0,50}"
                                ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" Text="*" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblHeadOffice" runat="server" Text="<%$ Resources:SR, HeadOffice %>"
                                AssociatedControlID="ddlHeadOffice" />:
                        </td>
                        <td>
                            <qwc:DropDownList ID="ddlHeadOffice" DataValueField="Id" DataTextField="Value" runat="server"
                                Visible="false" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" IsEmptyItemVisible="true" />
                            <qwc:Label ID="lblHeadOfficeContent" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="required caption">
                            <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, OrganisationName %>"
                                AssociatedControlID="tbName" />:&nbsp;<span>*</span>
                        </td>
                        <td>
                            <qwc:TextBox ID="tbName" runat="server" MaxLength="50" />
                            <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                               { %>
                            <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                EnableClientScript="true" ID="rfvName" ControlToValidate="tbName" Display="Dynamic"
                                runat="server" ErrorMessage="*" />
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td class="required caption">
                            <qwc:InfoLabel ID="lblVAT" runat="server" Text="<%$ Resources:SR, VATNumber %>" AssociatedControlID="tbVAT" />:
                            <span>*</span>
                        </td>
                        <td>
                            <qwc:TextBox ID="tbVAT" runat="server" MaxLength="20" />
                            <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                               { %>
                            <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                EnableClientScript="true" ID="rfvVAT" ControlToValidate="tbVAT" Display="Dynamic"
                                runat="server" ErrorMessage="*" />
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                            <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="required caption">
                            <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                            <span>*</span>
                        </td>
                        <td>
                            <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                            <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                EnableClientScript="true" ID="rfvEmail" ControlToValidate="tbEmail" Display="Dynamic"
                                runat="server" ErrorMessage="*" />
                            <asp:RegularExpressionValidator ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                                EnableClientScript="true" ControlToValidate="tbEmail" ID="revEmail" runat="server"
                                ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax %>" AssociatedControlID="tbFax" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                            <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                                runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblWebSite" runat="server" Text="<%$ Resources:SR, WebSite %>"
                                AssociatedControlID="tbWebSite" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbWebSite" runat="server" MaxLength="50" />
                        </td>
                    </tr>
                    <% if (Controller.IsVisibleControl(OrganisationEntity.PROP_SUPPLIEROF))
                       { %>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblSupplierOf" runat="server" Text="<%$ Resources:SR, SupplierOf %>"
                                AssociatedControlID="tbSupplierOf" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbSupplierOf" runat="server" MaxLength="250" />
                        </td>
                    </tr>
                    <% } %>
                </table>
            </td>
            <td>
                <% if (!(TabControl is Eproc2.Web.Framework.Tabs.Wizard))
                   { %>
                <qwc:Image ID="imgLogo" AllowPreview="true" runat="server" PanelID="imagePanel" />
                <% } %>
                &nbsp;
                <% if (Controller.IsVisibleControl(OrganisationEntity.PROP_DEFAULTBV))
                   { %>
                <table class="borderedOneRow">
                    <tr id="trBV" runat="server">
                        <td class="caption">
                            <qwc:InfoLabel ID="lblDefaultBV" runat="server" Text="<%$ Resources:SR, DefaultBV %>"
                                AssociatedControlID="ddlDefaultBV" />:
                        </td>
                        <td>
                            <qwc:DropDownList ID="ddlDefaultBV" DataValueField="Id" DataTextField="Value" runat="server"
                                ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                        </td>
                    </tr>
                </table>
                <% } %>
            </td>
        </tr>
        <tr>
            <td>
                <table class="subForm grey" border="0">
                    <tr>
                        <td class="caption" colspan="2">
                            <qwc:InfoLabel runat="server" ID="lblCompanyRegDetails" Text="<%$ Resources:SR, CompanyRegistrationDetails %>" />:
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel runat="server" ID="lblRegNumber" Text="<%$ Resources:SR, RegistrationNumber %>" AssociatedControlID="tbRegNumber" />:
                        </td>
                        <td>
                            <qwc:TextBox ID="tbRegNumber" runat="server" MaxLength="50" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption" colspan="2">
                            <qwc:InfoLabel runat="server" ID="lblRegAddress" Text="<%$ Resources:SR, RegisteredAddress %>" />:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <qwc:RadioButton ID="rbRegAddrAsOrgAddr" runat="server" GroupName="RegisteredAddress" Text="<%$ Resources:SR, SameAsOrganisationAddress %>" />
                            <qwc:RadioButton ID="rbRegAddrNewAddr" runat="server" GroupName="RegisteredAddress" Text="<%$ Resources:SR, NewAddress %>" />
                        </td>
                    </tr>
                    <% if (!Controller.IsViewMode)
                       { %>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAddNewAddress" runat="server"
                                Text="<%$ Resources:SR, AddNewAddress %>" OnClick="btnAddNewAddress_Click" />
                            <asp:Button ID="btnCreateCustomAddress" runat="server"
                                Text="<%$ Resources:SR, CreateCustomAddress %>"
                                OnClick="btnCreateCustomAddress_Click" />
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colspan="2">
                            <qwc:Label runat="server" ID="lblSelectedAddress" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
            </td>
        </tr>
    </table>

    <input type="hidden" runat="server" id="hViewModelStorage" />
</div>
