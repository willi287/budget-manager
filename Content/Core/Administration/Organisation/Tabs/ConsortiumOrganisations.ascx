<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsortiumOrganisations.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ConsortiumOrganisations" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="bb8e4153-cf82-4af2-876d-101392286c67" />
<table class="borderedOneRow">
    <tr>
        <td class="bold">
            <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, Consortium%>" AssociatedControlID="ddlConsortiums" />:
        </td>
        <td>
            <qwc:DropDownList ID="ddlConsortiums" AutoPostBack="true" DataValueField="Id" DataTextField="Value"
                runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgOrganisations" runat="server" KeyField="Id" SortExpression="None">
    <Columns>
        <qwc:CheckBoxColumn DataField="HasConsortium" UseHeaderCheckBox="true" EnableHighlight="true"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName %>" DataField="Name" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
