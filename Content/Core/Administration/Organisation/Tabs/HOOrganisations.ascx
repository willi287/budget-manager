<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HOOrganisations.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.HOOrganisations" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="da299a1b-ac6c-4e97-a498-46e0ea4ff0da" />
<table class="form">
    <tr>
        <td>
            <table class="subForm bordered">
                <tr>
                    <td style="padding-top: 4px">
                        <qwc:dropdownlist id="ddlConsortiums" autopostback="true" datavaluefield="Id" datatextfield="Name"
                            runat="server" onselectedindexchanged="ddlConsortiums_SelectedIndexChanged" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
<qwc:controlledgrid id="cgOrganisations" runat="server" keyfield="OrganisationId" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn DataField="InHO" UseHeaderCheckBox="true" EnableHighlight="true"/>                     
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName %>" DataField="Name" SortExpression="None"/>         
    </Columns>
</qwc:controlledgrid>
