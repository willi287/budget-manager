<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HODetails.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.HODetails" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="a7a15d16-64e7-47d6-b226-9d5108e15438" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, HeadOfficeName %>"
                            AssociatedControlID="tbName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" MaxLength="100" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="tbName"
                            EnableClientScript="true" ValidationGroup="NextValidationGroup" Display="Dynamic"
                            ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" runat="server" />
                        <%}%>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RegularExpressionValidator ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                            EnableClientScript="true" ControlToValidate="tbEmail" ID="revEmail" runat="server"
                            ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        <%}%>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax %>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <% if (!(TabControl is Eproc2.Web.Framework.Tabs.Wizard))
               {%>
            <qwc:Image ID="imgLogo" AllowPreview=true runat="server" PanelID="imagePanel" />
            <%}%>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
        </td>
    </tr>
</table>
