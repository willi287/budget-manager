<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.BillingDetails" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="130dda31-4a1b-4913-b331-bbccad23aac6" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblFeeContractName" runat="server" Text="<%$ Resources:SR, TransactionFeeContractName %>"
                            AssociatedControlID="tbFeeContractName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFeeContractName" runat="server" MaxLength="50" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvFeeContractName" ControlToValidate="tbFeeContractName"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                        <%} %>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td width="35%" class="bordered" valign="middle" align="center">
                        <qwc:QWCheckBox CssClass="bold" ID="cbDirectDebit" runat="server" Text="<%$ Resources:SR, DirectDebit %>" />
                    </td>
                    <td />
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblFeeAddress" CssClass="bold" runat="server" Text="<%$ Resources:SR, TransactionFeeInvoiceAddress %>" 
                        AssociatedControlID="btnCoppyAddress" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <asp:Button ID="btnCoppyAddress" runat="server" Text="<%$ Resources:SR, CopyOrganisationAddress %>"
                            OnClick="btnCoppyAddress_Click" />
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
    <tr>
        <td colspan="2">
            <e2w:AddressControl ID="ucAddress" runat="server" UniqueControlID="7CAE2ECC-9A4D-4AA0-8012-F4720643589C"
                LocalSearchEnabled="true" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblFeeEmail" runat="server" Text="<%$ Resources:SR, TransactionFeeEmailAddress %>"
                            AssociatedControlID="tbFeeEmail" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFeeEmail" runat="server" MaxLength="50" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvFeeEmail" ControlToValidate="tbFeeEmail" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                        <asp:RegularExpressionValidator EnableClientScript="true" ControlToValidate="tbFeeEmail"
                            ID="revEmail" runat="server" ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                            ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        <%
                            }%>
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblSageReference" runat="server" Text="<%$ Resources:SR, SageReference %>"
                            AssociatedControlID="tbSageReference" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSageReference" runat="server" MaxLength="8" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvSageReference" ControlToValidate="tbSageReference"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                            <asp:RegularExpressionValidator EnableClientScript="true" ControlToValidate="tbSageReference"
                            ID="RegularExpressionValidator1" runat="server" ToolTip="<%$ Resources:ValidationSR, SageReferenceValidation %>"
                            ErrorMessage="*" ValidationExpression="\w{1,8}" />
                        <%
                            }%>
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblFeeRate" runat="server" Text="<%$ Resources:SR, TransactionFeeRate %>"
                            AssociatedControlID="ddlFeeRate" />,%:  <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFeeRate" runat="server" DataValueField="Id" DataTextField="Value"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR, InvoiceDescription %>"
                            AssociatedControlID="tbDescription" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <% if (IsPioOrganisation)
                   { %>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblChildOrganisations" runat="server" Text="<%$ Resources:SR, ChildOrganisations %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblChildOrganisaiotsValue" runat="server" />
                    </td>
                </tr>
                <%} %>
                <%if (IsChildOrganisation)
                  { %>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPrimaryOrgType" runat="server" Text="<%$ Resources:SR, PrimaryOrgType %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPrimaryOrgTypeValue" runat="server" />
                    </td>
                </tr>
                <tr visible="<%# IsChildOrganisation %>">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPrimaryOrganisation" runat="server" Text="<%$ Resources:SR, PrimaryOrganisation%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPrimaryOrganisationValue" runat="server" />
                    </td>
                </tr>
                <%} %>
            </table>
        </td>
        <td />
    </tr>
</table>
