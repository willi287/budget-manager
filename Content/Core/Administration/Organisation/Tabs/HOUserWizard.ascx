<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HOUserWizard.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.HOUserWizard" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="c8303793-4981-40d9-9373-ccb90be32174" />
<table class="borderedOneRow">
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbShouldCreateHOUser" runat="server" Text="<%$ Resources:SR,  NeedHOUsersWizard %>" />
        </td>
    </tr>
</table>
