<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsortiumHOs.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ConsortiumHOs" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="c7565348-f5ec-4aff-a458-82fff34e6513" />
<table class="borderedOneRow">
    <tr>
        <td class="bold">
            <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, Consortium%>" AssociatedControlID="ddlConsortiums" />:
        </td>
        <td>
            <qwc:DropDownList Id="ddlConsortiums" AutoPostBack="true" DataValueField="Id" DataTextField="Value"  runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
    </tr>    
</table>
<qwc:ControlledGrid ID="cgHOs" runat="server" KeyField="Id" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn DataField="HasConsortium" UseHeaderCheckBox="true" EnableHighlight="true" />                     
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, HOName %>" DataField="Name"  SortExpression="None"/>         
    </Columns>
</qwc:ControlledGrid>   
