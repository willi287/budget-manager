<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" CodeBehind="RoleListContent.ascx.cs" AutoEventWireup="true"
    Inherits="Eproc2.Web.Content.Core.Administration.Role.RoleListContent" %>
<%@ Import Namespace="Eproc2.Core.Entities.ListItems"%>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e0289734-efb0-4fbf-9790-278043408047" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" >
    <Columns>
        <%--<qwc:SelectColumn />--%>
        <qwc:GroupRadioButtonColumn DataField="Selected" IsEditable="true" EnableHighlight="true" GroupName="main" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,RoleName%>" DataField="RoleName" SortExpression="Name" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" AllowSorting=false DataField="RoleTypeId" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CreatedBy %>" DataField="Creator" SortExpression="PrincBase.Name"  />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Organisation %>" DataField="Organisation" SortExpression="tOrganisation.Name" />
    </Columns>
</qwc:ControlledGrid>
