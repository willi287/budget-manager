﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="RoleList.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="ctl" TagName="RoleList" Src="RoleListContent.ascx" %>
<asp:Content ID="cntRole" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:RoleList ID="ContentControl" runat="server" />
</asp:Content>

