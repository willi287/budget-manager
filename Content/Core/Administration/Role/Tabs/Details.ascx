<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Role.Tabs.Details" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Administration.Role"%>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc1" %>
<uc1:PopupControl ID="PopupControl1" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="21b8fded-c6fd-4543-987b-93502881de3d" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblRoleName" runat="server" Text="<%$ Resources:SR,RoleName%>" AssociatedControlID="tbRoleName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbRoleName" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR,Description %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" MaxLength="200" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR,Type %>" AssociatedControlID="ddlRoleType" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlRoleType" AutoPostBack="true" DataValueField="Id" DataTextField="Value"
                            runat="server" OnSelectedIndexChanged="ddlRoleType_SelectedIndexChanged"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" >
                        </qwc:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
<br />
<table>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblPermissions" runat="server" Text="<%$ Resources:SR,Permissions %>" />:
        </td>
        <td></td>
    </tr>    
    <tr>
        <td>
            <qwc:TreeView ID="tvPermissions" ShowLines="true" runat="server" AutoCheckUnchekChild="true"
                ShowCheckBoxes="All">
            </qwc:TreeView>
        </td>
        <td valign=top>
       <% if (Controller.IsVisibleControl(RoleEntityController.PROP_RULES_LIST_CONTROL)) {%>
        <qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="None">
            <Columns>
                <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Rules %>" DataField="Rule" SortExpression="None"/>
                <qwc:RadioButtonColumn IsEditable=true AutoPostBack=true GroupName="Rule" HeaderTitle="<%$ Resources:SR,Yes %>" DataField="RuleEnabled" />
                <qwc:RadioButtonColumn IsEditable=true AutoPostBack=true OnCheckedChanged="RadioButtonColumn_CheckedChanged" GroupName="Rule" HeaderTitle="<%$ Resources:SR,No %>" DataField="RuleDisabled" />
            </Columns>
        </qwc:ControlledGrid>
        <% } %>
        </td>
    </tr>
</table>
