﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationTreeView" %>
<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>        
        <qwc:TreeViewEx ID="tvContent" ShowLines="true" runat="server" ExpandDepth="0" SelectedNodeStyle-CssClass="selected" />
    </ContentTemplate>
</asp:UpdatePanel>
