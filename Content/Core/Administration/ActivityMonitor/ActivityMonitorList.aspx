﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="ctl" TagName="ActivityMonitorList" Src="~/Content/Core/Administration/ActivityMonitor/ActivityMonitorListContent.ascx" %>
<asp:Content ID="cntContent" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:ActivityMonitorList ID="ContentControl" runat="server" />
</asp:Content>
