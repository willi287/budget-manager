<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="42c74a99-12ef-4057-a7c1-0c246f25afaa" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" AllowPaging="true" SortExpression="Date" runat="server" KeyField="Id" >
    <Columns>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,DateTime %>" DataField="Date" SortExpression="Date"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,User %>" DataField="PrincipalName" SortExpression="PrincipalName"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,ObjectType %>" DataField="ObjectType" AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ObjectName %>" DataField="ObjectName"  SortExpression="ObjectName"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Action %>" DataField="ActionId" AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SystemId %>" DataField="ObjectId"  SortExpression="ObjectId"/>
    </Columns>
</qwc:ControlledGrid>
