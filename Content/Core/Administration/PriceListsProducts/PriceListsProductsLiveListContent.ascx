﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceListsProductsLiveListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.PriceListsProducts.PriceListsProductsLiveListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="e2w" TagName="Search" Src="~/Controls/Core/SearchWithFields.ascx" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox ID="HelpBox" runat="server" HelpBoxId="0714d71d-e160-4b43-b7fc-b050d38e2b82" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="ProductCode" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, PriceListItemCode %>" DataField="ItemCode" SortExpression="ProductCode"/>         
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Name %>" DataField="Name" SortExpression="tBM_Product.Name" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="Type" AllowSorting="false" SortExpression="None" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, InvoicePrice %>" DataField="InvoicePrice" SortExpression="NetInvoicePrice" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PriceListCode %>" DataField="PriceListCode" SortExpression="tPriceList.PriceListCode" />
    </Columns>
</qwc:ControlledGrid>
