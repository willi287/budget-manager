﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.PriceListsProducts.Tabs.Main" %>
<%@ Register Src="~/Controls/Core/SetupAvailability.ascx" TagName="SetupAvailability"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/SetupInvoicePrice.ascx" TagName="SetupInvoicePrice" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/SetupDuration.ascx" TagName="SetupDuration" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/SetupMinIncrements.ascx" TagName="SetupMinIncrements"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="e2c" TagName="Calendar" Src="~/Controls/Core/Calendar.ascx" %>
<uc:HelpBox ID="HelpBox" runat="server" HelpBoxId="f0c56b02-1d79-4de7-9e1d-d95cc9a29bd8" />
<table class="form">
    <tbody>
        <tr>
            <td>
                <table class="subForm">
                    <tbody>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblPriceList" runat="server" Text="<%$ Resources:SR, PriceList %>"
                                    AssociatedControlID="lblPriceListValue" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblPriceListValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblProductName" runat="server" Text="<%$ Resources:SR, ProductName %>"
                                    AssociatedControlID="lblProductNameValue" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblProductNameValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR, Type %>" AssociatedControlID="lblTypeValue" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTypeValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel runat="server" ID="lblCustomFieldName" AssociatedControlID="txtCustomFieldName"></qwc:InfoLabel>:
                            </td>
                            <td>
                                <qwc:TextBox runat="server" ID="txtCustomFieldName" MaxLength="255" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <qwc:InfoLabel runat="server" ID="lblStandardCode" Text="<%$ Resources:SR, StandardCode %>"
                                    AssociatedControlID="lblStandardCodeValue"></qwc:InfoLabel>:
                            </td>
                            <td>
                                <qwc:Label ID="lblStandardCodeValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="required caption">
                                <qwc:InfoLabel runat="server" ID="lblCode" Text="<%$ Resources:SR, Code %>" AssociatedControlID="txtCode"></qwc:InfoLabel>:<span>*</span>
                            </td>
                            <td>
                                <qwc:TextBox runat="server" ID="txtCode" MaxLength="50" />
                                <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" EnableClientScript="true" ID="rfvCode" ControlToValidate="txtCode" Display="Dynamic" runat="server" ErrorMessage="*" />
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table class="subForm">
                    <tbody>
                        <asp:Panel runat="server" ID="createAndPublishDatePanel">
                           <asp:Panel runat="server" ID="creationDatePanel">
                                <tr>
                                    <td class="required caption">
                                        <qwc:InfoLabel runat="server" ID="lblCreationDate" Text="<%$ Resources:SR, CreationDate %>"
                                            AssociatedControlID="lblCreationDateValue"></qwc:InfoLabel>:<span>*</span>
                                    </td>
                                    <td>
                                        <qwc:Label runat="server" ID="lblCreationDateValue"></qwc:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel runat="server" ID="lblPublishDate" Text="<%$ Resources:SR, PublishDate %>"
                                            AssociatedControlID="calPublishDate"></qwc:InfoLabel>:<span>*</span>
                                </td>
                                <td>
                                    <e2c:Calendar Required="True" ID="calPublishDate" runat="server" AutoPostBack="True"/>
                                </td>
                           </tr>
                        </asp:Panel>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<uc:SetupDuration ID="ctrlSetupDuration" runat="server" />
<uc:SetupInvoicePrice ID="ctrlSetupInvoicePrice" runat="server" />
<uc:SetupMinIncrements ID="ctrlSetupMinIncrements" runat="server" />
<br />
<uc:SetupAvailability ID="ctrlSetupAvailability" runat="server" />
