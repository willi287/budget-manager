﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceListsProductsPendingListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.PriceListsProducts.PriceListsProductsPendingListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="e2w" TagName="Search" Src="~/Controls/Core/SearchWithFields.ascx" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox ID="HelpBox" runat="server" HelpBoxId="09b5e910-47ad-4c79-ab4b-f752e74eaaba" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="ProductCode" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn AlwaysVisible="true" IsEditable="true" DataField="Selected"/>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, PriceListItemCode %>" DataField="ItemCode" SortExpression="ProductCode"/>         
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Name %>" DataField="Name" SortExpression="Name" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="Type" AllowSorting="false" SortExpression="None" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,InvoicePrice %>" DataField="InvoicePrice" DataFormatString="" SortExpression="NetInvoicePrice" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PriceListCode %>" DataField="PriceListCode" SortExpression="tPriceList.PriceListCode" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ChangeType %>" DataField="PendingPriceListItemChangeType" AllowSorting="false" SortExpression="None" />
         <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,IsApproved %>" DataField="IsApprovedPendingItem" AllowSorting="true" SortExpression="State">
             <ItemCellStyle CssClass="columnCell state" />
         </qwc:BooleanTextColumn>
    </Columns>
</qwc:ControlledGrid>
