﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="BulkApprovePendingPriceListItemContent.ascx" TagName="BulkApprovePendingPriceListItem" TagPrefix="uc" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:BulkApprovePendingPriceListItem ID="Content" runat="server" />
</asp:Content>
