﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.BulkActionPreparationContentBase" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OptionsOfBulkUpdateProductsControl.ascx" TagName="OptionsOfBulkUpdateProducts" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="9f7ea127-8480-4816-9b3f-a33001170b70" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm" style="width: 500px;">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <uc:OptionsOfBulkUpdateProducts ID="ctrlOptionsOfBulkUpdateProducts" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
