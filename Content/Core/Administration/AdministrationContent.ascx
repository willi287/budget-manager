﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdministrationContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.AdministrationContent" %>
<table class="form">
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblSystem" runat="server" Text="<%$ Resources:SR,System %>" />:
                    </td>
                    <td>
                        <% = Controller.GetStringResource(SR_SYSTEM_NAME) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDeploymentVersion" runat="server" Text="<%$ Resources:SR,BuildNo %>" />:
                    </td>
                    <td>
                        <%= Controller.DeploymentVersion %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDeploymentDate" runat="server" Text="<%$ Resources:SR,DeploymentTime %>" />:
                    </td>
                    <td>
                        <%= Controller.DeploymentDate.ToString() %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
