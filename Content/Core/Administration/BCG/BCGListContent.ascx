<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.BCG.BCGListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="../../../../Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f1a7279d-d131-41ae-8a82-f5e039470108" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Administration/BCG/CatalogueGroupItem.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Title %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ConsortiumName %>" DataField="ConsortiumName" SortExpression="Cons.Name" />
    </Columns>
</qwc:ControlledGrid>
