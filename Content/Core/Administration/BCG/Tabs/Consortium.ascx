<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Consortium.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.BCG.Tabs.Consortium" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5d75bb4b-0a52-40a4-95dc-a925924e6a1a" />
<table class="borderedOneRow">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, Consortium%>" AssociatedControlID="ddlConsortiums" />:
        </td>
        <td>
            <qwc:DropDownList ID="ddlConsortiums" DataTextField="Value" DataValueField="Id" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
    </tr>
</table>
