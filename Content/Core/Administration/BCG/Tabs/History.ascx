<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="History.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.BCG.Tabs.History" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f1d03277-7a42-4898-a98f-d416f5a616b1" />
<qwc:ControlledGrid ID="cgHistoryList" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false" SortExpression="None">
    <columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:DateTimeColumn DataField="ActionTime" HeaderTitle="<%$ Resources:SR,DateTime %>" ShowTime="true" SortExpression="None"/>
         <qwc:TextColumn DataField="ActorNameFull"    HeaderTitle="<%$ Resources:SR,User %>" SortExpression="None" />
         <qwc:ResourceColumn DataField="ActionId"   HeaderTitle="<%$ Resources:SR,Changes %>" />         
    </columns>
</qwc:ControlledGrid>
