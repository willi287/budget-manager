<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BcgMain.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.BCG.Tabs.BcgMain" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="46ba4b4a-cd88-42b5-9450-a7033a5412c4" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <%if (!IsWizard)
                  {%>
                <tr runat="server" ID="vwIdRow">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" MaxLength="50" runat="server" Format="{0:D}" />
                        <asp:RegularExpressionValidator ID="rgxpVwIdValidator" runat="server" ControlToValidate="tbVWId"
                            EnableClientScript="true" style="white-space: nowrap" Display="Dynamic" ValidationExpression="\d{0,50}" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" Text="*" />
                    </td>
                    <td colspan="2" />
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,Consortium %>" />:
                    </td>
                    <td>
                        <qwc:Label ID='lblConsortiumValue' runat='server' />
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,Title %>"
                            AssociatedControlID="tbTitle" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTitle" runat="server" MaxLength="50" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="tbTitle"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />
                        <%}%>
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
<qwc:ControlledGrid ID="cgConsortiumOrganization" SortExpression="None" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:CheckBoxColumn DataField="InBCG" UseHeaderCheckBox="true" EnableHighlight="true" EnableCheckRow="true"   />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName %>" DataField="Organisation" SortExpression="None" />
        <qwc:RadioButtonColumn HeaderTitle="<%$ Resources:SR, Buyer %>" DataField="IsBuyer" GroupName="OrganisationId" />
        <qwc:RadioButtonColumn HeaderTitle="<%$ Resources:SR, Supplier %>" DataField="IsSupplier"  GroupName="OrganisationId" />
        <qwc:RadioButtonColumn HeaderTitle="<%$ Resources:SR, Installer %>" DataField="IsBlank"  GroupName="OrganisationId" />
    </Columns>
</qwc:ControlledGrid>
