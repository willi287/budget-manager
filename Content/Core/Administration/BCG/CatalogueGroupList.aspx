﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="BCGListContent.ascx" tagname="BCGListContent" tagprefix="ctrl" %>

<asp:Content ID="ctnBCGList" ContentPlaceHolderID="cphMainContent" runat="server">
   <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
   <ctrl:BCGListContent ID="ContentControl" runat="server" />
</asp:Content>
