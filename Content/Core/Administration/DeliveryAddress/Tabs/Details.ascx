<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.DeliveryAddress.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="b485fbbd-c7c9-4507-a3d4-123b27528d50" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblAddress" AssociatedControlID="tbAddress" runat="server" Text="<%$ Resources:SR,DeliveryAddress%>" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAddress" runat="server" MaxLength="200" TextMode="MultiLine" />
                        <asp:RequiredFieldValidator ID="rfvTemplateTitle" ErrorMessage="*" ControlToValidate="tbAddress"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>