﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientAreaListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.ClientArea.ClientAreaListContent" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="feabf62b-d926-4987-8a74-ef5df7d88d0f" />
<div id="search">
    <div id="searchFilter">
        <table id="searchtable">
            <tr>
                <td>
                <e2w:search id="ucSearch" runat="server" AdvancedEnabled="true" />
                </td>
                <td>
                  <qwc:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true" DataValueField="Id"
                    DataTextField="Value" SortOrder="None" />
                </td>
            </tr>
        </table>
   </div>
</div>
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDeleteClientAreaMsg %>" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="LocalAuthorityName">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, LocalAuthorityName %>" DataField="LocalAuthorityName" SortExpression="LocalAuthorityName"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ClientArea %>" DataField="ClientArea" SortExpression="ClientArea"/>
        <qwc:RadioButtonColumn HeaderTitle="<%$ Resources:SR, Default %>" DataField="IsDefault" AllowSorting="False" 
        SortExpression="None" IsEditable="True" OnCheckedChanged="DefaultClientAreaUpdate" AutoPostBack="True"/>
    </Columns>
</qwc:ControlledGrid>
