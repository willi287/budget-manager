﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register Src="ClientAreaListContent.ascx" TagPrefix="uc" TagName="ClientAreaListContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:ClientAreaListContent ID="clientAreaListContent" runat="server" />
</asp:Content>
