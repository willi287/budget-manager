﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.ClientArea.ClientAreaTabs.Main" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="2cea3217-8940-4b67-aded-9d7adecee30c" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblLocalAuthorityName" runat="server" Text="<%$ Resources:SR, LocalAuthorityName %>"
                            AssociatedControlID="ddlLocalAuthorityName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlLocalAuthorityName" runat="server" DataValueField="ID" DataTextField="Value" SortOrder="None" OnSelectedIndexChanged="ddlOrganisations_SelectedIndexChanged"/>
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblClientAreaName" runat="server" Text="<%$ Resources:SR, ClientArea %>"
                            AssociatedControlID="tClientAreaName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tClientAreaName" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="tClientAreaNameValidator" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="tClientAreaName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblImage" runat="server" Text="<%$ Resources:SR, Image %>" AssociatedControlID="fuImage" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <uc1:FileUpload ID="fuImage" runat="server" FileFilter="Image File |*.jpg" MaxFileLength="1025000"
                            IsSubmitOnUpload="true"  FormatMaxFileLength="<%$ Resources:BM_ValidationSR, MaximumFileSize %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <qwc:Image ID="logoImage" runat="server" AllowPreview="true" Height="100px" Width="144px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
