<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PIOChildListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.PIO.PIOChildListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="2f0340c0-28ce-4b20-b6a3-e06a03f43564" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" AllowSorting="false" >
    <Columns>         
         <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsSelected" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName%>" DataField="Title" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, AddressLine%>" DataField="AddressLine" SortExpression="None"/> 
    </Columns>
</qwc:ControlledGrid>
