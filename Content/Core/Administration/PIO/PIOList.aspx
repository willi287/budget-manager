﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register src="PIOListContent.ascx" tagname="PIOListContent" tagprefix="ctrl" %>

<asp:Content ID="ctnPIOList" ContentPlaceHolderID="cphMainContent" runat="server">
   <ctrl:PIOListContent ID="ContentControl" runat="server" />
</asp:Content>
