﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register src="PIOChildListContent.ascx" tagname="PIOChildListContent" tagprefix="ctrl" %>

<asp:Content ID="ctnPIOChildList" ContentPlaceHolderID="cphMainContent" runat="server">
   <ctrl:PIOChildListContent ID="ContentControl" runat="server" />
</asp:Content>
