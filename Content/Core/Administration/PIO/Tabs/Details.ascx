<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.PIO.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="033f66e2-c9fa-4641-b66e-c7466dedd214" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR,PioType%>" AssociatedControlID="ddlPIOType" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlPIOType" runat="server" AutoPostBack="true" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            DataValueField="Id" OnSelectedIndexChanged="ddlPIOType_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR,PrimaryOrganisation%>"
                            AssociatedControlID="ddlPrimaryOrganisation" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlPrimaryOrganisation" runat="server" AutoPostBack="true"
                            DataTextField="Value" DataValueField="Id" OnSelectedIndexChanged="ddlPrimaryOrganisation_SelectedIndexChanged" />
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
<br />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,ChildOrganisations%>" />
                    </td>
                </tr>
                <%if (!Controller.IsViewMode && IsOrganisationSelected)
                  { %>
                <tr>
                    <td>
                        <asp:ImageButton ID="btnAddChildOrganisations" runat="server" OnClick="btnAddChildOrganisations_Click" />
                    </td>
                </tr>
                <%} %>
            </table>
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgChildOrganisations" runat="server" SortExpression="Name"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName%>" DataField="Title" SortExpression="Name" />
        <%--<qwc:TextColumn HeaderTitle="<%$ Resources:SR, AddressLine%>" DataField="AddressLine" SortExpression="None"/> --%>
    </Columns>
</qwc:ControlledGrid>
