<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PIOListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="8fbf1035-ba8c-407f-b3eb-78bce5c1e5d1" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, PioTitle%>" DataField="PioTitle"  SortExpression="Name" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type%>" DataField="PioTypeId" SortExpression="ObjectTypeId" /> 
    </Columns>
</qwc:ControlledGrid>
