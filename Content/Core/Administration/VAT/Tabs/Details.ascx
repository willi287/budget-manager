<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.VAT.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="3d23e210-3588-41db-9c90-ccaede9d01a3" />
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="vertical-align: top; width: 70%">
            <table class="subForm">
                <tr>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblCode" runat="server" Text="<%$ Resources:SR,Code %>" AssociatedControlID="tbCode" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCode" runat="server" />
                    </td>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblStartDate" runat="server" Text="<%$ Resources:SR,StartDate %>" AssociatedControlID="tbStartDate" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbStartDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblRate" runat="server" Text="<%$ Resources:SR,Rate %>" AssociatedControlID="tbRate" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbRate" runat="server" />
                    </td>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblEndDate" runat="server" Text="<%$ Resources:SR,EndDate %>" AssociatedControlID="tbEndDate" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEndDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblStatus" runat="server" Text="<%$ Resources:SR,Status %>" AssociatedControlID="tbStatus" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbStatus" runat="server" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="bold">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR,Description %>" AssociatedControlID="tbDescription" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
