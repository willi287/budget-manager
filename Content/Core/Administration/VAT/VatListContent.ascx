<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="90fc7af0-e46c-46d3-8622-92fe6a877f63" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="VatCode" ConfirmationMsg="<%$ Resources:SR, MsgConfirmDeleteActionForInfo %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Code %>" DataField="VatCode" SortExpression="VatCode"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Rate %>" DataField="VatPercent" SortExpression="VatPercent"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Status %>" DataField="StatusString" SortExpression="StatusId" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Description" SortExpression="Description"/>
    </Columns>
</qwc:ControlledGrid>
