<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetail.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.ProcessSetup.ApprovalRules.Tabs.UserDetail" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="eb4594db-112e-44bd-951d-a6a6823f1e44" />
<table class="form">
    <tr>
    <td>
        <table class="subForm" style="width:40%">
            <tr>
                <td class="caption">
                    <qwc:RadioButton Checked="True" Text="<%$ Resources:SR,Role %>" ID="rbRole" runat="server" GroupName="groupUserRole" OnCheckedChanged="SelectedTypeChanged" AutoPostBack="True" />
                </td>
                <td>
                    <qwc:DropDownList ID="ddlRole" DataTextField="Value" DataValueField="Id" runat="server" AutoPostBack="true"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                </td>
            </tr>
            <tr>
                <td class="caption">
                    <qwc:RadioButton Text="<%$ Resources:SR,User %>" ID="rbUser" runat="server" GroupName="groupUserRole" OnCheckedChanged="SelectedTypeChanged" AutoPostBack="True" />
                </td>
                <td>                    
                    <qwc:DropDownList ID="ddlUser" DataTextField="Value" DataValueField="Id" runat="server" AutoPostBack="true"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                </td>
            </tr>
        </table>
        <hr />
        <table class="subForm">
            <tr>
                <td class="caption">
                    <qwc:QWCheckBox ID="chbSendEmail" Checked="true" Text="<%$ Resources:SR,SendNotificationMsg %>" runat="server"/>
                </td>
            </tr>
        </table>
        <hr />
        <table class ="subForm">
            <tr>
                <td class="caption">
                    <qwc:radiobutton GroupName="RightRole" id="rbApprover" Checked="true" Text="<%$ Resources:SR,Approver %>" runat="server" AutoPostBack="true"/>
                </td>
            </tr>
            <tr>
                <td class="caption">
                    <qwc:radiobutton GroupName="RightRole" id="rbAppliedTo" Text="<%$ Resources:SR,AppliedTo %>" runat="server" AutoPostBack='true'/>
                </td>
            </tr>
        </table>
    </td>
    </tr>
</table>
