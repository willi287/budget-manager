<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovalRuleDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.ProcessSetup.ApprovalRules.Tabs.ApprovalRuleDetail" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="128b878b-2ef4-4cee-b9d5-7ea71837df64" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbName" Text="<%$ Resources:SR,Name %>" AssociatedControlID="tbRuleName"
                            runat="server" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbRuleName" MaxLength="50" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbOrganisation" Text="<%$ Resources:SR,Organisation %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlOrganisation" DataTextField="Value" DataValueField="Id" CausesValidation="false"
                            OnSelectedIndexChanged="OnDropDownIndexChanged"
                            runat="server" AutoPostBack="true" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label1" Text="<%$ Resources:SR,AppliedTo %>" runat="server" />:
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbDocType" AssociatedControlID="ddlDocType" Text="<%$ Resources:SR,DocumentType %>" runat="server" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDocType" DataTextField="Value" DataValueField="Id" runat="server" CausesValidation="false"
                            OnSelectedIndexChanged="OnDropDownIndexChanged"
                            AutoPostBack="true" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbState" Text="<%$ Resources:SR,State %>" AssociatedControlID="ddlState"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlState" DataTextField="Value" DataValueField="Id" runat="server" CausesValidation="false"
                            OnSelectedIndexChanged="OnDropDownIndexChanged"
                            AutoPostBack="true" SortOrder="None" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbAction" Text="<%$ Resources:SR,Action %>" AssociatedControlID="ddlAction"
                            runat="server" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlAction" DataTextField="Value" DataValueField="Id" runat="server" CausesValidation="false"
                            OnSelectedIndexChanged="OnDropDownIndexChanged"
                            AutoPostBack="true" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
    <tr>
        <td>
        </td>
        <td>
            <table>
                <tr>
                    <td style="padding:0px">
                        <qwc:RadioButton GroupName="Price" ID="rbPriceRange" Checked="true" Text="<%$ Resources:SR,PriceRange %>"
                            runat="server" />
                    </td>
                    <td style="padding:0px">
                        <qwc:RadioButton GroupName="Price" ID="rbPercentRange" Text="<%$ Resources:SR,PercentRange %>"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="caption required">
            <qwc:InfoLabel ID="lbFrom" Text="<%$ Resources:SR,From %>" AssociatedControlID="tbFrom"
                runat="server" />:&nbsp;<span>*</span>
        </td>
        <td>
            <table class="skinny">
                <tr>
                    <td>
                        <qwc:TextBox ID="tbFrom" runat="server" />
                    </td>
                    <td>
                        <qwc:PriceValidator ID="revFrom" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                            ControlToValidate="tbFrom" EnableClientScript="true" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="tbFrom" ErrorMessage="*" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="caption required">
                        <qwc:InfoLabel ID="lbTo" Text="<%$ Resources:SR,To %>" AssociatedControlID="tbTo" runat="server" />:<span>*</span>
        </td>
        <td>
            <table class="skinny">
                <tr>
                    <td>
                        <qwc:TextBox ID="tbTo" runat="server" />
                    </td>
                    <td>
                        <qwc:PriceValidator ID="revTo" ErrorMessage="*" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                            ControlToValidate="tbTo" EnableClientScript="true" runat="server"></qwc:PriceValidator>
                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="tbTo"
                            ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</td>
<td>
    &nbsp;
</td>
    </tr>
</table>
<qwc:ImageActionButton ID="imgActionButton" runat="server" OnCommand="imgActionButton_OnCommand" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AllowEditing="false" SortExpression="None"
    AllowViewing="false" ID="cgApprovers" runat="server" KeyField="Id" OnDeleting="cgApprovers_Deleting1">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,RoleOrUser %>" DataField="RoleOrUser" SortExpression="None"/>
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,AppliedTo %>" DataField="IsAppliedTo"
            TrueValue="<%$ Resources:SR,Yes %>" FalseValue="<%$ Resources:SR,No %>" />
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,Approver %>" DataField="IsApprover"
            TrueValue="<%$ Resources:SR,Yes %>" FalseValue="<%$ Resources:SR,No %>" />
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,SendNotification %>" DataField="IsSendNotification"
            TrueValue="<%$ Resources:SR,Yes %>" FalseValue="<%$ Resources:SR,No %>" />
    </Columns>
</qwc:ControlledGrid>
