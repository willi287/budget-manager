<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register src="~/Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="cbb2e47e-b7d5-4f2b-9092-0424a61811f5" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, RuleInProcedureMsg %>" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name"  ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Administration/ProcessSetup/ApprovalRules/ApprovalRuleItem.aspx" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Rule %>" DataField="Name" SortExpression="Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, DocumentType %>" DataField="DocumentTypeString" AllowSorting="false" SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"  SortExpression="None"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Action %>" DataField="Action" AllowSorting="false"  SortExpression="None"/>
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,PriceRange %>"  DataField="PriceRange" SortExpression="IsPercentRange,LowAmount" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PercentChange %>" DataField="PercentChange" SortExpression="IsPercentRange,LowAmount" />
    </Columns>
</qwc:ControlledGrid>
