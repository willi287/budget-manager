﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ApprovalRuleItemContent.ascx" tagname="ApprovalRuleContent" tagprefix="uc1" %>

<asp:Content ID="ctnRuleItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ApprovalRuleContent ID="RuleItemContent" runat="server" />
</asp:Content>
