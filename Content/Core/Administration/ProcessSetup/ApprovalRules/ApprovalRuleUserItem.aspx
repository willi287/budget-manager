﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="ApprovalRuleUserItem.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.ProcessSetup.ApprovalRules.ApprovalRuleUserItem" %>
<%@ Register src="ApprovalRuleUserItemContent.ascx" tagname="ApprovalRuleUserContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="ctnRuleItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ApprovalRuleUserContent ID="UserItemContent" runat="server" />
</asp:Content>
