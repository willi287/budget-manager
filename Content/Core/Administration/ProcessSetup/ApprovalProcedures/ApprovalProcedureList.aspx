﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" Title="Untitled Page" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ApprovalProcedureListContent.ascx" tagname="ApprovalProcedureListContent" tagprefix="uc1" %>

<asp:Content ID="ctnProcedureList" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ApprovalProcedureListContent ID="ContentControl" runat="server" />
</asp:Content>
