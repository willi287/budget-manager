<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register src="~/Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="2342cb31-b108-4da4-9e26-77752df5cf60" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, ProcedureInContractMsg %>" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Description" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Administration/ProcessSetup/ApprovalProcedures/ApprovalProcedureItem.aspx" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProcedureName %>" DataField="Description" SortExpression="Description" />         
    </Columns>
</qwc:ControlledGrid>

