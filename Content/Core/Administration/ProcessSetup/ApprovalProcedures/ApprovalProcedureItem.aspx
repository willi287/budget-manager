﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="ApprovalProcedureItem.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.ProcessSetup.ApprovalProcedures.ApprovalProcedureItem" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ApprovalProcedureItemContent.ascx" tagname="ApprovalProcedureContent" tagprefix="uc1" %>

<asp:Content ID="ctnProcedureItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ApprovalProcedureContent ID="ProcedureItemContent" runat="server" />
</asp:Content>
