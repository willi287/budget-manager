<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovalProcedureDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.ProcessSetup.ApprovalProcedures.Tabs.ApprovalProcedureDetail" %>
<%@ Register Assembly="Eproc2.Web.Framework" Namespace="Eproc2.Web.Framework.Controls.Grid"
    TagPrefix="cc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="b2e8e97b-0cc3-4304-a624-64490c965d67" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbName" Text="<%$ Resources:SR,Name %>" runat="server" />: <span class="warning">
                            *</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbRuleName" MaxLength="100" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbOrganisation" Text="<%$ Resources:SR,Organisation %>" runat="server" />:
                        <span class="warning">*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlOrganisation" DataTextField="Value" DataValueField="Id"
                            runat="server" AutoPostBack="true" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <span />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <asp:Label ID="Label1" Text="<%$ Resources:SR,IncludeRules %>" runat="server" />:
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgRulesList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" AllowSorting="false">
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsInProcedure" IsEditable="true" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Rule %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,DocumentType %>" DataField="DocumentTypeString" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="State" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Action %>" DataField="Action" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,PriceRange %>" DataField="PriceRange" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PercentChange%>" DataField="PercentChange" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
