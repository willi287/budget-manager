﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false"
    Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase"
  %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="ctrl" TagName="ContentControl" Src="~/Content/Core/Administration/Principal/ConsortiumPrincipalHistoryItemContent.ascx" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ContentControl ID="ContentControl" runat="server" />
</asp:Content>
