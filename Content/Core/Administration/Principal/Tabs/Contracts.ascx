<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contracts.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Principal.Tabs.Contracts" %>


<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e742a445-b93e-4389-b9fd-af79c9851a68" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgContracts"
    runat="server" KeyField="Id" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn  DataField="HasPrincipal" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ContractName %>" DataField="ContractName" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>
