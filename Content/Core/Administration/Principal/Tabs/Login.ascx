<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Principal.Tabs.Login" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="151b1d6a-dfaf-4a8a-b00a-943050abe916" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>

                    <td class="required caption">
                        <qwc:InfoLabel ID="lblPassword" runat="server" Text="<%$ Resources:SR, Password %>" AssociatedControlID="tbPassword" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPassword" runat="server" TextMode="Password" />
                        <asp:RegularExpressionValidator  ID="revPassword" runat="server" Text="*" ControlToValidate="tbPassword" ValidationExpression=".{0,50}" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblConfirmPassword" runat="server" Text="<%$ Resources:SR, ConfirmPassword %>"
                            AssociatedControlID="tbConfirmPassword" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbConfirmPassword" runat="server" TextMode="Password" />
                        <asp:CompareValidator Display="Dynamic" ID="cvConfirmPassword" ControlToValidate="tbPassword"
                            ControlToCompare="tbConfirmPassword" runat="server" Text="*"  ToolTip="<%$ Resources:SR, EnterTheSamePassword %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>&nbsp;
        </td>
    </tr>
</table>
