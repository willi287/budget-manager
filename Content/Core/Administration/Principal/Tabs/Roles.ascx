<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Roles.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Principal.Tabs.Roles" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="22faccee-34b4-4a9b-95c4-f33e0c74e660" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgRoles"
    runat="server" KeyField="Id" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn  DataField="HasUser" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,RoleName %>" DataField="Name" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>
<qwc:QWCheckBox ID="cbBCGSettingsWizard" Visible="false" Checked="true" runat="server" Text="<%$ Resources:SR, NeedBCGSettingsWizard %>" />
