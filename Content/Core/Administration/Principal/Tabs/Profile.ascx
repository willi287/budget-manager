<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Principal.Tabs.Profile" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="9b084045-93a2-4500-a8c4-18b5659c5172" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <%if (Controller.IsFirstLogin())
                  {%>
                <qwc:InfoLabel ID="Label1" runat="server" Text="<%$ Resources:SR, MsgReviewYourDetails %>"
                    Font-Bold="True" />
                <%} %>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvLogin" ControlToValidate="tbEmail" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                        <asp:RegularExpressionValidator ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                            EnableClientScript="true" ControlToValidate="tbEmail" ID="revEmail" runat="server"
                            ErrorMessage="*" ValidationExpression="[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblForename" runat="server" Text="<%$ Resources:SR, Forename %>" AssociatedControlID="tbForename" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbForename" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblSurname" runat="server" Text="<%$ Resources:SR, Surname %>" AssociatedControlID="tbSurname" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSurname" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPosition" runat="server" Text="<%$ Resources:SR, Position %>" AssociatedControlID="tbPosition" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPosition" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblMobile" runat="server" Text="<%$ Resources:SR, Mobile%>" AssociatedControlID="tbMobile" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbMobile" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revMobile" ErrorMessage="*" ControlToValidate="tbMobile"
                            EnableClientScript="true" runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax%>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCulture" runat="server" Text="<%$ Resources:SR, Language %>" AssociatedControlID="ddlCulture" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlCulture" runat="server" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        &nbsp;
                    </td>
                    <td>
                        <qwc:QWCheckBox ID="cbIsDelayedNotification" runat="server" Text="<%$ Resources:SR, EnableDelayedNotification %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        &nbsp;
                    </td>
                    <td>
                        <qwc:QWCheckBox ID="cbDisplayInformationMessages" runat="server" Text="<%$ Resources:SR, DisplayInformationMessages %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <qwc:Image ID="imgLogo" runat="server" PanelID="panelId" />
            <table class="subForm bordered">
                <tr>
                    <td colspan="2">
                        <qwc:QWCheckBox ID="cbIsDisabledUser" runat="server" Text="<%$ Resources:SR, DisableUser %>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
        </td>
    </tr>
</table>
