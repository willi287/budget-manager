﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Wizard.Master" AutoEventWireup="true" CodeBehind="BCGSettingsWizard.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Principal.BCGSettingsWizard" %>

<%@ Register src="BCGSettingsWizardContent.ascx" tagname="BCGSettingsWizardContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="cntSteps" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:BCGSettingsWizardContent ID="ucBCGSettingsWizard" runat="server" />
</asp:Content>

