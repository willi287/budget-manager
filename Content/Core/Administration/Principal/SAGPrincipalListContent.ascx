<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="7644bc1b-cbb5-416e-a97e-ddb4d6388611" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <%--qwc:SelectColumn /--%>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserId %>" DataField="Login" SortExpression="Login"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserName %>" DataField="Name" SortExpression="Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="RoleName" SortExpression="RoleName"/>
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,AccountStatus %>" DataField="IsDisabled"
            FalseValue="<%$ Resources:SR,Active %>" TrueValue="<%$ Resources:SR,Disabled %>" />
    </Columns>
</qwc:ControlledGrid>
