﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="SAGPrincipalListContent.ascx" tagname="ListContent" tagprefix="uc" %>
<asp:Content ID="ctnProvider" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:ListContent ID="ContentControl" runat="server" />
</asp:Content>
