<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Templates.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="043e9f03-02f2-4b08-8e14-c6fcb68bd09f" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblTemplateName" AssociatedControlID="tbTemplateName" runat="server"
                            Text="<%$ Resources:SR,TemplateName%>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTemplateName" runat="server" MaxLength="150" />
                        <asp:RequiredFieldValidator ID="rfvTemplateName" ErrorMessage="*" ControlToValidate="tbTemplateName"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblTemplateType" runat="server" AssociatedControlID="ddlTemplateType"
                            Text="<%$ Resources:SR,TemplateType%>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlTemplateType" DataValueField="Id" Enabled="false" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTemplateText" runat="server" Text="<%$ Resources:SR,TemplateText%>" />:
                    </td>
                    <td>
                        <qwc:TextArea ID="tbTemplateText" TextMode="MultiLine" Rows="7" runat="server" MaxLength="500" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
