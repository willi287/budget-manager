<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="2ee78601-984d-4241-b1d2-a7c5fb4ea70b" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR,NTDeleteMsg %>"   runat="server"  />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="TemplateTitle" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>        
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,User %>" DataField="PrincipalName" SortExpression="PrincBase.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,TemplateName %>" DataField="TemplateTitle" SortExpression="TemplateTitle"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" DataField="NTType" />
    </Columns>
</qwc:ControlledGrid>
