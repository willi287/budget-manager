<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Outcome.Tabs.Details" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Import Namespace="Eproc2.Core.Entities.ListItems" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="dc253d1d-f16c-40d0-a42b-a9359341e595" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblAddress" AssociatedControlID="tbName" runat="server" Text="<%$ Resources:SR,OutcomeName%>" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" MaxLength="255" TextMode="SingleLine" />
                        <asp:RequiredFieldValidator ID="rfvTemplateTitle" ErrorMessage="*" ControlToValidate="tbName"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />
                    </td>
                </tr>
                <% if (Controller.IsVisibleControl(OutcomeListItem.PROP_INDIVIDUAL))
                   {%>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblCreatedBy" AssociatedControlID="tbName" runat="server" Text="<%$ Resources:SR,CreatedBy%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblCreatedByValue" runat="server" />
                    </td>
                </tr>
                <%} %>
            </table>
        </td>
        <td>&nbsp;
        </td>
    </tr>
</table>
