<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.HelpBoxes.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/HelpBoxContent.ascx" TagName="HelpBoxContent" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<uc:HelpBox runat="server" HelpBoxId="78d6da2f-036c-4065-827f-00b89d925cad" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblBusinessView" Text="<%$ Resources:SR,AppliedTo%>" runat="server" />:
                        <span class="warning">*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBusinessView" DataTextField="Value" DataValueField="Id" 
                            runat="server" AutoPostBack="true" SortOrder="None" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" 
                            OnSelectedIndexChanged="OnRoleChanged"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInformationHeading" runat="server" Text="<%$ Resources:SR,InformationHeading%>"
                            AssociatedControlID="tbInformationHeading" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbInformationHeading" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInformationContent" runat="server" Text="<%$ Resources:SR,InformationContent%>"
                            AssociatedControlID="tbInformationContent" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbInformationContent" runat="server" MaxLength="800" TextMode="MultiLine"
                            Height="100px" />
                    </td>
                </tr>
                <tr>                
                    <td colspan="2" class="grey">
                        <uc1:FileUpload SelectFileText="<%$ Resources:SR,UploadImage%>" MaxFileLength="4194304"
                            ID="fileUpload" UploadedFileName="" FileFilter="Graphic(*.gif; *.jpg; *.eps; *.bmp, *.png)|*.gif; *.jpg; *.jpeg; *.eps; *.bmp; *.png"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblLocation" runat="server" Text="<%$ Resources:SR,Location%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblLocationContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblStatus" runat="server" Text="<%$ Resources:SR,Status%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblStatusContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td colspan="2">
            <asp:Button ID="btPreview" Text="<%$ Resources:SR,Preview %>" runat="server" OnClick="Preview" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <uc:HelpBoxContent ID="helpBoxContent" runat="server" Visible="false" />
        </td>
    </tr>
</table>
