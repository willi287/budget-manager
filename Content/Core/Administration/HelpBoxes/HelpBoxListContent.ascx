<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="d762c4db-7829-4dd8-88ab-def04f752da1" />    
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Location" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InformationHeading %>" DataField="InformationHeading" SortExpression="tHelpBoxData.InformationHeading" />
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="Status" 
            FalseValue="<%$ Resources:SR,Disabled %>"
            TrueValue="<%$ Resources:SR,Enabled %>"
            AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Location %>" DataField="Location" SortExpression="Location"/>
    </Columns>
</qwc:ControlledGrid>
