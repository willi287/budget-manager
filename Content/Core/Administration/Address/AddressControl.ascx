<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressControl.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Address.AddressControl" %>
<%@ Register Src="../../../../Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="uc1" %>
<table width="100%">
    <tr>
        <td>
            <%if (!Controller.IsViewMode && Enabled && Data.UdprnServiceUnavailable)
              { %>
            <table class="form" border="0">
                <tr>
                    <td class="red">
                        <%= Resources.SR.Msg_UdprnServiceNotAvailable%>
                    </td>
                </tr>
            </table>
            <%} %>
            <%if (!Controller.IsViewMode && Enabled)
              { %>
<%--            <table class="subForm" border="0">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="uprnLabel" runat="server" Text="<%$ Resources:Cons_SR, UPRN %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="uprnText" MaxLength="35" runat="server"></qwc:TextBox>
                    </td>
                </tr>
            </table>--%>
            <table class="subForm grey" border="0">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblFlatNo" runat="server" Text="<%$ Resources:SR, FlatNo %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtFlatNo" MaxLength="50" Format="{0}" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblHouseName" runat="server" Text="<%$ Resources:SR, HouseName %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtHouseName" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblHouseNumber" runat="server" Text="<%$ Resources:SR, HouseNumber %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                       <qwc:TextBox ID="txtHouseNumber" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
            </table>
            <table class="subForm" border="0">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblPostCode" runat="server" Text="<%$ Resources:SR, Postcode %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtPostCode" CssClass="short" MaxLength="10" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblStreetLabel" runat="server" Text="<%$ Resources:SR, Street %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtStreet" MaxLength="255" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTownLabel" runat="server" Text="<%$ Resources:SR, Town %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtTown" MaxLength="70" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCountyLabel" runat="server" Text="<%$ Resources:SR, County %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtCounty" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnFind" runat="server" Text="<%$ Resources:SR, FindAddress %>" OnClick="btnFind_Click" />
                        <asp:Button ID="btnRegisterCustomAddress" runat="server" Text="<%$ Resources:SR, RegisterCustomAddress %>"
                            OnClick="btnRegisterCustomAddress_Click" />
                        <uc1:PopupControl ID="pcMessage" runat="server" />
                    </td>
                </tr>
            </table>
            <%} %>
            <%if (EnableSelectedPanel)
              { %>
            <table class="grey borderedAddress" width="400">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblAddressLine" runat="server" Text="<%$ Resources:SR, AddressLine %>"></qwc:InfoLabel>:
                    </td>
                    <td colspan="2">
                        <qwc:Label ID="qlblAddressLineValue" runat="server"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblTown" runat="server" Text="<%$ Resources:SR, Town %>"></qwc:InfoLabel>:
                    </td>
                    <td colspan="2">
                        <qwc:Label ID="qlblTownValue" runat="server"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblCounty" runat="server" Text="<%$ Resources:SR, County %>"></qwc:InfoLabel>:
                    </td>
                    <td colspan="2">
                        <qwc:Label ID="qlblCountyValue" runat="server"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblPostCodeSelected" runat="server" Text="<%$ Resources:SR, Postcode %>"></qwc:InfoLabel>:
                    </td>
                    <td colspan="2">
                        <qwc:Label ID="qlblPostCodeValue" runat="server"></qwc:Label>
                    </td>
                </tr>
            </table>
            <%} %>
            <%if (!Controller.IsViewMode && Enabled)
              { %>
            <qwc:Label ID="qlblSelectAddress" CssClass="bold" Text="<%$ Resources:SR, SelectAddressFromList %>"
                runat="server"></qwc:Label>
            <br />
            <%} %>
        </td>
        <td style="vertical-align: top">
            <%if (!Controller.IsViewMode && Enabled)
              { %>
            <table class="hlpBx">
                <tr>
                    <td style="white-space: nowrap">
                        <asp:Image ID="imgArrow" runat="server" />                                            
                        <p style="margin-left: 140px; margin-top: -100px;">
                            <span class="hlpBxHeadder"><%= Resources.SR.AddressSearchHelp%> </span>
                            <br />
                            <span class="hlpBxContent"><%= Resources.SR.SearchRequirements %>
                                <br />
                                <span style="font-weight: bold"><%= Resources.SR.EnterUtilityInfo %>
                                    <br />
                                    - <%= Resources.SR.eitherPostcode %> </span>
                                <br />
                                <%= Resources.SR.Or.ToLower() %>
                                <br />
                                <span style="font-weight: bold">- <%= Resources.SR.StreetAndTown%></span> </span>
                        </p>
                    </td>
                </tr>
            </table>
            <%} %>
        </td>
    </tr>
    <%if (!Controller.IsViewMode && Enabled)
      { %>
    <tr>
        <td colspan="2" class="subAddress">
            <qwc:ImageActionButton ID="btnAddSelected" runat="server" Text="<%$ Resources:SR, AddSelected %>"
                OnCommand="btnAddSelected_Click" />
            <qwc:ControlledGrid ShowHeaderIfNoData="false" ID="cgAddress" runat="server" AllowSelecting="true"
                KeyField="Id" AllowEditing="false" AllowViewing="false" SortExpression="None" AllowDeleting="false">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:CheckBoxColumn DataField="IsSelected" UseHeaderCheckBox="true" />
                    <qwc:TextColumn DataField="Flat" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, FlatNo %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="HouseName" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseName %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="HouseNumber" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseNumber %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="Street" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Street %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="City" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Town %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="County" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, County %>">
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="Postcode" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Postcode %>">
                    </qwc:TextColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <%} %>
</table>
