<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" CodeBehind="AddressListContent.ascx.cs" AutoEventWireup="true"
    Inherits="Eproc2.Web.Content.Core.Administration.Address.AddressListContent" %>
    <%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="046f73b4-2109-45ce-b1c5-ee10fd3d8d4a" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:QWCheckBox ID="qwcShowMyAddresses" runat="server" AutoPostBack=true 
    oncheckedchanged="qwcShowMyAddresses_CheckedChanged" Text="<%$ Resources:SR, ShowMyAddresses %>" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" SortExpression="Code" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Code %>" DataField="Code" SortExpression="Code"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FlatNo %>" DataField="Flat"  SortExpression="Flat"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,HouseName %>" DataField="HouseName"  SortExpression="HouseName"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,HouseNo %>" DataField="HouseNumber"  SortExpression="HouseNumber"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Street %>" DataField="Street"  SortExpression="Street"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Town %>" DataField="City"  SortExpression="City"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,County %>" DataField="County"  SortExpression="County"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Postcode %>" DataField="Postcode"  SortExpression="Postcode"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UDPRN %>" DataField="UDPRN"  SortExpression="UDPRN"/>        
    </Columns>
</qwc:ControlledGrid>


