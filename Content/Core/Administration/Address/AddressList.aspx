﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="AddressList.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register TagPrefix="ctl" TagName="AddressList" Src="~/Content/Core/Administration/Address/AddressListContent.ascx" %>
<asp:Content ID="cntAddress" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:AddressList ID="AddressListControl" runat="server" />
</asp:Content>

