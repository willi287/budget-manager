﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.Address.CreateSingle" %>
<%@ Register src="CreateSingleContent.ascx" tagname="CreateSingleContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:CreateSingleContent ID="ContentControl" runat="server" />    
</asp:Content>
