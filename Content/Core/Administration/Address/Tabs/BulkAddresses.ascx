<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulkAddresses.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Address.Tabs.BulkAddresses" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../../Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="d3de6daa-9fec-4455-aeb1-34adf0df4d95" />
<uc2:PopupControl ID="pcMessage" runat="server" />
<table class="form" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table class="subForm grey" style="width: 70%">
                <tr>
                    <td>
                        <uc1:FileUpload SelectFileText="<%$ Resources:SR,FileToUpload%>" MaxFileLength="4194304"
                            ID="fileUpload" runat="server" FileFilter="Excel (*.csv)|*.csv"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <table class="bordered">
                <tr>
                    <td class="caption" style="padding-left: 15px; white-space: nowrap;">
                        <qwc:Label ID="qlblAddressListFromTheFile" Text="<%$ Resources:SR, AddressListFromTheFile %>"
                            runat="server"></qwc:Label>:
                    </td>
                    <td class="formData">
                        <qwc:QWCheckBox ID="chShowUnmatched" AutoPostBack="true" Style="vertical-align: top;"
                            runat="server" Text="<%$ Resources:SR, ShowUnmatched %>" OnCheckedChanged="chShowUnmatched_CheckedChanged" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnFind" runat="server" Text="<%$ Resources:SR, Find %>" OnClick="btnFind_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Label ID="qlblSelectAddress" Text="<%$ Resources:SR, SelectAddressFromList %>"
                runat="server"></qwc:Label>
        </td>
    </tr>
</table>
            <div class="subAddress" width="100%">
                <qwc:ControlledGrid ID="cgAddress" runat="server" AllowViewing="false" AllowEditing="true" SortExpression="None"
                    AllowDeleting="true" AllowSelecting="false" KeyField="Id" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>">
                    <Columns>
                        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR, Action %>" />
                        <qwc:TextColumn DataField="Flat" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, FlatNo %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="HouseName" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseName %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="HouseNumber" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseNumber %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="Street" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Street %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="City" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Town %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="County" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, County %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="Postcode" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, Postcode %>">
                        </qwc:TextColumn>
                        <qwc:TextColumn DataField="UDPRN" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:SR, UDPRN %>">
                        </qwc:TextColumn>
                        <qwc:BooleanTextColumn DataField="IsMatched" ColumnType="String" HeaderTitle="<%$ Resources:SR, Status %>"
                            TrueValue="<%$ Resources:SR, Matched %>" FalseValue="<%$ Resources:SR, Unmatched %>">
                        </qwc:BooleanTextColumn>
                    </Columns>
                </qwc:ControlledGrid>
            </div>

