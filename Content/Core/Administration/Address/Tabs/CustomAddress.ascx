<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomAddress.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Address.Tabs.CustomAddress" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e6892a4f-69e6-404d-89d4-d8fc21188987" />
<table class="form">
    <tr>
        <td>
            <br />
            <table class="bordered" width="300" style="height: 50px;" width="300px">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR, Code %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:Label ID="qlblCodeValue" runat="server"></qwc:Label>
                    </td>
                </tr>
            </table>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR, FlatNo %>"></qwc:InfoLabel>:
                    </td>
                    <td class="formData">
                        <qwc:TextBox ID="txtFlatNo" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR, HouseName %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtHouseName" MaxLength="50" runat="server"></qwc:TextBox>
                        
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:SR, HouseNumber %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                       <qwc:TextBox ID="txtHouseNumber" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblStreet" runat="server" Text="<%$ Resources:SR, Street %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtStreet" MaxLength="255" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblCity" runat="server" Text="<%$ Resources:SR, City %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtCity" MaxLength="70" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblCounty" runat="server" Text="<%$ Resources:SR, County %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtCounty" MaxLength="50" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblPostCode" runat="server" Text="<%$ Resources:SR, Postcode %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtPostCode" MaxLength="10" CssClass="short" runat="server"></qwc:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <% if (Controller.IsVisibleControl(AddressEntity.PROP_UDPRN) && !string.IsNullOrEmpty(Data.UDPRN))
                   {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblUDPRN" runat="server" Text="<%$ Resources:SR, UDPRN %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:Label ID="txtUdprn" runat="server"></qwc:Label>
                    </td>
                </tr>
                <%
                    }%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblCustomCode" runat="server" Text="<%$ Resources:SR, CustomCode %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtCustomCode" MaxLength="15" runat="server"></qwc:TextBox>
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
