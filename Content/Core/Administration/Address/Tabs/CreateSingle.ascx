<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateSingle.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Address.Tabs.CreateSingle" %>
<%@ Register src="../AddressControl.ascx" tagname="AddressControl" tagprefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="a4a9de09-8291-4e9a-8cc1-db3bf679901b" />
<uc1:AddressControl ID="AddressControl1" LocalSearchEnabled="true" RegisterCustomAddressButtonEnabled="false" runat="server" />
