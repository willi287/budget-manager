﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailsTab.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.PbaServiceProvider.Tabs.DetailsTab" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/AutomationGroupingDates.ascx" TagName="AutomationGroupingDates"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/OrderAutomationSettings.ascx" TagName="OrderAutomationSettings"
    TagPrefix="e2o" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="fd04785e-ba26-455f-af6a-113c752aa217" />
<table class="form">
    <tbody>
        <tr>
            <td>
                <qwc:Label ID="lblDefaultOrderAutomationSettings" runat="server" CssClass="bold" Text="<%$ Resources:SR, DefaultOrderAutomationSettings %>" />:
            </td>
        </tr>
        <tr>
            <td>
                <e2o:OrderAutomationSettings ID="ctrlOrderAutomationSettings" runat="server" />
                <uc:AutomationGroupingDates  ID="ctrlAutomationGroupingDates" runat="server" />
            </td>
            <td valign="top">
                <table class="subForm">
                    <tbody>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>