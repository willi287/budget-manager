﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register TagPrefix="ctl" TagName="PbaServiceProviderList" Src="~/Content/Core/Administration/PbaServiceProvider/PbaServiceProviderListContent.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:PbaServiceProviderList ID="ContentControl" runat="server" />
</asp:Content>

