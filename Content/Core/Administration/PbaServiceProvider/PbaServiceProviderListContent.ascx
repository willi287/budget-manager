﻿<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox" runat="server" HelpBoxId="b63c00a7-498e-41ea-b554-bdd7133a5c9c" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="true" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<br />
<qwc:ControlledGrid ID="cgList" SortExpression="PrincBase.Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,IndividualName %>" DataField="IndividualName"  SortExpression="PrincBase.Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,LocalAuthorityName %>" DataField="LocalAuthorityName"  SortExpression="tOrgBase.Name"/>
    </Columns>
</qwc:ControlledGrid>
