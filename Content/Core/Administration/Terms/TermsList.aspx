﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<%@ Register src="TermsListContent.ascx" tagname="TermsListContent" tagprefix="uc1" %>

<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:TermsListContent ID="ContentControl" runat="server" />
</asp:Content>
