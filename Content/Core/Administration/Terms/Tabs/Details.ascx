<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.Terms.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="1f4863d1-75c7-4a33-a67b-d59f72585760" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFullDescription" runat="server" Text="<%$ Resources:SR,TermsOfUse%>"
                            AssociatedControlID="tbFullDescription" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFullDescription" runat="server" MaxLength="50000" TextMode="MultiLine"
                            Height="100px" Width="80%" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
