<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="f8e76774-be01-4d03-afe7-56ee61c179d8" />    
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Caption" ConfirmationMsg="<%$ Resources:SR, MsgConfirmDeleteActionForInfo %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Caption" SortExpression="Caption"/>
    </Columns>
</qwc:ControlledGrid>
