<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="3da886ca-ef33-4edc-ac9d-cd66404ba694" />

<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="CreationTime" runat="server"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Headline %>" DataField="Header" SortExpression="Header" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,StartDate %>" DataField="StartDate" ShowTime="false" SortExpression="StartDate" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,EndDate %>" DataField="EndDate" ShowTime="false" SortExpression="EndDate"/>
    </Columns>
</qwc:ControlledGrid>
