<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.News.News" %>
<%@ Register Src="~/Content/Core/Administration/News/NewsTemplateControl.ascx" TagName="NewsTemplateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <qwc:NewsRepeater runat="server" ID="nrNews" AllowPaging="true">
            <HeaderTemplate>
                <uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="ab6331c1-a9d2-4746-a891-ed63979ee2a3" />
            </HeaderTemplate>
            <ItemTemplate>
                <uc1:NewsTemplateControl ID="NewsItemControl1" runat="server" />
            </ItemTemplate>
        </qwc:NewsRepeater>
    </ContentTemplate>
</asp:UpdatePanel>