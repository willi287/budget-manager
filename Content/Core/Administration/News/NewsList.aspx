﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" MasterPageFile="~/Masters/Main.Master"
    Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>

<%@ Register TagPrefix="ctl" TagName="NewsListContent" Src="~/Content/Core/Administration/News/NewsListContent.ascx" %>
    <asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:NewsListContent ID="ContentControl" runat="server" />
    </asp:Content>
