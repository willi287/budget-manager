<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsItemDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.News.Tabs.NewsItemDetail" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ab7c00e0-f750-4ecf-bb14-845096235a80" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbArticleHeadline" runat="server" Text="<%$ Resources:SR,ArticleHeadline%>" />:&nbsp;<span class="warning">*</span>
                    </td>
                    <td class="formData">
                        <qwc:TextBox ID="tbArticleHeadline" runat="server" MaxLength="200" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbCreationDate" runat="server" Text="<%$ Resources:SR,CreationDate%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCreationDate" runat="server" MaxLength="200" Width="210px" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbArticleType" runat="server" Text="<%$ Resources:SR,ArticleType%>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlArticleType" runat="server" DataTextField="Value" DataValueField="Id"
                            Height="22px" Width="218px" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbArticleDetail" runat="server" Text="<%$ Resources:SR,ArticleDetails%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbArticleDetail" runat="server" MaxLength="2000" TextMode="MultiLine"
                            Height="100px" Width="80%" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbStartDate" runat="server" Text="<%$ Resources:SR,StartDate%>" />:
                        <span class="warning">*</span>
                    </td>
                    <td class="formData">
                        <e2c:Calendar ID="calStartDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbEndDate" runat="server" Text="<%$ Resources:SR,EndDate%>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calEndDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="grey">
                        <uc1:FileUpload SelectFileText="<%$ Resources:SR,UploadPDF%>" MaxFileLength="4194304"
                            ID="fileUpload" UploadedFileName="" FileFilter="PDF (*.pdf)|*.pdf" runat="server" />
                        <qwc:Label ID="lblFileName" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
