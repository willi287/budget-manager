﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsTemplateControl.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.News.NewsTemplateControl" %>
<div class="newsitem">
    <div>
        <qwc:Label runat="server" ID="lblDate" />
    </div>
    <div class="header">
        <qwc:Label runat="server" ID="lblHeader" />
    </div>
    <div class="detail">
        <qwc:Label runat="server" ID="lblDetail" />
        <qwc:Label runat="server" ID="lblMoreDetail" Style="display: none" />
    </div>
    <div>
        [
        <asp:HyperLink runat="server" ID="hlReadMore">
        <asp:Literal runat="server" Text="<%$ Resources:SR,ReadMore %>" />
        </asp:HyperLink><asp:HyperLink runat="server" ID="hlMinimize" Style="display: none">
        <asp:Literal runat="server" Text="<%$ Resources:SR,MinimizeNews %>" />
        </asp:HyperLink>
        ] <span runat="server" id="viewPDFSpan">[
            <asp:HyperLink ID="hlViewPDF" NavigateUrl="" ImageUrl="" runat="server">
                <asp:Image ID="imgViewPDF" Visible="false" runat="server" />
                <asp:Literal runat="server" Text="<%$ Resources:SR,ViewPDF %>" />
            </asp:HyperLink>
            ] </span>
    </div>
    <br />
    <hr />
    <br />
</div>
