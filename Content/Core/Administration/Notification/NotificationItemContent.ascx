﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationItemContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Notification.NotificationItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTitle" runat="server" Text="<%$ Resources:SR, NotificationTitle %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTitleContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, EmailContent %>" />:
                    </td>
                    <td>
                        <qwc:Label ReadOnly="true" ID="lblEmailContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
