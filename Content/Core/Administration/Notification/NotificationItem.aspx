﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" EnableEventValidation="false" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="ctrl" TagName="ViewEdit" Src="~/Content/Core/Administration/Notification/NotificationItemContent.ascx" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ViewEdit ID="ContentControl" runat="server" />
</asp:Content>
