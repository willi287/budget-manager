﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumnetsListPageBase" Title="Untitled Page" %>
<%@ Register TagPrefix="ctl" TagName="DNList" Src="~/Content/Core/Process/DN/DNListContent.ascx" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
        <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
        <ctl:DNList ID="ContentControl" runat="server"></ctl:DNList>
</asp:Content>
