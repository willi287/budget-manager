﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.DN.Tabs.PurchaseItems" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="9c1f60c1-1739-415c-aab0-bc1a9d6e1a9b" />
<asp:PlaceHolder ID="phRecalculationWarning" runat="server" Visible="False" EnableViewState="False">
    <asp:Label ID="lblRecalculationWarningMsg" runat="server" CssClass="error"/>
</asp:PlaceHolder>
<table class="form">
    <tr>
        <td align="left" colspan="2">
            <qwc:ActionButtonRepeater ID="ActionsRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" OnDeleting="cgPurchaseItems_OnDeleting">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsSelectedForDeliveryNote" EnableHighlight="true"/>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn DataField="ProductCode" HeaderTitle="<%$ Resources:SR,ProductCode %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="SerialNumber" HeaderTitle="<%$ Resources:SR,SerialNumber %>" SortExpression="None" ColumnType="String" OnDataBound="cgPurchaseItemsSerialNumber_OnDataBound">
                        <ItemCellStyle CssClass="short" />
                        <Validators>
                            <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:SR,MsgSerialNumberRequired %>" />
                        </Validators>
                    </qwc:TextColumn>
                    <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:SR,ProductName %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="UnitIdObjectCode" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="QuantityToUse" HeaderTitle="<%$ Resources:SR,ToBeDelivered %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Quantity" HeaderTitle="<%$ Resources:SR,Shipped %>" SortExpression="None">
                        <ItemCellStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="Accepted" HeaderTitle="<%$ Resources:SR,Accepted %>" SortExpression="None">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="Damaged" HeaderTitle="<%$ Resources:SR,Damaged %>" SortExpression="None">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="NotNeeded" HeaderTitle="<%$ Resources:SR,NotNeeded %>" SortExpression="None">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="NotDeliveredCalculatedVisibleValue" HeaderTitle="<%$ Resources:SR,NotDelivered %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Completed" HeaderTitle="<%$ Resources:SR,Completed %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="InvoicePrice" HeaderTitle="<%$ Resources:SR,Price %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="TotalNet" HeaderTitle="<%$ Resources:SR,NetTotal %>" SortExpression="None"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <asp:Button ID="btnRecalculate" runat="server" Text="<%$ Resources:SR,RefreshQuantities %>"
                OnClick="btnRecalculate_Click" Visible="False"/>
            <asp:Button ID="btnAcceptAll" runat="server" Text="<%$ Resources:SR,AcceptAll %>"
                OnClick="btnAcceptAll_Click" Visible="False"/>

        </td>
    </tr>
</table>
