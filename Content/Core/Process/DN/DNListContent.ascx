﻿<%@ Import Namespace="Eproc2.Web.Framework.Helpers"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DNListContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.DN.DNListContent" %>
<%@ Register src="~/Controls/Core/FreeSearch.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="349b3e9e-be29-4be6-b2d0-b5da0753a6f3" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<uc:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR,MsgDNHavePIWithRequiredSN %>"/>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<!-- Sort by DN number(because invoice number are set after data request), need fix in future when will be next version of NHibernate -->
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="SubmitDate"  SortDirection="False" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn IsEditable="true"/>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"        NavigateUrl="~/Content/Core/Process/DN/DNItem.aspx"   />
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DateSubmitted %>"    DataField="SubmitDate" SortExpression="SubmitDate" ShowTime="False" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,DNNo %>"             DataField="DocumentNo" SortExpression="VisibleDocumentNo" />         
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PONumber %>"             DataField="PoNo" SortExpression="PurchOrder.VisibleDocumentNo"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InvoiceNumber %>"             DataField="InvoiceNo" SortExpression="VisibleDocumentNo"/> 
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DeliveryDate %>"    DataField="DeliveryDate" SortExpression="ActualDeliveryDate" ShowTime="False" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerName %>"        DataField="BuyerName" SortExpression="buyer.Name"/> 
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>"             DataField="SupplierName"  SortExpression="supplier.Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InstallerName %>"             DataField="InstallerName" SortExpression="installer.Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PropertyAddress %>"           DataField="Address" SortExpression="aAddrPost" />
    </Columns>
</qwc:ControlledGrid>

