﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true"
    CodeBehind="InformationRequestList.aspx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.IR.InformationRequestList" %>

<%@ Register TagPrefix="ctl" TagName="IRList" Src="~/Content/Core/Process/IR/InformationRequestListContent.ascx" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctl:IRList ID="ContentControl" runat="server"></ctl:IRList>
</asp:Content>
