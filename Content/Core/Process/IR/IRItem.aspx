﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false"
    Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase"  %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="ctrl" TagName="ViewEdit" Src="~/Content/Core/Process/IR/IRItemContent.ascx" %>

<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ViewEdit ID="ContentControl" runat="server" />
</asp:Content>