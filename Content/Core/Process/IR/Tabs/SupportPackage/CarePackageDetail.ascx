﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarePackageDetail.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.IR.Tabs.SupportPackage.CarePackageDetail" %>
<%@ Import Namespace="Eproc2.Core.Common" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Process.IR" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="0f4fd2b5-ea81-45c9-8fb2-d8be1d9fcb2f" />


<div>
    <table class="subForm grey">
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="packageName" Text="<%$ Resources:SR,OutcomeOrPackageName %>" runat="server" />:
            </td>
            <td colspan="2">
                <qwc:Label ID="lblPackageName" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="startDate" Text="<%$ Resources:SR,WhenItShouldStart %>" runat="server" />
            </td>
            <td colspan="2">
                <qwc:Label ID="lblStartDate" runat="server"/>
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="replyBy" Text="<%$ Resources:SR,ReplyBy %>" runat="server" />:
            </td>
            <td style="width: 50px">
                <qwc:Label ID="lblReplyBy" runat="server" />
            </td>
            <td>
                <qwc:Label ID="lblReplyDaysRemaining" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="budget" Text="<%$ Resources:SR,Budget %>" runat="server" />:
            </td>
            <td colspan="2">
                <qwc:Label ID="lblBudget" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="caption" colspan="3">
                <qwc:InfoLabel ID="whatIWantSummary" Text="<%$ Resources:SR,BriefSummaryOfWhatIWant %>" runat="server" />:
            </td>
        </tr>
        <tr>
            <td style="height: auto" colspan="3">
                <qwc:TextBox ID="txtWhatIWantSummary" TextMode="MultiLine" Height="50" runat="server" ReadOnly="True"/>
            </td>
        </tr>
        <tr>
            <td class="caption" colspan="3">
                <qwc:InfoLabel ID="aboutMe" Text="<%$ Resources:SR,AboutMe %>" runat="server" />:
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: auto">
                <qwc:TextBox ID="txtAboutMe" TextMode="MultiLine" Height="50" runat="server" ReadOnly="True" />
            </td>
        </tr>
        <tr>
            <td class="caption" colspan="3">
                <qwc:InfoLabel ID="detailsOfWhatIWant" Text="<%$ Resources:SR,DetailsOfWhatIWant %>" runat="server" />:
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: auto">
                <qwc:TextBox ID="txtdetailsOfWhatIWant" TextMode="MultiLine" Height="50" runat="server" ReadOnly="True" />
            </td>
        </tr>
        <tr>
            <td class="caption" colspan="3">
                <qwc:InfoLabel ID="thingsToAware" Text="<%$ Resources:SR,ThingsToAware %>" runat="server" />:
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: auto">
                <qwc:TextBox ID="txtthingsToAware" TextMode="MultiLine" Height="50" runat="server" ReadOnly="True" />
            </td>
        </tr>
    </table>
</div>
