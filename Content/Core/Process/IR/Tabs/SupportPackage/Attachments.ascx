﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attachments.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.IR.Tabs.SupportPackage.Attachments"%>
    
    
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="9c2655b9-1d79-46fc-83fe-5044d55bd307" />

<qwc:ControlledGrid ID="cgAttachmentList" runat="server" KeyField="Id" AllowViewing="false" SortExpression="None" >
    <columns>
         <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,FileName %>"/>
         <qwc:ClientClickLinkColumn DataField="Url" HeaderTitle="<%$ Resources:SR,Download %>" Caption="<%$ Resources:SR,Download %>" ImgUrl="ImgUrl"/>         
    </columns>
</qwc:ControlledGrid>
