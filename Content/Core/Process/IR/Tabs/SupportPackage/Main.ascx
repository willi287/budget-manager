﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.IR.Tabs.SupportPackage.Main" %>
<%@ Import Namespace="Eproc2.Core.Common" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Process.IR" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="0cf17a4e-a5e5-4a35-b103-a5da2b08c4b1" />
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>

<table cellspacing="5px" class="form">
    <tr>
        <td>
            <table class="subForm grey">
                <%--                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="individualNameLabel" Text="<%$ Resources:SR,IndividualsName %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblIndividualsName" runat="server" />
                    </td>
                </tr>--%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="buyerOrganisationLabel" Text="<%$ Resources:SR,BuyerOrganisation %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyerOrganisation" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="referenceNumberLabel" Text="<%$ Resources:SR,ReferenceNumber %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblReferenceNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="documentStatusLabel" Text="<%$ Resources:SR,DocumentStatus %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDocumentStatus" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="locationLabel" Text="<%$ Resources:SR,Location %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lbllocation" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="packageNameLabel" Text="<%$ Resources:SR,PackageName %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPackageName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="requestedMeetingLabel" Text="<%$ Resources:SR,RequestedMeeting %>" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblRequestedMeeting" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="dateLastUpdatedLabel" Text="<%$ Resources:SR,DateLastUpdated %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateLastUpdated" runat="server" />
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="form">
    <tr>
        <td class="caption" style="font-size: 13px;">
            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:SR,Messages %>'></asp:Label><br />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Literal ID="lblRespondToIndividual" runat="server" Text="<%$ Resources:SR,RespondToIndividual %>" />:
        </td>
    </tr>
    <tr>
        <td colspan="3" valign="middle">
            <table>
                <tr>
                    <td width="100%">
                        <%--use custom control TextArea (ingerited from asp:TextBox), which allows maxlength in multiline texboxes. So, input is disabled, when maxlength is reached.
                        Don't need validation using regex. This fixes a bug when validation denies saving when we try to save string with a line folding--%>
                        <qwc:TextArea ID="tbRespondArea" runat="server" MaxLength="1000" TextMode="MultiLine"
                            Width="70%" Height="100" />
                    </td>
                    <td valign="middle"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <uc1:FileUpload SelectFileText="<%$ Resources:SR,FileToUpload%>" MaxFileLength="4194304" MaxLength="210"
                ID="fileUpload" UploadedFileName="" runat="server" FormatMaxFileLength="<%$ Resources:BM_ValidationSR, MsgWrongFileSize %>"
                FileFilter="Excel (*.xlsx, *.xls)|*.xlsx;*.xls|PDF (*.pdf)|*.pdf|Word Document (*.docx, *.doc)|*.docx;*.doc|PowerPoint (*.ppt, *.pptx)|*.ppt;*.pptx" MsgFileFilterAccepted="<%$ Resources:BM_ValidationSR, MsgFileFilterAccepted %>" />
        </td>
    </tr>
</table>
<br />

<div style="background-color: #eeeade; height: 100%; width: 95%; max-height: 300px; overflow: auto;">
    <asp:Repeater ID="rptMessages" runat="server">
        <ItemTemplate>
            <div class="<%# Eval("CssClass") %>">
                <table width="100%" runat="server" id="tNotifications">
                    <tr>
                        <td class="caption" style="width: 1%">
                            <asp:Literal ID="lblFrom" runat="server" Text="From" />: 
                        </td>
                        <td>
                            <asp:Literal ID="Label2" runat="server" Text='<%# ((IRMessageListItem)Container.DataItem).From%>'></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption" style="width: 1%">
                            <asp:Literal ID="lblDate" runat="server" Text="Date" />: 
                        </td>
                        <td>
                            <asp:Literal ID="Label3" runat="server" Text='<%# Formatter.FormatDateTime(((IRMessageListItem)Container.DataItem).Date, string.Format("{0} {1}", DataFormatterBase.FORMAT_DATE, DataFormatterBase.FORMAT_TIME_SPAN)) %>' />
                        </td>
                    </tr>
                    <div id="attachmentsDiv">
                        <tr>
                            <td class="caption" style="width: 1%">
                                <asp:Literal ID="Literal1" runat="server" Text="Attachment" />: 
                            </td>
                            <td>
                                <asp:HyperLink ID="Literal2" runat="server" Text='<%# ((IRMessageListItem)Container.DataItem).AttachmentName%>' OnDataBinding="MessageUrl_DataBinding" />
                            </td>
                        </tr>
                    </div>
                    <tr>
                        <td colspan="2">
                            <asp:Literal ID="Label4" runat="server" Text='<%# ((IRMessageListItem)Container.DataItem).Message%>'></asp:Literal>
                        </td>
                    </tr>

                </table>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
