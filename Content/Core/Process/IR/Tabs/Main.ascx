﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.IR.Tabs.Main" %>
<%@ Import Namespace="Eproc2.Core.Common" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Process.IR" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="798985e3-e110-4e95-8252-dddf479967a5" />

<table cellspacing="5px" class="form">
    <tr>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="individualNameLabel" Text="<%$ Resources:SR,IndividualsName %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblIndividualsName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="buyerOrganisationLabel" Text="<%$ Resources:SR,BuyerOrganisation %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyerOrganisation" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="referenceNumberLabel" Text="<%$ Resources:SR,ReferenceNumber %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblReferenceNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="documentStatusLabel" Text="<%$ Resources:SR,DocumentStatus %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDocumentStatus" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="productCodeLabel" Text="<%$ Resources:SR,ProductCode %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblProductCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="serviceNameLabel" Text="<%$ Resources:SR,ServiceName %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblServiceName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="requestedMeetingLabel" Text="<%$ Resources:SR,RequestedMeeting %>" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblRequestedMeeting" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="dateLastUpdatedLabel" Text="<%$ Resources:SR,DateLastUpdated %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateLastUpdated" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="form">
    <tr>
        <td class="caption" style="font-size: 13px;">
            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:SR,Messages %>'></asp:Label><br/>
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Literal ID="lblRespondToIndividual" runat="server" Text="<%$ Resources:SR,RespondToIndividual %>" />:
        </td>
    </tr>
    <tr>
        <td colspan="3" valign="middle">
            <table>
                <tr>
                    <td width="100%">
                        <%--use custom control TextArea (ingerited from asp:TextBox), which allows maxlength in multiline texboxes. So, input is disabled, when maxlength is reached.
                        Don't need validation using regex. This fixes a bug when validation denies saving when we try to save string with a line folding--%>
                        <qwc:TextArea ID="tbRespondArea" runat="server" MaxLength="1000" TextMode="MultiLine"
                            Width="70%" Height="100" />
                    </td>
                    <td valign="middle"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />

<div style="background-color: #eeeade; height: 100%; width:95%; max-height: 300px; overflow: auto;">
    <asp:Repeater ID="rptMessages" runat="server">
        <ItemTemplate>
            <div class="<%# Eval("CssClass") %>">
                <table width="100%" runat="server" id="tNotifications">
                    <tr>
                        <td class="caption" style="width:1%">
                            <asp:Literal ID="lblFrom" runat="server" Text="From" />: 
                        </td>
                        <td>
                            <asp:Literal ID="Label2" runat="server" Text='<%# ((IRMessageListItem)Container.DataItem).From%>'></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption" style="width:1%">
                            <asp:Literal ID="lblDate" runat="server" Text="Date" />: 
                        </td>
                        <td>
                            <asp:Literal ID="Label3" runat="server" Text='<%# Formatter.FormatDateTime(((IRMessageListItem)Container.DataItem).Date, string.Format("{0} {1}", DataFormatterBase.FORMAT_DATE, DataFormatterBase.FORMAT_TIME_SPAN)) %>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Literal ID="Label4" runat="server" Text='<%# ((IRMessageListItem)Container.DataItem).Message%>'></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
