﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.IR.InformationRequestListContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="e4944e6b-f6b4-4a96-9e14-157e3c16978c" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" CssClass="GridEx informationRequest" SortExpression="UnreadBySupplierMsgCounter,UpdateTime" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="Url"/>
        <qwc:ImageColumn AllowPreview="true" HeaderTitle="<%$ Resources:SR,Type %>" DataField="IRTypeIcon" 
            SortExpression="Type" AlternativeImageUrl="IRTypeIcon" BlankImageUrl="IRTypeIcon" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,State %>" ColumnType="String" DataField="IRState" AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PersonFrom %>" ColumnType="String" DataField="CreatorName" AllowSorting="true" IsEditable="False" SortExpression="PrincBase.Name" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,ReplyBy %>" DataField="ReplyByDate" SortExpression="ReplyByDate" />        
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ReferenceNumber %>" ColumnType="String" DataField="IRNumber" AllowSorting="true" IsEditable="False" SortExpression="IRNumber" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SentBy %>" ColumnType="String" DataField="LastUpdater" AllowSorting="true" IsEditable="False" SortExpression="LastUpdater" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DateLastUpdated %>" DataField="UpdateTime" SortExpression="UpdateTime" />
    </Columns>
</qwc:ControlledGrid>
