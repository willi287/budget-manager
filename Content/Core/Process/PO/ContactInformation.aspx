﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false"
    Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase"
    Title="Untitled Page" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ContactInformationContent.ascx" tagname="ContactInformationContent" tagprefix="uc1" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />    
    <uc1:ContactInformationContent ID="ContactInformationContent" runat="server" />    
</asp:Content>
