<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedDocuments.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.RelatedDocuments" %>
<%@ Register Src="~/Content/Core/Process/WI/Tabs/RelatedDocuments.ascx" TagName="RD"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3269a4a5-3d58-472a-8ce4-d3e5fe061d3a" />
<asp:Button ID="btnRelatedDocuments" runat="server" Text="<%$ Resources:SR,ConsolidatedPO %>"
    OnClick="btnRelatedDocuments_Click" />
<e2w:RD Id="ucRD" runat="server" ShowHelpBox="false"/>
