<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.PurchaseItems" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="a5a04539-6a16-4520-9a0b-b9c9d71952e5" />
<table width="100%">
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAddPI" OnCommand="ActionButton_OnCommand" runat="server" Visible="false" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" OnDeleting="cgPurchaseItems_OnDeleting">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHighlight="true" />
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn DataField="ProductCode" HeaderTitle="<%$ Resources:SR,ProductCode %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:SR,ProductName %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Quantity" HeaderTitle="<%$ Resources:SR,Quantity %>" SortExpression="None">
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="InvoicePrice" HeaderTitle="<%$ Resources:SR,Price %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="UnitCost" HeaderTitle="<%$ Resources:SR,UnitCost %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="VatPercent" HeaderTitle="<%$ Resources:SR,VAT %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="UnitIdObjectCode" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Ordered" HeaderTitle="<%$ Resources:SR,Ordered %>" SortExpression="None">
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                                 />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="Completed" HeaderTitle="<%$ Resources:SR,Completed %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="NotNeeded" HeaderTitle="<%$ Resources:SR,NotNeeded %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="AcceptedOrDefault" HeaderTitle="<%$ Resources:SR,Accepted %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Damaged" HeaderTitle="<%$ Resources:SR,Damaged %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Shipped" HeaderTitle="<%$ Resources:SR,Shipped %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="NotDeliveredVisibleValue" HeaderTitle="<%$ Resources:SR,ToBeDelivered %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="ToBeOrdered" HeaderTitle="<%$ Resources:SR,ToBeOrdered %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="Order" HeaderTitle="<%$ Resources:SR,Order %>" SortExpression="None">
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                                />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:TextColumn DataField="ChangeReasonName" HeaderTitle="<%$ Resources:SR,ChangeReason %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="TotalNet" HeaderTitle="<%$ Resources:SR,NetTotal %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total" />
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
            <table class="separator" id="tRecalculateSeparator" runat="server">
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <th width="225px">
                        <asp:Button ID="btnRecalculate" runat="server" Text="<%$ Resources:SR,Recalculate %>"
                            OnClick="btnRecalculate_Click" />
                    </th>
                    <th class="total">
                        &nbsp;
                    </th>
                </tr>
            </table>
            <table class="netvatgross" id="tblNetVatGross" runat="server">
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalNetValue"  runat="server" />
                    </td>
                </tr>
                <tr id="trTotalVat" runat="server">
                    <td>
                        <qwc:InfoLabel ID="lblTotalVat" runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalVatValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalGross" runat="server" Text="<%$ Resources:SR,TotalGROSS %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalGrossValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
