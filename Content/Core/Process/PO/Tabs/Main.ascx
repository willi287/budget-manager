<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register src="../../../../../Controls/Core/Address.ascx" tagname="Address" tagprefix="e2c" %>
<%@ Register src="../../../../../Controls/Core/Calendar.ascx" tagname="Calendar" tagprefix="e2c" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="d3d5606b-4dca-4b0a-9ea6-af2e98ab2bdd" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentNo" runat="server" Text="<%$ Resources:SR,DocumentCode %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDocumentNoContent" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblDeliveryDate" runat="server" Text="<%$ Resources:SR,DeliveryDate %>" AssociatedControlID="calRequiredDeliveryDate" />:
                        <span>*</span>
                    </td>
                    <td>
                        <e2c:Calendar ID="calRequiredDeliveryDate" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblDeliveryTerms" runat="server" Text="<%$ Resources:SR,DeliveryTerms %>" AssociatedControlID="ddlDeliveryTerms"  />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDeliveryTerms" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                         <spring:ValidationError id="DeliveryTermsError" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblBcg" runat="server" Text="<%$ Resources:SR,BuyerCatalogue %>" AssociatedControlID="ddlBcg"  />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBcg" AutoPostBack="true"  DataTextField="Value" DataValueField="Id" runat="server" 
                            onselectedindexchanged="ddlBcg_SelectedIndexChanged" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"/>
                        <spring:ValidationError id="BcgError" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblContract" runat="server" Text="<%$ Resources:SR,ContractName %>" AssociatedControlID="ddlContractName"  />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlContractName" AutoPostBack="true" onselectedindexchanged="ddlBcg_SelectedIndexChanged" DataTextField="Value" DataValueField="Id" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblSupplier" runat="server" Text="<%$ Resources:SR,Supplier %>"  AssociatedControlID="ddlSupplier" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlSupplier" DataTextField="Value" DataValueField="Id" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm formInfo">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblState" runat="server" Text="<%$ Resources:SR,State %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbState" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDateSubmitted" runat="server" Text="<%$ Resources:SR,DateSubmitted %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateSubmittedContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblBuyer" runat="server" Text="<%$ Resources:SR,Buyer %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyerContent" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblApprovalProcedure" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbApprovalProcedure" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVersion" runat="server" Text="<%$ Resources:SR,Version %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblVersionContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblProjectBudget" runat="server" Text="<%$ Resources:SR,ProjectBudget %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblProjectBudgetContent" runat="server" />
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <qwc:InfoLabel ID="lblPropertyAddress" runat="server" Text="<%$ Resources:SR,PropertyAddress %>" />
        </td>
        <td >
            <qwc:InfoLabel ID="lblDeliveryAddress" runat="server" Text="<%$ Resources:SR,DeliveryAddress %>" />
        </td>
    </tr>
    <tr>
        <td>
            <e2c:Address id="ucPropertyAddress" runat="server" />
        </td>
        <td>
            <e2C:Address id="ucDeliveryAddress" runat="server" />
        </td>
    </tr>
</table>


