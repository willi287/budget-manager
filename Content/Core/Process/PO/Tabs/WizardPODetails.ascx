<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPODetails.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.WizardPODetails" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="5b11104c-6b00-41e8-8960-65273db92efa" />
<e2w:PopupControl ID="pcError" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblBcg" runat="server" Text="<%$ Resources:SR,BuyerCatalogue %>" AssociatedControlID="ddlBcg" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBcg" AutoPostBack="true" DataTextField="Value" DataValueField="Id"
                            CausesValidation="false" runat="server" 
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"/>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvFeeContractName" ControlToValidate="ddlBcg"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
    <td>
        <table class="subForm">
            <tr>
                <td class="caption required">
                    <qwc:InfoLabel ID="lblContract" runat="server" Text="<%$ Resources:SR,ContractName %>"
                        AssociatedControlID="ddlContractName" />:&nbsp;<span>*</span>
                </td>
                <td>
                    <qwc:DropDownList ID="ddlContractName" DataTextField="Value" CausesValidation="false"
                        DataValueField="Id" AutoPostBack="true" runat="server" 
                        ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"/>                        
                </td>
            </tr>
        </table>
        </td>
        <td>
            <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                EnableClientScript="true" ID="rfvContractName" ControlToValidate="ddlContractName"
                Display="Dynamic" runat="server" ErrorMessage="*" InitialValue="00000000-0000-0000-0000-000000000000" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
    <td>
        <table class="subForm">
            <tr>
                <td class="caption required">
                    <qwc:InfoLabel ID="lblTemplateType" runat="server" AssociatedControlID="ddlTemplateType"
                        Text="<%$ Resources:SR,OrderType %>" />:&nbsp;<span>*</span>
                </td>
                <td>
                    <qwc:DropDownList ID="ddlTemplateType" AutoPostBack="false" DataTextField="Value"
                        DataValueField="Id" runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"/>
                </td>
            </tr>
        </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
