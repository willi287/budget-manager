<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPOSummaryItemTemplate.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.WizardPOSummaryItemTemplate" %>
<table width="100%" height="*">
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems"
                runat="server" AllowEditing="false" KeyField="Id" AllowSorting="false" AllowPaging="false"
                AllowViewing="false" SortExpression="None">
                <Columns>
                    <qwc:TextColumn DataField="Code" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
                    <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
                    <qwc:TextColumn DataField="SupplierName" SortExpression="None" HeaderTitle="<%$ Resources:SR,Supplier %>" />
                    <qwc:DecimalColumn DataField="NetInvoicePrice" SortExpression="None" HeaderTitle="<%$ Resources:SR,Price %>" />
                    <qwc:DecimalColumn DataField="VatPercent" SortExpression="None" HeaderTitle="<%$ Resources:SR,VatPercent %>" />
                    <qwc:TextColumn DataField="UnitOfMeasure" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" />
                    <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
                    <qwc:DecimalColumn DataField="Quantity" SortExpression="None" IsEditable="true" HeaderTitle="<%$ Resources:SR,Quantity %>">
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalNet" SortExpression="None" HeaderTitle="<%$ Resources:SR,NetTotal %>">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
            <table class="separator">
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <th width="225px">
                        <asp:Button ID="tbRecalculate" runat="server" UseSubmitBehavior="true" Text="<%$ Resources:SR,Recalculate %>" />
                    </th>
                    <th class="total">
                        &nbsp;
                    </th>
                </tr>
            </table>
            <table class="netvatgross" ID ="tPrices" runat="server">
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalNetValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalVat" runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalVatValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalGross" runat="server"  Text="<%$ Resources:SR,TotalGROSS %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalGrossValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

