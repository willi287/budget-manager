<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformation.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.ContactInformation" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="936f68f7-80f1-4db9-803f-7b62c2010c30" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblOrganisationName" runat="server" Text="<%$ Resources:SR,OrganisationName%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblOrganisationNameContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblAddress" runat="server" Text="<%$ Resources:SR,Address%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblAddressContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPhone" runat="server" Text="<%$ Resources:SR,Phone%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPhoneContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblFax" runat="server" Text="<%$ Resources:SR,Fax%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblFaxContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblWebSite" runat="server" Text="<%$ Resources:SR,WebSite%>" />:
                    </td>
                    <td>
                        <asp:HyperLink ID="hlWebSite" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgUsers" runat="server" KeyField="Id" SortExpression="None">
    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="None"/>
    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Email %>" DataField="Email" SortExpression="None"/>
    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Phone %>" DataField="Phone" SortExpression="None"/>
</qwc:ControlledGrid>
