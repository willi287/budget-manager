<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SPOPurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.SPOPurchaseItems" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ee9c4573-873d-47a4-a5cb-225fc362f285" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPI"
    runat="server" KeyField="Id" SortExpression="None">
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHighlight="true" />
        <qwc:TextColumn DataField="ProductCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
        <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
        <qwc:DecimalColumn DataField="UnitCost" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitCost %>"/>
        <qwc:DecimalColumn DataField="VatPercent" SortExpression="None" HeaderTitle="<%$ Resources:SR,Vat %>"/>
        <qwc:TextColumn DataField="UnitIdObjectCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" />
        <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
        <qwc:DecimalColumn DataField="Quantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,Quantity %>">
            <Validators>
                <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
            </Validators>
        </qwc:DecimalColumn>
    </Columns>
</qwc:ControlledGrid>
<qwc:ActionButtonRepeater ID="ActionsRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
