<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PIDetails.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.PIDetails" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="8b6e60d3-78aa-4733-bc63-1c3f3ae5d686" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblProductCode" runat="server" Text="<%$ Resources:SR,ProductCode %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblValueProductCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCustomProductCode" AssociatedControlID="txtCustomProductCode" runat="server"
                            Text="<%$ Resources:SR,CustomProductCode %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtCustomProductCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblProductName" runat="server" AssociatedControlID="txtProductName"
                            Text="<%$ Resources:SR,ProductName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="txtProductName" runat="server" MaxLength="255" />
                        <asp:RequiredFieldValidator Display="Dynamic" ID="rfvProductName" ControlToValidate="txtProductName"
                            ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredVEMsg %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblUnits" runat="server" AssociatedControlID="ddlUnits" Text="<%$ Resources:SR,Units %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlUnits" DataValueField="Id" DataTextField="Value" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblUnitCost" AssociatedControlID="txtUnitCost" runat="server" Text="<%$ Resources:SR,UnitCost %>" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="txtUnitCost" runat="server" />
                        <qwc:PriceValidator Display="Dynamic" ID="pvUnitCost" ControlToValidate="txtUnitCost"
                            ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR,IncorrectFormatMsg %>"  runat="server" />
                        <asp:RequiredFieldValidator Display="Dynamic" ID="rfvUnitCost" ControlToValidate="txtUnitCost"
                            ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredVEMsg %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblUnitQuantity" AssociatedControlID="txtUnitQuantity" runat="server" Text="<%$ Resources:SR, UnitQuantity %>" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="txtUnitQuantity" runat="server" />
                        <asp:RangeValidator Display="Dynamic" ID="rvUnitQuantity" ControlToValidate="txtUnitQuantity"
                            Type="Double" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR,IncorrectFormatMsg %>"
                            MinimumValue="0" MaximumValue="39614081257132168796771975167" runat="server" />
                        <asp:RequiredFieldValidator Display="Dynamic" ID="rfvUnitQuantity" ControlToValidate="txtUnitQuantity"
                            ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredVEMsg %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVat" runat="server" AssociatedControlID="ddlVat" Text="<%$ Resources:SR,VAT %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlVat" DataValueField="Id" DataTextField="Value" runat="server" SortOrder="None" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblQuantity" AssociatedControlID="txtQuantity" runat="server" Text="<%$ Resources:SR,Quantity %>" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="txtQuantity" runat="server" />
                        <asp:RangeValidator Display="Dynamic" ID="rvQuantity" ControlToValidate="txtQuantity"
                            Type="Double" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR,IncorrectFormatMsg %>"
                            MinimumValue="0" MaximumValue="39614081257132168796771975167" runat="server" />
                        <asp:RequiredFieldValidator Display="Dynamic" ID="rfvQuantity" ControlToValidate="txtQuantity"
                            ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredVEMsg %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblChangeReason" AssociatedControlID="ddlChangeReason" runat="server"
                            Text="<%$ Resources:SR,ChangeReason %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlChangeReason" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNotes" AssociatedControlID="txtNote" runat="server" Text="<%$ Resources:SR,Notes %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtNote" MaxLength="1000" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
