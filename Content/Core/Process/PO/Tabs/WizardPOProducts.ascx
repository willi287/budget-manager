<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPOProducts.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.WizardPOProducts" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5d90daba-4951-4705-a2af-980347b121ac" />
<asp:PlaceHolder ID="plhTabs" runat="server">
    <table class="bordered">
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="lblSupplier" runat="server" Text="<%$ Resources:SR,Supplier %>" />:
            </td>
            <td>
                <qwc:DropDownList ID="ddlSupplier" AutoPostBack="true" DataTextField="Name" DataValueField="OrganisationId"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                    runat="server" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" />
            </td>
        </tr>
    </table>
</asp:PlaceHolder>
