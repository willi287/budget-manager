<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemovedItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.RemovedItems" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="59896024-3460-48a0-8d5c-874f461a8a3d" />
<qwc:ControlledGrid ID="cgPurchaseItems" KeyField="Id" runat="server" SortExpression="None">
    <Columns>
        <qwc:TextColumn DataField="ProductCode" HeaderTitle="<%$ Resources:SR,ProductCode %>" SortExpression="None"/>
        <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:SR,ProductName %>" SortExpression="None"/>
        <qwc:DecimalColumn DataField="InvoicePrice" HeaderTitle="<%$ Resources:SR,UnitCost %>" SortExpression="None"/>
        <qwc:TextColumn DataField="UnitIdObjectCode" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" SortExpression="None"/>
        <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None"/>        
        <qwc:TextColumn DataField="ChangeReasonName" HeaderTitle="<%$ Resources:SR,ChangeReason %>" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
