<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SPOReason.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.SPOReason" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="8ba83259-a6d2-4048-ac8b-7aa3c7df5146" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPI"
    runat="server" KeyField="Id" SelectKey="" SortExpression="None">
    <Columns>
        <qwc:TextColumn DataField="ProductCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
        <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
        <qwc:DecimalColumn DataField="UnitCost" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitCost %>"/>
        <qwc:DecimalColumn DataField="VatPercent" SortExpression="None" HeaderTitle="<%$ Resources:SR,Vat %>"/>
        <qwc:TextColumn DataField="UnitIdObjectCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" />
        <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
        <qwc:DecimalColumn DataField="Quantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,Quantity %>">
            <Validators>
                <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
            </Validators>
        </qwc:DecimalColumn>
        <qwc:DropDownColumn DataField="ChangeReason" HeaderTitle="<%$ Resources:SR,ChangeReason %>" />
        <qwc:TextColumn DataField="Note" SortExpression="None" ColumnType="String" ControlStyle-Width="178px" HeaderTitle="<%$ Resources:SR, Notes %>" />
    </Columns>
</qwc:ControlledGrid>
