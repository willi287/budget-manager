<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPOProductsTemplate.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.WizardPOProductsTemplate" %>
<qwc:ControlledGrid ID="cgProductList" runat="server" SortExpression="Code" AllowEditing="false"
    AllowDeleting="false" EnableStylishRow=false AllowViewing="true" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsContractProduct" EnableHighlight="true" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,Price %>" DataField="NetInvoicePrice" SortExpression="None"/>
        <qwc:DecimalColumn DataField="VatPercent" SortExpression="None" HeaderTitle="<%$ Resources:SR,VatPercent %>"/>
        <qwc:TextColumn DataField="UnitOfMeasure" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" />
        <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
        <qwc:DecimalColumn DataField="Quantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,Quantity %>">
            <Validators>
                <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
            </Validators>
        </qwc:DecimalColumn>
    </Columns>
</qwc:ControlledGrid>
<asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:SR,SelectAdditionalProducts %>"
    OnClick="btnAdd_Click" />
