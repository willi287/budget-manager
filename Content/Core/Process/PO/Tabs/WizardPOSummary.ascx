﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPOSummary.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.Tabs.WizardPOSummary" %>
<%@ Register Src="WizardPOSummaryItemTemplate.ascx" TagName="WizardPOSummaryItemTemplate"
    TagPrefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="9fe1d293-bf0f-46cc-b821-804ccf95b97c" />

<asp:Table id="tblTemplate" runat="server" CssClass="bordered" Width="50%">
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <qwc:QWCheckBox ID="cbUpdateTemplate" Checked="false" runat="server" Text="<%$ Resources:SR,UpdateTemplate %>" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <qwc:QWCheckBox ID="cbCreateNew" runat="server" Checked="false" Text="<%$ Resources:SR,CreateNewTemplate %>"
                AutoPostBack="true" />
        </asp:TableCell>
    </asp:TableRow>
 
    <asp:TableRow ID="trTemplateName">
        <asp:TableCell CssClass="caption required">
            <qwc:InfoLabel ID="lblTemplateName" runat="server" AssociatedControlID="tbTemplateName"
                Text="<%$ Resources:SR,TemplateName %>" />:
                <span>*</span>
        </asp:TableCell>
        <asp:TableCell>
            <qwc:TextBox ID="tbTemplateName" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
<br />
<table class="grey" cellspacing="0" id="tbTotal" runat="server" width="50%">
    <tr >
        <td>
            <qwc:Label ID="lblTotalOrderTitle" CssClass="bold" Text="<%$ Resources:SR,TotalInvoicePriceNetOfVat %>"
                runat="server" />:
        </td>
        <td align="center" width="35%">
            <qwc:Label ID="lblTotalOrderValue"  runat="server" />
        </td>
    </tr>
</table>
<br />
<qwc:RepeatControl ID="SummaryRepeatControl" runat="server" OnItemCommand="srControl_OnItemCommand">
    <ItemTemplate>
        <uc1:WizardPOSummaryItemTemplate ID="WizardPOSummaryItemTemplate1" runat="server" />
    </ItemTemplate>
</qwc:RepeatControl>




