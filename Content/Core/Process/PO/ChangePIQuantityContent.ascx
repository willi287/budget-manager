<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePIQuantityContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PO.ChangePIQuantityContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="a3a77997-aab0-498c-bb93-4ad0da2f9eac" />
<table width="300">
    <tr>
        <td><qwc:InfoLabel ID="lblProductCode" runat="server" Text="<%$ Resources:SR,ProductCode %>"/></td>
        <td>
            <qwc:InfoLabel ID="lblValueProductCode" runat="server"></qwc:InfoLabel>
        </td>
    </tr>
    <tr>
        <td><qwc:InfoLabel ID="lblUnits" runat="server" Text="<%$ Resources:SR,Units %>"/></td>
        <td>
            <qwc:InfoLabel ID="lblValueUnits" runat="server"></qwc:InfoLabel>
        </td>
    </tr>
    <tr>
        <td><qwc:InfoLabel ID="lblUnitCost" runat="server" Text="<%$ Resources:SR,UnitCost %>"/></td>
        <td>
            <qwc:InfoLabel ID="lblValueUnitCost" runat="server"></qwc:InfoLabel>
        </td>
    </tr>
    <tr>
        <td><qwc:InfoLabel ID="lblQuantity" runat="server" Text="<%$ Resources:SR,Quantity %>"/></td>
        <td>
            <qwc:TextBox ID="txtQuantity" runat="server" Width="150px"></qwc:TextBox>
        </td>
    </tr>
    <tr>
        <td><qwc:InfoLabel ID="lblChangeReason" runat="server" Text="<%$ Resources:SR,ChangeReason %>"/></td>
        <td>
            <qwc:DropDownList ID="ddlChangeReason" runat="server" Width="150px">
            </qwc:DropDownList>
        </td>
    </tr>
    <tr>
        <td><qwc:InfoLabel ID="lblNotes" runat="server" Text="<%$ Resources:SR,Notes %>"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:TextBox ID="txtNote" runat="server" Height="60px" TextMode="MultiLine" 
                Width="250px"></qwc:TextBox>
        </td>
    </tr>
</table>
