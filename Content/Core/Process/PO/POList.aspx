﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumnetsListPageBase" Title="Untitled Page" %>
<%@ Register TagPrefix="ctl" TagName="POList" Src="~/Content/Core/Process/PO/POListContent.ascx" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
   <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
   <ctl:POList ID="ContentControl" runat="server"></ctl:POList>
</asp:Content>
