﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.PO.POListContent" %>
<%@ Register Src="../../../../Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:popupcontrol id="pcDelete" text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="01a04907-25e9-4d73-a2f8-0db3e771b76f" />
<div id="search">
    <e2w:search id="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="SubmitDate" SortDirection="False" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <%--//NOTE: SHN: DataField is set for select column just for correct unbinding--%>
        <qwc:SelectColumn IsEditable="true" DataField="Selected" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Process/PO/POItem.aspx" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DateSubmitted %>" DataField="SubmitDate" SortExpression="SubmitDate" ShowTime="False" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PONumber %>" DataField="VisibleDocumentNo" SortExpression="DocumentNo" />
        <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,Version %>" DataField="IsSpo" SortExpression="ParentId" TrueValue="<%$ Resources:SR, Supplementary %>" FalseValue="<%$ Resources:SR, Original %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetValue %>" DataField="Total" SortExpression="None" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierInvoiceNo %>" ColumnType="String" DataField="SupplierInvoiceNo" AllowSorting="false" IsEditable="true" MaxLength="53" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>
