﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentDetails.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PY.Tabs.PaymentDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Content/Core/Process/PY/Tabs/PaymentDetailsTotalItem.ascx" TagName="InvoiceTotals" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="155ba9c9-7da7-4ea3-b01a-5a628d9a2fda" />
<table width="100%" height="*" cellpadding="0" cellspacing="0">
    <tr>
        <td class="caption">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,InvoicedAmount%>" />:
        </td>
        <td>
            <qwc:Label ID="lblInvoicedAmount" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,BalanceOutstanding%>" />:
        </td>
        <td>
            <qwc:Label ID="lblBalanceOutstanding" runat="server" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnRecalculate" runat="server" Text="<%$ Resources:SR,Recalculate %>"
                OnClick="btnRecalculate_Click" />
        </td>
    </tr>
    <tr>
        <td class="caption" colspan="2">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,PaymentsAlreadyReceived%>" />:
        </td>
    </tr>
</table>
<br />
<table width="100%" height="*" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding-left: 15px; height: 35px" colspan="2">
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" Caption="<%$ Resources:SR,Add %>" />
        </td>
    </tr>
    <tr><td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgGips" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" AllowDeleting="true" OnDeleting="cgGips_OnDeleting">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,PaymentDate %>" DataField="PaymentDate" SortExpression="None" ShowTime="False" IsEditable="true" IsEditableDataField="IsEditable" CalendarControlPath="~/Controls/Core/Calendar.ascx">
                        <Validators>
                            <qwc:ColumnRequiredFieldValidator ID="paymentDateValidator" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"/>
                        </Validators>
                    </qwc:DateTimeColumn>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PaymentReference %>" DataField="PaymentReference" SortExpression="None" IsEditable="true" IsEditableDataField="IsEditable" ColumnType="String" MaxLength="70" >
                        <Validators>
                            <%--<qwc:ColumnRequiredFieldValidator ID="paymentReferenceValidator" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"/>--%>
                        </Validators>
                    </qwc:TextColumn>
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,AmountReceived %>" DataField="AmountReceived" SortExpression="None" IsEditable="true" IsEditableDataField="IsEditable" PrecedingText="£">
                        <Validators>
                            <qwc:ColumnRequiredFieldValidator ID="AmountReceivedRequiredValidator" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td></tr>
    <tr>
        <td class="caption" colspan="2">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,CreditNotes%>" />:
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgCrN" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" AllowDeleting="false">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsSelectedForPayment" EnableHighlight="true">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:CheckBoxColumn>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:HyperLinkColumn IsEditable="false" DataField="VisibleDocumentNo" HeaderTitle="<%$ Resources:SR,CreditNoteNumber %>"
                        NavigateUrl="NavigateUrl" />
                    <qwc:TextColumn DataField="ContractIdObject.PrincipalIdObject.Name" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,Individual %>" />
                    <qwc:HyperLinkColumn DataField="InvoiceNumber" HeaderTitle="<%$ Resources:SR,ParentInvoiceNumber %>"
                        NavigateUrl="InvoiceNavigateUrl" />
                    
                    <qwc:DecimalColumn DataField="Total" HeaderTitle="<%$ Resources:SR,Net %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalVat" HeaderTitle="<%$ Resources:SR,VATwoPercent %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="GrossTotal" HeaderTitle="<%$ Resources:SR,Gross %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none" />
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalCrN" CssClass="bold" runat="server" Text="<%$ Resources:SR,TotalCreditNotes %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNNetValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNVatValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNGrossValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
