<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PY.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="17fff513-de94-44fa-bf40-074770c471ac" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentNo" runat="server" Text="<%$ Resources:SR,DocumentCode %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDocumentNoContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPaymentReference" runat="server" Text="<%$ Resources:SR,PaymentReference %>" />:
                    </td>
                    <td>
                    <% if (IsPaymentReferenceEnabled)
                       {%>
                        <qwc:TextBox ID="tbPaymentReference" runat="server" MaxLength="53" />
                     <%}
                       else
                       {%>
                        <qwc:Label ID="lblPaymentReferenceValue" runat="server" />
                     <%}%>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblPaymentDate" runat="server" Text="<%$ Resources:SR,PaymentDate %>"
                            AssociatedControlID="calRequiredPaymentDate" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <e2c:Calendar ID="calRequiredPaymentDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPaymentDueDate" runat="server" Text="<%$ Resources:SR,PaymentDueDate %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPaymentDueDateValue" runat="server" />
                    </td>
                </tr>
                <tr id="invoiceDateRow" runat="server">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceDate" runat="server" Text="<%$ Resources:SR,InvoiceDate%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblInvoiceDateValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblState" runat="server" Text="<%$ Resources:SR,State %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbState" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDateCreated" runat="server" Text="<%$ Resources:SR,DateCreated %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateCreatedContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblSubmitDate" runat="server" Text="<%$ Resources:SR,DateSubmitted %>" />:
                    </td>
                     <td>
                        <qwc:Label ID="lblSubmitDateContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <e2c:CurrentOwner ID="ucCurrentOwner" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:SR,Buyer %>" runat="server" />:
                    </td>
                    <td class="bold">
                        <qwc:Label ID="lblBuyer" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="5" class="caption" />
                    <td>
                        <qwc:Label ID="lblBuyerAddressLine" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblBuyerTownCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblBuyerPostCode" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label9" Text="<%$ Resources:SR,Supplier %>" runat="server" />:
                    </td>
                    <td class="bold">
                        <qwc:Label ID="lblSupplier" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" class="caption" />
                    <td>
                        <qwc:Label ID="lblupplierAddressLine" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblSupplierTownCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblSupplierPostCode" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label10" Text="<%$ Resources:SR,VATNumber %>" runat="server" />:
                    </td>
                    <td class="bold">
                        <qwc:Label ID="lblVatNumber" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
