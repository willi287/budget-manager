<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentDetailsTotalItem.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PY.Tabs.PaymentDetailsTotalItem" %>
<tr>
    <td>
        <qwc:InfoLabel ID="lblTotal" CssClass="bold" runat="server" Text="<%$ Resources:SR,TotalInvoices %>" />:
    </td>
    <td>
        <qwc:Label ID="lblTotalNetValue" runat="server" />
    </td>
    <td>
        <qwc:Label ID="lblTotalVatValue" runat="server" />
    </td>
    <td>
        <qwc:Label ID="lblTotalGrossValue" runat="server" />
    </td>
</tr>
