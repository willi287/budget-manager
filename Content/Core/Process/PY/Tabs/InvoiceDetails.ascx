﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceDetails.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PY.Tabs.InvoiceDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Content/Core/Process/PY/Tabs/PaymentDetailsTotalItem.ascx" TagName="InvoiceTotals" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="555ba9c9-7da7-4ea3-b01a-5a628d9a2fda" />
<table width="100%" height="*" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding-left: 15px; height: 35px">
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" Caption="<%$ Resources:SR,Add %>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgInvoices"
                runat="server" AllowEditing="false" KeyField="InvoiceId" AllowViewing="false" OnDeleting="cgInvoices_OnDeleting">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:ActionColumn>
                    <qwc:HyperLinkColumn DataField="VisibleDocumentNo" HeaderTitle="<%$ Resources:SR,InvoiceNumber %>"
                        NavigateUrl="NavigateUrl" />
                    <qwc:HyperLinkColumn DataField="POVisibleDocumentNo" HeaderTitle="<%$ Resources:BM_SR,PoNumber %>" SortExpression="None" NavigateUrl="PONavigateUrl" />
                    <qwc:TextColumn DataField="ContractName" HeaderTitle="<%$ Resources:BM_SR,Individual %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="PropertyAddress" HeaderTitle="<%$ Resources:SR,PropertyAddress %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="Total" HeaderTitle="<%$ Resources:SR,Net %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalVat" HeaderTitle="<%$ Resources:SR,VATwoPercent %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="GrossTotal" HeaderTitle="<%$ Resources:SR,Gross %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tbody>
                    <qwc:RepeatControl ID="repInvTotal" runat="server">
                        <ItemTemplate>
                            <uc1:InvoiceTotals ID="InvoiceTotalsItem" runat="server" />
                        </ItemTemplate>
                    </qwc:RepeatControl>
                </tbody>
            </table>
        </td>
    </tr>
</table>
