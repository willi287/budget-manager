﻿<%@ Import Namespace="Eproc2.Web.Content.Core.Process"%>
<%@ Import namespace="Eproc2.Core.Entities"%>
<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="PyListContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PY.PyListContent" %>
<%@ Register src="~/Controls/Core/FreeSearch.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>

<%@ Register assembly="Qulix.Web" namespace="Qulix.Web.Controls" tagprefix="cc1" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="63382672-c3c3-4a6e-a0e3-fa15ee0d694e" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR,CantDelete %>"/>
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="SubmitDate" SortDirection="false" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn IsEditable="true"/>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"        NavigateUrl="~/Content/Core/Process/PY/PYItem.aspx"   />
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DateSubmitted %>"    DataField="SubmitDate" ShowTime="False" SortExpression="SubmitDate"/>
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,PaymentDate %>"    DataField="PaymentDate" ShowTime="False" SortExpression="PaymentDate"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PYNo %>"             DataField="DocumentNo" SortExpression="DocumentNo" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerName %>"        DataField="BuyerName" SortExpression="buyer.Name"/> 
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>"             DataField="SupplierName"  SortExpression="supplier.Name"/>
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,GrossValue %>"         DataField="GrossTotal" SortExpression="GrossTotal"/>         
    </Columns>
</qwc:ControlledGrid>
