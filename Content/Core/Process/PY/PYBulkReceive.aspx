﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" CodeBehind="PYBulkReceive.aspx.cs" Inherits="Eproc2.Web.Content.Core.Process.PY.PYBulkReceive" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="PYBulkReceiveContent.ascx" TagName="CreatePIContent" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:CreatePIContent ID="PYBulkReceiveContent1" runat="server" />
</asp:Content>
