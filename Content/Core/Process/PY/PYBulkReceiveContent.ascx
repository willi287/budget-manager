<%@ Import Namespace="Eproc2.Web.Content.Core.Process.PY" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PYBulkReceiveContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.PY.PYBulkReceiveContent" %>
<%@ Register src="~/Controls/Core/Calendar.ascx" tagname="Calendar" tagprefix="e2c" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="16f9cebc-1fa4-4fa9-aca2-898353a6547b" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table width="400" cellpadding="0" cellspacing="0">
    <tr>
        <td class="caption required">
            <qwc:InfoLabel ID="lblPaymentDate" runat="server" Text="<%$ Resources:SR,PaymentDate %>" AssociatedControlID="calRequiredPaymentDate" />
            <span>*</span>
        </td>
        <td>
            <e2c:Calendar ID="calRequiredPaymentDate" runat="server" />
        </td>
    </tr>
</table>
<uc:PopupControl ID="ucPopupControl" runat="server"/>
<asp:HiddenField ID="IsWarningShown" runat="server"/>

