﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.PY.PYItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="../../../../Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<uc:PopupControl ID="ucPopupControl" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
<asp:HiddenField ID="IsWarningShown" runat="server"/>
