<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.CRN.Tabs.PurchaseItems" %>
<%@ Register src="CRNPurchaseItemsList.ascx" tagname="CRNPurchaseItemsList" tagprefix="uc1" %>

 <%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="86d4eb73-b184-4e8a-8b25-44d96ea7e186" />
<div class="summary" style="width: 100%">
    <%if (((IList)repPI.DataSource).Count > 1)
      {%>
    <table class="netvatgross">
        <tr>
            <td>
                <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNet%>" />:
            </td>
            <td class="caption">
                <qwc:Label ID="lblTotalNetContent" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <qwc:InfoLabel ID="lblTotalVat" runat="server" Text="<%$ Resources:SR,TotalVat%>" />:
            </td>
            <td class="caption">
                <qwc:Label ID="lblTotalVatContent" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <qwc:InfoLabel ID="lblTotalInvoicePrice" runat="server" Text="<%$ Resources:SR, TotalInvoicePriceNetOfVat%>" />:
            </td>
            <td class="caption">
                <qwc:Label ID="lblTotalInvoicePriceContent" runat="server" />
            </td>
        </tr>
    </table>
    <%} %>    
</div>
<br />
<div class="summary" style="width: 100%">
    <table class="netvatgross">
        <tr>
            <td style="width: 30%">
                <qwc:InfoLabel ID="Label3" Text="<%$ Resources:SR,GeneralDiscount %>" runat="server"  Style="white-space: nowrap" />:
            </td>
            <td style="width: 30%">
                <qwc:PriceTextBox ID="tbGeneralDiscount" runat="server" />
               </td>
            <td>
                <qwc:PriceValidator ID="pvGeneralDiscount" runat="server" ErrorMessage="*" ControlToValidate="tbGeneralDiscount" ToolTip="<%$ Resources:ValidationSR,IncorrectData %>"/>
                 </td>
            <td>
                <qwc:DropDownList ID="ddlVatPercent" runat="server" DataTextField="Value" DataValueField="Key" SortOrder="None" />
                 </td>
            <td>
                <asp:Button ID="btRefresh" runat="server" Text="<%$ Resources:SR,Recalculate %>" OnClick="btRefresh_OnClick" />
            </td>
        </tr>
    </table>
</div>
<table Width="50%">
    <tr valign="top">
        <td colspan="2">
            <qwc:Label ID="lblGDNote" runat="server" Text="<%$ Resources:SR,GDNote %>" />
        </td>
    </tr>
</table>
<qwc:RepeatControl ID="repPI" runat="server">
    <ItemTemplate>
        <uc1:CRNPurchaseItemsList ID="CRNPIList" runat="server" />
    </ItemTemplate>
</qwc:RepeatControl>
