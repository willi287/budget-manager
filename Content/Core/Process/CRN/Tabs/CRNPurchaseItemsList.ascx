<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRNPurchaseItemsList.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.CRN.Tabs.CrNPurchaseItemsList" %>

<table width="100%">
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems" SortExpression="None"
                runat="server" AllowDeleting = "false" AllowEditing = "false" KeyField = "Id" AllowViewing="false" >
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHiddeReadonlyRow="true" IsEditable="true" EnableHighlight="true"/>
                    <qwc:TextColumn DataField="ProductCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
                    <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
                    <qwc:DecimalColumn DataField="InvoicePrice" SortExpression="None" HeaderTitle="<%$ Resources:SR,Price %>"/>
                    <qwc:TextColumn DataField="UnitIdObjectCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" />
                    <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
                    <qwc:DecimalColumn DataField="Quantity" SortExpression="None" HeaderTitle="<%$ Resources:SR,Accepted %>" IsEditable="true">
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR,IncorrectData %>"/>
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalNet" SortExpression="None" HeaderTitle="<%$ Resources:SR,NetTotal %>"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross" >
                <tr>
                    <td>
                        <qwc:InfoLabel ID="Label1" Text="<%$ Resources:SR,TotalNet %>" runat="server"/>:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalNet" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="Label2" Text="<%$ Resources:SR,VatAt %>" runat="server" /> <qwc:Label ID="lblVAT" runat="server" Format="{0:F1}"/>%:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalVat" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalGross" runat="server" Text="<%$ Resources:SR,TotalGross%>" />:
                    </td>
                    <td>
			            <qwc:Label ID="lblTotalGrossContent" runat="server" />
			        </td>
			    </tr> 
            </table>
        </td>
    </tr>
</table>
