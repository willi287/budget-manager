﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.WI.DocumentItemContentBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<spring:ValidationError ID="validationError" runat="server"/>
<qwc:AccountInfoControl ID="pbaAccountControl" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
