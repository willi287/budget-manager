<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganisationContactLink.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.Controls.OrganisationContactLink" %>
<table width="100%" class="skinny">
    <tr>
        <td style="vertical-align:top">
            <qwc:Label ID="lblName" runat="server"/>
        </td>
        <td style="text-align:right">
            <asp:HyperLink ID="hlContact" Text="<%$ Resources:SR,ContactInformation %>" runat="server"/>    
        </td>
    </tr>
</table>
