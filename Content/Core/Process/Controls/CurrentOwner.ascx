<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentOwner.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.Controls.CurrentOwner" %>
<table width="100%" class="skinny">
    <tr>
        <td class="caption">
            <asp:LinkButton ID="lbtnCurrentOwner" runat="server" Text="<%$ Resources:SR,CurrentOwner %>"
                OnClick="lbtnCurrentOwner_Click" />
        </td>
    </tr>
    <tr>
        <td style="padding-left: 30px;">
            <ul>
                <asp:Repeater ID="repOwners" runat="server" Visible="false">
                    <ItemTemplate>
                        <li>
                            <%# Eval("Name") %></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </td>
    </tr>
</table>
