<%@ Import Namespace="Eproc2.Core.Workflow.Information"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.INV.Tabs.PurchaseItems" %>
<%@ Register Assembly="Eproc2.Web.Framework" Namespace="Eproc2.Web.Framework.Controls"
    TagPrefix="uc" %>
<%@ Register Src="~/Content/Core/Process/INV/Tabs/PurchaseItemsTemplate.ascx" TagName="PurchaseItemsTemplate"
    TagPrefix="uc" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="84ed3568-329d-4c45-8e79-6c75435ce893" />
<table width="100%" height="*">
    <tr>
        <td>
            <asp:Label class="error" ID="lblInvoiceSplitted" runat="server" Visible="false" Text="<%$ Resources:SR,RoundingProblemForSplittedInvoice %>" />
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Panel ID="pnNetVatGross" runat="server" CssClass="summary">
                <table class="netvatgross">
                    <tbody>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalNetValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalVat"  runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalVatValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalInvoicePrice" runat="server" Text="<%$ Resources:SR,TotalInvoicePriceNetOfVat %>" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalInvoicePriceValue" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <uc:RepeatControl ID="PIRepeatControl" runat="server">
                <ItemTemplate>
                    <uc:PurchaseItemsTemplate ID="PurchaseItemsTemplateControl" runat="server" />
                </ItemTemplate>
            </uc:RepeatControl>
        </td>
    </tr>
    <%
        if (Controller.IsSetButtonsEnabled())
        {
    %>
    <tr>
        <td>
            <!-- NOTE: Just need post back to recalculate -->
            <asp:Button ID="btnSetToZero" runat="server" CausesValidation="false" Text="<%$ Resources:SR,SetToZero %>"
                OnClick="btnSetToZero_Click" />
            <asp:Button ID="btnSetToDefault" runat="server" CausesValidation="false" Text="<%$ Resources:SR,SetToDefault %>"
                OnClick="btnSetToDefault_Click" />
        </td>
    </tr>
    <%
        }
    %>
</table>
