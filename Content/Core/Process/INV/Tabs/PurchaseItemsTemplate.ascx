﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItemsTemplate.ascx.cs" 
    Inherits="Eproc2.Web.Content.Core.Process.INV.Tabs.PurchaseItemsTemplate" %>
<%@ Register Assembly="Eproc2.Web.Framework" Namespace="Eproc2.Web.Framework.Controls.Grid"
    TagPrefix="cc" %>
<table width="100%" height="*">
    <tr>
        <td class="bold">
            <qwc:Label ID="lbVatAt" runat="server" Text="<%$ Resources:SR,VatAt %>" CssClass="bold" /> <qwc:Label ID="lblVatRate" runat="server" Format="{0:F1}" CssClass="bold"/>%:
        </td>
    </tr>
    <tr>
        <td colspan="2">
           <cc:ControlledGrid ID="cgList" AllowSorting="True" SortExpression="RequestedDate" runat="server" KeyField="Id">
                <Columns>                                        
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" AllowSorting="False" SortExpression="None" DataField="Selected" EnableHighlight="true" />
                    <qwc:TextColumn DataField="ProductCode" SortExpression="ProductCode" HeaderTitle="<%$ Resources:BM_SR,ItemCode %>" />
                    <qwc:TextColumn DataField="Name" SortExpression="Name" HeaderTitle="<%$ Resources:BM_SR, ItemName %>" />
                    <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="UnitQuantity" HeaderTitle="<%$ Resources:BM_SR,UnitQuantity %>" />
                    <qwc:TextColumn DataField="UnitIdObjectCode" SortExpression="UnitIdObjectCode" HeaderTitle="<%$ Resources:BM_SR,UnitOfMeasure %>" />
                    <qwc:DecimalColumn DataField="InvoicePrice" SortExpression="InvoicePrice" HeaderTitle="<%$ Resources:BM_SR,NetPrice %>" />
                    <qwc:DateTimeColumn DataField="RequestedDate" SortExpression="RequestedDate" HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" />
                    <qwc:TextColumn DataField="AppointmentTimes" SortExpression="None" AllowSorting="False" HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" />
                    <qwc:TextColumn DataField="ActualDateAndTimes" SortExpression="None" AllowSorting="False" HeaderTitle="<%$ Resources:BM_SR,ActualDateAndTimes %>" />
                    <qwc:DecimalColumn DataField="Quantity" SortExpression="Quantity" HeaderTitle="<%$ Resources:BM_SR,UnitsOrdered %>" />
                    <qwc:DecimalColumn DataField="QuantityToUse" SortExpression="QuantityToUse" HeaderTitle="<%$ Resources:BM_SR,UnitsReceived %>" /> <%-- TODO: ALI: is it correct? --%>                    
                    <%--<qwc:DecimalColumn DataField="TotalGross" HeaderTitle="<%$ Resources:BM_SR,TotalCost %>" /> --%>
                    <qwc:DecimalColumn DataField="TotalNet" SortExpression="TotalNet" HeaderTitle="<%$ Resources:BM_SR,NetTotal %>">
                      <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:DecimalColumn>
                </Columns>
            </cc:ControlledGrid> 
        </td>
    </tr>
    <%if(Controller.ShouldDisplayPrices)
 {%>
    <tr>
        <td align="right">
            <table class="netvatgross" >
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalNetValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalVat" Text="<%$ Resources:SR,TotalVAT %>" runat="server" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalVatValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <%if (!HasSingleVat())
                          {%>
                          <qwc:InfoLabel ID="InfoLabel1" Text="<%$Resources:SR,TotalGROSS %>" runat="server" />:
                           <%
                            }
                          else
                            {%>
                            <qwc:InfoLabel ID="lblTotalGross" Text="<%$Resources:BM_SR,TotalForThisOrder %>" runat="server" />:
                            <br />
                            <div class="totalGrossUnder"><asp:Literal Text="<%$ Resources:BM_SR,Total %>" ID="totalLiteral" runat="server" />&nbsp;
                            (<asp:Literal Text="<%$ Resources:BM_SR,Gross %>" ID="grossLiteral" runat="server" />)</div>                                
                            <%
                            }%>
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalGrossValue" runat="server" />
                              
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <%
 }%>
</table>
