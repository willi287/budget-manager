<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.INV.Tabs.Main" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="2a2e5e7c-b991-4b72-9a39-fb6f56b26dc0" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentNo" runat="server" Text="<%$ Resources:SR,DocumentCode %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblDocumentNoValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPONumberCaption" runat="server" Text="<%$ Resources:SR,PONumber %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblPONumberValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAmountToBePaid" runat="server" Text="<%$ Resources:SR,AmountToBePaid %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblAmountToBePaidValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPaymentDueDate" runat="server" Text="<%$ Resources:SR,PaymentDueDate %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblPaymentDueDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceCreationDate" runat="server" Text="<%$ Resources:SR,InvoiceCreationDate %>" />:
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblInvoiceCreationDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bold">
                        <qwc:InfoLabel ID="lblPreviousPayments" runat="server" Text="<%$ Resources:SR,PreviousPayments %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <qwc:ControlledGrid ID="cgPayments" runat="server" KeyField="Id" AllowSorting="false" SortExpression="None">
                            <Columns>
                                <qwc:TextColumn DataField="DocumentNo" SortExpression="None" HeaderTitle="<%$ Resources:SR,PaymentNo %>" />
                                <qwc:DateTimeColumn DataField="PaymentDate" SortExpression="None" HeaderTitle="<%$ Resources:SR,PaymentDate %>" />
                                <qwc:DecimalColumn DataField="Total" SortExpression="None" HeaderTitle="<%$ Resources:SR,AmountPaid %>" />
                            </Columns>
                        </qwc:ControlledGrid>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceDate" runat="server" Text="<%$ Resources:SR,InvoiceVatPoint %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblInvoiceDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDeliveryDate" runat="server" Text="<%$ Resources:SR,DeliveryDate %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblDeliveryDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDeliveryAddress" runat="server" Text="<%$ Resources:SR,DeliveryAddress %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblDeliveryAddressValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblContractName" runat="server" Text="<%$ Resources:SR,ContractName %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblContractNameValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblSupplySummary" runat="server" Text="<%$ Resources:SR,SupplySummary %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblSupplySummaryValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCaptionBuyer" Text="<%$ Resources:SR,Buyer %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBuyer" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="5" />
                    <td>
                        <qwc:Label ID="lblBuyerAddressLine" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblBuyerTownCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblBuyerPostCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td />
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCaptionSupplier" Text="<%$ Resources:SR,Supplier %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblSupplier" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" />
                    <td>
                        <qwc:Label ID="lblupplierAddressLine" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblSupplierTownCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblSupplierPostCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCaptionVat" Text="<%$ Resources:SR,VATNumber %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblVatNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <e2c:CurrentOwner ID="ucCurrentOwner" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
