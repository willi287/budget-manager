<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DRNotes.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.INV.Tabs.DRNotes" %>
<%@ Register Src="~/Content/Core/Process/WI/Tabs/Notes.ascx" TagName="Notes" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="29702ff5-fe63-4ba1-b82a-66d6a640a978" />
<br />
<table class="form">
    <tr>
        <td>
            <table class="bordered" style="height: 100%;">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDr" runat="server" Text="<%$ Resources:SR, DisputeReason %>" AssociatedControlID="ddlDr" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDr" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<e2w:Notes IsHelpBoxVisible="false" ID="ucNotes" runat="server" />
