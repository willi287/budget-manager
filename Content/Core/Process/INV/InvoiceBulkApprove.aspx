﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" CodeBehind="InvoiceBulkApprove.aspx.cs" Inherits="Eproc2.Web.Content.Core.Process.INV.InvoiceBulkApprove" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="InvoiceBulkApproveContent.ascx" TagName="Date" TagPrefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:Date ID="Content" runat="server" />
</asp:Content>
