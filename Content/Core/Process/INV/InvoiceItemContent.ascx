﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.INV.InvoiceItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<uc:PopupControl ID="ucPopupControl" runat="server" />
<!-- Need fake items to wrap ImgActionsRepeater actions with "WebFor_doPostBackWithOptions" -->
<asp:TextBox ID="tbFake" runat="server" Visible="false" />
<asp:CustomValidator ID="cvFake" runat="server" ControlToValidate="tbFake" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
