﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true"
    Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" Title="Untitled Page" %>
<%@ Register Src="~/Content/Core/Process/INV/InvoiceHistoryItemContent.ascx" TagName="ContentItem"
    TagPrefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="cntContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:ContentItem ID="ContentControl" runat="server" />
</asp:Content>
