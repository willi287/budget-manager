<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Register src="~/Controls/Core/FreeSearch.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="849535cb-f85a-4b6e-b23a-d75847fd7172" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server"  SortExpression="CreateTime" SortDirection="false" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn IsEditable="true"/>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Process/INV/InvoiceItem.aspx"   />
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,InvoiceCreationDate %>" DataField="InvoiceCreationDate" SortExpression="CreateTime" ShowTime="False" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PONumber %>" DataField="DocumentNo" SortExpression="DocumentNo"/>
         <qwc:BooleanTextColumn HeaderTitle="<%$ Resources:SR,Version %>" DataField="IsSpo" SortExpression="ParentId" TrueValue="<%$ Resources:SR, Supplementary %>" FalseValue="<%$ Resources:SR, Original %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name"/> 
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>" DataField="SupplierName"  SortExpression="supplier.Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PropertyAddress %>" DataField="Address" SortExpression="tAddress.Address" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InstallerName %>" DataField="InstallerName" SortExpression="installer.Name" />
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DeliveryDate %>" DataField="DeliveryDate" SortExpression="RequiredDelivery" ShowTime="False" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetValue %>" DataField="Total" SortExpression="None"/>         
    </Columns>
</qwc:ControlledGrid>
