﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" CodeBehind="InvoiceBulkDispute.aspx.cs" Inherits="Eproc2.Web.Content.Core.Process.INV.InvoiceBulkDispute" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="InvoiceBulkDisputeContent.ascx" TagName="Note" TagPrefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:Note ID="Content" runat="server" />
</asp:Content>
