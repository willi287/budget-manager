<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceBulkDisputeContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.INV.InvoiceBulkDisputeContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="d8da3eb1-e196-4ab4-a62c-ecd37f23920e" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm" style="width:  500px;">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDr" runat="server" Text="<%$ Resources:SR, DisputeReason %>" AssociatedControlID="ddlDr" />:
                    </td>
                    <td class="formData">
                        <qwc:DropDownList ID="ddlDr" DataValueField="Id" DataTextField="Value"
                            runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
