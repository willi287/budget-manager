﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceBulkApproveContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.INV.InvoiceBulkApproveContent" %>
<%@ Register TagPrefix="e2c" TagName="Calendar" Src="~/Controls/Core/Calendar.ascx" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="7b3f75d7-5798-44b8-af81-b89f035397e9" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm" style="width: 500px;">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblStartDate" runat="server" Text="<%$ Resources:SR, GroupingStartDate %>" AssociatedControlID="calStartDate" />:
                    </td>
                    <td class="formData">
                        <e2c:Calendar ID="calStartDate" runat="server" Required="True" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEndtDate" runat="server" Text="<%$ Resources:SR, GroupingEndDate %>" AssociatedControlID="calEndDate" />:
                    </td>
                    <td class="formData">
                        <e2c:Calendar ID="calEndDate" runat="server" Required="True" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>