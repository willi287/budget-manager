<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedDocuments.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.WI.Tabs.RelatedDocuments" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3a0e34e6-75b1-4046-bdf5-0fb526147d5d" ID="helpBox"/>
<qwc:ControlledGrid ID="cgDocumentsList" runat="server" KeyField="Id" SortExpression="None">
    <columns>
         <qwc:TreeColumn HeaderTitle="<%$ Resources:SR,Document %>" DataField="DocumentTypeName" AllowResources="true" NestedLevelField="NestedLevel" >
            <ItemCellStyle CssClass="treeColumn" />
         </qwc:TreeColumn>
         <qwc:HyperLinkColumn HeaderTitle="<%$ Resources:SR,DocumentCode %>" NavigateUrl="NavigateUrl"  DataField="DocumentCode" />
         <qwc:ResourceColumn AllowEmpty=true HeaderTitle="<%$ Resources:SR,State %>" DataField="WIStateId" /> 
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,Modified %>" DataField="Modified" SortExpression="None" />
    </columns>
</qwc:ControlledGrid>
