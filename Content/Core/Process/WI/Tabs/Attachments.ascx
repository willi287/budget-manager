<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attachments.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.WI.Tabs.Attachments" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="1ff26628-e657-4a10-8017-626f936384d9" />

<qwc:ImageActionButton ID="btnCreateAttachment" runat="server" OnCommand="attachmentAction_OnCommand" />

<qwc:ControlledGrid ID="cgAttachmentList" runat="server" KeyField="Id" AllowViewing="false" SortExpression="None" 
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" OnEditing="cgAttachmentList_OnEditing"  OnDeleting="cgAttachmentList_OnDeleting">
    <columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,FileName %>"/>
         <qwc:TextColumn DataField="Description" SortExpression="None" HeaderTitle="<%$ Resources:SR,Description %>" />
         <qwc:ClientClickLinkColumn DataField="Url" HeaderTitle="<%$ Resources:SR,Download %>" Caption="<%$ Resources:SR,Download %>" ImgUrl="ImgUrl"/>         
    </columns>
</qwc:ControlledGrid>
