<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttachmentItemDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.WI.Tabs.AttachmentItemDetail" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<uc:HelpBox runat="server" HelpBoxId="9776be5e-4bda-409d-8d56-4d8827104e35" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbFileName" runat="server" Text="<%$ Resources:SR,FileName%>" AssociatedControlID="tbFileNameValue" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFileNameValue" MaxLength="50" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbDescription" runat="server" Text="<%$ Resources:SR,Description%>" />:
                    </td>
                    <td>
                        <qwc:TextArea ID="tbDescriptionValue" runat="server" MaxLength="3000" TextMode="MultiLine"
                            Height="100px" Width="80%" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc1:FileUpload SelectFileText="<%$ Resources:SR,FileToUpload%>" MaxFileLength="4194304"
                            ID="fileUpload" UploadedFileName="" runat="server" 
                            FileFilter="Excel (*.xlsx, *.xls, *.csv)|*.xlsx;*.xls;*.csv|Graphic (*.gif, *.jpg, *.eps, *.bmp, *.png)|*.gif;*.jpg;*.eps;*.bmp;*.png|PDF (*.pdf)|*.pdf|Word Document (*.docx, *.doc)|*.docx;*.doc"/>
                    </td>
                </tr>
               
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
