<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notes.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.WI.Tabs.Notes" %>
<%@ Register Src="../../../../../Controls/Core/AddNote.ascx" TagName="note" TagPrefix="e2c" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox ID="ucHelpBox" runat="server" HelpBoxId="ba65e8a5-8b13-47ee-aca3-2467d8c480f7" />
<table class="main" cellSpacing="1" cellPadding="1" width="100%" border="0">
    <tr>
        <td>
            <qwc:ImageActionButton id="btnAddNote" runat="server" OnCommand="btnAddNote_Click" Caption="<%$ Resources:SR,AddNote %>"/>
            <qwc:ImageActionButton id="btnSaveNote" ValidationGroup="Note" runat="server" OnCommand="btnSaveNote_Click" Caption="<%$ Resources:SR,SaveNote %>"
                Visible="false"/>
        </td>                            
    </tr>    
</table>
<asp:Panel ID="pnlNewNote" runat="server" Width="100%">
    <e2c:note ID="cntrNewNote" runat="server"/>
</asp:Panel>
<table class="main" cellSpacing="1" cellPadding="1" width="100%" border="0">
    <tr>
        <td class="caption">
            <qwc:InfoLabel id="lblNotesLog" runat="server" Text="<%$ Resources:SR,NotesLog %>" />:
        </td>        
    </tr>
    <tr>
        <td>
            <asp:TextBox id="tbNotesLog" runat="server" TextMode="MultiLine" ReadOnly="True" Width="99%" Height="265"/>
        </td>        
    </tr>
</table>
