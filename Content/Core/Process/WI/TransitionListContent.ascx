<%@ Import namespace="Eproc2.Core.Entities"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransitionListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.WI.TransitionListContent" %>
<%@ Register src="../../../../Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="1c0e00a6-ba4a-4a96-896b-04657ca1d9a0" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="VisibleDocumentNo" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn IsEditable="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,DocumentCode %>" DataField="DocumentCode" SortExpression="VisibleDocumentNo"/>
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,DocumentType %>" DataField="DocumentTypeId" AllowSorting="false" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Action %>" DataField="ActionId" AllowSorting="false" />
    </Columns>
</qwc:ControlledGrid>
