<%@ Import Namespace="Eproc2.Web.Content.Core.Process.PY" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoteActionContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.WI.NoteActionContent" %>
<%@ Register src="../../../../Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<%@ Register Src="../../../../Controls/Core/AddNote.ascx" TagName="note" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5d05ad9b-7016-4127-8e0b-56b98f6c40ee" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<e2c:note ID="cntrNewNote" runat="server"/>
<uc:PopupControl ID="ucPopupControl" runat="server"/>
<asp:HiddenField ID="IsWarningShown" runat="server"/>

