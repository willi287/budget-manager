﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" CodeBehind="NoteAction.aspx.cs" Inherits="Eproc2.Web.Content.Core.Process.WI.NoteAction" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="NoteActionContent.ascx" TagName="Note" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:Note ID="Content" runat="server" />
</asp:Content>
