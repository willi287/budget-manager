﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumnetsListPageBase" Title="Untitled Page" %>
<%@ Register TagPrefix="ctl" TagName="TransitionList" Src="~/Content/Core/Process/WI/TransitionListContent.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
        <ctl:TransitionList ID="ContentControl" runat="server"></ctl:TransitionList>
</asp:Content>
