﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<%@ Register src="AttachmentItemContent.ascx" tagname="AttachmentItemContent" tagprefix="uc1" %>

<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:AttachmentItemContent ID="ContentControl" runat="server" />
</asp:Content>
