<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailsWizard.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.ROT.Tabs.DetailsWizard" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="98de5678-5205-4bfb-a222-85f89920a73d" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblBCG" runat="server" Text="<%$ Resources:SR, BuyerCatalogueGroup %>"
                            AssociatedControlID="ddlBCG" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBCG" DataValueField="Id" DataTextField="Value" runat="server"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblContract" runat="server" Text="<%$ Resources:SR, Contract %>" AssociatedControlID="ddlContract" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlContract" DataValueField="Id" DataTextField="Value" runat="server"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvContract" ControlToValidate="ddlContract" Display="Dynamic"
                            runat="server" ErrorMessage="*" InitialValue="00000000-0000-0000-0000-000000000000" />                    
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblTemplateType" runat="server" Text="<%$ Resources:SR, TemplateType %>"
                            AssociatedControlID="ddlTemplateType" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlTemplateType" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblTemplateName" runat="server" Text="<%$ Resources:SR, TemplateName %>"
                            AssociatedControlID="tbTemplateName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTemplateName" runat="server" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvTemplateName" ControlToValidate="tbTemplateName"
                            Display="Dynamic" runat="server" ErrorMessage="*" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
