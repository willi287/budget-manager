<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Catalogue.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.ROT.Tabs.Catalogue" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="4a06e049-ec82-4d57-97cd-837b4641203b" />
<table class="bordered">
    <tr>
        <td class="bold">
            <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, ChooseYourSupplier%>"
                AssociatedControlID="ddlSupplier" />:
        </td>
        <td>
            <qwc:DropDownList ID="ddlSupplier" AutoPostBack="true" DataTextField="Name" DataValueField="OrganisationId"
                ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                runat="server" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" />
        </td>
</table>
<br />
<qwc:GroupGrid ManualGrouping="true" ID="cgProductList" runat="server" AllowEditing="false" EnableStylishRow="false"
    AllowDeleting="false" GroupIdProperty="Id" GroupNameProperty="CategoryName" AllowViewing="true" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHighlight="true" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Contract/ContractProductView.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,Price %>" DataField="NetInvoicePrice" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,VatPercent %>" DataField="VatPercent" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" DataField="UnitOfMeasure" />
        <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
        <qwc:DecimalColumn DataField="Quantity" HeaderTitle="<%$ Resources:SR,Quantity %>">
            <Validators>
                <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
            </Validators>
        </qwc:DecimalColumn>
    </Columns>
</qwc:GroupGrid>
<asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:SR,SelectAdditionalProducts %>"
    OnClick="btnAdd_Click" />
