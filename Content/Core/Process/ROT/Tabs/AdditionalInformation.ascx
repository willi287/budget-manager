<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalInformation.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Process.ROT.Tabs.AdditionalInformationWizard" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="4db8674e-3b5c-4be0-90a9-b31919fe16dd" />
<asp:Table ID="tbTemplate" CssClass="bordered" runat="server" Visible="false">
    <asp:TableRow>
        <asp:TableCell>
            <qwc:InfoLabel ID="lblParentTemplate" CssClass="bold" runat="server" Text="<%$ Resources:SR, Template%>"
                AssociatedControlID="ddlParentTemplate" />
        </asp:TableCell>
        <asp:TableCell>
            <qwc:DropDownList ID="ddlParentTemplate" DataValueField="Id" DataTextField="Value"
                ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                runat="server" />
            <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                EnableClientScript="true" ID="rfvParentTemplate" ControlToValidate="ddlParentTemplate"
                Display="Dynamic" runat="server" ErrorMessage="*" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
