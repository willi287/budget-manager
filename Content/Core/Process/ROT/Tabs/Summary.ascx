<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.ROT.Tabs.Summary" %>
<%@ Import Namespace="Eproc2.Core.Logic.Helpers" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Process"%>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="317dd094-905b-41d0-89e6-f57dfe737d55" />
<table width="100%" height="*">
    <tr>
        <td>
            <table class="priceSummary" cellspacing="0">
                <tr>
                    <td>
                        <qwc:Label ID="lblTotalOrderTitle" Text="<%$ Resources:SR,TotalInvoicePriceNetOfVat %>"
                            runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalOrderValue" runat="server" />
                    </td>
                </tr>

            </table>
            <br />
            <asp:Repeater ID="repSummury" runat="server" OnItemDataBound="repSummury_ItemDataBound"
                OnItemCommand="repSummury_ItemCommand">
                <ItemTemplate>
                    <qwc:ControlledGrid ID="cgProductList" runat="server" SortExpression="Code"
                        AllowEditing="false" AllowDeleting="false" AllowViewing="false" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
                        KeyField="Id">
                        <Columns>
                            <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code"/>
                            <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="Name"/>
                            <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Supplier %>" DataField="SupplierName" SortExpression="tSupplierOrg.Name"/>
                            <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, Price %>" DataField="NetInvoicePrice" SortExpression="tPriceListItem.NetInvoicePrice"/>
                            <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,VatPercent %>" DataField="VatPercent" SortExpression="None"/>
                            <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" DataField="UnitOfMeasure" SortExpression="None"/>
                            <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None"/>
                            <qwc:DecimalColumn DataField="Quantity" IsEditable="true" HeaderTitle="<%$ Resources:SR,Quantity %>" SortExpression="None">
                                <Validators>
                                    <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                                </Validators>
                            </qwc:DecimalColumn>
                            <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NetTotal %>" DataField="NetTotal" SortExpression="None">
                                <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                            </qwc:TextColumn>
                        </Columns>
                    </qwc:ControlledGrid>
                    <table class="separator">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th width="225px">
                                <asp:Button ID="btnRecalculate" runat="server" UseSubmitBehavior="true" Text="<%$ Resources:SR,Recalculate %>" />
                            </th>
                            <th class="total">
                                &nbsp;
                            </th>
                        </tr>
                    </table>
                    <table class="netvatgross" >
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalNet" Text="<%$ Resources:SR,TotalNET %>" runat="server" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalNetValue" Format="<%#Controller.PIHelper.GetDisplayFormatString(PurchaseItemPropertyDataTypeEnum.Total) %>" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalVat" Text="<%$ Resources:SR,TotalVAT %>" runat="server" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalVatValue" Format="<%#Controller.PIHelper.GetDisplayFormatString(PurchaseItemPropertyDataTypeEnum.Total)%>" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalGross" Text="<%$ Resources:SR,TotalGROSS %>" runat="server" />:
                            </td>
                            <td>
                                <qwc:Label ID="lblTotalGrossValue" Format="<%#Controller.PIHelper.GetDisplayFormatString(PurchaseItemPropertyDataTypeEnum.Total) %>" runat="server" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
        </td>
    </tr>
</table>
