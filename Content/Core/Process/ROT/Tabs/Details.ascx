<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.ROT.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="16354039-9c0a-4cca-a6df-bd18a5e163a0" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblBCG" runat="server" Text="<%$ Resources:SR, BuyerCatalogueGroup %>"
                            AssociatedControlID="lblBCGContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBCGContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblContract" runat="server" Text="<%$ Resources:SR, Contract %>" AssociatedControlID="lblContractContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblContractContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblTemplateName" runat="server"
                            AssociatedControlID="tbTemplateName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTemplateName" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
