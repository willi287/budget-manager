<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="8b52a1cf-91af-4ffe-a4cf-9e67426525ad" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="Name" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <%--qwc:SelectColumn /--%>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="NavigateUrl" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="Name"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,TemplateType %>" DataField="TemplateTypeName" AllowSorting="false" />
    </Columns>
</qwc:ControlledGrid>
