﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tracking.ascx.cs" Inherits="Eproc2.Web.Content.Core.Common.Tabs.Tracking" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="779b3179-8f03-4dae-9877-b8f7ad2bba11" />
<asp:Label ID="tbTextBox" runat="server" />

<qwc:ControlledGrid ID="cgTrackingList" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false" SortExpression="None" >
    <columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:DateTimeColumn DataField="ActionTime" HeaderTitle="<%$ Resources:SR,DateTime %>" ShowTime="true"  SortExpression="None" />
         <qwc:TextColumn DataField="ActorNameFull"    HeaderTitle="<%$ Resources:SR,User %>"  SortExpression="None" />
         <qwc:TextColumn DataField="ChangesText" HeaderTitle="<%$ Resources:SR,Changes %>"  SortExpression="None" />         
    </columns>
</qwc:ControlledGrid>
