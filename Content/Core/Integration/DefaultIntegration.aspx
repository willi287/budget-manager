﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" CodeBehind="DefaultIntegration.aspx.cs" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.DefaultIntegration" %>
<%@ Import Namespace="Eproc2.Web.Content.Core" %>
<%@ Register src="DefaultIntegrationContent.ascx" tagname="DefaultIntegrationContent" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:DefaultIntegrationContent ID="ContentControl" runat="server" />
</asp:Content>
