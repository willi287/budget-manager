﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncorrectBVContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Integration.IncorrectBVContent" %>
<table>
    <tr>
        <td class="bvIcon">
            <asp:Image ID="IncorrectBVImage" runat="server" />
        </td>
        <td>
            <h3>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:SR,IntegrationViewNA %>" />
            </h3>
            <br />
            <div class="details">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:SR,IntegrationIncorrectBVMainContent %>" />
                <br />
                <br />
                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:SR,PleaseSwitchToBuyerOrSupplier %>" />
            </div>
        </td>
    </tr>
</table>