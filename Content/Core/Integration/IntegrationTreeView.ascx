﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntegrationTreeView.ascx.cs" Inherits="Eproc2.Web.Content.Core.Integration.IntegrationTreeView" %>
<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:TreeView ID="tvContent" runat="server" ExpandDepth="1" ShowLines="True" SelectedNodeStyle-CssClass="selected" />
    </ContentTemplate>
</asp:UpdatePanel>
