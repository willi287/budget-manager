﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.Controls.ListPageBase" %>
<%@ Import Namespace="Eproc2.Web.Content.Core" %>
<%@ Register src="IncorrectBVContent.ascx" tagname="IncorrectBVContent" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:IncorrectBVContent ID="ContentControl" runat="server" />
</asp:Content>

<script runat="server">

    /// <summary>
    /// Select item in main menu
    /// </summary>
    public override Guid SystemId
    {
        get
        {
            return SystemIdentifier.INTEGRATION_SYSTEM_ID;
        }
    }

</script>
