<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultIntegrationContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Integration.DefaultIntegrationContent" %>
    <%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<table>
    <tr>
        <td class="bvIcon">
            <asp:Image ID="DefaultBVImage" runat="server" />
        </td>
        <td>
            <h3>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:SR,IntegrationModule %>" />
            </h3>
            <br />
            <div class="details">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:SR,IntegrationDescription %>" />
                <br />
                <br />
                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:SR,IntegrationHelp %>" /> 
            </div>
        </td>
    </tr>
</table>