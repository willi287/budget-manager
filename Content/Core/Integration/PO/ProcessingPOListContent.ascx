﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.PO.ProcessingPOListContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox runat="server" HelpBoxId="8B999C2E-B59B-487B-B533-570EE8B8D810" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="None">
    <Columns> 
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FileName %>"  DataField="FileName" SortExpression="FileName" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UploadedBy %>" DataField="UserName" SortExpression="PrincBase.Name"/>          
    </Columns>
</qwc:ControlledGrid>
