﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.IntegrtionPageBase" %>
<%@ Register TagPrefix="ctrl" TagName="ContentControl" Src="FailureDetailsContent.ascx" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <ctrl:ContentControl ID="ContentControl" runat="server" />
</asp:Content>
