﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.PO.ProcessedPOListContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="820A9ACB-4E49-4E72-87EB-AC3BBDC501C2" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="None">
    <Columns> 
        <qwc:HyperLinkColumn NavigateUrl="Url" HeaderTitle="<%$ Resources:SR,PONumber %>"  DataField="PoNumber" SortExpression="tCons_PurchaseOrder.VisibleDocumentNo" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InvoiceNumber %>" DataField="InvoiceNumber" SortExpression="tCons_PurchaseOrder.InitialSupplierInvoiceNumber"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FileName %>"  DataField="FileName" SortExpression="tJob.FileName"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UploadedBy %>"  DataField="UserName" SortExpression="PrincBase.Name"/>          
    </Columns>
</qwc:ControlledGrid>
