﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.IntegrtionPageBase"
    MasterPageFile="~/Masters/FullScreen.Master"  %>
<%@ Register TagPrefix="ctrl" TagName="ContentControl" Src="ValidateAddressContent.ascx" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ContentControl ID="ContentControl" runat="server" />
</asp:Content>
