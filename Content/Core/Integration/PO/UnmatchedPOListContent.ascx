﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.PO.UnmatchedPOListContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="566C6849-E258-495A-BEB2-CC2906E6500B" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="None">
    <Columns>
        <qwc:ButtonColumn Text="<%$ Resources:SR, Validate %>" HeaderTitle="<%$ Resources:SR, Action %>" OnClicked ="ValidateUnmatchedAddress" AllowSorting="false"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Buyer %>"                DataField="Buyer"  SortExpression="buyer.Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Supplier %>"             DataField="Supplier" SortExpression="supplier.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ContractName %>"         DataField="ContractName"  SortExpression="tCons_Contract.ContractName"/>    
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PONumber %>"             DataField="PoNumber"  SortExpression="VwPONumber"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InvoiceNumber %>"        DataField="InvoiceNumber" SortExpression="InvoiceNumber"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PropertyAddress %>"      DataField="PropertyAddress"  SortExpression="PropertyAddress"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NetTotal %>"             DataField="NetTotal"  SortExpression="NetTotal"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FileName %>"             DataField="FileName"  SortExpression="tJob.FileName"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UploadedBy %>"           DataField="UserName" SortExpression="PrincBase.Name"/>          
    </Columns>
</qwc:ControlledGrid>
