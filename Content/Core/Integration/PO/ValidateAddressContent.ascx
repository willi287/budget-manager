﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidateAddressContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Integration.PO.ValidateAddressContent" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="FF9FC6C8-E386-4130-B67F-BE5566FFCD33" />
<table class="form"> 
    <tr>
        <td colspan="2">
            <uc1:AddressControl ID="AddressControl" LocalSearchEnabled="true" runat="server" />
        </td>
    </tr>
</table>
