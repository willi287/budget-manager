﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Integration.PO.FailedPOListContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="4A001EFB-F9D0-4757-92AC-FF8DE66512D6" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="None">
    <Columns> 
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Integration/PO/FailureDetails.aspx" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,FileName %>"  DataField="FileName" SortExpression="FileName" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UploadedBy %>" DataField="UserName" SortExpression="PrincBase.Name" />  
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ErrorCode %>"  DataField="ErrorMessage" AllowSorting="false" SortExpression="None"/>                  
         <qwc:ClientClickLinkColumn DataField="Url" HeaderTitle="<%$ Resources:SR,Download %>" Caption="<%$ Resources:SR,Download %>" ImgUrl="ImgUrl"/>         
    </Columns>
</qwc:ControlledGrid>