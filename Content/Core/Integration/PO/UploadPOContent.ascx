﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadPOContent.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Integration.PO.UploadPOContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="FileUpload" Src="~/Controls/Silverlight/FileUpload.ascx" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="21B0EEB8-1BF6-43DF-8307-6B947C5BB7EB" />

<table class="form">
        <tr>
            <td valign="top">
                <table class="subForm">
                    <!-- NOTE: KEO: now Document Type is not used, so type is constant. It will be replaced with info from Job -->
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR,Type %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblPurchaseOrder" runat="server" Text="<%$ Resources:SR, UploadTypePurchaseOrder %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lbSelectFile" runat="server" Text="<%$ Resources:SR, SelectFile %>" />:
                        </td>
                        <td>
                        <uc:FileUpload maxfilelength="4194304" id="fileUpload" filefilter="XML (*.xml)|*.xml"
                                runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
