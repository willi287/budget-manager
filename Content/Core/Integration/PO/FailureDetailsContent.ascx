﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FailureDetailsContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Integration.PO.FailureDetailsContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="795E86FF-111B-4D33-9B09-AD85B61EB132" />

<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">                
                <tr >
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFileName" runat="server" Text="<%$ Resources:SR,FileName %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbFileName" runat="server" />
                    </td>                    
                </tr>
                <tr >
                    <td class="caption">
                        <qwc:InfoLabel ID="lblUploadedBy" runat="server" Text="<%$ Resources:SR,UploadedBy %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbUploadedBy" runat="server" />
                    </td>                    
                </tr>
                <tr >
                    <td class="caption">
                        <qwc:InfoLabel ID="lblErrorCode" runat="server" Text="<%$ Resources:SR,ErrorCode %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbErrorCode" runat="server" />
                    </td>                    
                </tr>
                <tr >
                    <td class="caption">
                        <qwc:InfoLabel ID="lblReason" runat="server" Text="<%$ Resources:SR,Reason %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbReason" runat="server" />
                    </td>                    
                </tr>
            </table>
        </td>        
    </tr>
</table>