﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Contract.ContractPageBase"%>
<%@ Register src="ContractItemContent.ascx" tagname="ContractItemContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ContractItemContent ID="ContentControl" runat="server" />
</asp:Content>
