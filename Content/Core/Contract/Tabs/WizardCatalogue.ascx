<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardCatalogue.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Contract.Tabs.WizardCatalogue" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="54a1b2c9-70a9-4cd6-a756-9e9f24e151f9" />
<div class="groupDiv" style="width: 70%;">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <qwc:InfoLabel CssClass="bold" ID="lblSupplier" runat="server" Text="<%$ Resources:SR,Supplier %>" />:&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td valign="bottom">
                    
                <qwc:DropDownList ID="ddlSupplier" AutoPostBack="true" DataTextField="Value" DataValueField="Id"
                        runat="server" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" 
                        ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
            </td>
            <td valign="bottom" width="10%" align="left">
                    <asp:Button ID="btnAddAllSupplierProducts" runat="server" Text="<%$ Resources:SR,AddAllSupplierProducts %>" OnClick="btnAddAllSupplierProducts_Click" />
                    <asp:Button ID="btnRemoveAllSupplierProducts" runat="server" Text="<%$ Resources:SR,RemoveAllSupplierProducts %>" OnClick="btnRemoveAllSupplierProducts_Click" />
            </td>
        </tr>
    </table>
</div>
    <asp:Label ID="lblNoProductsAvailableForSupplier" runat="server" CssClass="error" Text="<%$ Resources:ValidationSR,NoProductsAvailableForSupplier %>"></asp:Label>
<qwc:GroupGrid ManualGrouping="true" ID="cgProductList" runat="server" AllowEditing="false" AllowDeleting="false" 
    AllowViewing="true" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="ProductId"
       AllowPaging=true GroupIdProperty="Id" GroupNameProperty="CategoryName" >
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsContractProduct" EnableHighlight="true" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Catalog/ProductItemViewWithoutTree.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,Price %>" DataField="Price" SortExpression="Price" />
    </Columns>
</qwc:GroupGrid>
<asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:SR,SelectAdditionalProducts %>"
    OnClick="btnAdd_Click" />
