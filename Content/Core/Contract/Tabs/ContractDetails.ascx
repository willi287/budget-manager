<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Eproc2.Core.Logic.Contract" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Contract.Tabs.ContractDetails" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="cb8e21df-06c8-483a-a5e9-c339fa2a7f85" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <%
                    if (Data.ContractStateId == ContractBlo.STATE_ID_ARCHIVED)
                    {%>
                <tr>
                    <td class="warning">
                        <%=string.Format(SR.FormatContractArchived, Data.ArchivedDate)%>
                    </td>
                </tr>
                <%}%>
                <tr runat="server" ID="vwIdRow" visible="false">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" MaxLength="150" runat="server" />
                    </td>
                    <td colspan="2" />
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblContractName" AssociatedControlID="txtContractName" runat="server" Text="<%$ Resources:SR,ContractName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="txtContractName" runat="server" MaxLength="150" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" ControlToValidate="txtContractName"
                            EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblConsortium" AssociatedControlID="qlConsortium" runat="server" Text="<%$ Resources:SR,Consortium %>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:Label ID="qlConsortium" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblBcg" AssociatedControlID="qlBcg" runat="server" Text="<%$ Resources:SR,BuyerCatalogueGroup %>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:Label ID="qlBcg" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="caption">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR,Description %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" TextMode="MultiLine" Height="50" MaxLength="250" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblApprovalProcedure" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlApprovalProcedure" DataTextField="Value" DataValueField="Id"
                            runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
