<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Contract.Tabs.OrganisationDetails" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="111302e4-2dd4-43c5-bd5e-bc153d85bf18" />
<div class="groupDiv" style="width:50%">
    <table width="100%">
        <tr>
            <td class="caption">
                <qwc:InfoLabel ID="lblContractName" runat="server" Text="<%$ Resources:SR,ContractName %>" />:
            </td>
            <td class="formData">
                <qwc:Label ID="qlblContractName" runat="server" MaxLength="50" />
            </td>
        </tr>
    </table>
</div>
<qwc:ControlledGrid ID="cgOrganizationUsers" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="PrincipalId" SortExpression="None">
    <Columns>
        <qwc:CheckBoxColumn DataField="IsContractPrincipal" UseHeaderCheckBox="true" EnableHighlight="true" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, UserId %>" DataField="Login" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Name %>" DataField="Name" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Role %>" DataField="Role" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
