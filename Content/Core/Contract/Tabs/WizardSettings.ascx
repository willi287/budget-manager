<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardSettings.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Contract.Tabs.WizardSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e514edea-dc66-4704-afa9-20e9287e559c" />
<table width="70%">
    <tr runat="server" id="trWarning">
        <td colspan="2">
            <qwc:Label runat="server" Text="<%$ Resources:SR,NoSettingsWarning %>"/>
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,ContractName %>" />:
        </td>
        <td class="formData">
            <qwc:Label ID="txtContractName" runat="server" MaxLength="150" />
        </td>
    </tr>
    <%if (Controller.IsVisibleControl(ContractOrganisationEntity.PROP_SAGECODE))
      {%>
    <tr>
        <td valign="top" class="caption">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,SageNominalCode %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtNominalCode" MaxLength="8" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" ControlToValidate="txtNominalCode"
                            EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNominalCode" 
                ErrorMessage="*"  EnableClientScript="true" style="white-space: nowrap" Display="Dynamic" ValidationExpression="^\w{1,8}$" ToolTip="<%$ Resources:ValidationSR, SageNominalCodeValidation %>">
                </asp:RegularExpressionValidator>
        </td>
    </tr>
    <%}%>
    <%if (Controller.IsVisibleControl(ContractEntity.BUYER_CODES))
      {%>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode1" runat="server" Text="<%$ Resources:SR,OrganisationCode1 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode1" MaxLength="50" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode2" runat="server" Text="<%$ Resources:SR,OrganisationCode2 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode2" MaxLength="50" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode3" runat="server" Text="<%$ Resources:SR,OrganisationCode3 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode3" MaxLength="50" runat="server" />
        </td>
    </tr>
    <%}%>
</table>
