<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="Eproc2.Web.Content.Core.Contract.Tabs.Settings" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="c2ffc94e-7ba8-4de9-a569-5f700668fa6b" />
<table width="70%">
    <tr>
        <td class="caption">
            <qwc:InfoLabel runat="server" Text="<%$ Resources:SR,ContractName %>" />:
        </td>
        <td class="formData">
            <qwc:Label ID="txtContractName" runat="server" MaxLength="150" />
        </td>
    </tr>
   <%if (Controller.IsVisibleControl(ContractEntity.SAG_SAGE_CODES_VIEW))
      {%>
    <tr>
        <td valign="top" class="caption">
            <qwc:InfoLabel ID="Label1" runat="server" Text="<%$ Resources:SR,SageNominalCode %>" />:
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">
            <qwc:ControlledGrid ID="cgSageNominalCodes" runat="server" KeyField="Id"SortExpression="None" >
                <Columns>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Organisation %>" DataField="Name" SortExpression="None" />
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, OrganisationRole %>" DataField="BusinessRole" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR, SageNominalCode %>" DataField="SageCode" SortExpression="None" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <%}%>
    <%if (Controller.IsVisibleControl(ContractOrganisationEntity.PROP_SAGECODE))
      {%>
    <tr>
        <td valign="top" class="caption">
            <qwc:InfoLabel ID="lblNominalCode" runat="server" Text="<%$ Resources:SR,SageNominalCode %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtNominalCode" MaxLength="8" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                ControlToValidate="txtNominalCode" EnableClientScript="true" runat="server">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNominalCode"
                ErrorMessage="*" EnableClientScript="true" Style="white-space: nowrap" Display="Dynamic"
                ValidationExpression="^\w{1,8}$" ToolTip="<%$ Resources:ValidationSR, SageNominalCodeValidation %>">
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <%}%>
    <%if (Controller.IsVisibleControl(ContractEntity.BUYER_CODES))
      {%>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode1" runat="server" Text="<%$ Resources:SR,OrganisationCode1 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode1" MaxLength="50" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode2" runat="server" Text="<%$ Resources:SR,OrganisationCode2 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode2" MaxLength="50" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblBuyerCode3" runat="server" Text="<%$ Resources:SR,OrganisationCode3 %>" />:
        </td>
        <td>
            <qwc:TextBox ID="txtBuyerCode3" MaxLength="50" runat="server" />
        </td>
    </tr>
    <%}%>
</table>
