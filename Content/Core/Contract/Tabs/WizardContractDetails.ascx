<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardContractDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Contract.Tabs.WizardContractDetails" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="0492fe59-c2a7-4856-bb59-affd2bf819c6" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblContractName" AssociatedControlID="txtContractName" runat="server"
                            Text="<%$ Resources:SR,ContractName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td class="formData">
                        <qwc:TextBox ID="txtContractName" runat="server" MaxLength="150" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtContractName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="caption">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR,Description %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" MaxLength="250" TextMode="MultiLine" Height="50" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblConsortium" runat="server" Text="<%$ Resources:SR,Consortium %>"
                            AssociatedControlID="ddlConsortium" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlConsortium" AutoPostBack="true" DataTextField="Value" DataValueField="Id"
                            runat="server" OnSelectedIndexChanged="ddlConsortium_SelectedIndexChanged" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="ddlConsortium" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblBcg" runat="server" Text="<%$ Resources:SR,BuyerCatalogueGroup %>"
                            AssociatedControlID="ddlBcg" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBcg" DataTextField="Value" DataValueField="Id" runat="server"
                            AutoPostBack="false" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="ddlBcg" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblApprovalProcedure" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlApprovalProcedure" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
