<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractWizardContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Contract.ContractWizardContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
