﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Contract.ContractPageBase" Title="Product" %>
<%@ Register Src="~/Content/Core/Catalog/ProductItemViewContent.ascx" TagName="ProductItemViewContent" TagPrefix="uc1" %>

<asp:Content ID="ContractProductItemContent" ContentPlaceHolderID="cphMainContent" runat="server">
    
    <uc1:ProductItemViewContent ID="ProductView" runat="server" />
    
</asp:Content>
