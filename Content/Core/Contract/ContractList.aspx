﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Contract.ContractListPageBase" %>
<%@ Register src="ContractListContent.ascx" tagname="ContractListContent" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:ContractListContent ID="ContentControl" runat="server" />
</asp:Content>
