<%@ Import Namespace="Eproc2.Core.Entities.ListItems"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingArchiveContractListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Contract.PendingArchiveContractListContent" %>
<%@ Register assembly="Qulix.Web" namespace="Qulix.Web.Controls" tagprefix="qwc" %>
<%@ Register src="~/Controls/Core/Search.ascx" tagname="Search" tagprefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f4bd4c36-ddd3-4591-8cde-cd6bb0ee5b9a" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<qwc:ControlledGrid ID="cgContractList" runat="server" KeyField="Id" SortExpression="ContractName" ManualSource="true">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Contract/ContractItem.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ContractName %>" DataField="ContractName"
            SortExpression="ContractName" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="ContractStateId"
            SortExpression="ContractStateId" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ApprovalProcedure %>" DataField="ApprovalProcedure"
            SortExpression="Description" />
    </Columns>
</qwc:ControlledGrid>
