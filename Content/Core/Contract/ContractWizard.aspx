﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Wizard.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Contract.ContractPageBase" %>
<%@ Register src="ContractWizardContent.ascx" tagname="ContractWizardContent" tagprefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ContractWizardContent ID="ContractWizardContent1" runat="server" />
</asp:Content>
