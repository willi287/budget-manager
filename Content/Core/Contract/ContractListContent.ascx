<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Contract.ContractListContent" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="ec8c5d9f-4bcf-40d1-bed0-fa920d1f253b" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="ContractName" 
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/Core/Contract/ContractItem.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ContractName %>" DataField="ContractName"
            SortExpression="ContractName" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,State %>" DataField="ContractStateId"
            SortExpression="ContractStateId" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ApprovalProcedure %>" DataField="ApprovalProcedure"
            SortExpression="AppProc.Description" />
    </Columns>
</qwc:ControlledGrid>
