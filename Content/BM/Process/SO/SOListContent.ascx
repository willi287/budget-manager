<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.SO.SOListContent" CodeBehind="~/Content/BM/Process/SO/SOListContent.ascx.cs" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ae3d8ce5-0b7b-4527-a6cd-122c1f43bb41" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete%>" runat="server" />
<e2w:PopupControl ID="pcCreateTemplate" Text="<%$ Resources:BM_SR, SelectOneDocument%>" runat="server" />
<e2w:PopupControl ID="pcSelectDocument" Text="<%$ Resources:BM_SR, SelectDocument%>" runat="server" />

<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="SubmitDate" SortDirection="False" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:SelectColumn IsEditable="true" DataField="Selected" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DateSubmitted %>" DataField="SubmitDate" SortExpression="SubmitDate" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, SONo %>" DataField="VisibleDocumentNo" SortExpression="VisibleDocumentNo" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ServiceProviderName %>" DataField="SupplierName" SortExpression="supplier.Name" />
        <qwc:ImageColumn HeaderTitle="<%$ Resources:BM_SR, ServiceProviderLogo %>" DataField="SupplierLogo" Width="75" AlternativeImageUrl="SupplierLogo" BlankImageUrl="BlankImage" AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, StartDate %>" DataField="StartDate" IsEditable="false" SortExpression="StartDate" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, EndDate %>" DataField="EndDate" IsEditable="false" SortExpression="EndDate" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, TotalCost %>" DataField="GrossTotal" SortExpression="GrossTotal" />
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
