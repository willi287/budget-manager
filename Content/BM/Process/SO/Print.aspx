﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false"
    Inherits="Eproc2.Web.Content.Core.Process.PrintPageBase"
    Title="Untitled Page" %>
<%@ Register TagPrefix="ctrl" TagName="Print" Src="PrintContent.ascx" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <ctrl:Print ID="ContentControl" runat="server" />
</asp:Content>
