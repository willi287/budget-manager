<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardConfirmation.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardConfirmation" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ad21e5cc-fca6-4d9e-946d-a1e7e7d6ac25" />
<qwc:ControlledGrid ID="cgDocuments" SortExpression="None" ShowHeaderIfNoData="true" runat="server" KeyField="Id" SelectKey="">
    <Columns>
        <qwc:TextColumn DataField="CustomCode" ColumnType="String" HeaderTitle="<%$ Resources:SR, CustomCode%>"   SortExpression="None" />
        <qwc:TextColumn DataField="DocumentType" ColumnType="String" HeaderTitle="<%$ Resources:SR, DocumentType %>"   SortExpression="None" />
        <qwc:TextColumn DataField="ItemName" ColumnType="String" HeaderTitle="<%$ Resources:BM_SR, ItemName%>"   SortExpression="None" />
        <qwc:DateTimeColumn DataField="RequestedDate" HeaderTitle="<%$ Resources:BM_SR, RequestedDate%>" SortExpression="None"/>
        <qwc:DropDownColumn IsEditable="true" DataField="PaymentMethod" HeaderTitle="<%$ Resources:BM_SR, PaymentMethod%>" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>
<br>
<%if(Controller.IsSubmitOrderEnabled()){%>
   <table class="grey">
    <tr>
        <td>
            <qwc:QWCheckBox ID="chSubmitPo" runat=server Text="<%$ Resources:BM_SR, SubmitOrder  %>" />
        </td>
    </tr>
</table>
 <%}%>
