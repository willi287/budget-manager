<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardAdditionalInformation.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardAdditionalInformation" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="c322df47-1ac2-406e-be99-bfe171ccb078" />
<qwc:ControlledGrid ID="cgDocuments" SortExpression="None" ShowHeaderIfNoData="true"
    runat="server" KeyField="Id" SelectKey="">
    <Columns>
        <qwc:TextColumn DataField="CustomCode" SortExpression="None" ColumnType="String"
            HeaderTitle="<%$ Resources:BM_SR, DocRef%>" IsEditable="true">
            <ControlStyle CssClass="documentNo" />
            <Validators>
                <qwc:ColumnRegExpValidator ValidationExpression=".{0,50}" />
            </Validators>
        </qwc:TextColumn>
        <qwc:TextColumn DataField="DocumentId" SortExpression="None" ColumnType="String"
            HeaderTitle="">
            <HeaderCellStyle CssClass="hidden-column" />
            <ItemCellStyle CssClass="hidden-column documentId" />
        </qwc:TextColumn>
        <qwc:TextColumn DataField="DocumentType" SortExpression="None" ColumnType="String"
            HeaderTitle="<%$ Resources:BM_SR, DocType %>" />
        <qwc:TextColumn DataField="ItemName" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:BM_SR, ItemName%>" />
        <qwc:DateTimeColumn DataField="RequestedDate" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, RequestedDate%>" />
        <qwc:TextColumn ColumnType="String" DataField="AppointmentTimes" SortExpression="None"
            HeaderTitle="<%$ Resources: BM_SR, AppointmentTimes %>" />
    </Columns>
</qwc:ControlledGrid>
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="captionNowrap" valign="top">
                        <asp:Label ID="lblBuyerOrderNumber" runat="server" Text="<%$ Resources: BM_SR,BuyerOrderNumber %>" />
                    </td>
                    <td>
                        <qwc:TextBox runat="server" ID="tbBuyerOrderNumber" MaxLength="50" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<% if (Controller.IsAssesmentMeetingOptionsEnabled)
   {%>
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption" colspan="4">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources: BM_SR, AssessmentMeeting%>" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="captionNowrap" valign="top">
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources: BM_SR, DateAndTime %>" />
                    </td>
                    <td>
                        <e2c:calendar runat="server" id="calAssesmentDate" />
                        <qwc:SelectTime runat="server" ID="stAssesmentTime" />
                    </td>
                    <td class="captionNowrap" valign="top">
                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources: BM_SR, Location %>" />
                    </td>
                    <td>
                        <qwc:TextBox runat="server" ID="tbLocation" MaxLength="100" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<%}
   else
   {%>
<br />
<%}%>
<table class="form">
    <tr>
        <td>
            <table class="subForm grey">
                <tr>
                    <th class="caption">
                        <asp:Label ID="lblNotes" runat="server" Text="<%$ Resources: SR, Notes %>" />
                    </th>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <qwc:QWCheckBox ID="cbAttachNotesToPO" runat="server" Text="<%$ Resources:BM_SR, AttachNotesToPurchaseOrders %>"
                            Checked="true" />
                        &nbsp;
                        <qwc:QWCheckBox ID="cbAttachNotesToSO" runat="server" Text="<%$ Resources:BM_SR, AttachNotesToScheduleOrders %>"
                            Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption" width="30%">
                        <qwc:Label ID="lblSelectNote" runat="server" Text="<%$ Resources:BM_SR, SelectNotesTemplate %>"
                            AssociatedControlID="tbNewNote" />:
                    </td>
                    <td class="formData row">
                        <qwc:DropDownList ID="ddlNoteTemplate" DataTextField="Value" DataValueField="Id"
                            runat="server" />
                        <asp:Button ID="btnUpdateNote" ValidationGroup="Note" runat="server" Text="<%$ Resources: SR, Insert %>" />
                    </td>
                </tr>
                <tr>
                    <td class="formData" colspan="2">
                        <table class="skinny" width="100%">
                            <tr>
                                <td>
                                    <qwc:TextArea ID="tbNewNote" TextMode="MultiLine" Rows="7" runat="server" MaxLength="500"
                                        Width="640px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <% if (Controller.IsNotesLogEnabled)
               {%>
            <table class="subForm grey" style="height: 100%">
                <tr>
                    <th class="caption">
                        <asp:Label ID="lblNotesLog" runat="server" Text="<%$ Resources: SR, NotesLog %>" />
                    </th>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="formData" colspan="2" rowspan="3">
                        <table class="skinny" width="100%">
                            <tr>
                                <td>
                                    <qwc:TextBox ID="tbNotesLog" TextMode="MultiLine" Rows="11" runat="server" Style="width: 100%;"
                                        disabled="disabled" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}
               else
               {%>
            &nbsp;
            <%} %>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="subForm">
                <tr>
                    <th class="caption">
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources: SR, Attachments %>" />:
                    </th>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <qwc:QWCheckBox ID="cbAttachmentToPO" runat="server" Text="<%$ Resources:BM_SR, AttachToPurchaseOrder%>"
                            Checked="true" />
                        &nbsp;
                        <qwc:QWCheckBox ID="cbAttachmentToSO" runat="server" Text="<%$ Resources:BM_SR, AttachToScheduleOrder%>"
                            Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:ImageActionButton ID="btnCreateAttachment" runat="server" OnCommand="attachmentAction_OnCommand" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<qwc:ControlledGrid ID="cgAttachmentList" runat="server" KeyField="Id" AllowViewing="false"
    SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    OnEditing="cgAttachmentList_OnEditing" OnDeleting="cgAttachmentList_OnDeleting">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:SR,FileName %>" />
        <qwc:TextColumn DataField="Description" SortExpression="None" HeaderTitle="<%$ Resources:SR,Description %>" />
    </Columns>
</qwc:ControlledGrid>
