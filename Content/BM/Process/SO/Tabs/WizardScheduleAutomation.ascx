﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardScheduleAutomation.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardScheduleAutomation" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/ScheduleAutomationOptions.ascx" TagName="ScheduleAutomationOptions"
    TagPrefix="sa" %>
<uc:HelpBox runat="server" HelpBoxId="6deb6d96-8564-49f8-859f-fa8e50cb2ca6" />
<%if (DoesExistRepetingItemsInOrder)
  {%>
<qwc:ControlledGrid ID="cgDocuments" SortExpression="None" ShowHeaderIfNoData="true"
    runat="server" KeyField="Id">
    <Columns>
        <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHiddeReadonlyRow="true"
            IsEditable="true" EnableHighlight="true" />
        <qwc:TextColumn DataField="CustomCode" SortExpression="None" ColumnType="String"
            HeaderTitle="<%$ Resources:SR, CustomCode%>" />
        <qwc:TextColumn DataField="DocumentType" SortExpression="None" ColumnType="String"
            HeaderTitle="<%$ Resources:SR, DocumentType %>" />
        <qwc:TextColumn DataField="ItemName" SortExpression="None" ColumnType="String" HeaderTitle="<%$ Resources:BM_SR, ItemName%>" />
        <qwc:DateTimeColumn DataField="RequestedDate" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, RequestedDate%>" />
        <qwc:TextColumn ColumnType="String" DataField="AppointmentTimes" SortExpression="None"
            HeaderTitle="<%$ Resources: BM_SR, AppointmentTimes %>" />
        <qwc:TextColumn DataField="ScheduleAutomation" SortExpression="None" ColumnType="String"
            HeaderTitle="<%$ Resources:BM_SR, ScheduleAutomation%>" />
    </Columns>
</qwc:ControlledGrid>
<div style="text-align: right">
    <asp:Button ID="btUpdate" runat="server" Text="<%$ Resources:BM_SR, Update %>" OnClick="Update" />
</div>
<asp:Panel runat="server" ID="pAutomationOptions">
    <table class="form">
        <tbody>
            <tr>
                <td colspan="2">
                    <table>
                        <tbody>
                            <tr>
                                <td style="vertical-align: middle">
                                    <div class="row">
                                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources: BM_SR, AutomationOptions%>"
                                            CssClass="bold" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <sa:ScheduleAutomationOptions runat="server" ID="ctrlScheduleAutomationOptions" />
                </td>
                <td valign="top">
                    <table class="subForm">
                        <tbody>
                            <tr>
                                <td class="grey" style="width: 30%; vertical-align: middle">
                                    <div class="row">
                                        <qwc:QWCheckBox ID="cbAutoConfirmDN" CssClass="bold" runat="server" Text="<%$ Resources:SR, AutoConfirmDN %>" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
<%}
  else
  {%>
<asp:Label ID="lblSheduleAutomationSettingsAreNotAvailable" runat="server" Text="<%$ Resources: BM_SR, SheduleAutomationSettingsAreNotAvailable%>" />
<% }%>