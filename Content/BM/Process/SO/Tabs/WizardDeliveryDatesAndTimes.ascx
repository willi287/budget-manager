﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardDeliveryDatesAndTimes.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardDeliveryDatesAndTimes" %>
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/BM/DTS/DateTimeSelector.ascx" TagName="DateTimeSelector"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="99577c61-4294-45a1-918a-9709299e574f" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:BM_SR, CantDeleteSOPI %>" runat="server" />
<table class="form">
    <tr>
        <td>
            <table>
                <tr>
                    <td class="caption">
                        <qwc:Label runat="server" Text="<%$ Resources:BM_SR,ProviderName%>"
                            AssociatedControlID="lblProviderNameContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblProviderNameContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems"
                runat="server" AllowEditing="false" KeyField="FamilyId" AllowViewing="true"
                SortExpression="None" OnDeleting="DeletePurchaseItems">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="NavigateUrl" />
                    <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:BM_SR,ItemName %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None" />
                    <qwc:TextColumn DataField="UnitIdObjectCode" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="GrossPrice" HeaderTitle="<%$ Resources:BM_SR,UnitPrice %>" SortExpression="None" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,TotalVat %>" DataField="VatPercent" SortExpression="None" />
                    <qwc:TextColumn DataField="DateTimeAvailable" HeaderTitle="<%$ Resources:BM_SR,DateTimeAvailable %>" SortExpression="None">
                        <HeaderCellStyle Width="20%" />
                    </qwc:TextColumn>
                    <qwc:ExternalEditorColumn OnEdit="OnRequestedDateEdit" DataField="RequestedDate" ColumnType="Date"
                        HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" IsEditable="true" />
                    <qwc:TextColumn DataField="AppointmentTimes" HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="Ordered" HeaderTitle="<%$ Resources:BM_SR,TotalUnits %>" IsEditable="true" IsEditableDataField="IsNotScheduled" SortExpression="None">
                        <ItemCellStyle CssClass="short" />
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalGross" HeaderTitle="<%$ Resources:SR,TotalGROSS %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="FirstInvoiceCost" HeaderTitle="<%$ Resources:SR,ExpectedCostOfFirstInvoice %>" SortExpression="None" />
                    <qwc:ImageColumn HeaderCellStyle-CssClass="scheduledImage" DataField="ScheduledImageUrl" AlternativeImageUrl="ScheduledImageUrl" BlankImageUrl="ScheduledImageUrl" />
                </Columns>
            </qwc:ControlledGrid>
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" />
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button runat="server" Text="<%$ Resources:SR, Recalculate %>" OnClick="Recalculate" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <qwc:InfoLabel Text="<%$ Resources:BM_SR, TotalForThisOrder %>"
                                runat="server" />
                        </div>
                        <div class="totalGrossUnder">
                            <qwc:Label class="bold" Text="<%$ Resources:BM_SR, Total %>" runat="server" />
                            <br />
                            (<qwc:Label Text="<%$ Resources:BM_SR, gross %>" runat="server" />)
                        </div>
                    </td>
                    <td valign="top">
                        <qwc:Label ID="lblTotal" runat="server" />
                    </td>
                </tr>
                <% if (Controller.ShowScheduledTotals)
                   { %>
                <tr>
                    <td>
                        <div>
                            <qwc:InfoLabel ID="Label4" Text="<%$ Resources:BM_SR, ScheduledTotals %>" runat="server" />
                        </div>
                        <div class="totalGrossUnder">
                            <qwc:Label ID="Label5" class="bold" Text="<%$ Resources:BM_SR, Total %>" runat="server" />
                            <br />
                            (<qwc:Label ID="Label6" Text="<%$ Resources:BM_SR, gross %>" runat="server" />)
                        </div>
                    </td>
                    <td valign="top">
                        <qwc:Label ID="lblScheduledTotal" runat="server" />
                    </td>
                </tr>
                <% } %>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table>
                <tr>
                    <td>
                        <div class="grey" style="padding: 5px;"><qwc:Label ID="lblInvoiceCreationPeriod" runat="server" CssClass="bold" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<uc1:PopupTemplatedControl ID="dateTimeSelectorPopup" OnSubmit="OnScheduleSubmit" OnCancel="OnScheduleCancel" runat="server"
    CancelText="<%$ Resources:BM_SR,Close %>">
    <Title>
        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:BM_SR,DatesAndTimes %>" />
    </Title>
    <Content>
        <uc2:DateTimeSelector ID="DateTimeSelector1" runat="server" NoEndDateEnabled="true" />
    </Content>
</uc1:PopupTemplatedControl>
