﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssessmentMeeting.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.AssessmentMeeting" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="35df2502-bf9f-41bf-a580-1043106a5892" />
<table>
  <tr>
    <td>
      <qwc:Image ID="calendarIcon" runat="server" />
    </td>
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,AssessmentMeetingDateSection %>" CssClass="bold" ID="dateLabel" runat="server" />:
    </td>
    <td>
      <e2c:calendar ID="dateValue" runat="server"/>
    </td>
    <td width="10%">&nbsp;</td>
    <td>
      <qwc:Image ID="clockIcon" runat="server" />
    </td>    
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,AssessmentMeetingTimeSection %>" CssClass="bold" ID="timeLabel" runat="server" />:
    </td>
    <td>
      <qwc:SelectTime ID="timeValue" runat="server" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,AssessmentMeetingLocationSection %>" CssClass="bold" ID="locationLabel" runat="server" />:
    </td>
    <td>
      <qwc:TextBox ID="locationValue" runat="server" MaxLength="100"/>
    </td>
    <td colspan="4">&nbsp;</td>
  </tr>
</table>
<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
