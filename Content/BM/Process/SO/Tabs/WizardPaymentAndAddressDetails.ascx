<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardPaymentAndAddressDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardPaymentAndAddressDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="65435203-b1c9-45b7-970c-b4eeaa6f4ff5" />
<table class="form">
    <tr>
        <td class="caption">
            <qwc:Label ID="lblDeliveryAddress" runat="server" Text="<%$ Resources:SR,DeliveryAddress %>"
                AssociatedControlID="lblDeliveryAddressContent" />:
        </td>
        <td>
            <qwc:Label ID="lblDeliveryAddressContent" runat="server" />
        </td>
        <td>
            <asp:Button runat="server" ID="btAddDeliveryAddress" Text="<%$ Resources:BM_SR,AddNewAddress%>"
                OnCommand="OnExecuteAction" />
        </td>
        <td>
            <asp:Button runat="server" ID="btSelectDeliveryAddress" Text="<%$ Resources:BM_SR,SelectFromSavedAddresses%>"
                OnCommand="OnExecuteAction" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Label ID="lblDeliveryNotes" runat="server" Text="<%$ Resources:SR,DeliveryNote %>"
                AssociatedControlID="tbDeliveryNotes" />:
        </td>
        <td>
            <qwc:TextBox ID="tbDeliveryNotes" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250" />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <hr />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Label ID="lblInvoiceAddress" runat="server" Text="<%$ Resources:BM_SR,InvoiceAddress %>"
                AssociatedControlID="lblInvoiceAddressContent" />:
        </td>
        <td>
            <qwc:Label ID="lblInvoiceAddressContent" runat="server" />
        </td>
        <td>
            <asp:Button runat="server" ID="btAddInvoiceAddress" Text="<%$ Resources:BM_SR,AddNewAddress%>"
                OnCommand="OnExecuteAction" />
        </td>
        <td>
            <asp:Button runat="server" ID="btSelectInvoiceAddress" Text="<%$ Resources:BM_SR,SelectFromSavedAddresses%>"
                OnCommand="OnExecuteAction" />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <hr />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:Label runat="server" Text="<%$ Resources:BM_SR, DeliveryDetails%>"
                AssociatedControlID="lblInvoiceAddressContent" />:
        </td>
        <td colspan="3" />
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            <qwc:ControlledGrid ID="cgPurchaseItems" SortExpression="None" runat="server" AllowEditing="false"
                KeyField="FamilyId" AllowViewing="true">
                <Columns>
                    <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:BM_SR,ItemName %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="Ordered" HeaderTitle="<%$ Resources:BM_SR, OrderedUnits %>"
                        SortExpression="None" />
                    <qwc:DecimalColumn DataField="TotalGross" HeaderTitle="<%$ Resources:SR, Price%>"
                        SortExpression="None" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td colspan="2" />
        <td align="right" colspan="2">
            <table class="bold" style="width: 100%">
                <tr>
                    <td>
                        <qwc:Label runat="server" Text="<%$ Resources:SR,TotalNet%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalNetContent" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label runat="server" Text="<%$ Resources:SR,TotalVat%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalVatContent" Enabled="false" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label runat="server" Text="<%$ Resources:BM_SR,TotalInvoicePrice%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalInvoicePriceContent" runat="server" Enabled="false" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
