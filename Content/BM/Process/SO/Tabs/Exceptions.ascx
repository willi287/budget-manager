<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Exceptions.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.Exceptions" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="21097c34-4f77-4e5c-9c34-c7cad6d56352" />
<qwc:ControlledGrid ID="cgList" runat="server" ShowHeaderIfNoData="false" ShowMessageIfNoData="true" SortExpression="None"   
                    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ItemName %>" DataField="ItemName" SortExpression="None"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, AppointmentDate %>" DataField="AppointmentDate" SortExpression="None"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, AppointmentTimes %>" DataField="AppointmentTime" SortExpression="None"/>
		<qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Action %>" DataField="Action"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, ActionDate %>" DataField="ActionDate" SortExpression="None" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, UserName %>" DataField="PrincipalName" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>

<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
