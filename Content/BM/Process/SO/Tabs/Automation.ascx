﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Automation.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.Automation" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/ScheduleAutomationOptions.ascx" TagName="ScheduleAutomationOptions"
    TagPrefix="sa" %>
<uc:HelpBox runat="server" HelpBoxId="b45f42db-a156-4ac2-adbd-9642b1fb7512" />

<asp:Panel runat="server" ID="pAutomationOptions">
    <table>
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td class="soMainTabSectionHeader">
                                    <asp:Image runat="server" ID="imgAutomationOptions" />
                                </td>
                                <td class="soMainTabSectionHeader" width="100%">
                                    <asp:Label ID="lblAutomationOptions" runat="server" CssClass="bold" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td class="soMainTabSectionHeader">
                                    <asp:Image runat="server" ID="imgDocumentAutomation" />
                                </td>
                                <td class="soMainTabSectionHeader">
                                    <asp:Label ID="lblDocumentAutomation" runat="server" CssClass="bold" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <sa:ScheduleAutomationOptions runat="server" ID="ctrlScheduleAutomationOptions" />
                </td>
                <td valign="top">
                    <table class="subForm">
                        <tbody>
                            <tr>
                                <td class="grey" style="width: 30%;">
                                    <qwc:QWCheckBox ID="cbAutoConfirmDN" CssClass="bold" runat="server" Text="<%$ Resources:SR, AutoConfirmDN %>" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>