<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardSummary.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.WizardSummary" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="bed32d85-4c35-45c7-a36f-c6272b939530" />
<table class="form">
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems"
                runat="server" AllowEditing="false" KeyField="FamilyId" AllowDeleting="false" SortExpression="None">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="NavigateUrl" />
                    <qwc:TextColumn DataField="Name" HeaderTitle="<%$ Resources:SR,ProductName %>" SortExpression="None"/>
                    <qwc:DecimalColumn DataField="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>" SortExpression="None" />
                    <qwc:TextColumn DataField="UnitIdObjectCode" HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="GrossPrice" HeaderTitle="<%$ Resources:SR,UnitCost %>" SortExpression="None"/>
                    <qwc:DateTimeColumn DataField="RequestedDate" HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="AppointmentTimes" HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="PlannedActivity" SortExpression="None" IsEditableDataField="IsOneOfOrder" IsEditable="true" ColumnType="String" HeaderTitle="<%$ Resources:BM_SR, PlannedActivity%>" MaxLength="30"/>
                    <qwc:ResourceColumn DataField="PaymentMethod" HeaderTitle="<%$ Resources:BM_SR, PaymentMethod%>" />
                    <qwc:DecimalColumn DataField="Ordered" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,OrderedUnits %>">
                        <ItemCellStyle CssClass="short" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalGross" SortExpression="None" HeaderTitle="<%$ Resources:SR,TotalGROSS %>" />
                    <qwc:ImageColumn HeaderTitle="" DataField="ScheduledImageUrl" AlternativeImageUrl="ScheduledImageUrl" BlankImageUrl="ScheduledImageUrl"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tr>
                    <td>
                        <div>
                            <qwc:InfoLabel ID="Label1" Text="<%$ Resources:BM_SR, TotalForThisOrder %>"
                                runat="server" /></div>
                        <div class="totalGrossUnder">
                            <qwc:Label ID="Label2" class="bold" Text="<%$ Resources:BM_SR, Total %>" runat="server" />
                            <br />
                            (<qwc:Label ID="Label3" Text="<%$ Resources:BM_SR, gross %>" runat="server" />)
                        </div>
                    </td>
                    <td valign="top">
                        <qwc:Label ID="lblTotal" runat="server" />
                    </td>
                </tr>
                <% if (Controller.ShowScheduledTotals) { %>
                <tr>
                    <td>
                        <div>
                            <qwc:InfoLabel ID="Label4" Text="<%$ Resources:BM_SR, ScheduledTotals %>" runat="server" /></div>
                        <div class="totalGrossUnder">
                            <qwc:Label ID="Label5" class="bold" Text="<%$ Resources:BM_SR, Total %>" runat="server" />
                            <br />
                            (<qwc:Label ID="Label6" Text="<%$ Resources:BM_SR, gross %>" runat="server" />)
                            </div>                    
                    </td>
                    <td valign="top">
                        <qwc:Label ID="lblScheduledTotal" runat="server" />
                    </td>
                </tr>
                <% } %>
            </table>
        </td>
    </tr>
</table>
