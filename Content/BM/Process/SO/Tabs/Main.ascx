﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls.Validators" TagPrefix="qwc" %>
<uc:HelpBox runat="server" HelpBoxId="b90df00b-6b5a-4e2e-a5b0-0e6cd58f5409" />
<uc2:InfoBox ID="infoBox" runat="server" />
<br />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblTemplateName" Text="<%$ Resources:SR, TemplateName %>" runat="server"
                            AssociatedControlID="tbTemplateName" />
                        <qwc:Label ID="lblDocumentNumber" Text="<%$ Resources:BM_SR, DocumentNumber %>" runat="server"
                            AssociatedControlID="tbDocumentNumber" Visible="false" />
                    </td>
                    <td>
                        <qwc:TextBox ID="tbTemplateName" runat="server" MaxLength="30" />
                        <qwc:TextBox ID="tbDocumentNumber" runat="server" MaxLength="50" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label Text="<%$ Resources:BM_SR, BuyerOrderNumber %>" runat="server"
                            AssociatedControlID="tbBuyerOrderNumber" />
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBuyerOrderNumber" runat="server" MaxLength="50" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td />
                    <td>
                        <qwc:Label Text="<%$ Resources:BM_SR, TotalUnits %>" ID="lblTotalUnits" CssClass="bold"
                            runat="server" />:
                        <qwc:Label ID="lblTotalUnitsValue" CssClass="bold" runat="server" />
                    </td>
                    <td>
                        <qwc:Label Text="<%$ Resources:BM_SR, NextOrderCost %>" ID="lblNextOrderCost" CssClass="bold"
                            runat="server" />
                        <qwc:Label ID="lblNextOrderCostValue" CssClass="bold" runat="server" />
                        <qwc:Label Text="<%$ Resources:BM_SR, NoOrdersInPeriod %>" ID="lblNoOrdersInPeriod" CssClass="bold"
                            runat="server" Visible="False"/>
                    </td>
                </tr>
                <tr>
                    <td />
                    <td>
                        <qwc:Label Text="<%$ Resources:BM_SR, TotalScheduleCost %>" ID="totalOrderCostLabel"
                            CssClass="bold" runat="server" />:
                        <qwc:Label ID="totalOrderCostValue" CssClass="bold" runat="server" />
                    </td>
                    <td />
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="dateTimeSelector">
    <table width="100%" cellspacing="0" cellpadding="0">
        <col width="40%" />
        <col width="10%" />
        <col width="50%" />
        <tr>
            <td class="soMainTabSectionHeader">
                <asp:Image ID="calendarIconOfStartDate" ImageAlign="AbsMiddle" runat="server" />
                <qwc:Label Text="<%$ Resources:BM_SR, StartDateSection  %>" ID="startDateLabel" CssClass="bold"
                    runat="server" />
            </td>
            <td>
                &nbsp;
            </td>
            <td class="soMainTabSectionHeader">
                <asp:Image ID="calendarIconOfEndDate" ImageAlign="AbsMiddle" runat="server" />
                <qwc:Label Text="<%$ Resources:BM_SR, EndDateSection %>" ID="endDateLabel" CssClass="bold"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td class="gray panels soMainTabSectionContent startDate">
                <e2c:Calendar ID="calStartDate" class="state_field" runat="server" />
            </td>
            <td>
                <asp:Image ID="imgStartToEndDateArrow" runat="server" />
            </td>
            <td class="gray panels soMainTabSectionContent">
                <table class="endDate">
                    <tr class="endCustom">
                        <td>
                            <qwc:RadioButton ID="rbEndDate" GroupName="EndDate" runat="server"
                                Text="<%$ Resources:BM_SR, EndDate %>" />
                        </td>
                    </tr>
                    <tr class="endCustom">
                        <td>
                            <e2c:Calendar ID="calEndDate" class="state_field" runat="server" />
                        </td>
                    </tr>
                    <tr class="endPeriod">
                        <td>
                            <qwc:RadioButton ID="rbEndAfter" GroupName="EndDate" runat="server"
                                Text="<%$ Resources:BM_SR, EndAfter %>" />
                        </td>
                    </tr>
                    <tr class="endPeriod">
                        <td>
                            <qwc:TextBox ID="tbEndPeriod" runat="server" MaxLength="3" CssClass="shortest state_field" />
                            <qwc:IntegerValidator ID="rvEndPeriod" runat="server" Text="*" ControlToValidate="tbEndPeriod"
                                ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                            <asp:Label ID="litPeriodDays" CssClass="daily" runat="server" Text="<%$ Resources:BM_SR, Days %>" />
                            <asp:Label ID="litPeriodWeeks" CssClass="weekly" runat="server" Text="<%$ Resources:BM_SR, Weeks %>" />
                            <asp:Label ID="litPeriodMonths" CssClass="monthly" runat="server" Text="<%$ Resources:BM_SR, Monthes%>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td class="soMainTabSectionHeader" colspan="3">
                <asp:Image ID="clockIcon" ImageAlign="AbsMiddle" runat="server" />
                <qwc:Label Text="<%$ Resources:BM_SR, AppointmentTimesSection %>" ID="appointmentTimesLabel"
                    runat="server" CssClass="bold" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0" class="appointment">
                    <col width="40%" />
                    <col width="10%" />
                    <col width="50%" />
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbIsTimeSpecified" GroupName="SpecificTime" runat="server" CssClass="isTimeSpecified"
                                Text="<%$ Resources:BM_SR, SpecificTimeIsRequired %>" />
                        </td>
                        <td>
                        </td>
                        <td>
                            <qwc:RadioButton ID="rbAnytime" GroupName="SpecificTime" runat="server" CssClass="anytime"
                                Text="<%$ Resources:BM_SR, Anytime %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="gray panels soMainTabSectionContent">
                            <qwc:Label Text="<%$ Resources:BM_SR, StartTime %>" ID="startTimeInnerLabel" runat="server"
                                Enabled="false" />
                            <br />
                            <qwc:SelectTime ID="stStartTime" runat="server" CssClass="startTime state_field"
                                AllowEmptyValue="true" />
                        </td>
                        <td>
                            <asp:Image ID="imgAppointmentTimesArrow" runat="server" />
                        </td>
                        <td class="gray panels soMainTabSectionContent">
                            <qwc:Label Text="<%$ Resources:BM_SR, EndTime %>" ID="endTimeInnerLabel" runat="server"
                                Enabled="false" />
                            <br />
                            <qwc:SelectTime ID="stEndTime" runat="server" CssClass="endTime state_field" AllowEmptyValue="true" />
                            <hr />
                            <qwc:Label Text="<%$ Resources:BM_SR, NumberOfHours %>" ID="numberOfHoursInnerLabel"
                                runat="server" Enabled="false" />
                            <br />
                            <qwc:SelectTime ID="stDuration" IsDurationMode="true" runat="server" CssClass="duration state_field" />
                            <hr />
                            <qwc:QWCheckBox ID="cbAllDay" Text="<%$ Resources:BM_SR, AllDay24Hours %>" runat="server"
                                CssClass="allDay state_field" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td class="soMainTabSectionHeader">
                <asp:Image ID="repeatingPatternIcon" ImageAlign="AbsMiddle" runat="server" />
                <qwc:Label Text="<%$ Resources:BM_SR, RepeatingPatternSection %>" ID="Label2" runat="server"
                    CssClass="bold" />
            </td>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr class="repeatingPattern">
            <td class="gray selectors soMainTabSectionContent selectors state_field">
                <qwc:GroupRadioButton ID="rbDaily" Text="<%$ Resources:BM_SR,Daily %>" runat="server"
                    CssClass="daily" AutoPostBack="false" GroupName="selectors" />
                <br />
                <qwc:GroupRadioButton ID="rbWeekly" Text="<%$ Resources:BM_SR,Weekly %>" runat="server"
                    CssClass="weekly" AutoPostBack="false" GroupName="selectors" />
                <br />
                <qwc:GroupRadioButton ID="rbMonthly" Text="<%$ Resources:BM_SR,Monthly %>" runat="server"
                    CssClass="monthly" AutoPostBack="false" GroupName="selectors" />
            </td>
            <td>
                <asp:Image ID="arrow" runat="server" />
            </td>
            <td class="gray panels soMainTabSectionContent">
                <asp:Panel ID="pDailyOptions" runat="server" CssClass="daily">
                    <div>
                        <qwc:GroupRadioButton ID="rbRepeatEveryDay" CssClass="state_field" Text="<%$ Resources:BM_SR,RepeatEvery %>"
                            runat="server" GroupName="daily" />
                        <qwc:TextBox ID="tbDailyPeriod" runat="server" CssClass="shortest state_field" MaxLength="3" />
                        <qwc:IntegerValidator ID="rvDailyPeriod" runat="server" Text="*" ControlToValidate="tbDailyPeriod"
                            ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                        <asp:Literal ID="litDays" Text="<%$ Resources:BM_SR,days %>" runat="server" />
                    </div>
                    <div>
                        <qwc:GroupRadioButton ID="rbRepeatEveryWeekday" CssClass="state_field" Text="<%$ Resources:BM_SR,RepeatEveryWeekday %>"
                            runat="server" GroupName="daily" />
                    </div>
                    <div>
                        <qwc:GroupRadioButton ID="rbRepeatEveryWeekend" CssClass="state_field" Text="<%$ Resources:BM_SR,RepeatEveryWeekend %>"
                            runat="server" GroupName="daily" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pWeeklyOptions" runat="server" CssClass="weekly">
                    <div>
                        <div class="bold">
                            <asp:Literal ID="litRepeatEvery" runat="server" Text="<%$ Resources:BM_SR,RepeatEvery %>" />
                            <qwc:TextBox ID="tbWeekPeriod" runat="server" CssClass="shortest state_field" MaxLength="2" />
                            <qwc:IntegerValidator ID="rvWeekPeriod" runat="server" Text="*" ControlToValidate="tbWeekPeriod"
                                ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                            <asp:Literal ID="litWeekOn" runat="server" Text="<%$ Resources:BM_SR,WeekOn %>" />
                        </div>
                        <asp:CheckBoxList ID="cblWeeklyPatern" CssClass="state_field" runat="server" RepeatDirection="Vertical" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pMonthlyOptions" runat="server" CssClass="monthly">
                    <div class="repeatEveryDayMonthly">
                        <qwc:GroupRadioButton ID="rbRepeatEveryDayMonthly" CssClass="state_field allDaysOfWeek"
                            runat="server" Text="<%$ Resources:BM_SR,day %>" GroupName="monthly" />
                        <qwc:TextBox ID="tbDayNumber" runat="server" CssClass="dayNumber shortest state_field"
                            MaxLength="2" />
                        <qwc:IntegerValidator ID="rvDayNumber" runat="server" Text="*" ControlToValidate="tbDayNumber"
                            ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                        <asp:Literal ID="litOfEvery1" runat="server" Text="<%$ Resources:BM_SR, ofEvery %>" />
                        <qwc:TextBox ID="tbMonthPeriod1" runat="server" CssClass="monthPeriod shortest state_field"
                            MaxLength="2" />
                        <qwc:IntegerValidator ID="rvMonthPeriod1" runat="server" Text="*" ControlToValidate="tbMonthPeriod1"
                            ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                        <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:BM_SR, monthPart %>" />
                    </div>
                    <qwc:Label ID="lblMonthlyAlert" CssClass="red hidden" runat="server" Text="<%$ Resources:BM_SR, MonthlyAlert%>" />
                    <asp:HiddenField ID="hfTemplate" Value="<%$ Resources:BM_SR, MonthlyAlert%>" runat="server" />
                    <br />
                    <div class="row repeatEveryXDayMonthly">
                        <qwc:GroupRadioButton ID="rbRepeatEveryXDayMonthly" CssClass="state_field" Text="<%$ Resources:BM_SR,the %>"
                            runat="server" GroupName="monthly" />
                        <qwc:DropDownListControllable ID="ddlNumber" runat="server" CssClass="dayNumber short state_field" />
                        <qwc:DropDownListControllable ID="ddlDayOfWeek" runat="server" CssClass="weekDay short state_field" />
                        <qwc:Label ID="litOfEvery" Text="<%$ Resources:BM_SR,ofEvery %>" runat="server" />
                        <qwc:TextBox ID="tbMonthPeriod2" runat="server" CssClass="monthPeriod shortest state_field"
                            MaxLength="2" />
                        <qwc:IntegerValidator ID="rvMonthPeriod2" runat="server" Text="*" ControlToValidate="tbMonthPeriod2"
                            ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                        <qwc:Label ID="litMonths" Text="<%$ Resources:BM_SR,months %>" runat="server" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <div class="DivNeedSave" style="visibility: hidden">
        <asp:HiddenField ID="hfNeedSave" Value="false" runat="server" />
    </div>
    <div class="DivDataChanged" style="visibility: hidden">
        <asp:HiddenField ID="hfDataChanged" Value="false" runat="server" />
    </div>
    <div class="DivAreAppointmentsModified" style="visibility: hidden">
        <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
    </div>
</div>
