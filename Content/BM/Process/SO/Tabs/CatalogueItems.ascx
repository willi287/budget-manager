﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueItems.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.CatalogueItems" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers"%>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.GridColumns" TagPrefix="ewc" %>
<uc:HelpBox runat="server" HelpBoxId="4c2bcad7-7245-4063-af69-867ec6df4b62" />

<qwc:ControlledGrid ID="cgList" AllowSorting="True" runat="server" SortExpression="RequestedDate" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>				
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ItemName %>" DataField="Name" SortExpression="Name" />
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitQuantity %>" DataField="UnitQuantity" SortExpression="UnitQuantity"/> 
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" DataField="UnitIdObjectCode" SortExpression="UnitIdObjectCode"/>
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitPrice %>" DataField="GrossPrice" SortExpression="GrossPrice"/>
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,VAT %>" DataField="VatPercent" SortExpression="VatPercent"/>		
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,DayTimeAvailable %>" DataField="DateTimeAvailable" SortExpression="None" AllowSorting="False">
            <HeaderCellStyle Width="20%" />            
        </qwc:TextColumn>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" DataField="RequestedDate" ShowTime="false" SortExpression="RequestedDate"/>		
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" DataField = "AppointmentTimes" SortExpression="None" AllowSorting="False"/>
		<%--<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,TotalUnits %>" DataField="Quantity" SortExpression="None"/>
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,ItemCostGross %>" DataField="TotalGross" SortExpression="None" />--%>
        <qwc:ImageColumn HeaderCellStyle-CssClass="scheduledImage" DataField="ScheduledImageUrl" AlternativeImageUrl="ScheduledImageUrl" BlankImageUrl="ScheduledImageUrl" />
    </Columns>
</qwc:ControlledGrid>

<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
