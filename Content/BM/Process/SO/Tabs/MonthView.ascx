<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.PrototypesViewBase" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ac9e6578-4a5b-4f49-96b4-78a5104a40a6" />
<table class="form">
    <tr>
        <td>
            <qwc:ControlledGrid ID="cgList" SortExpression="None" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
                KeyField="Id">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, Month %>" DataField="Date" DataFormatString="MMMM, yyyy" SortExpression="None"/>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Quantity %>" DataField="Quantity" SortExpression="None"/>
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, TotalCost %>" DataField="TotalCost" SortExpression="None"/>
                    <qwc:ImageColumn HeaderTitle="<%$ Resources:BM_SR, OrderIssued %>" DataField="OrderIssuedImageUrl" AlternativeImageUrl="OrderIssuedImageUrl" BlankImageUrl="OrderIssuedImageUrl" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="information bold" width="30%">
                <tr>
                    <td align="left" style="width: 80%">
                        <qwc:InfoLabel ID="lblTotalCost" Text="<%$ Resources:BM_SR, TotalCost %>" runat="server" />:
                    </td>
                    <td align="left" class="caption">
                        <qwc:Label ID="lblTotalCostContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
