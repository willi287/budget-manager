﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonthlyWeeklyView.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.MonthlyWeeklyView" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register assembly="Qulix.Web" namespace="Qulix.Web.Controls.Grids" tagprefix="cc1" %>
<%@ Register assembly="Qulix.Web" namespace="Qulix.Web.Controls.Grids.Columns" tagprefix="cc2" %>
<%@ Register assembly="Eproc2.Web.Framework" namespace="Eproc2.Web.Framework.Controls" tagprefix="cc3" %>
<%@ Register assembly="Qulix.Web" namespace="Qulix.Web.Controls" tagprefix="qwc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>

<uc:HelpBox runat="server" HelpBoxId="5E4EA600-40EF-45D8-81F1-6F3257C53A75" />            
<e2w:PopupControl ID="pcPreventDeleteLastAppointmentGroup" runat="server" />
<table class="form">
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAddAppointment" runat="server" Caption="<%$ Resources:SR, Add %>" OnCommand="btnAddAppointment_Click" />        
            <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="None" ShowHeaderIfNoData="false" ShowMessageIfNoData="false" 
                                ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:DateTimeColumn  DataField="Date" SortExpression="None" />                    
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Quantity %>" DataField="Quantity" SortExpression="None" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, TotalCost %>" DataField="TotalCost" SortExpression="None" />
                    <qwc:ImageColumn HeaderTitle="<%$ Resources:BM_SR, OrderIssued %>" DataField="OrderIssuedImageUrl" AlternativeImageUrl="OrderIssuedImageUrl" BlankImageUrl="OrderIssuedImageUrl" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="information bold" width="30%">
                <tr>
                    <td class="bold" align="left" style="width: 80%">
                        <qwc:InfoLabel ID="lblTotalCost" Text="<%$ Resources:BM_SR,TotalCost %>" runat="server" />:
                    </td>
                    <td align="left" class="caption">
                        <qwc:Label ID="lblTotalCostContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="DivNeedSave" style="visibility: hidden">    
    <asp:HiddenField ID="hfNeedSave" Value="false" runat="server" />
</div>

<div class="DivDataChanged" style="visibility: hidden">    
    <asp:HiddenField ID="hfDataChanged" Value="false" runat="server" />
</div>

<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
