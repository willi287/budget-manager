<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedDocuments.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.SO.Tabs.RelatedDocuments" %>
<%@ Register Src="~/Content/Core/Process/WI/Tabs/RelatedDocuments.ascx" TagName="RD"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="76de0203-62bc-46a4-8c2c-95e51f298960" />
<e2w:RD Id="ucRD" runat="server" ShowHelpBox="false"/>
<div class="DivAreAppointmentsModified" style="visibility: hidden">
    <asp:HiddenField ID="hfAreAppointmentsModified" Value="false" runat="server" />
</div>
