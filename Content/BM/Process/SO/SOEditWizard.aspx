﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Wizard.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" %>

<%@ Register src="SOEditWizardContent.ascx" tagname="SOEditWizardContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="cntSteps" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:SOEditWizardContent ID="ucSOEditWizard" runat="server" />
</asp:Content>

