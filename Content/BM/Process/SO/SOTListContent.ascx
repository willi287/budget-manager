<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SOTListContent.ascx.cs" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="0f08273e-04a4-4eaf-856d-4f553bc34131" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete%>" runat="server" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, User %>" DataField="User" SortExpression="None"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, TemplateName %>" DataField="TemplateName" SortExpression="Name" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="None"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ServiceProviderName %>" DataField="ServiceProvider" SortExpression="supplier.Name"/>
    </Columns>
</qwc:ControlledGrid>