﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.SO.SOWizardContentControlBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
