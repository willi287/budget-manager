<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false" Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" %>
<%@ Register Src="SOItemContent.ascx" TagPrefix="uc" TagName="SOItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:SOItemContent ID="sOItemContent" runat="server" />
</asp:Content>
