﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.WI.Tabs.AddressDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="b0c102cb-a495-43ef-83d7-ce6e2031080a" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDeliveryAddress" runat="server" Text="<%$ Resources:SR,DeliveryAddress %>" AssociatedControlID="lblDeliveryAddressContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDeliveryAddressContent" runat="server" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btAddDeliveryAddress" text="<%$ Resources:BM_SR,AddNewAddress%>" OnCommand="OnExecuteAction" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btSelectDeliveryAddress" text="<%$ Resources:BM_SR,SelectFromSavedAddresses%>" OnCommand="OnExecuteAction" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDeliveryNotes" runat="server" Text="<%$ Resources:SR,DeliveryNote %>" AssociatedControlID="tbDeliveryNotes" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDeliveryNotes" runat="server" TextMode="MultiLine" Rows="5" MaxLength="250"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblInvoiceAddress" runat="server" Text="<%$ Resources:BM_SR,InvoiceAddress %>" AssociatedControlID="lblInvoiceAddressContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblInvoiceAddressContent" runat="server" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btAddInvoiceAddress" text="<%$ Resources:BM_SR,AddNewAddress%>" OnCommand="OnExecuteAction" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btSelectInvoiceAddress" text="<%$ Resources:BM_SR,SelectFromSavedAddresses%>" OnCommand="OnExecuteAction" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
