<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" %>
<%@ Register Src="PoPrototypeItemContent.ascx" TagPrefix="uc" TagName="PoPrototypeItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
	<spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:PoPrototypeItemContent ID="pOPrototypeItemContent" runat="server" />
</asp:Content>
