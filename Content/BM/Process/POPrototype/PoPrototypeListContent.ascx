﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.POPrototype.PoPrototypeListContent"
    CodeBehind="~/Content/BM/Process/PoPrototype/PoPrototypeListContent.ascx.cs" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>

<e2w:PopupControl ID="pcPreventDeleteLastAppointment" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="38cd820e-ec40-4f14-98d6-b4bd1bc63f13" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<qwc:AccountInfoControl ID="pbaAccountControl" runat="server" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblTitle" runat="server" AssociatedControlID="lblDate" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDate" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <td>
            <qwc:ControlledGrid ID="cgList" SortExpression="DateOfAppointment" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
                KeyField="Id" OnEditing="cgList_Editing" OnViewing="cgList_Editing" OnDeleting="cgList_Deleting">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, Date %>" DataField="DateOfAppointment" SortExpression="DateOfAppointment" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, DayField %>" DataField="Day" SortExpression="None"
                        DataFormatString="dddd" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, TotalCost %>" DataField="TotalGross" SortExpression="TotalGross"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="information bold" width="30%">
                <tr>
                    <td align="left" style="width: 80%">
                        <qwc:InfoLabel ID="lblTotalCost" Text="<%$ Resources:BM_SR,TotalCost %>" runat="server" />:
                    </td>
                    <td align="left" class="caption">
                        <qwc:Label ID="lblTotalCostContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
