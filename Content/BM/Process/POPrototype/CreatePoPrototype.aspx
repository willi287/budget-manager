﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register Src="CreatePoPrototypeContent.ascx" TagPrefix="uc" TagName="CreatePoPrototypeContent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:CreatePoPrototypeContent ID="CreatePoPrototypeContent" runat="server" />
</asp:Content>
