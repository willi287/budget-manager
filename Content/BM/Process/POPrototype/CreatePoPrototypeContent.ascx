﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreatePoPrototypeContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.POPrototype.CreatePoPrototypeContent" %>
<%@ Register Src="~/Controls/Core/MultiDatesPicker.ascx" TagName="MultiDatesPicker" TagPrefix="qwc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="e124f825-d826-4749-8a5f-a37d01321358" />

<table class="form">
    <tbody>
        <tr>
            <td>
                <qwc2:MultiDatesPicker runat="server" ID="mdpAppointmentDates" />
            </td>
        </tr>
    </tbody>
</table>
