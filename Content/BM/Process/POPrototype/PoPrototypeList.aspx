<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumnetsListPageBase" %>
<%@ Register Src="PoPrototypeListContent.ascx" TagPrefix="uc" TagName="PoPrototypeListContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:PoPrototypeListContent ID="pOPrototypeListContent" runat="server" />
</asp:Content>
