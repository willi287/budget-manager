<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyView.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.POPrototype.Tabs.DailyView" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="27d24758-92c6-48ac-b50c-4abdeb5192c3" />
<table class="form">
    <tr>
        <td>
            <table>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDate" runat="server" Text="<%$ Resources:SR,Date %>" AssociatedControlID="lblDateContent" />:
					</td>
					<td>
                        <qwc:Label ID="lblDateContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<table class="form">
    <tr>
        <td colspan="2" valign="top" class="caption">
            <asp:Image ID="imgStartTimeClock" runat="server" />
            <qwc:Label ID="Label1" runat="server" Text="<%$ Resources:BM_SR, AppointmentTimesSection%>" AssociatedControlID="imgStartTimeClock" />
        </td>
    </tr>
    <tr>
        <td valign="top">
			<table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:RadioButton ID="rbIsTimeSpecified" GroupName="SpecificTime" Text="<%$ Resources:BM_SR, SpecificTimeIsRequired %>" AutoPostBack="true" OnCheckedChanged="IsTimeSpecifiedChanged" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="subForm grey">
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblStartTimeSpan" runat="server" Text="<%$ Resources:BM_SR,StartTime%>" AssociatedControlID="stStartTimeSpan" />:
                                </td>
                                <td>
                                    <qwc:SelectTime ID="stStartTimeSpan" AllowEmptyValue="true" AutoPostBack="true" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
		        <tr>
                    <td colspan="2">
                        <table class="subForm grey">
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblPlannedActivity" runat="server" Text="<%$ Resources:BM_SR,PlannedActivity%>" AssociatedControlID="tbPlannedActivity" />:
                                </td>
                                <td>
                                    <qwc:TextBox MaxLength="250" ID="tbPlannedActivity" runat="server" AutoPostBack="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:RadioButton ID="rbAnytime" GroupName="SpecificTime" Text="<%$ Resources:BM_SR, Anytime %>" AutoPostBack="true" OnCheckedChanged="IsTimeSpecifiedChanged" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="subForm grey">
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblEndTimeSpan" runat="server" Text="<%$ Resources:BM_SR,EndTime%>" AssociatedControlID="stEndTimeSpan" />:
                                </td>
							</tr>
							<tr>
                                <td>
                                    <qwc:SelectTime ID="stEndTimeSpan" AllowEmptyValue="true" runat="server" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption">
                                    <qwc:Label ID="lblDuration" runat="server" Text="<%$ Resources:BM_SR,NumberOfHours%>" AssociatedControlID="stNumberOfHours" />:
                                </td>
							</tr>
							<tr>
                                <td>
                                    <qwc:SelectTime ID="stNumberOfHours" runat="server" OnValueChanged="DurationChanged" IsDurationMode="true" AutoPostBack="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <qwc:QWCheckBox ID="cbAllDay" Text="<%$ Resources:BM_SR, AllDay24Hours %>" runat="server" AutoPostBack="true" OnCheckedChanged="AllDayChanged" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>