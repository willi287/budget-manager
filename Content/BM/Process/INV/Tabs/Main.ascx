﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.INV.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>

<uc:HelpBox runat="server" HelpBoxId="c9187d53-8b73-41b6-a7da-1e8fab8daa3d" />
<uc2:InfoBox ID="infoBox" runat="server" />
<br />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:BM_SR,PONumber %>" ID="poNumberLabel" runat="server" />:
                    </td>
    
                    <td>
                        <qwc:Label ID="poNumberValue" runat="server" />
                    </td>
                </tr>
  
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:BM_SR, AmountToBePaid %>" ID="amountToBePaidLabel" runat="server" />:
                    </td>
    
                    <td>
                        <qwc:Label ID="amountToBePaidValue" runat="server" />
                    </td>
                </tr>
  
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel Text="<%$ Resources:BM_SR, PaymentDueDate %>" ID="paymentDueDateLabel" runat="server" />:
                    </td>
    
                    <td>
                        <qwc:Label ID="paymentDueDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceCreationDate" runat="server" Text="<%$ Resources:SR,InvoiceCreationDate %>" />:
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblInvoiceCreationDateValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceDate" runat="server" Text="<%$ Resources:SR,InvoiceVatPoint %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblInvoiceDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblGroupingStartDate" runat="server" Text="<%$ Resources:SR,GroupingStartDate %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblGroupingStartDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblGroupingEndDate" runat="server" Text="<%$ Resources:SR,GroupingEndDate %>" />
                    </td>
                    <td class="formData">
                        <qwc:Label ID="lblGroupingEndDateValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<br />
<qwc:Label Text="<%$ Resources:BM_SR, PreviousPayments %>" ID="previousPaymentsLabel" CssClass="bold" runat="server" />:
<qwc:ControlledGrid ID="previousPaymentsGrid" SortExpression="None" KeyField="Id" AllowSorting="false" runat="server">
  <Columns>
    <qwc:TextColumn DataField="VisibleDocumentNo" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, PaymentNo %>" />
    <qwc:DateTimeColumn DataField="PaymentDate" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, Date %>" ShowTime="false" />
    <qwc:DecimalColumn DataField="Total" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, Amount %>" />
  </Columns>
</qwc:ControlledGrid>

<br />

<table>
  <tr>
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,PoDate %>" ID="poDateLabel" CssClass="bold" runat="server" />:
    </td>
    
    <td>
      <qwc:Label ID="poDateValue" runat="server" />
    </td>
  </tr>
  
  <tr>
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,ProviderVatNo %>" ID="providerVatNoLabel"  CssClass="bold" runat="server" />:
    </td>
    
    <td>
      <qwc:Label ID="providerVatNoValue" runat="server" />
    </td>
  </tr>
  
  <tr>
    <td>
      <qwc:Label Text="<%$ Resources:BM_SR,BuyerAccountRef %>" ID="buyerAccountRefLabel"  CssClass="bold" runat="server" />:
    </td>
    
    <td>
      <qwc:Label ID="buyerAccountRefValue" runat="server" />
    </td>
  </tr>
</table>
