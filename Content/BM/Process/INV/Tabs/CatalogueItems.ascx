﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueItems.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.INV.Tabs.CatalogueItems" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Assembly="Eproc2.Web.Framework" Namespace="Eproc2.Web.Framework.Controls"
    TagPrefix="uc" %>
<%@ Register Src="~/Content/Core/Process/INV/Tabs/PurchaseItemsTemplate.ascx" TagName="PurchaseItemsTemplate"
    TagPrefix="uc" %>


<uc:HelpBox runat="server" HelpBoxId="89dd7b39-1cde-4b7c-9a06-4b00c2e8e852" />
<table width="100%" height="*">
    <tr>
        <td>
            <asp:Label class="error" ID="lblInvoiceSplitted" runat="server" Visible="false" Text="<%$ Resources:SR,RoundingProblemForSplittedInvoice %>" />
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Panel ID="pnNetVatGross" runat="server">
                <table class="information">
                    <tbody>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                            </td>
                            <td class="caption">
                                <qwc:Label ID="lblTotalNetValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalVat"  runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                            </td>
                            <td class="caption">
                                <qwc:Label ID="lblTotalVatValue" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <qwc:InfoLabel ID="lblTotalInvoicePrice" runat="server" Text="<%$ Resources:BM_SR,TotalForThisOrder %>" />
                                &nbsp;<asp:Literal Text="<%$ Resources:BM_SR,TotalGrossInParenthesis %>" ID="totalLiteral" runat="server" />:                                
                            </td>
                            <td class="caption">
                                <qwc:Label ID="lblTotalInvoicePriceValue" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <uc:RepeatControl ID="PIRepeatControl" runat="server">
                <ItemTemplate>
                    <uc:PurchaseItemsTemplate ID="PurchaseItemsTemplateControl" runat="server" />
                </ItemTemplate>
            </uc:RepeatControl>
        </td>
    </tr>
    <%
        if (Controller.IsSetButtonsEnabled())
        {
    %>
    <tr>
        <td>
            <!-- NOTE: Just need post back to recalculate -->
            <asp:Button ID="btnSetToZero" runat="server" CausesValidation="false" Text="<%$ Resources:SR,SetToZero %>"
                OnClick="btnSetToZero_Click" />
            <asp:Button ID="btnSetToDefault" runat="server" CausesValidation="false" Text="<%$ Resources:SR,SetToDefault %>"
                OnClick="btnSetToDefault_Click" />
        </td>
    </tr>
    <%
        }
    %>
</table>
