<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentPageBase" %>
<%@ Register Src="InvoiceItemContent.ascx" TagPrefix="uc" TagName="InvoiceItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
<spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:InvoiceItemContent ID="invoiceItemContent" runat="server" />
</asp:Content>
