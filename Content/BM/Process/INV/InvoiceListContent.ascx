<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Import Namespace="Eproc2.Web.Content.Core.Process.INV" %>
<%@ Register src="~/Controls/Core/FreeSearch.ascx" tagname="Search" tagprefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="849535cb-f85a-4b6e-b23a-d75847fd7172" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="CreateTime" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>
         <qwc:SelectColumn IsEditable="true"/>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"/>
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,InvoiceCreationDate %>" DataField="InvoiceCreationDate" SortExpression="CreateTime" ShowTime="False" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,InvoiceNumber %>" DataField="InvoiceNumber" SortExpression="VisibleDocumentNo"/>
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,GroupingStartDate %>" DataField="GroupingPeriodStartDate" SortExpression="PurchOrder.GroupingPeriodStartDate" ShowTime="False"/>
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,GroupingEndDate %>" DataField="GroupingPeriodEndDate" SortExpression="PurchOrder.GroupingPeriodEndDate" ShowTime="False"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name"/> 
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>" DataField="SupplierName"  SortExpression="supplier.Name"/>
         <qwc:ImageColumn AllowPreview="true" HeaderTitle="<%$ Resources:BM_SR,ServiceProviderLogo %>" DataField="SupplierLogo" Width="75" AlternativeImageUrl="SupplierLogo" BlankImageUrl="BlankImage" AllowSorting="false" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,GrossValue %>" DataField="GrossTotal" SortExpression="GrossTotal"/>  
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Invoiced %>" DataField="Invoiced" AllowSorting="false" SortExpression="None"/>
         <qwc:BooleanIconColumn HeaderTitle="<%$ Resources:SR,CreditNote %>" AllowSorting="False" SortExpression="None" DataField="IsCreatedCreditNote" FalseIconUrlField="NotCreatedCreditNoteIconUrl" TrueIconUrlField="CreatedCreditNoteIconUrl" TrueIconTooltip="<%$ Resources:SR,CreditNoteCreated %>"/>
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
