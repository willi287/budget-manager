<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Register Src="../../../../Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5220ff83-d32f-4a8b-b122-b7638521554c" />
<div id="search-fullscreen">
    <e2w:Search ID="ucSearch" AdvancedEnabled=false runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table width="100%" height="*">
    <tr>
        <td align="left"  class="caption">
            <asp:Label ID="lblTickInvoices" runat="server" Text="<%$ Resources:SR,TickInvoicesToAddToExistingPayment %>" />
        </td>
    </tr>
    <tr>
        <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="VisibleDocumentNo" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
            <Columns>
                <qwc:SelectColumn IsEditable="true" />
                <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="NavigateUrl" />
                <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,DateCreated %>" DataField="InvoiceCreationDate"
                    SortExpression="CreateTime" ShowTime="False" />
                <qwc:HyperLinkColumn DataField="InvoiceNumber" HeaderTitle="<%$ Resources:BM_SR,InvoiceNumber %>"
                        NavigateUrl="NavigateUrl" SortExpression="VisibleDocumentNo" />
                <qwc:DateTimeColumn runat="server" HeaderTitle="<%$ Resources:SR,GroupingStartDate%>" DataField="GroupingPeriodStartDate" SortExpression="PurchOrder.GroupingPeriodStartDate" ShowTime="False" />
                <qwc:DateTimeColumn runat="server" HeaderTitle="<%$ Resources:SR,GroupingEndDate%>" DataField="GroupingPeriodEndDate" SortExpression="PurchOrder.GroupingPeriodEndDate" ShowTime="False" />
                <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,GrossValue %>" DataField="GrossTotal"
                    SortExpression="GrossTotal" />
                <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,InvoiceAddress %>" DataField="InvoiceAddress"
                    SortExpression="tAddress.Address" />
            </Columns>
        </qwc:ControlledGrid>
    </tr>
</table>
