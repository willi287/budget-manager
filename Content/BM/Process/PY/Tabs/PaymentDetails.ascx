﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentDetails.ascx.cs" Inherits="Eproc2.Web.Content.Core.Process.PY.Tabs.PaymentDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Content/Core/Process/PY/Tabs/PaymentDetailsTotalItem.ascx" TagName="InvoiceTotals" TagPrefix="uc1" %>

<uc:HelpBox runat="server" HelpBoxId="155ba9c9-7da7-4ea3-b01a-5a628d9a2fda" />
<table width="100%" height="*" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <qwc:ActionButtonRepeater ID="ActionsRepeater" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgInvoices"
                runat="server" AllowEditing="false" KeyField="InvoiceId" AllowViewing="false" >
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:ActionColumn>
                    <qwc:HyperLinkColumn DataField="VisibleDocumentNo" HeaderTitle="<%$ Resources:SR,InvoiceNumber %>"
                        NavigateUrl="NavigateUrl" />
                    <qwc:TextColumn DataField="POVisibleDocumentNo" HeaderTitle="<%$ Resources:BM_SR,PoNumber %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="ContractName" HeaderTitle="<%$ Resources:BM_SR,Individual %>" SortExpression="None"/>
                    <qwc:TextColumn DataField="PropertyAddress" HeaderTitle="<%$ Resources:SR,PropertyAddress %>" SortExpression="None" />
                    <qwc:DecimalColumn DataField="Total" HeaderTitle="<%$ Resources:SR,Net %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalVat" HeaderTitle="<%$ Resources:SR,VATwoPercent %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="GrossTotal" HeaderTitle="<%$ Resources:SR,Gross %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
            <table class="separator" id="tRecalculateSeparator" runat="server">
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <th width="225px">
                        <asp:Button ID="btnRecalculate" runat="server" Text="<%$ Resources:SR,Recalculate %>"
                            OnClick="btnRecalculate_Click" />
                    </th>
                    <th class="total">
                        &nbsp;
                    </th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tbody>
                    <qwc:RepeatControl ID="repInvTotal" runat="server">
                        <ItemTemplate>
                            <uc1:InvoiceTotals ID="InvoiceTotalsItem" runat="server" />
                        </ItemTemplate>
                    </qwc:RepeatControl>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 15px; height: 35px">
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" Caption="<%$ Resources:SR,Add %>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgCrN" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" AllowDeleting="false">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="IsSelectedForPayment" EnableHighlight="true">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                    </qwc:CheckBoxColumn>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:HyperLinkColumn IsEditable="false" DataField="VisibleDocumentNo" HeaderTitle="<%$ Resources:SR,CreditNoteNumber %>"
                        NavigateUrl="NavigateUrl" />
                    <qwc:TextColumn DataField="ContractIdObject.PrincipalIdObject.Name" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,Individual %>" />
                    <qwc:HyperLinkColumn DataField="InvoiceNumber" HeaderTitle="<%$ Resources:SR,ParentInvoiceNumber %>"
                        NavigateUrl="InvoiceNavigateUrl" />
                    
                    <qwc:DecimalColumn DataField="Total" HeaderTitle="<%$ Resources:SR,Net %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="TotalVat" HeaderTitle="<%$ Resources:SR,VATwoPercent %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="GrossTotal" HeaderTitle="<%$ Resources:SR,Gross %>" SortExpression="None">
                        <HeaderCellStyle CssClass="total"></HeaderCellStyle>
                        <ItemCellStyle CssClass="none"/>
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalCrN" CssClass="bold" runat="server" Text="<%$ Resources:SR,TotalCreditNotes %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNNetValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNVatValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalCrNGrossValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="netvatgross">
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblToPaid" CssClass="bold" runat="server" Text="<%$ Resources:SR,AmountToBePaid %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblToPaidNetValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblToPaidVatValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblToPaidGrossValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
