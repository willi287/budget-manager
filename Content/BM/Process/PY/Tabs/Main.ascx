﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.PY.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<uc:HelpBox runat="server" HelpBoxId="aa1f0503-6d62-4fb1-b1da-73f56f66741a" />
<uc2:InfoBox ID="infoBox" runat="server" />
<br />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,GroupedInvoiceGroupingStartDate%>"></qwc:InfoLabel>
                    </td>
                    <td>
                        <qwc:Label runat="server" ID="lblGroupedInvoiceGroupingStartDate"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,GroupedInvoiceGroupingEndDate%>"></qwc:InfoLabel>
                    </td>
                    <td>
                        <qwc:Label runat="server" ID="lblGroupedInvoiceGroupingEndDate"></qwc:Label>
                    </td>
                </tr>
                <tr id="paymentDueDateRow" runat="server">
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,PaymentDueDate%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPaymentDueDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,InvoicedAmount%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblInvoicedAmount" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,BalanceOutstanding%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblBalanceOutstanding" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel runat="server" Text="<%$ Resources:BM_SR,InvoiceAddress%>" />:
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <qwc:Label ID="lblInvoiceAddress" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <%if (Controller.IsChangeInvoiceAddressVisible(Data))
                                  {%>
                                <td>
                                    <asp:Button runat="server" ID="btAddInvoiceAddress" Text="<%$ Resources:BM_SR,AddNewAddress%>" OnCommand="OnExecuteAction" />
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btSelectInvoiceAddress" Text="<%$ Resources:BM_SR,SelectFromSavedAddresses%>" OnCommand="OnExecuteAction" />
                                </td>
                                <%} %>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDateCreated" runat="server" Text="<%$ Resources:SR,DateCreated %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateCreatedContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <e2c:CurrentOwner ID="ucCurrentOwner" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label10" Text="<%$ Resources:BM_SR,ProviderVatNo %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblVatNumber" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
