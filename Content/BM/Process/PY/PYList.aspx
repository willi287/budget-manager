﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumnetsListPageBase" %>
<%@ Register Src="PYListContent.ascx" TagPrefix="uc" TagName="PYListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:PYListContent ID="pyListContent" runat="server" />
</asp:Content>
