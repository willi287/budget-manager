﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="63382672-c3c3-4a6e-a0e3-fa15ee0d694e" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="SubmitDate" SortDirection="False">
    <Columns>
        <qwc:SelectColumn IsEditable="true"/>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />				
		<qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, SupplierName %>" DataField="SupplierName" SortExpression="supplier.Name"/>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, GroupingStartDate %>" DataField="GroupingStartDate" ShowTime="false" SortExpression="GroupingStartDate" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, GroupingEndDate %>" DataField="GroupingEndDate" ShowTime="false" SortExpression="GroupingEndDate" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, PaymentDueDate %>" DataField="PaymentDueDate" ShowTime="false" SortExpression="PaymentDueDate" />
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DateSubmitted %>" DataField="SubmitDate" ShowTime="false" SortExpression="SubmitDate" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, PaymentNumber %>" DataField="PaymentNumber" SortExpression="VisibleDocumentNo"/>
		<qwc:DecimalColumn HeaderTitle = "<%$ Resources:BM_SR, InvoicedAmount %>" DataField="InvoicedAmount" SortExpression="InvoicedAmount"/>
        <qwc:DecimalColumn HeaderTitle = "<%$ Resources:BM_SR, BalanceOutstanding %>" DataField="BalanceOutstanding" SortExpression="BalanceOutstanding"/>
        <qwc:BooleanIconColumn HeaderTitle="<%$ Resources:SR,CreditNote %>" AllowSorting="False" SortExpression="None" DataField="IsAppliedCreditNote" FalseIconUrlField="NotAppliedCreditNoteIconUrl" TrueIconUrlField="AppliedCreditNoteIconUrl" TrueIconTooltip="<%$ Resources:SR,CreditNoteApplied %>"/>
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
