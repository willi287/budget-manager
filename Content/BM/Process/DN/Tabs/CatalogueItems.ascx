﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueItems.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.DN.Tabs.CatalogueItems" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/BM/DTS/DateTimeSelector.ascx" TagName="DateTimeSelector" TagPrefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f961c0ee-db8c-422f-bd44-a1cd9a0b2cdf" />
<asp:PlaceHolder ID="phRecalculationWarning" runat="server" Visible="False" EnableViewState="False">
    <asp:Label ID="lblRecalculationWarningMsg" runat="server" CssClass="error"/>
</asp:PlaceHolder>
<table class="form">
    <tr>
        <td align="left" colspan="2">
            <qwc:ActionButtonRepeater ID="ActionsRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems" SortExpression="None"
                runat="server" AllowEditing="false" KeyField="Id" AllowViewing="false" AllowSorting="True" OnDeleting="cgPurchaseItems_OnDeleting">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHighlight="true"/>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />                    
                    <qwc:TextColumn DataField="ProductCode" SortExpression="ProductCode" HeaderTitle="<%$ Resources:BM_SR,ItemCode %>" />                                        
                    <qwc:TextColumn DataField="Name" SortExpression="Name" HeaderTitle="<%$ Resources:BM_SR,ItemName %>" />
                    <qwc:DecimalColumn DataField="UnitQuantity" SortExpression="UnitQuantity" HeaderTitle="<%$ Resources:SR,UnitQuantity %>"/>
                    <qwc:TextColumn DataField="UnitIdObjectCode" SortExpression="UnitIdObjectCode" HeaderTitle="<%$ Resources:BM_SR,UnitOfMeasure %>" />
                    <qwc:DecimalColumn DataField="QuantityToUse" SortExpression="QuantityToUse" HeaderTitle="<%$ Resources:SR,ToBeDelivered %>" /> <%-- PI.NotDelivered of PO!!!!!!! --%>
                    <qwc:DateTimeColumn DataField="RequestedDate" SortExpression="RequestedDate" HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" ShowTime="false" /> <%-- PO.DeliveryDate --%>
                    <qwc:TextColumn DataField="AppointmentTimes" HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" AllowSorting="False" SortExpression="None" />
                    <qwc:DecimalColumn DataField="QuantityDeliver" HeaderTitle="<%$ Resources:SR,Deliver %>" SortExpression="QuantityDeliver" />
                    <qwc:ExternalEditorColumn OnEdit="OnActualTimesEdit" SortExpression="None" AllowSorting="False" DataField="ActualTimes" HeaderTitle="<%$ Resources:BM_SR,ActualTimes %>" IsEditableDataField="IsScheduled" ColumnType="String" />
                    <qwc:TextColumn DataField="Quantity" HeaderTitle="<%$ Resources:SR,Quantity %>" SortExpression="Quantity"/>
                    <qwc:TextColumn DataField="QuantityShipped" HeaderTitle="<%$ Resources:SR,Shipped %>" SortExpression="QuantityShipped"/>
                    <qwc:DecimalColumn DataField="Accepted" HeaderTitle="<%$ Resources:SR,Accepted %>" SortExpression="Accepted">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="Damaged" HeaderTitle="<%$ Resources:SR,Damaged %>" SortExpression="Damaged">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="NotNeeded" HeaderTitle="<%$ Resources:SR,NotNeeded %>" SortExpression="NotNeeded">
                        <ControlStyle CssClass="short"/>
                        <Validators>
                            <qwc:ColumnDecimalValidator MinimumValue="0" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="NotDeliveredCalculatedVisibleValue" SortExpression="NotDeliveredCalculatedVisibleValue" HeaderTitle="<%$ Resources:SR,NotDelivered %>" /> <%-- PI. Quantity - PI. Accepted - PI. Not Needed - PI. Damaged --%>
                    <qwc:DecimalColumn DataField="Completed" SortExpression="Completed" HeaderTitle="<%$ Resources:SR,Completed %>" /> <%-- PI. Accepted + PI. NotNeeded --%>
                    <qwc:DecimalColumn DataField="InvoicePrice" SortExpression="InvoicePrice" HeaderTitle="<%$ Resources:SR,Price %>" />
                    <qwc:DecimalColumn DataField="TotalNet" SortExpression="TotalNet" HeaderTitle="<%$ Resources:BM_SR,NetTotal %>" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <asp:Button ID="btnRecalculate" runat="server" Text="<%$ Resources:SR,RefreshQuantities %>"
                OnClick="btnRecalculate_Click" Visible="False"/>
            <asp:Button ID="btnAcceptAll" runat="server" Text="<%$ Resources:SR,AcceptAll %>"
                OnClick="btnAcceptAll_Click" Visible="False"/>

        </td>
    </tr>
</table>
<uc1:PopupTemplatedControl id="PopupTemplatedControl1" onsubmit="OnScheduleSubmit" oncancel="OnScheduleCancel" runat="server" CancelText="<%$ Resources:BM_SR,Close %>">
    <Title>
        <asp:Literal runat="server" Text="<%$ Resources:BM_SR,DatesAndTimes %>" />
    </Title>
    <Content>
        <uc2:DateTimeSelector ID="DateTimeSelector1" runat="server" NoEndDateEnabled="true" />
    </Content>
</uc1:PopupTemplatedControl>