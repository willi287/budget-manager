<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.DN.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<uc:HelpBox runat="server" HelpBoxId="5bd4e625-e42f-4fca-ab9c-7397cee2aa9c" />
<uc2:InfoBox id="infoBox" runat="server" />

<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblCustomCode" runat="server" Text="<%$ Resources:BM_SR,DocumentCode %>"
                            AssociatedControlID="lblCustomCodeContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblCustomCodeContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblGrossTotal" runat="server" Text="<%$ Resources:BM_SR,TotalOrderCost%>"
                            AssociatedControlID="lblGrossTotalContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblGrossTotalContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>  
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblSupplierInvoiceNumber" runat="server" Text="<%$ Resources:BM_SR,SupplierInvoiceNo %>"
                            AssociatedControlID="tbSupplierInvoiceNumberContent" />:
                    </td>
                    <td>
                    
                        <qwc:TextBox ID="tbSupplierInvoiceNumberContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblSupplierDeliveryNoteNumber" runat="server" Text="<%$ Resources:BM_SR,SupplierDeliveryNoteNo %>"
                            AssociatedControlID="lblSupplierDeliveryNoteNumberContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblSupplierDeliveryNoteNumberContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblActualDeliveryDate" runat="server" Text="<%$ Resources:BM_SR,ActualDeliveryDate %>"
                            AssociatedControlID="calActualDeliveryDateContent" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calActualDeliveryDateContent" runat="server" />                        
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblRemarks" runat="server" Text="<%$ Resources:SR,Remarks %>" AssociatedControlID="tbRemarks" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbRemarks" runat="server" Rows="10" TextMode="MultiLine"/>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDateCreated" runat="server" Text="<%$ Resources:BM_SR,DateCreated %>" AssociatedControlID="lblPONumberContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDateCreatedContent" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblPONumber" runat="server" Text="<%$ Resources:BM_SR,PoNo %>" AssociatedControlID="lblPONumberContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblPONumberContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDeliveryDate" runat="server" Text="<%$ Resources:BM_SR,DeliveryDate %>"
                            AssociatedControlID="lblConfirmationDateContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblDeliveryDateContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblConfirmationDate" runat="server" Text="<%$ Resources:SR,ConfirmationDate %>"
                            AssociatedControlID="lblConfirmationDateContent" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblConfirmationDateContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <e2c:CurrentOwner ID="ucCurrentOwner" runat="server" />
                    </td>
                </tr>
            </table>
        </td> 
    </tr>
</table> 
