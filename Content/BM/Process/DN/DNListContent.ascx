﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="349b3e9e-be29-4be6-b2d0-b5da0753a6f3" /> 
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="SubmitDate" SortDirection="False">
    <Columns>
        <qwc:SelectColumn IsEditable="true"/>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />				
		<qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DateSubmitted %>" DataField="SubmitDate" ShowTime="false" SortExpression="SubmitDate"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, DNNo %>" DataField="VisibleDocumentNo" SortExpression="VisibleDocumentNo"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, SupplierName %>" DataField="SupplierName" SortExpression="supplier.Name"/>
		<qwc:ImageColumn AllowPreview="true" HeaderTitle="<%$ Resources:BM_SR,ServiceProviderLogo %>" DataField="SupplierLogo" Width="75" AlternativeImageUrl="SupplierLogo" BlankImageUrl="BlankImage" AllowSorting="false"  />		
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR, DeliveryDate %>" DataField="DeliveryDate" ShowTime="false" SortExpression="PurchOrder.DeliveryDate" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, InvoiceCode %>" DataField="InvoiceNo" SortExpression="InvoiceNo"  />
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, PONumber %>" DataField="PoNo" SortExpression="PoNo" />		
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
