<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseItems.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.CRN.Tabs.PurchaseItems" %>
<%@ Register src="CRNPurchaseItemsList.ascx" tagname="CRNPurchaseItemsList" tagprefix="uc1" %>

 <%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="86d4eb73-b184-4e8a-8b25-44d96ea7e186" />
<div>
    <table Width="50%" class="information">
    <%if (((IList)repPI.DataSource).Count > 1)
      {%>
      <tr>
        <td class="caption" style="width:80%" >
            <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNet%>" />:
        </td>
        <td >
			<qwc:Label ID="lblTotalNetContent" runat="server" />
        </td>		
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblTotalVat" runat="server" Text="<%$ Resources:SR,TotalVat%>" />:
        </td>
        <td >
			<qwc:Label ID="lblTotalVatContent" runat="server" />
        </td>		
    </tr>
    <tr>
        <td class="caption">            
            <qwc:InfoLabel ID="lblTotalInvoicePrice" runat="server" Text="<%$ Resources:BM_SR,TotalGrossTotalForThisOrder %>" />            
                          
            <%--<qwc:InfoLabel ID="lblTotalCost" runat="server" Text="<%$ Resources:BM_SR,TotalInvoicePrice%>" />:--%>
        </td>
        <td >
			<qwc:Label ID="lblTotalCostContent" runat="server" />
        </td>		
    </tr>
    </table>    
    <%} %>
    <table width="50%" class="information">
      <tr valign="top">
        <td class="caption" >
            <qwc:InfoLabel ID="Label3" Text="<%$ Resources:SR,GeneralDiscount %>" runat="server" />:
        </td>
        <td width="25%">
            <qwc:PriceTextBox ID="tbGeneralDiscount" runat="server" Width="50%"/>
        </td>
        <td>
            <qwc:PriceValidator ID="pvGeneralDiscount" runat="server" ErrorMessage="*" ControlToValidate="tbGeneralDiscount" ToolTip="<%$ Resources:ValidationSR,IncorrectData %>"/>
        </td>
        <td>
            <qwc:DropDownList ID="ddlVatPercent" runat="server" DataTextField="Value" DataValueField="Key" SortOrder="None" />
        </td>
        <td>
            <asp:Button ID="btRefresh" runat="server" Text="<%$ Resources:SR,Recalculate %>" OnClick="btRefresh_OnClick" />
        </td>
      </tr>   
    </table> 
</div>



<qwc:RepeatControl ID="repPI" runat="server">
    <ItemTemplate>
        <uc1:CRNPurchaseItemsList ID="CRNPIList" runat="server" />            
    </ItemTemplate>
</qwc:RepeatControl>

