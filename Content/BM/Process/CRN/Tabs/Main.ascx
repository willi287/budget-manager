<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.CRN.Tabs.Main" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>

<uc:HelpBox runat="server" HelpBoxId="b4a6c8a6-72da-4a04-ac5e-6fb2fd9d4a67" />

<uc2:InfoBox ID="infoBox" runat="server" />
<table width="100%" class="form">
    <tr>
        <td>
            <table class="subform">
                
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label10" Text="<%$ Resources:SR,GrossAmountToBeCredited %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalGross" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label5" Text="<%$ Resources:SR,CreatedBy %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblCreatedBy" runat="server" />
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="Label6" Text="<%$ Resources:BM_SR,SupplierInvoiceNumber %>" runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblSupplierInvoiceCode" runat="server" />
                    </td>
                </tr>
                
                 <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                 
              
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <table>
              <tr>
    <td class="caption">
      <qwc:InfoLabel Text="<%$ Resources:BM_SR,BuyerAccountRef %>" ID="buyerAccountRefLabel" runat="server" />:
    </td>
    
    <td>
      <qwc:Label ID="buyerAccountRefValue" runat="server" />
    </td>
  </tr>
            </table>
        </td>
        <td>
            <table>

                <tr>
                    <td class="caption grey">
                        <qwc:InfoLabel ID="lblVatNumberCaption" Text="<%$ Resources:BM_SR,ProviderVatNo %>" runat="server" />:
                    </td>
                    <td class="caption grey">
                        <qwc:Label ID="lblVatNumber" runat="server" />
                    </td>
                </tr>

            </table>
        </td>
    </tr>
   
</table>
