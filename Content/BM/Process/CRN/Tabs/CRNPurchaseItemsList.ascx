<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRNPurchaseItemsList.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.CRN.Tabs.CrNPurchaseItemsList" %>
<table width="100%">
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPurchaseItems" SortExpression="RequestedDate"
                runat="server" AllowDeleting = "false" AllowEditing = "false" KeyField = "Id" AllowViewing="false" AllowSorting="True">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHiddeReadonlyRow="true" EnableHighlight="true"/>
                    <qwc:TextColumn DataField="ProductCode"  SortExpression="ProductCode" HeaderTitle="<%$ Resources:BM_SR,ItemCode %>" />
                    <qwc:TextColumn DataField="Name"  SortExpression="Name" HeaderTitle="<%$ Resources:BM_SR,ItemName %>" />
                    
                    <qwc:DateTimeColumn DataField="RequestedDate"  SortExpression="RequestedDate" ShowTime="false" HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" />
                    <qwc:TextColumn DataField="AppointmentTimes" SortExpression="None" AllowSorting="False" HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" />
                    
                    
                    <qwc:DecimalColumn DataField="InvoicePrice"  SortExpression="InvoicePrice" HeaderTitle="<%$ Resources:BM_SR,NetPrice %>" />                    
                    <qwc:TextColumn DataField="UnitIdObjectCode"  SortExpression="UnitIdObjectCode" HeaderTitle="<%$ Resources:BM_SR,UnitOfMeasure %>" />
                    
                                                            
                    <qwc:DecimalColumn DataField="Quantity" SortExpression="Quantity"  HeaderTitle="<%$ Resources:BM_SR,TotalUnitsReceived %>"/>
                    <qwc:DateTimeColumn DataField="ActualDateAndTimes" AllowSorting="False"  SortExpression="None" ShowTime="false" HeaderTitle="<%$ Resources:BM_SR,ActualDateAndTimes %>" />
                    <qwc:DecimalColumn DataField="TotalNet"  SortExpression="TotalNet" HeaderTitle="<%$ Resources:BM_SR,NetTotal %>" />
                    <%--<qwc:DecimalColumn DataField="TotalGross"  SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,ItemCostIncludingVat %>" />--%>
                    
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td align="right">
            <table class="information" style="width:190px" >
                <tr>
                    <td align="left" style="width:80%" class="caption">
                        <qwc:InfoLabel ID="Label1" Text="<%$ Resources:SR,TotalNet %>" runat="server"/>:
                    </td>
                    <td align="left" >
                        <qwc:Label ID="lblTotalNet" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width:80%" class="caption">
                        <qwc:Label ID="lblVAT" runat="server" Format="<%$ Resources:SR,TotalVatAt %>"/>
                    </td>
                    <td align="left">
                        <qwc:Label ID="lblTotalVat" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width:80%" class="caption" >
                         <%if (PurchaseItemDataItem.IsMultipleVats)
                          {%>
                            <qwc:InfoLabel ID="lblTotalGrossShort" Text="<%$Resources:SR,TotalGROSS %>" runat="server" />:
                           <%
                            }
                          else
                            {%>
                            <div > 
                                <qwc:InfoLabel ID="lblTotalGross" Text="<%$Resources:BM_SR,TotalForThisOrder %>" runat="server" />:
                            </div>
                            
                            <div class="totalGrossUnder"><asp:Literal Text="<%$ Resources:BM_SR,Total %>" ID="totalLiteral" runat="server" />&nbsp;
                            (<asp:Literal Text="<%$ Resources:BM_SR,Gross %>" ID="grossLiteral" runat="server" />)</div>                                
                            <%
                            }%>
                    </td>
                    <td align="left">
			            <qwc:Label ID="lblTotalGrossContent" runat="server" />
			        </td>
			    </tr>
            </table>
        </td>
    </tr>
</table>
