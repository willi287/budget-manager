﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Process.DocumentsListContentBase" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="9b34a203-066a-4af3-8ac9-62190d45405c" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="SubmitDate" SortDirection="False"> 
    <Columns>
        <qwc:SelectColumn IsEditable="true"/>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />		
		<qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DateSubmitted %>" DataField="SubmitDate" ShowTime="false"  SortExpression="SubmitDate"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, CreditNoteNumber %>" DataField="VisibleDocumentNo" SortExpression="VisibleDocumentNo"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, SupplierName %>" DataField="SupplierName" SortExpression="supplier.Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, PaymentNo %>" DataField="GroupedInvoiceNo" SortExpression="tBM_Payment.VisibleDocumentNo" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, BuyerName %>" DataField="BuyerName" SortExpression="buyer.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, InvoiceNumber %>" DataField="InvoiceNo" SortExpression="VisibleDocumentNo" />
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, GrossAmount %>" DataField="GrossTotal" SortExpression="GrossTotal"/>
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
