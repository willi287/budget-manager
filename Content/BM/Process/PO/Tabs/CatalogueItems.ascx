﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueItems.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Process.PO.Tabs.CatalogueItems" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="c58651c5-96ae-4e13-bdc1-b8db702104e7" />
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:BM_SR, CantDeleteSOPI1 %>" runat="server" />
<table width="100%">
    <tr>
        <td>
            <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="RequestedDate" AllowSorting="True" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
                KeyField="Id" AllowViewing="false" AllowEditing="False" AllowDeleting="True" OnDeleting="cgList_Deleting">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="NavigateUrl" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ItemName %>" DataField="Name" SortExpression="Name" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitQuantity %>" DataField="UnitQuantity"
                        SortExpression="UnitQuantity" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" DataField="UnitIdObjectCode"
                        SortExpression="UnitIdObjectCode" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitPrice %>" DataField="InvoicePrice"
                        SortExpression="InvoicePrice" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,VAT %>" DataField="VatPercent" SortExpression="VatPercent" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,DayTimeAvailable %>" DataField="DateTimeAvailable"
                        SortExpression="None" AllowSorting="False">
                        <HeaderCellStyle Width="20%" />
                    </qwc:TextColumn>
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" DataField="RequestedDate"
                        ShowTime="false" SortExpression="RequestedDate" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" DataField="AppointmentTimes"
                        SortExpression="None" AllowSorting="False" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,TotalUnits %>" DataField="Quantity"
                        SortExpression="Quantity" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ToBeDelivered %>" DataField="NotDeliveredVisibleValue"
                        SortExpression="NotDeliveredVisibleValue" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetTotal %>" DataField="TotalNet"
                        SortExpression="TotalNet" />
                </Columns>
            </qwc:ControlledGrid>
            <div align="right">
                <table class="netvatgross">
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:SR,TotalNET %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalNetValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblTotalVat" runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblTotalVatValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel Text="<%$ Resources:BM_SR,TotalForThisOrder %>" ID="totalForThisOrderLiteral"
                                runat="server" />
                            <br />
                            <div class="totalGrossUnder">
                                <asp:Literal Text="<%$ Resources:BM_SR,Total %>" ID="totalLiteral" runat="server" />&nbsp;(<asp:Literal
                                    Text="<%$ Resources:BM_SR,Gross %>" ID="grossLiteral" runat="server" />)</div>
                        </td>
                        <td>
                            <qwc:Label ID="poTotalGrossLabel" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblIsPayPalPayment" runat="server" Text="<%$ Resources:BM_SR,PaidWithPayPal %>" />
                        </td>
                        <td>
                            <qwc:Label ID="lblIsPayPalPaymentValue" runat="server" />
                        </td>
                    </tr>
                    <%if (Controller.IsVisibleControl(BM_PurchaseOrderEntity.PROP_PAYPALTRANSACTIONID))
                      {%>
                    <tr>
                        <td>
                            <qwc:InfoLabel ID="lblPayPalTransactionId" runat="server" Text="<%$ Resources:BM_SR,PayPalTransactionId %>" />
                        </td>
                        <td>
                            <qwc:Label ID="lblPayPalTransactionIdValue" runat="server" />
                        </td>
                    </tr>
                    <% } %>
                </table>
            </div>
        </td>
    </tr>
</table>
