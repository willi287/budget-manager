<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.PO.Tabs.ConsolidatedPOHistory" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="85D3B713-7F2D-4C72-B707-9CFE00BD8749" />
<qwc:ControlledGrid ID="cgHistoryList" runat="server" KeyField="Id" SortExpression="None">
    <Columns>
        <qwc:HyperLinkColumn DataField="DocumentNo" HeaderTitle="<%$ Resources:SR,DocumentNo %>"
            NavigateUrl="Url" />
        <qwc:TextColumn DataField="ProductCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
        <qwc:TextColumn DataField="ProductName" SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
        <qwc:DecimalColumn DataField="UnitCost" SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitCost %>" />
        <qwc:DecimalColumn DataField="Ordered" SortExpression="None" HeaderTitle="<%$ Resources:SR,Ordered%>" />
        <qwc:DecimalColumn DataField="Completed" SortExpression="None" HeaderTitle="<%$ Resources:SR,Completed %>" />
        <qwc:DecimalColumn DataField="NotNeeded" SortExpression="None" HeaderTitle="<%$ Resources:SR,NotNeeded%>" />
        <qwc:DecimalColumn DataField="Awaited" SortExpression="None" HeaderTitle="<%$ Resources:SR,Awaited%>" />
        <qwc:ResourceColumn DataField="ChangeType" AllowEmpty="true" HeaderTitle="<%$ Resources:SR,ChangeType%>" />
        <qwc:DecimalColumn DataField="TotalNet" SortExpression="None" HeaderTitle="<%$ Resources:SR,TotalNet%>" />
        <qwc:DecimalColumn DataField="VATCode" SortExpression="None" HeaderTitle="<%$ Resources:SR,VATCode%>" />
        <qwc:DecimalColumn DataField="VatPercent" SortExpression="None" HeaderTitle="<%$ Resources:SR,VatPercent%>" />
        <qwc:DecimalColumn DataField="TotalGross" SortExpression="None" HeaderTitle="<%$ Resources:SR,TotalGross %>" />
    </Columns>
</qwc:ControlledGrid>
