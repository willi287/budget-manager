<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Process.PO.Tabs.Main" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="Spring.Web" Namespace="Spring.Web.UI.Controls" TagPrefix="spring" %>
<%@ Register Src="../../../../../Controls/Core/Address.ascx" TagName="Address" TagPrefix="e2c" %>
<%@ Register Src="../../../../../Controls/Core/Calendar.ascx" TagName="Calendar"
    TagPrefix="e2c" %>
<%@ Register Src="~/Content/Core/Process/Controls/CurrentOwner.ascx" TagName="CurrentOwner"
    TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/InfoBox.ascx" TagName="InfoBox" TagPrefix="uc2" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<uc:HelpBox runat="server" HelpBoxId="d3d5606b-4dca-4b0a-9ea6-af2e98ab2bdd" />
<uc2:InfoBox ID="infoBox" runat="server" />
<br />
<table class="form grey">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblVersion" runat="server" Text="<%$ Resources:SR,Version %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblVersionContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblGroupingStartDate" runat="server" Text="<%$ Resources:SR,GroupingStartDate %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblGroupingStartDateContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblGroupingEndDate" runat="server" Text="<%$ Resources:SR,GroupingEndDate %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblGroupingEndDateContent" runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="vwIdRow">
                    <td class="caption">
                        <asp:Label ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" MaxLength="50" runat="server" />
                    </td>
                    <td colspan="2" />
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td  class="caption">
                        <asp:Label ID="totalOrderCostLabel" runat="server" Text="<%$ Resources:BM_SR,TotalOrderCost %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="totalOrderCostValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td  class="caption">
                        <asp:Label ID="approvalProcedureLabel" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="approvalProcedureValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
