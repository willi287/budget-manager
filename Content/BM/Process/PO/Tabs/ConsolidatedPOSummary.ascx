<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.PO.Tabs.ConsolidatedPOSummary" %>
<%@ Register Src="~/Controls/Core/Address.ascx" TagName="Address" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PropertyAddress.ascx" TagName="PropertyAddress"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="64258B8C-3590-4971-88CF-9CFE00BD7163" />
<table width="100%">
    <tr>
        <td>
            <qwc:ControlledGrid ID="cgSummaryList" runat="server" KeyField="Id" SortExpression="None" >
                <Columns>
                    <qwc:TextColumn DataField="ProductCode"  SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductCode %>" />
                    <qwc:TextColumn DataField="ProductName"  SortExpression="None" HeaderTitle="<%$ Resources:SR,ProductName %>" />
                    <qwc:DecimalColumn DataField="UnitCost"  SortExpression="None" HeaderTitle="<%$ Resources:SR,UnitCost %>" />
                    <qwc:DecimalColumn DataField="Ordered"  SortExpression="None" HeaderTitle="<%$ Resources:SR,Ordered%>" />
                    <qwc:DecimalColumn DataField="Added"  SortExpression="None" HeaderTitle="<%$ Resources:SR,Added%>" />
                    <qwc:DecimalColumn DataField="Removed"  SortExpression="None" HeaderTitle="<%$ Resources:SR,Removed%>" />
                    <qwc:DecimalColumn DataField="Completed"  SortExpression="None" HeaderTitle="<%$ Resources:SR,Completed %>" />
                    <qwc:DecimalColumn DataField="Awaited"  SortExpression="None" HeaderTitle="<%$ Resources:SR,Awaited%>" />
                    <qwc:DecimalColumn DataField="VATCode"  SortExpression="None" HeaderTitle="<%$ Resources:SR,VATCode%>" />
                    <qwc:DecimalColumn DataField="VatPercent"  SortExpression="None" HeaderTitle="<%$ Resources:SR,VATPercent%>" />
                    <qwc:DecimalColumn DataField="OrderNet"  SortExpression="None" HeaderTitle="<%$ Resources:SR,OrderNet %>">
                        <ItemCellStyle CssClass="total" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="ActualNet"  SortExpression="None" HeaderTitle="<%$ Resources:SR,ActualNet %>">
                        <ItemCellStyle CssClass="total" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn DataField="ChangeNet"  SortExpression="None" HeaderTitle="<%$ Resources:SR,ChangeNet %>">
                        <ItemCellStyle CssClass="total" />
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
            <table class="netvatgross">
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalOrderNet" runat="server" Text="<%$ Resources:SR,NetInvoicePrice %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalOrderNetValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalActualNetValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalChangeNetValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalOrderVat" runat="server" Text="<%$ Resources:SR,TotalVAT %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalOrderVatValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalActualVatValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalChangeVatValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblTotalOrderGross" runat="server" Text="<%$ Resources:SR,TotalGROSS %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalOrderGrossValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalActualGrossValue" runat="server" />
                    </td>
                    <td>
                        <qwc:Label ID="lblTotalChangeGrossValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
