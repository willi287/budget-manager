<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.PO.Tabs.ConsolidatedPOMain" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="Address"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PropertyAddress.ascx" TagName="PropertyAddress"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="414D79A3-46E6-4E1A-8324-9CFE00BD7E34" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentNo" runat="server" Text="<%$ Resources:BM_SR,DocumentNumber %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDocumentNo" MaxLength="50" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentReference" runat="server" Text="<%$ Resources:BM_SR,DocumentReference %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDocumentReference" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblServiceProvider" runat="server" Text="<%$ Resources:BM_SR,ServiceProvider %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbServiceProvider" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblIndividual" runat="server" Text="<%$ Resources:BM_SR,Individual%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbIndividual" runat="server" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblApprovalProcedure" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbApprovalProcedure" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDocumentStatus" runat="server" Text="<%$ Resources:BM_SR,DocumentStatus %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDocumentStatus" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDateCreated" runat="server" Text="<%$ Resources:SR,DateCreated %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDateCreated" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDateSubmitted" runat="server" Text="<%$ Resources:SR,DateSubmitted %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDateSubmitted" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblBuyer" runat="server" Text="<%$ Resources:SR,Buyer %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBuyerOrganization" runat="server" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVersion" runat="server" Text="<%$ Resources:SR,Version %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVersion" runat="server" Enabled="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<hr />
<table class="form">
    <tr>
        <th class="caption">
            <qwc:InfoLabel ID="lblInvoiceAddressHead" runat="server" Text="<%$ Resources:BM_SR, InvoiceAddress %>" />
        </th>
        <th class="caption">
            <qwc:InfoLabel ID="lblDeliveryAddressHead" runat="server" Text="<%$ Resources:BM_SR, DeliveryAddress %>" />
        </th>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInvoiceAddressCaption" runat="server" Text="<%$ Resources:BM_SR, InvoiceAddress %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblInvoiceAddressValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDeliveryAddressLine" runat="server" Text="<%$ Resources:BM_SR, DeliveryAddress %>"
                            AssociatedControlID="tbDeliveryAddress" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDeliveryAddress" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDeliveryInstructions" runat="server" Text="<%$ Resources:BM_SR, DeliveryNotes %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="txtDeliveryInstructions" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ItemName %>" DataField="Name" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitQuantity %>" DataField="UnitQuantity" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UnitOfMeasure %>" DataField="UnitIdObjectCode" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,UnitPrice %>" DataField="GrossPrice" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,DayTimeAvailable %>" DataField="DateTimeAvailable" SortExpression="None"/>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,RequestedDate %>" DataField="RequestedDate" SortExpression="None"
            ShowTime="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" DataField="AppointmentTimes"  SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,TotalUnits %>" DataField="Quantity" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,ItemCostGross %>" DataField="TotalGross" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
<div align="right">
    <table>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:BM_SR,TotalForThisOrder %>" ID="totalForThisOrderLiteral"
                    CssClass="totalGross" runat="server" />
            </td>
            <td>
                <qwc:Label ID="poTotalGrossLabel" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <b>
                    <asp:Literal Text="<%$ Resources:BM_SR,Total %>" ID="totalLiteral" runat="server" /></b><br />
                (<asp:Literal Text="<%$ Resources:BM_SR,Gross %>" ID="grossLiteral" runat="server" />)
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
