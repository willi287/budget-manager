﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Process.PO.POListContent" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="01a04907-25e9-4d73-a2f8-0db3e771b76f" />
<div id="search">
	<e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="VisibleDocumentNo" SortDirection="True" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
	<Columns>
	 <qwc:SelectColumn IsEditable="true" DataField="Selected" />        
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />		
		<qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, State %>" DataField="State" AllowSorting="false"/>
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DateSubmitted %>" DataField="SubmitDate" ShowTime="false" SortExpression="SubmitDate"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Individual %>" DataField="Individual" SortExpression="tBM_Principal.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR, PONumber %>" DataField="VisibleDocumentNo" SortExpression="VisibleDocumentNo"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ScheduleOrderSmall %>" DataField="ScheduleOrderVisibleDocumentNumber" SortExpression="tBM_ScheduleOrder.VisibleDocumentNo"/>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, GroupingStartDate %>" DataField="GroupingStartDate" ShowTime="false" SortExpression="GroupingPeriodStartDate"/>
	    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, GroupingEndDate %>" DataField="GroupingEndDate" ShowTime="false" SortExpression="GroupingPeriodEndDate"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, BuyerName %>" DataField="BuyerName" SortExpression="tBuyerOrg.Name"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ServiceProviderName %>" DataField="SupplierName" SortExpression="None"/>		
		<qwc:ImageColumn AllowPreview="true" HeaderTitle="<%$ Resources:BM_SR,ServiceProviderLogo %>" DataField="SupplierLogo" Width="75" AlternativeImageUrl="SupplierLogo" BlankImageUrl="BlankImage" AllowSorting="false" />		
		<qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, DeliveryDate %>" DataField="DeliveryDate" ShowTime="false" SortExpression="DeliveryDate"/>
		<qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, GrossValue %>" DataField="GrossTotal" SortExpression="GrossTotal"/>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierInvoiceNo %>" ColumnType="String" DataField="SupplierInvoiceNo" SortExpression="None" AllowSorting="false" IsEditable="true" MaxLength="53" /> 
    </Columns>
</qwc:ControlledGrid>

<div class="js-document-details-handler-path-wrap"><asp:HiddenField runat="server" ID="hfDocumentDetailsHandlerPath" /></div>
<script type="text/javascript">
    $(document).ready(function () {
        initProductInformationPopups();
    });
</script>
