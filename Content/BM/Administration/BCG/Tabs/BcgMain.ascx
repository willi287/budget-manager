<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BcgMain.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.BCG.Tabs.BcgMain" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="46ba4b4a-cd88-42b5-9450-a7033a5412c4" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblConsortium" runat="server" Text="<%$ Resources:SR,Consortium %>" />:
                    </td>
                    <td>
                        <qwc:Label ID='lblConsortiumValue' runat='server' />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTitle" runat="server" Text="<%$ Resources:SR,BuyerCatalogueName %>"/>:
                    </td>
                    <td>
                        <qwc:Label ID="lblTitleValue" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR,Type %>"/>:
                    </td>
                    <td>
                        <qwc:Label ID="lblTypeValue" runat="server"/>
                    </td>
                </tr>
                <tr runat="server" id="vwIdRow">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbVWId" runat="server" Format="{0:D}"/>
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
<qwc:ControlledGrid ID="cgConsortiumOrganization" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" OnRowDataBinding="cgConsortiumOrganization_OnRowDataBinding" SortExpression="None">
    <Columns>
        <qwc:CheckBoxColumn DataField="InBCG" UseHeaderCheckBox="true" EnableHighlight="true" EnableCheckRow="true"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, OrganisationName %>" DataField="Organisation" SortExpression="None"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:BM_SR,LocalAuthority %>" DataField="IsBuyer"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:SR,Supplier %>" DataField="IsSupplier"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:BM_SR,BudgetManager %>" DataField="IsBlank"/>
    </Columns>
</qwc:ControlledGrid>
