<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardOwner.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.BCG.Tabs.WizardOwner" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="dacc4d02-4e37-4970-8eec-9dd491fa77f7" />
<asp:Table ID="tbOwner" runat="server">
    <asp:TableRow>
        <asp:TableCell CssClass="caption">
            <qwc:InfoLabel ID="lblLocalAuthority" runat="server" Text="<%$ Resources:BM_SR, SelectLocalAuthority%>"
                AssociatedControlID="ddlLocalAuthority" />:
        </asp:TableCell>
        <asp:TableCell>
            <qwc:DropDownList ID="ddlLocalAuthority" DataTextField="Value" DataValueField="Id"
                runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                AutoPostBack="true" IsEmptyItemVisible="true" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="rowIndividual" runat="server">
        <asp:TableCell CssClass="caption">
            <qwc:InfoLabel ID="lblIndividual" runat="server" Text="<%$ Resources:BM_SR, SelectIndividual%>"
                AssociatedControlID="ddlIndividual" />:
        </asp:TableCell>
        <asp:TableCell>
            <qwc:DropDownList ID="ddlIndividual" DataTextField="Value" DataValueField="Id" runat="server"
                ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                IsEmptyItemVisible="true" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
