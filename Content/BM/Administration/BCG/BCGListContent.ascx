<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BCGListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.BCG.BCGListContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f1a7279d-d131-41ae-8a82-f5e039470108" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" OnViewing="OnViewGridItem" OnEditing="OnEditGridItem" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
    <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Title %>" DataField="Name" SortExpression="Name"/>         
         <qwc:ResourceColumn HeaderTitle="<%$ Resources:BM_SR, BcgType %>" DataField="BcgType" SortExpression="BcgType" />
    </Columns>
</qwc:ControlledGrid>
