﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.AttributeIcon.Tabs.Details" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="../../../../../Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<uc:HelpBox runat="server" HelpBoxId="b03d6a2e-3bf2-4172-8e83-b8118adcfc96" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td colspan="2" class="grey">
                        <uc1:FileUpload MaxFileLength="4194304" SelectFileText="<%$ Resources:BM_SR,SelectFile%>"
                            ID="fileUpload" runat="server" FileFilter="Graphic (*.gif, *.jpg, *.eps, *.bmp, *.png)|*.gif;*.bmp;*.jpeg;*.jpg;*.png;*.eps" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:Label ID="lblName" runat="server" AssociatedControlID="tbName" Text="<%$ Resources:BM_SR,IconName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="tbName" runat="server" CssClass="attributeIconNameTextBox"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="tbName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>                
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<script type="text/javascript">
    $(".attributeIconNameTextBox").limit('50');
</script>