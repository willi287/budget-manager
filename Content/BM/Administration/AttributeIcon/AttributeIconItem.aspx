<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register Src="AttributeIconItemContent.ascx" TagPrefix="uc" TagName="AttributeIconItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
 <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:AttributeIconItemContent ID="attributeIconItemContent" runat="server" />
</asp:Content>
