<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register Src="AttributeIconListContent.ascx" TagPrefix="uc" TagName="AttributeIconListContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:AttributeIconListContent ID="attributeIconListContent" runat="server" />
</asp:Content>
