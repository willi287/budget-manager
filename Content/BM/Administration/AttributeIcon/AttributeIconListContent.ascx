<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Controls.ListContentControlBase" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="11e9b98c-7063-4c7d-b5fe-6ff03a368f55" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="Name">
    <Columns>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
		<qwc:ImageColumn Width="25" HeaderTitle="<%$ Resources:SR,IconImage %>" DataField="FilePath" AlternativeImageUrl="FilePath" BlankImageUrl="FilePath"></qwc:ImageColumn>
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR,AttributeIconName %>" DataField="Name" SortExpression="Name"/>
    </Columns>
</qwc:ControlledGrid>

