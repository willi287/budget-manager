﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabReferenceTypeDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.ReferenceType.Tabs.TabReferenceTypeDetail" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="69A62093-2AFD-4BEC-958E-9C4D013292A0" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblName" AssociatedControlID="txtName" runat="server" Text="<%$ Resources:BM_SR,ReferenceTypeName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="txtName" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblValidator" AssociatedControlID="txtValidator" runat="server"
                            Text="<%$ Resources:BM_SR,Validation %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="txtValidator" TextMode=MultiLine Height="50" runat="server" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtValidator" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblExample" AssociatedControlID="txtExample" runat="server" Text="<%$ Resources:BM_SR,Example %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="txtExample" runat="server" MaxLength="200" />   
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtExample" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>         
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
