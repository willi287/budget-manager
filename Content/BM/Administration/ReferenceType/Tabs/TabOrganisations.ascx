﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabOrganisations.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.ReferenceType.Tabs.TabOrganisations" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="833bbf45-8d63-4a1c-8ff9-a1db009a8c53" />
<qwc:ControlledGrid ID="cgOrganisations" SortExpression="None" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false">
    <Columns>
         <qwc:CheckBoxColumn  DataField="Selected" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NotificationName %>" DataField="Name" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>