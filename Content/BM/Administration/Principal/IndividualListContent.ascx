<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Principal.IndividualListContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="de8a578c-40b4-47e5-a893-cf1afe755658" />
<div id="search">
    <table id="searchtable">
        <tr>
            <td>
                <e2w:Search ID="ucSearch" runat="server" />
            </td>
            <td>
                <qwc:DropDownList ID="ddlRole" runat="server" DataTextField="Value" DataValueField="Id" AutoPostBack="true"/>
            </td>
        </tr>
    </table>
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" runat="server" OnCommand="ActionRepeater_OnCommand" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" OnEditing="OnEditing">
    <Columns>
        <qwc:SelectColumn EnableHighlight=true DataField="Selected" IsEditable="true" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, UserName %>" DataField="UserName" SortExpression="Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, UserRole %>" DataField="UserRole" AllowSorting="false" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, PersonalBudgetManager %>" DataField="PersonalBudgetManager" SortExpression="tBM_Principal.Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, LocalAuthorityName %>" DataField="LocalAuthorityName" SortExpression="tBM_Org.Name" />
    </Columns>
</qwc:ControlledGrid>
