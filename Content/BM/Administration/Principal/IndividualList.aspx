<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Principal.IndividualListPageBase" %>
<%@ Register Src="IndividualListContent.ascx" TagPrefix="uc" TagName="IndividualListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:IndividualListContent ID="ContentControl" runat="server" />
</asp:Content>
