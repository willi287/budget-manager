<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardTabPrincipalPbm.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.WizardTabPrincipalPbm" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="82fa8c09-fe0a-4b4b-b02b-433b366cd11d" />
<table class="form">
    <tr>
        <td valign="top">
            <%if (Data.IsAnIndividual)
              {%>
            <table runat="server" id="tableIndividualInvoiceAddressDefaulting" class="subForm gray">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="InfoLabel1" runat="server" Text="<%$ Resources:BM_SR, IndividualsDefaultInvoiceAddress%>" />:
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:RadioButton AutoPostBack="true" GroupName="IndividualInvoiceAddressDefaulting"
                            ID="rbtnPersonalBudgetManagerAddressDefaulting" Text="<%$ Resources:BM_SR,PersonalBudgetManager  %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:RadioButton AutoPostBack="true" GroupName="IndividualInvoiceAddressDefaulting"
                            ID="rbtnIndividualAddressDefaulting" Text="<%$ Resources:BM_SR,Individual  %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption" style="padding-left: 0px;">
                        <table>
                            <tr>
                                <td style="padding-left: 5px; padding-right: 3px;">
                                    <qwc:RadioButton AutoPostBack="true" GroupName="IndividualInvoiceAddressDefaulting"
                                        ID="rbtnNewInvoiceAddress" Text="<%$ Resources:BM_SR,NewAddress  %>" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAddNewAddress" runat="server" Text="<%$ Resources:BM_SR,BtnAddNewAddress  %>"
                                        OnCommand="OnExecuteAction" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCreateCustomAddress" runat="server" Text="<%$ Resources:BM_SR,CreateCustomAddress  %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <qwc:Label ID="lbInvoiceAddressLine" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
        </td>
    </tr>
</table>
<% if (Data.IsAnIndividual)
   { %>
<table class="form bordered">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblInputName" runat="server" Text="<%$ Resources:BM_SR, PleaseInputOrganisationName %>" />:
        </td>
        <td>
            <qwc:PredictiveSearchTextBox runat="server" ID="ptbOrganisation" PostBackOnSelect="true"
                OnValueSelected="OnOrganisationSelected" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblSelectedOrganisation" runat="server" Text="<%$ Resources:BM_SR, SelectedOrganisation %>" />:
        </td>
        <td>
            <qwc:Label ID="lblSelectedOrganisationContent" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid runat="server" ID="cgPbm" KeyField="Id" AllowPaging="true" SortExpression="None">
                <Columns>
                    <qwc:GroupRadioButtonColumn GroupName="Select" EnableHighlight="true" DataField="Selected" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserName %>" DataField="Name" SortExpression="None" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="RoleName" SortExpression="None" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
<% }
   else
   { %>
<qwc:Label runat="server" Text="<%$ Resources:SR,WizardTabPrincipalPbmNotIndividualMsg %>" />
<% } %>
