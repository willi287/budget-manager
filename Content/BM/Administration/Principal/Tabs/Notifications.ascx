﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notifications.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.Notifications" %>

<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="d9d171f7-c829-4a54-81d6-d6a3c51294d4" />
<qwc:ControlledGrid ID="cgNotifications" SortExpression="None" runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false">
    <Columns>
         <qwc:CheckBoxColumn  DataField="Selected" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NotificationName %>" DataField="Name" SortExpression="None" />
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,ViewContent %>" />
    </Columns>
</qwc:ControlledGrid>
