<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.Roles" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="22faccee-34b4-4a9b-95c4-f33e0c74e660" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgRoles" SortExpression="None" 
    runat="server" KeyField="Id">
    <Columns>
         <qwc:CheckBoxColumn  DataField="HasUser" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,RoleName %>" DataField="Name"  SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
