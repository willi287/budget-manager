﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.Settings" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="99AD60F5-7E9F-4D04-BBC7-A05A00D87607" />

<table class="form">
    <tr>
        <td>
            <table class="subForm gray">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="InfoLabel2" runat="server" Text="<%$ Resources:BM_SR, SystemSettings %>" />:
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:QWCheckBox ID="cbIsDelayedNotification" runat="server" Text="<%$ Resources:SR, EnableDelayedNotification %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:QWCheckBox ID="cbDisplayInformationMessages" runat="server" Text="<%$ Resources:SR, DisplayInformationMessages %>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
