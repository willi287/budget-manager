<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WizardTabPrincipalNotifications.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.WizardTabPrincipalNotifications" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="45978fa7-b0fd-42f7-aff4-c71aa503ed3c" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgNotifications"
    runat="server" KeyField="Id" AllowDeleting="false" AllowEditing="false" SortExpression="None">
    <Columns>
         <qwc:CheckBoxColumn  DataField="HasPrincipal" UseHeaderCheckBox="true" EnableHighlight="true"/>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,ViewContent %>"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,NotificationName %>" DataField="Name" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
