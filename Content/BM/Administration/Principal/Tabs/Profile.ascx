<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.Profile" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="9b084045-93a2-4500-a8c4-18b5659c5172" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <%if (Controller.IsFirstLogin())
                  {%>
                <qwc:InfoLabel ID="Label1" runat="server" Text="<%$ Resources:SR, MsgReviewYourDetails %>"
                    Font-Bold="True" />
                <%} %>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                        <% if (!IsContactOnlyYesRadioButton.Checked)
                           {%>
                        <span>*</span>
                        <%
                           }%>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvLogin" ControlToValidate="tbEmail" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblForename" runat="server" Text="<%$ Resources:SR, Forename %>" AssociatedControlID="tbForename" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbForename" runat="server" MaxLength="25" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblSurname" runat="server" Text="<%$ Resources:SR, Surname %>" AssociatedControlID="tbSurname" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSurname" runat="server" MaxLength="25" />
                    </td>
                </tr>
                <tr id="IsContactOnlyLabelRow" visible="false" runat="server">
                    <td class="caption">
                        <qwc:InfoLabel ID="IsContactOnlyLabel" Visible="false" runat="server" Text="<%$ Resources:BM_SR, IsContactOnly %>" />:
                    </td>
                    <td class="gray">
                        <qwc:RadioButton class="gray" AutoPostBack="true" GroupName="IsContactOnly" Visible="false" ID="IsContactOnlyNoRadioButton"
                            Text="<%$ Resources:SR,No  %>" runat="server" />
                        <qwc:RadioButton class="gray" AutoPostBack="true" GroupName="IsContactOnly" Visible="false" ID="IsContactOnlyYesRadioButton"
                            Text="<%$ Resources:SR,Yes  %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPosition" runat="server" Text="<%$ Resources:SR, Position %>" AssociatedControlID="tbPosition" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPosition" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblMobile" runat="server" Text="<%$ Resources:SR, Mobile%>" AssociatedControlID="tbMobile" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbMobile" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revMobile" ErrorMessage="*" ControlToValidate="tbMobile"
                            EnableClientScript="true" runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax%>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCulture" runat="server" Text="<%$ Resources:SR, Language %>" AssociatedControlID="ddlCulture" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlCulture" runat="server" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <% if (IsBusinessUser())
                {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDefaultBusinessView" runat="server" Text="<%$ Resources:BM_SR, DefaultBusinessView %>" AssociatedControlID="ddlDefaultBusinessView" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDefaultBusinessView" runat="server" DataValueField="Id" DataTextField="Value"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <% if (BM_Controller.IsLaUser((BM_PrincipalEntity)Data))
                {%>
                 <tr>
                    <td class="caption" style="width: auto; ">
                        <qwc:InfoLabel ID="lblClientArea" runat="server" Text="<%$ Resources:SR, ClientArea %>" AssociatedControlID="ddllblClientArea" />:
                    </td>
                    <td>
                        <% if (BM_Controller.IsCurrentUserLa(Controller.IsViewMode)){%>
                            <asp:Label runat="server" ID="lblClientAreaValue"></asp:Label>
                         <%}else {%>
                            <qwc:DropDownList ID="ddllblClientArea" runat="server" DataValueField="ID" DataTextField="Value" SortOrder="None" />
                        <%}%>
                    </td>
                </tr>
                <%}%>
                <%}%>
                <% if (IsBusinessUser())
                {%>
                <tr>
                    <td class="caption">
                        &nbsp;
                    </td>
                    <td>
                        <qwc:QWCheckBox ID="cbReceiveNewsletters" runat="server" Text="<%$ Resources:BM_SR, ReceiveNewsletters %>" />
                    </td>
                </tr>
                <%}%>
            </table>
        </td>
        <td valign="top">
            <qwc:Image ID="imgLogo" AllowPreview=true runat="server" Width="150px" Height="150px" PanelID="panelId" />
            <table class="subForm bordered">
                <tr>
                    <td colspan="2">
                        <qwc:QWCheckBox ID="cbIsDisabledUser" runat="server" Text="<%$ Resources:SR, DisableUser %>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
        </td>
    </tr>
</table>
