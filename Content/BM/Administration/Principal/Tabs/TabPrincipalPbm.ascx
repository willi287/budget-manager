<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Principal.Tabs.WizardTabPrincipalPbm" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="b68cb7c8-58a0-40e1-acd5-9a06ec0974bb" />
<table class="form">
    <tr>
        <td>
            <table class="subForm gray">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="InfoLabel2" runat="server" Text="<%$ Resources:BM_SR, SystemSettings %>" />:
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:qwcheckbox id="cbIsDelayedNotification" runat="server" text="<%$ Resources:SR, EnableDelayedNotification %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:qwcheckbox id="cbDisplayInformationMessages" runat="server" text="<%$ Resources:SR, DisplayInformationMessages %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <%if (Data.IsAnIndividual)
              {%>
            <table runat="server" id="tableIndividualInvoiceAddressDefaulting" class="subForm gray">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="InfoLabel1" runat="server" Text="<%$ Resources:BM_SR, IndividualsDefaultInvoiceAddress %>" />:
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:radiobutton autopostback="true" groupname="IndividualInvoiceAddressDefaulting"
                            id="rbtnPersonalBudgetManagerAddressDefaulting"
                            text="<%$ Resources:BM_SR,PersonalBudgetManager  %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:radiobutton autopostback="true" groupname="IndividualInvoiceAddressDefaulting"
                            id="rbtnIndividualAddressDefaulting" text="<%$ Resources:BM_SR,Individual  %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption" style="padding-left: 0px;">
                        <table runat="server">
                            <tr>
                                <td style="padding-left: 5px; padding-right: 3px;">
                                    <qwc:radiobutton autopostback="true" groupname="IndividualInvoiceAddressDefaulting"
                                        id="rbtnNewInvoiceAddress" text="<%$ Resources:BM_SR,NewAddress  %>"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAddNewAddress" runat="server" Text="<%$ Resources:BM_SR,BtnAddNewAddress  %>"
                                        OnCommand="OnExecuteAction" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCreateCustomAddress" runat="server" Text="<%$ Resources:BM_SR,CreateCustomAddress  %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <qwc:label id="lbInvoiceAddressLine" runat="server" data-bind="text: isEnabled" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <%if (Controller.IsVisibleControl(BM_PrincipalEntity.PROP_ISSTAFFMEMBER))
                  {%>
                <tr>
                    <td>
                        <qwc:qwcheckbox autopostback="true" id="chIsStaffMember" runat="server" text="<%$ Resources:BM_SR, IsStaffMember  %>" />
                    </td>
                </tr>
                <%}%>
                <%if (Controller.IsVisibleControl(BM_PrincipalEntity.PROP_ISANINDIVIDUAL))
                  {%>
                <tr>
                    <td colspan="2">
                        <qwc:qwcheckbox autopostback="true" id="chIsIndividual" runat="server" text="<%$ Resources:BM_SR, IsIndividual  %>" />
                    </td>
                </tr>
                <%}%>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <%if (Controller.IsVisibleControl(BM_PrincipalEntity.PROP_ISSUPPORTTEAMMEMBER))
                  {%>
                <tr>
                    <td>
                        <qwc:qwcheckbox autopostback="true" id="chIsSupportTeamMember" runat="server" text="<%$ Resources:BM_SR, IsSupportTeamMember  %>" />
                    </td>
                </tr>
                <%}%>
                <%if (Controller.IsVisibleControl(BM_PrincipalEntity.PROP_ISPBMFORANOTHERUSER))
                  {%>
                <tr>
                    <td colspan="2">
                        <qwc:qwcheckbox autopostback="true" id="chIsPbmForAnotherUser" runat="server" text="<%$ Resources:BM_SR, IsPbmForAnotherUser  %>"
                            oncheckedchanged="IsPbmForAnotherUserChanged" />
                    </td>
                </tr>
                <%}%>
            </table>
        </td>
    </tr>
</table>
<%if (chIsIndividual.Checked)
  {%>
<table class="form bordered">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblInputName" runat="server" Text="<%$ Resources:BM_SR, PleaseInputOrganisationName %>" />:
        </td>
        <td>
            <qwc:predictivesearchtextbox runat="server" id="ptbOrganisation" postbackonselect="true"
                onvalueselected="OnOrganisationSelected" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblSelectedOrganisation" runat="server" Text="<%$ Resources:BM_SR, SelectedOrganisation %>" />:
        </td>
        <td>
            <qwc:label id="lblSelectedOrganisationContent" runat="server" style="column-span: all" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:controlledgrid runat="server" id="cgPbm" keyfield="Id" allowpaging="true" sortexpression="None">
                <Columns>
                    <qwc:GroupRadioButtonColumn GroupName="Select" EnableHighlight="true" DataField="Selected" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserName %>" DataField="Name" SortExpression="None" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="RoleName" SortExpression="None" />
                </Columns>
            </qwc:controlledgrid>
        </td>
    </tr>
</table>
<%}%>