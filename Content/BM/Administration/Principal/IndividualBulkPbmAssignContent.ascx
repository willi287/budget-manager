﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndividualBulkPbmAssignContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Principal.IndividualBulkPbmAssignContent" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="70A2C8AD-340E-45cd-9828-716FCA73F2A4" />
<table class="form bordered">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblSelectedOrganisation" runat="server" Text="<%$ Resources:BM_SR, SelectedOrganisation %>" />:
        </td>
        <td>
            <qwc:Label ID="lblSelectedOrganisationContent" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid runat="server" ID="cgPbm" KeyField="Id" AllowPaging="true" SortExpression="None">
                <Columns>
                    <qwc:GroupRadioButtonColumn GroupName="Select" EnableHighlight="true" DataField="Selected" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,UserName %>" DataField="Name" SortExpression="None"/>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="RoleName" SortExpression="None"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
