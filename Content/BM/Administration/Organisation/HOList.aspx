﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="HOList.aspx.cs" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="HOListContent.ascx" tagname="HOListContent" tagprefix="uc1" %>
<asp:Content ID="ctnHO" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc1:HOListContent ID="ContentBase" runat="server" />
</asp:Content>
