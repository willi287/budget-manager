<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColumnsCustomising.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.Organisation.Tabs.ColumnsCustomising" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="f3176088-a6cd-45ed-9561-3995912c81f7" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbSOColumnsType" runat="server" Text="<%$ Resources:BM_SR,SOcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlSOColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbPOColumnsType" runat="server" Text="<%$ Resources:SR,POcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlPOColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbDNColumnsType" runat="server" Text="<%$ Resources:SR,DNcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDNColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbINVColumnsType" runat="server" Text="<%$ Resources:SR,INVcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlINVColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbCRNColumnsType" runat="server" Text="<%$ Resources:SR,CRNcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlCRNColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbPYColumnsType" runat="server" Text="<%$ Resources:SR,PYcolumnsSet %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlPYColumnsType" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework"
                            runat="server" AutoPostBack="true" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
