<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessCard.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.Organisation.Tabs.BusinessCard" %>
<%@ Register Src="~/Content/BM/BusinessCards/CommonBusinessCard.ascx" TagPrefix="e2w" TagName="BusinessCard" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="729b498d-9649-4512-83cd-672e22a34737" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblBusinessCardRepresentation" runat="server" Text="<%$ Resources:BM_SR,BusinessCardRepresentation %>" />:
                        &nbsp;
                        <asp:Button ID="btUpdate" runat="server" Text="<%$ Resources:BM_SR, Update %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblInformation" runat="server" Text="<%$ Resources:BM_SR, BCTabInformation%>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <e2w:BusinessCard runat="server" id="bcOrganisation" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <e2w:BusinessCard runat="server" id="bcTemplate"/>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption" colspan="2">
                        <qwc:Label ID="lblAdditionalInformationForBusinessCards" runat="server" Text="<%$ Resources:BM_SR,AdditionalInformationForBusinessCard %>"
                            AssociatedControlID="tbFreeTextField" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblFreeTextField" runat="server" Text="<%$ Resources:BM_SR,FreeTextField %>"
                            AssociatedControlID="tbFreeTextField" />
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFreeTextField" runat="server" TextMode="MultiLine" />
                        <asp:RegularExpressionValidator ID="valFreeText" ControlToValidate="tbFreeTextField" runat="server" ErrorMessage="*" ValidationExpression=".{0,150}"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblImageForBusinessCard" runat="server" Text="<%$ Resources:BM_SR,ImageForBusinessCard %>"
                            AssociatedControlID="imgBusinessCardImage" />:
                    </td>
                    <td>
                        <asp:Image runat="server" ID="imgBusinessCardImage" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="grey">
                        <e2w:FileUpload selectfiletext="<%$ Resources:SR,UploadImage%>" maxfilelength="4194304" Width="100%"
                            id="fileUpload" uploadedfilename="" filefilter="Graphic (*.gif, *.jpg, *.eps, *.bmp, *.png)|*.gif; *.jpg; *.jpeg; *.eps; *.bmp; *.png"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
