<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BCGSettings.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.Organisation.Tabs.BCG" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="c31d9a7b-d674-4b44-a135-24aa052ab839" />
<uc2:PopupControl ID="pcMessage" runat="server" />
<table class="borderedOneRow">
    <tr>
        <td class="bold">
            <qwc:InfoLabel ID="lblConsortiumName" Text="<%$ Resources:SR, Consortium %>" runat="server" />:
        </td>
        <td>
            <qwc:DropDownList ID="ddlConsortiums" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlConsortiums_SelectedIndexChanged"  ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
        </td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgBCG" SortExpression="None"
    runat="server" KeyField="Id" AllowDeleting="false">
    <Columns>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources: BM_SR, IsEnabled %>" DataField="IsEnabled" UseHeaderCheckBox="false" EnableHighlight="true" EnableCheckRow="true"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BCGName %>" DataField="Name" SortExpression="None"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:BM_SR,LocalAuthority %>" DataField="IsBuyer"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:SR,Supplier %>" DataField="IsSupplier"/>
        <qwc:CheckBoxColumn HeaderTitle="<%$ Resources:BM_SR,BudgetManager %>" DataField="IsBlank"/>
    </Columns>
</qwc:ControlledGrid>
