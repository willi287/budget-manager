﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StoreSettings.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Organisation.Tabs.StoreSettings" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="e2w" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="DA9BF4F3-4EC3-4F6E-A9C9-2E12928EDB1F" />

<table class="form">
    <tr>
        <td>
            <br />
            <table class="subForm grey">
                <tr>
                    <td style="width: 200px;" colspan="2">
                        <qwc:InfoLabel ID="lblProviderLifeScenarios" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR,ProviderLifeScenarios %>" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" colspan="2">
                        <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgLifeScenarios"
                            runat="server" KeyField="Id" SortExpression="None">
                            <Columns>
                                <qwc:CheckBoxColumn DataField="Selected" UseHeaderCheckBox="true" EnableHighlight="true" />
                                <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,LifeScenario %>" DataField="Name" SortExpression="None" />
                            </Columns>
                        </qwc:ControlledGrid>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;" colspan="2">
                        <qwc:InfoLabel ID="lblProviderStoreSettings" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, ProviderStoreSettings %>" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;">
                        <qwc:InfoLabel ID="lblStoreType" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreType %>" />
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rbStoreType" runat="server">
                            <asp:ListItem Value="7D678D53-001C-4A2A-BD0E-2525A96AD713" Text="No Store" Selected="true" />
                            <asp:ListItem Value="EC6DCBE9-1A88-46FA-8952-26FDF2225669" Text="Standard Store" />
                            <asp:ListItem Value="AB0EC358-71C4-4C5D-A4DC-336F535B33F1" Text="Premium Store" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblStoreName" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreName %>" />
                    </td>
                    <td>
                        <qwc:TextBox name="tbStoreName" ID="tbStoreName" runat="server" />
                        <asp:RegularExpressionValidator ID="reStoreNameValidator" runat="server" EnableClientScript="true" ControlToValidate="tbStoreName"
                            ErrorMessage="*" ToolTip="<%$ Resources:BM_ValidationSR, StoreNameOutOfRange %>" ValidationExpression="[^\r\n]{0,50}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:InfoLabel ID="lblStoreDescription" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreDescription %>" />
                    </td>
                    <td>
                        <qwc:TextBox ID="tbStoreDescription" runat="server" TextMode="MultiLine" Width="225" Height="100" />
                        <asp:RegularExpressionValidator ID="reStoreDescriptionValidator" runat="server" EnableClientScript="true" ControlToValidate="tbStoreDescription"
                            ErrorMessage="*" ToolTip="<%$ Resources:BM_ValidationSR, StoreDescriptionOutOfRange %>" ValidationExpression="[^~]{0,250}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <qwc:QWCheckBox ID="cbFeturedStore" runat="server" Text="<%$ Resources:BM_SR, FeaturedStore %>" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="imgStoreHeader image-scale-container">
                        <qwc:Image ID="imgStoreHeader" AllowPreview="true" runat="server" PanelID="storeHeaderPanel" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">
                        <qwc:InfoLabel ID="lblStoreHeader" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreHeader %>" />
                    </td>
                    <td>
                        <e2w:FileUpload MaxFileLength="4194304" ID="fuStoreHeader" Width="300px" UploadedFileName="" FileFilter="Graphic (*.gif, *.jpg, *.bmp, *.png)|*.gif;*.jpg;*.bmp;*.png;" IsSubmitOnUpload="true"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="imgStoreLogo image-scale-container">
                        <qwc:Image ID="imgStoreLogo" AllowPreview="true" runat="server" PanelID="storeLogoPanel" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">
                        <qwc:InfoLabel ID="lblStoreLogo" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreLogo %>" />
                    </td>
                    <td>
                        <e2w:FileUpload MaxFileLength="4194304" ID="fuStoreLogo" Width="300px" UploadedFileName="" FileFilter="Graphic (*.gif, *.jpg, *.bmp, *.png)|*.gif;*.jpg;*.bmp;*.png;" IsSubmitOnUpload="true"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="imgStoreBackground image-scale-container">
                        <qwc:Image ID="imgStoreBackground" AllowPreview="true" runat="server" PanelID="storeBackgroundPanel" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">
                        <qwc:InfoLabel ID="lblStoreBackground" runat="server" CssClass="bold" Text="<%$ Resources:BM_SR, StoreBackground %>" />
                    </td>
                    <td>
                        <e2w:FileUpload MaxFileLength="4194304" ID="fuStoreBackground" Width="300px" UploadedFileName="" FileFilter="Graphic (*.gif, *.jpg, *.bmp, *.png)|*.gif;*.jpg;*.bmp;*.png;" IsSubmitOnUpload="true"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
