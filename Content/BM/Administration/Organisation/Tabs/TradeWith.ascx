﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TradeWith.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Organisation.Tabs.TradeWith" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="69F0EBE4-F56C-41E3-B0C6-50275505DAAB" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgItems"
    runat="server" KeyField="Id"  SortExpression="None">
    <Columns>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,OrganisationName %>" DataField="OrganisationName"  SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Role %>" DataField="Role"  SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerCatalogueGroup %>" DataField="Bcg"  SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, BuyerAccountReference %>" DataField="RefNumber" MaxLength="50" ColumnType="String"  SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
