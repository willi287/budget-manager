<%@ Import Namespace="Eproc2.Web.Framework.Tabs" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsortiumDetailsWizard.ascx.cs"
    Inherits="Eproc2.Web.Content.Core.Administration.Organisation.Tabs.ConsortiumDetailsWizard" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="608350d3-7a2a-4e31-a7de-a32f229ae2cf" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <% if (!(TabControl is Eproc2.Web.Framework.Tabs.Wizard))
                   {%>
                <tr runat="server" id="vwIdRow">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" MaxLength="50" runat="server"  Format="{0:D}"/>
                        <asp:RegularExpressionValidator ID="rgxpVwIdValidator" runat="server" ControlToValidate="tbVWId"
                            EnableClientScript="true" style="white-space: nowrap" Display="Dynamic" ValidationExpression="\d{0,50}" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" Text="*" />
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, ConsortiumName %>"
                            AssociatedControlID="tbName" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbName" runat="server" MaxLength="100" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvName" ControlToValidate="tbName" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ControlToValidate="tbPhone" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <% if (TabControl is Eproc2.Web.Framework.Tabs.Wizard)
                           {%>
                        <asp:RegularExpressionValidator ToolTip="<%$ Resources:ValidationSR, RexExpEmailMsgInj %>"
                            EnableClientScript="true" ControlToValidate="tbEmail" ID="revEmail" runat="server"
                            ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax %>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ControlToValidate="tbFax" EnableClientScript="true"
                            runat="server" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <% if (!(TabControl is Eproc2.Web.Framework.Tabs.Wizard))
               {%>
            <qwc:Image ID="imgLogo" AllowPreview=true runat="server" PanelID="imagePanel" />
            <%}%>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
        </td>
    </tr>
</table>
