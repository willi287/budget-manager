﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ContactTypeItemContent.ascx" tagname="itemcontent" tagprefix="ContactType" %>
<asp:Content ID="ctnProjectItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ContactType:itemcontent ID="ContentControl" runat="server" />
</asp:Content>
