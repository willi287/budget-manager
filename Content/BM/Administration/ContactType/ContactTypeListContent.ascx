<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="445354cb-4d49-4b00-9e24-5c0461ce353e" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDelete %>" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgList" runat="server" KeyField="Id" SortExpression="Name">
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, ContactTypeName %>" DataField="Name" SortExpression="Name"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, Format%>" DataField="Example" SortExpression="Example"/>
    </Columns>
</qwc:ControlledGrid>
