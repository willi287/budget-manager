<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.Address.PbaAddressListContent" 
    CodeBehind="PbaAddressListContent.ascx.cs" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="a84c27ea-7132-463c-baa2-3dbca0eadcec" />
<div id="search-fullscreen">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="false" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<qwc:ControlledGrid ShowHeaderIfNoData="true" ID="cgList" runat="server" AllowSelecting="true"
    KeyField="Id" AllowEditing="false" AllowViewing="false" AllowDeleting="false" SortExpression="None">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:CheckBoxColumn DataField="IsSelected" UseHeaderCheckBox="true" />
        <qwc:TextColumn DataField="Flat" SortExpression="tAddress.Flat" ColumnType="String" HeaderTitle="<%$ Resources:SR, FlatNo %>" />
        <qwc:TextColumn DataField="HouseName" SortExpression="tAddress.HouseName" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseName %>" />
        <qwc:TextColumn DataField="HouseNumber" SortExpression="tAddress.HouseNumber" ColumnType="String" HeaderTitle="<%$ Resources:SR, HouseNumber %>" />
        <qwc:TextColumn DataField="Street" SortExpression="tAddress.Street" ColumnType="String" HeaderTitle="<%$ Resources:SR, Street %>" />
        <qwc:TextColumn DataField="City" SortExpression="tAddress.City" ColumnType="String" HeaderTitle="<%$ Resources:SR, Town %>" />
        <qwc:TextColumn DataField="County" SortExpression="tAddress.County" ColumnType="String" HeaderTitle="<%$ Resources:SR, County %>" />
        <qwc:TextColumn DataField="Postcode" SortExpression="tAddress.Postcode" ColumnType="String" HeaderTitle="<%$ Resources:SR, Postcode %>" />
    </Columns>
</qwc:ControlledGrid>
