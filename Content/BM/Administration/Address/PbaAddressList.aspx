<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register Src="PbaAddressListContent.ascx" TagPrefix="uc" TagName="PbaAddressListContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:PbaAddressListContent ID="pbaAddressListContent" runat="server" />
</asp:Content>
