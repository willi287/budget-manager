<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingSourceListContent.ascx.cs" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc2" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="8ee32a6c-2cfe-44cc-a991-df03589e6c20" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDelete %>" />
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgList" runat="server" KeyField="Id" SortExpression="Name">
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
         <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, FundingSourceName%>" DataField="Name" SortExpression="Name"/>
    </Columns>
</qwc:ControlledGrid>
