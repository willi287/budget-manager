﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="FundingSourceListContent.ascx" tagname="listcontent" tagprefix="fundingsource" %>
<asp:Content ID="ctnProjectItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <fundingsource:listcontent ID="ContentControl" runat="server" />
</asp:Content>
