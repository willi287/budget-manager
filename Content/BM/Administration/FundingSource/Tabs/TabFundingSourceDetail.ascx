﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabFundingSourceDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.FundingSource.Tabs.TabFundingSourceDetail" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="A4A1702A-EC63-4C8E-93A5-9C310102CBEB" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblName" AssociatedControlID="txtName" runat="server" Text="<%$ Resources:BM_SR,FundingSourceName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td align="left" class="formData">
                        <qwc:TextBox ID="txtName" runat="server" MaxLength="150" CssClass="fundingSourceNameTextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgOrganisations"
    runat="server" KeyField="Id" SortExpression="None">
    <Columns>
        <qwc:CheckBoxColumn DataField="Selected" UseHeaderCheckBox="true" EnableHighlight="true" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,FundingAuthorityName %>" DataField="Name" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
<script type="text/javascript">
    $('.fundingSourceNameTextBox').limit('50');
</script>
