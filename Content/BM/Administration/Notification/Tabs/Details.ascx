<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.Notification.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="6521bba7-2b77-4caa-9468-cfe8e7329c46" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTitle" runat="server" Text="<%$ Resources:SR, NotificationTitle %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTitleContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, EmailContent %>" />:
                    </td>
                    <td>
                        <qwc:Label ReadOnly="true" ID="lblEmailContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
