<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register Src="NotificationItemContent.ascx" TagPrefix="uc" TagName="NotificationItemContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:NotificationItemContent ID="notificationItemContent" runat="server" />
</asp:Content>
