<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.UnassignedAccountListContent" %>
<%@ Import Namespace="Eproc2.Core.Common"%>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="D0BC1927-6D14-446E-8EFE-1848C4E7EE4E" />
<div id="search">
    <table class="skinny">
        <tr>
            <td>
                <e2w:search id="ucSearch" runat="server" AdvancedEnabled="true" />
            </td>
            <td>
                <qwc:QWCheckBox ID="cbFilter" AutoPostBack="true" runat="server" Text="<%$ Resources:BM_SR, ShowAllIndividuals %>"  />
            </td>
        </tr>
    </table>
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" SortExpression="Name" >
    <Columns>
        <qwc:SelectColumn IsEditable="true" DataField="Selected"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsName %>" DataField="Name" SortExpression="Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsReference %>" DataField="Reference" SortExpression="Reference"  />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, AccountName %>" DataField="AccountName" SortExpression="tBM_BankAccount.AccountName"  />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, Balance %>" DataField="Balance"  SortExpression="CurrentBalance" />
    </Columns>
</qwc:ControlledGrid>
