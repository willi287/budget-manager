﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.Controls.WizardContentControlBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<script type="text/javascript">
    Sys.Application.add_load(WireEvents);
    function WireEvents() { $(document).ready(function() { if ($("div.psAction").size() == 0) $("div.wizard_navigation>a").remove(); if ($("div.wizard_navigation>a").size() == 0) $("div.psAction>a").prependTo("div.wizard_navigation"); $("div.psAction").remove(); }); }
</script>

<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
