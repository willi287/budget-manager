﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" CodeBehind="BankAccountHistoryItem.aspx.cs" Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.BankAccountHistoryItem" %>
<%@ Register Src="~/Content/BM/Administration/BankAccount/BankAccountHistoryItemContent.ascx" TagName="BankAccountHistoryItemContent" TagPrefix="uc" %>
<asp:Content ID="cntBcgHistoryItem" ContentPlaceHolderID="cphMainContent" runat="server">
  <uc:BankAccountHistoryItemContent ID="historyItemContent" runat="server" /> 
</asp:Content>

