﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register src="UnassignedAccountListContent.ascx" tagname="ListContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="ctnContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:ListContent ID="ContentControl" runat="server" />
</asp:Content>
