<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.GroupAccountListContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="5ea6a04a-88e9-4862-b459-fd6efb591691" />
<script type="text/javascript">
    Sys.Application.add_load(WireEvents);
    function WireEvents() 
    {
        $(document).ready(function() {
            $("table.bankAccount tr:disabled :input").attr("disabled", "disabled");
            $("table.bankAccount tr:disabled").removeAttr("disabled");
            $("table.bankAccount input[checked]").addClass("default");
        });
    }
    </script>
<div id="search">
    <table id="searchtable">
        <tr>
            <td>
                <e2w:search id="ucSearch" runat="server" AdvancedEnabled="true" />
            </td>
            <td>
                <qwc:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true" DataValueField="Id"
                    DataTextField="Value" SortOrder="None" />
            </td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="hfNoDefault" runat="server" OnValueChanged="hfNoDefault_CheckedChange" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<qwc:ControlledGrid ID="cgList" CssClass="GridEx bankAccount" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" SortExpression="AccountName">
    <Columns>
        <qwc:GroupRadioButtonColumn Visible="false" EnableHighlight="false"  AllowSorting="false" DataField="Selected" HeaderTitle="<%$ Resources:BM_SR, Default %>" GroupName="select" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, AccountName %>" DataField="AccountName" SortExpression="AccountName"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, AccountNumber %>" DataField="AccountNumber" SortExpression="AccountNumber"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, SortCode %>" DataField="SortCode" SortExpression="SortCode"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, LastReconciledStatementPeriod %>" DataField="LastReconciledStatementPeriod" SortExpression="SortCode"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, ReconciledBalance %>" DataField="Balance" SortExpression="Balance"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="Type" />
        <qwc:GroupRadioButtonColumn DataField="IsDefault" HeaderTitle="<%$ Resources:BM_SR, Default %>"
            GroupName="IsDefault" AllowSorting="false"  />
    </Columns>
</qwc:ControlledGrid>
