﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Wizard.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.ReconcileWizard" 
CodeBehind="ReconcileWizard.aspx.cs"%>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ReconcileWizardContent.ascx" tagname="content" tagprefix="bmw" %>
<asp:Content ID="ctnProjectItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <bmw:content ID="ContentControl" runat="server" />
</asp:Content>


