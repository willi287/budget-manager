﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register src="GroupAccountListContent.ascx" tagname="ListContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="ctnContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(WireEvents);
        function WireEvents() { $(document).ready(function() { $("#search").css("margin", "145px 0 0 7px"); }); }
    </script>
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:ListContent ID="ContentControl" runat="server" />
</asp:Content>
