<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaAccount.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.Tabs.BaAccount" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="a046e7b3-0b2b-4125-908f-5a20b89551aa" />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblAccountName" Text="<%$ Resources:BM_SR, AccountName %>" AssociatedControlID="tbAccountName"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAccountName" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblAccountNumber" Text="<%$ Resources:BM_SR,AccountNumber %>"
                            AssociatedControlID="tbAccountNumber" runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAccountNumber" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblBankName" Text="<%$ Resources:BM_SR,BankName %>" AssociatedControlID="tbBankName"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBankName" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblSortCode" Text="<%$ Resources:BM_SR,SortCode %>" AssociatedControlID="tbSortCode"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSortCode" runat="server" MaxLength="8" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">&nbsp;
                    </td>
                    <td>
                        <qwc:RadioButton ID="rbGroup" runat="server" GroupName="AccountType" Text="<%$ Resources:BM_SR,Group  %>" />
                        <qwc:RadioButton ID="rbIndividual" runat="server" GroupName="AccountType" Text="<%$ Resources:BM_SR,Individual  %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblStatementEndDate" Text="<%$ Resources:BM_SR, StatementEndDate %>"
                            AssociatedControlID="calStatementEndDate" runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <e2c:Calendar ID="calStatementEndDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblStatementReceived" Text="<%$ Resources:BM_SR,StatementReceived %>"
                            AssociatedControlID="ddlStatementReceived" runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlStatementReceived" SortOrder="None" runat="server" DataTextField="Value"
                            DataValueField="Id" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblStatementEdnOn" Text="<%$ Resources:BM_SR,StatementEndOn %>"
                            AssociatedControlID="ddlStatementEndOn" runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlStatementEndOn" SortOrder="None" runat="server" DataTextField="Value"
                            DataValueField="Id" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: bottom">
            <qwc:ImageActionRepeater CssClass="" ID="arActions" ShowWaitControl="false" runat="server" Visible="<%#Controller.AllowActions %>" />
            &nbsp;
        </td>
        <td style="vertical-align: bottom">
            <table class="subForm">
                <tr>
                    <td class="caption">&nbsp;
                    </td>
                    <td class="caption" style="vertical-align: bottom">
                        <qwc:InfoLabel ID="lblAccountBalance" Text="<%$ Resources:BM_SR,ReconciledBalance %>"
                            runat="server" />:
                    </td>
                    <td style="vertical-align: bottom">
                        <qwc:Label ID="lblAccountBalanceValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">&nbsp;
                    </td>
                    <td class="caption" style="vertical-align: bottom">
                        <qwc:InfoLabel ID="lblStatementPeriod" Text="<%$ Resources:BM_SR,LastReconciledStatementPeriod %>"
                            runat="server" />:
                    </td>
                    <td style="vertical-align: bottom">
                        <qwc:Label ID="lblStatementPeriodValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:BM_SR, RemoveIndividualFromBAConfirm %>" ID="cgItems"
                runat="server" AllowDeleting="<%#Controller.AllowActions %>" AllowViewing="false" AllowEditing="false"
                AllowAdding="false" KeyField="Id" SortExpression="None">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" EnableHighlight="true" ControlStyle-Width="50px" />
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsName %>" DataField="Name" SortExpression="None" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsReference %>" DataField="Reference" SortExpression="None" />
                    <%--<qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, Balance %>" DataField="Balance" SortExpression="None"/>--%>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
