<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdjDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.Tabs.AdjDetails" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="715D3459-65AB-4661-8ACB-DF9796891149" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblType" Text="<%$ Resources:SR,Type %>" AssociatedControlID="ddlType"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlType" runat="server" DataTextField="Value"
                            DataValueField="Id" SortOrder="None" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblDate" Text="<%$ Resources:SR, Date %>"
                            AssociatedControlID="calDate" runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <e2c:calendar id="calDate" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblDescription" Text="<%$ Resources:SR,Description %>" AssociatedControlID="tbDescription"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" Rows="4" TextMode="MultiLine" MaxLength="50" />
                        <asp:RegularExpressionValidator runat="server" Text="*" ToolTip="<%$ Resources: ValidationSR,CommonRangeVEMsg %>" ControlToValidate="tbDescription"  ValidationExpression=".{0,50}"/>
                    </td>
                </tr>
                 <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblAmount" Text="<%$ Resources:SR,Amount %>" AssociatedControlID="tbAmount"
                            runat="server" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbAmount" runat="server" />
                    </td>
                </tr>
            </table> 
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
