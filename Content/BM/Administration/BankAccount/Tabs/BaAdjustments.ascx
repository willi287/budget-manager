<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaAdjustments.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.Tabs.BaAdjustments" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
<uc:helpbox runat="server" helpboxid="50CA10FC-E354-4BF6-B2DE-C43F93283795" />
<table class="form">
    <tr>
        <td>
            <qwc:DropDownList ID="ddlFilter" DataTextField="Value" DataValueField="Id" runat="server"
                 AutoPostBack="true" LabelRender="false" SortOrder="None" />
        </td>
    </tr>
    <tr>
        <td>
        <qwc:ImageActionButton ID="btnAddAdjustmnt" runat="server" Visible="<%#Controller.AllowActions %>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgItems"
                runat="server" AllowDeleting="<%#Controller.AllowActions %>" AllowViewing="false" AllowEditing="<%#Controller.AllowActions %>"
                AllowAdding="false" KeyField="Id" SortExpression="None">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR, Date %>" DataField="Date" SortExpression="None"/>
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" DataField="Type" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsName %>" DataField="IndividualName" SortExpression="None"/>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR, IndividualsReference %>" DataField="IndividualReference" SortExpression="None"/>
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR, Amount %>" DataField="Amount" SortExpression="None"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
