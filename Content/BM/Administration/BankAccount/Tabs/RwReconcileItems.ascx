<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RwReconcileItems.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.Tabs.RwReconcileItems" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="01CAEA0E-0AC9-43B8-9214-3414D276B992" />
<qwc:ImageActionButton CssClass="psAction" ID="btnPartlySaved" runat="server" />
<table class="form">
    <tr>
        <td>
            <fieldset style="padding-left: 10px; padding-right: 10px">
                <legend>
                    <%=BM_SR.SelectedPaymentTotals %></legend>
                <table class="subForm bsForm bold" style="text-align: center">
                    <tr class="alternatingItem">
                        <td style="width: 50%">
                            <asp:Label ID="lblMoneyIn" runat="server" Text="<%$ Resources:BM_SR,MoneyIn %>" CssClass="moneyIn" />
                        </td>
                        <td>
                            <asp:Label ID="lblMoneyOut" runat="server" Text="<%$ Resources:BM_SR,MoneyOut %>"
                                CssClass="moneyOut" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:Label ID="lblMoneyInValue" runat="server" />
                        </td>
                        <td>
                            <qwc:Label ID="lblMoneyOutValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:BM_SR, RefreshQuantities %>" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
        <td>
            <fieldset style="padding-left: 10px; padding-right: 10px">
                <legend>
                    <%=BM_SR.ReconciledBalance %></legend>
                <table class="subForm bsForm bold" style="text-align: center">
                    <tr class="alternatingItem">
                        <td>
                            <asp:Label ID="lblSelectedBalance" runat="server" Text="<%$ Resources:BM_SR,SelectedBalance %>" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblStatementBalance" runat="server" Text="<%$ Resources:BM_SR,StatementBalance %>" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblUnreconciled" runat="server" Text="<%$ Resources:BM_SR,Unreconciled %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:Label ID="lblSelectedBalanceValue" runat="server" />
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <qwc:Label ID="lblStatementBalanceValue" runat="server" />
                        </td>
                        <td>
                            =
                        </td>
                        <td>
                            <qwc:Label ID="lblUnreconciledValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblStatementEndDate" runat="server" Text="<%$ Resources:BM_SR,StatementEndDate %>" />:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <qwc:Label ID="lblStatementEndDateValue" runat="server" />
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbShowAllUnreconciled" runat="server" AutoPostBack=true
                Text="<%$ Resources:BM_SR, ShowAllUnreconciledTransactions %>" />
       </td>
    </tr>
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAddAdjustmnt" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ID="cgList" SortExpression="DueDate" runat="server" KeyField="Id">
                <Columns>
                    <qwc:CheckBoxColumn UseHeaderCheckBox="true" DataField="Selected" IsEditableDataField="Editable" EnableHighlight="true" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,Due %>" DataField="DueDate" SortExpression="DueDate" />
                    <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,Actual %>" DataField="ActualDate" IsEditableDataField="Editable" SortExpression="None">
                        <Validators>
                            <qwc:ColumnRequiredFieldValidator ID="DueDate_RequiredFieldValidatorID" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"/>
                            <qwc:ColumnRangeValidator ID="DueDate_RangeValidatorID" CultureInvariantValues="true" Type="Date"
                                MinimumValue="1900/01/01" MaximumValue="2100/01/01" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>" />
                        </Validators>
                    </qwc:DateTimeColumn>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Individual %>" DataField="PrincipalName" SortExpression="None"/>
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="TransactionType"
                        AllowSorting="false" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,PaymentSourceDescription %>" DataField="PaymentSource" SortExpression="None"/>
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Status %>" DataField="Status" AllowSorting="false" />
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,MoneyIn %>" DataField="InAmount" SortExpression="None">
                        <HeaderCellStyle CssClass="moneyIn" />
                        <ItemCellStyle CssClass="columnCell green" />
                    </qwc:DecimalColumn>
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,MoneyOut %>" DataField="OutAmount" SortExpression="None">
                        <HeaderCellStyle CssClass="moneyOut" />
                        <ItemCellStyle CssClass="columnCell red" />
                    </qwc:DecimalColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAddAdjustmentAlt" runat="server" />
        </td>
    </tr>
</table>
