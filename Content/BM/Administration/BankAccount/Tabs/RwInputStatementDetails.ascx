<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RwInputStatementDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.BankAccount.Tabs.RwInputStatementDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<uc:helpbox runat="server" helpboxid="56C6FD35-8B00-40CF-AFFC-9D67451DAE5F" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAccountName" Text="<%$ Resources:BM_SR, BankAccountName %>"
                            runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblAccountNameValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAccountNumber" Text="<%$ Resources:BM_SR, AccountNumber %>"
                            runat="server" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblAccountNumberValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblStatementEndDate" Text="<%$ Resources:BM_SR,StatementEndDate %>"
                            AssociatedControlID="ddlStatementEndDate" runat="server" />:&nbsp<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlStatementEndDate" SortOrder="None" runat="server" DataTextField="Value"
                            DataValueField="Id" CssClass="ddlStatementEndDate"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblEndBalance" Text="<%$ Resources:BM_SR,StatementEndBalance %>"
                            AssociatedControlID="tbEndBalance" runat="server" />:&nbsp<span>*</span>
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbEndBalance" runat="server" />
                           <qwc:SignAwarePriceValidator ID="pvEndBalance" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbEndBalance" EnableClientScript="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInterestEarned" Text="<%$ Resources:BM_SR,InterestEarned %>"
                            AssociatedControlID="tbInterestEarned" runat="server" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbInterestEarned" runat="server" />
                                                   <qwc:PriceValidator ID="pvInterestEarned" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbInterestEarned" EnableClientScript="true" runat="server" />
                                   
                  
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAccountCharges" Text="<%$ Resources:BM_SR,AccountCharges %>"
                            AssociatedControlID="tbAccountCharges" runat="server" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbAccountCharges" runat="server" />
                           <qwc:PriceValidator ID="pvAccountCharges" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbAccountCharges" EnableClientScript="true" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblInterestDate" Text="<%$ Resources:BM_SR,Date %>" AssociatedControlID="calInterest"
                            runat="server" />:
                    </td>
                    <td>
                        <e2c:calendar id="calInterest" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblChargesDate" Text="<%$ Resources:BM_SR,Date %>" AssociatedControlID="calCharges"
                            runat="server" />:
                    </td>
                    <td>
                        <e2c:calendar id="calCharges" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
