<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListContentBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="a6d8f8c7-e9ce-4f7d-8c2d-e98bdb5e6c9f" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<uc:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR,CantDelete %>"/>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" SortExpression="Name">
    <Columns>
		<qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
		<qwc:TextColumn HeaderTitle="<%$ Resources:SR,FundingAuthorityName %>" DataField="Name" SortExpression="Name"/>
    </Columns>
</qwc:ControlledGrid>

