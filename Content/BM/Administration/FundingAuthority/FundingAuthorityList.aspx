<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register Src="FundingAuthorityListContent.ascx" TagPrefix="uc" TagName="FundingAuthorityListContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:FundingAuthorityListContent ID="fundingAuthorityListContent" runat="server" />
</asp:Content>
