<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register Src="FundingAuthorityItemContent.ascx" TagPrefix="uc" TagName="FundingAuthorityItemContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:FundingAuthorityItemContent ID="fundingAuthorityItemContent" runat="server" />
</asp:Content>
