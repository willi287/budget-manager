<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.FundingAuthority.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="6ee3dffa-ad82-4c9d-bbeb-18c9c0208def" />
<table class="form">
    <tr>
        <td>
           	<table class="subForm">
				<tr>
                    <td class="caption required">
                    	<qwc:Label ID="lblName" runat="server" Text="<%$ Resources:BM_SR,FundingAuthorityName %>" AssociatedControlID="tbName"/>:<span>*</span>
                    </td>
                    <td>
                    	<qwc:TextBox ID="tbName" runat="server" MaxLength="100"/>
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="false" ID="rfvName" ControlToValidate="tbName" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                    </td>			
				</tr>				
         	</table>
		</td>
		<td>
		    &nbsp;
		</td>
	</tr>
</table>
