﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="IndividualFolderSettingsListContent.ascx" tagname="listcontent" tagprefix="IndividualFolderSettings" %>
<asp:Content ID="ctnProjectItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <IndividualFolderSettings:listcontent ID="ContentControl" runat="server" />
</asp:Content>
