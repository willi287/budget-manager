﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabIndividualSummarySettingsDetail.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Administration.IndividualFolderSettings.Tabs.TabIndividualSummarySettingsDetail" %>
<%@ Import Namespace="Eproc2.BM.Entities"%>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="F1214803-EEFB-4064-A6E4-9C4F00D39692" />
<table class="form">
    <tr>
        <td>
            <div>
                <table class="subForm grey">
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="InfoLabel1" runat="server" Text="<%$ Resources:BM_SR,MainContentDisplay %>" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <qwc:RadioButton ID="rbShowTable" GroupName="showTable" runat=server Text="<%$ Resources:BM_SR,AlwaysShowTableOfIndividuals %>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <qwc:RadioButton ID="rbHideTable" GroupName="showTable" runat=server Text="<%$ Resources:BM_SR,HideTableOfIndividuals %>" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td>
            <div>
                <table class="subForm grey">
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="InfoLabel2" runat="server" Text="<%$ Resources:BM_SR,FolderTreeToDisplay %>" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbShowName" AutoPostBack=true GroupName="showname" runat=server Text="<%$ Resources:BM_SR,ShowIndividualsName %>" />
                        </td>
                        <td>
                            <qwc:RadioButton ID="rbShowAllFolders" AutoPostBack=true GroupName="showAllFolders" runat=server Text="<%$ Resources:BM_SR,ShowAllFolders %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbShowReference" AutoPostBack=true GroupName="showname" runat=server Text="<%$ Resources:BM_SR,ShowIndividualsReference %>" />
                        </td>
                        <td>
                            <qwc:RadioButton ID="rbNoFoldersToDisplay" AutoPostBack=true GroupName="showAllFolders" runat=server Text="<%$ Resources:BM_SR,NoFoldersToDisplay %>" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
   <%if(Controller.IsVisibleControl(BM_IndividualSummarySettingsEntity.PROP_REFERENCETYPEID)){%>
   <tr>
        <td>
        </td>
        <td>
            <div>
                <table class="subForm grey">
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="InfoLabel3" runat="server" Text="<%$ Resources:BM_SR,IndividualReference %>" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <qwc:DropDownList ID="ddlReferences" DataTextField="Value" DataValueField="Id" runat="server"></qwc:DropDownList>
                        </td>                        
                    </tr>
                </table>
            </div>
        </td>
    </tr>
 <%}%>
</table>
