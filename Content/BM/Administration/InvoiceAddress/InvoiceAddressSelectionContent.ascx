﻿<%@ Control Language="C#" AutoEventWireup="true" 
CodeBehind="InvoiceAddressSelectionContent.ascx.cs" 
Inherits="Eproc2.Web.Content.BM.Administration.InvoiceAddress.InvoiceAddressSelectionContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>

<uc:HelpBox runat="server" HelpBoxId="13A2EB1E-45FE-436E-BCA1-65EB6C1D5976" />
<div id="notTreeSearch">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="false" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="AddressLine" runat="server" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,InvoiceAddress %>" DataField="AddressLine" SortExpression="AddressLine"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AddressName %>" DataField="Name" SortExpression="Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AddressReference %>" DataField="Reference" SortExpression="Reference"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Description" SortExpression="Description"/>
    </Columns>
</qwc:ControlledGrid>