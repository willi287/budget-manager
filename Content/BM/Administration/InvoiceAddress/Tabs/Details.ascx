﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Details.ascx.cs" Inherits="Eproc2.Web.Content.BM.Administration.InvoiceAddress.Tabs.Details" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl" TagPrefix="e2w" %>
<uc:HelpBox runat="server" HelpBoxId="EBD37A25-CBC2-4ACE-AC77-D5636CACA95D" />
<table class="form" width="100%">
    <tr>
        <td style="width:70%;">
            <table class="subForm" width="100%">
                <tr>
                    <td class="required caption" style="width:30%;">
                        <qwc:InfoLabel ID="lblOrgName" AssociatedControlID="ddlOrganisations" runat="server" Text="<%$ Resources:BM_SR,OrganisationName%>" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlOrganisations" runat="server" DataTextField="Value" DataValueField="Id" IsEmptyItemVisible="true" AutoPostBack="True" />
                        <asp:RequiredFieldValidator ID="rfvTemplateTitle" ErrorMessage="*" ControlToValidate="ddlOrganisations"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption" style="width:30%;">
                        <qwc:InfoLabel ID="lblBuyerReference" AssociatedControlID="ddlBuyerReference" runat="server" Text="<%$ Resources:BM_SR, BuyerReference %>" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlBuyerReference" runat="server" DataTextField="Name" DataValueField="Id" IsEmptyItemVisible="true" />
                        <%--<asp:RequiredFieldValidator ID="rfvBuyerReference" ErrorMessage="*" ControlToValidate="ddlBuyerReference"
                            EnableClientScript="true" Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            runat="server" />--%>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAddressName" AssociatedControlID="tbAddressName" runat="server" Text="<%$ Resources:BM_SR,AddressName%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAddressName" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAddressReference" AssociatedControlID="tbAddressReference" runat="server" Text="<%$ Resources:BM_SR,AddressReference%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAddressReference" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDescription" AssociatedControlID="tbDescription" runat="server" Text="<%$ Resources:SR,Description%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDescription" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
        </td>
    </tr>
</table>