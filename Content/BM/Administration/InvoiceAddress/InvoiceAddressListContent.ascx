﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Administration.InvoiceAddress.InvoiceAddressListContent" CodeBehind="InvoiceAddressListContent.ascx.cs" %>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl"
    TagPrefix="e2w" %>

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="5D46FE8F-8D79-4753-B734-3019DA0B42BD" />
<div id="search">
    <e2w:Search ID="ucSearch" Url="tut.by" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="AddressLine" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,InvoiceAddress %>" DataField="AddressLine" SortExpression="AddressLine"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AddressName %>" DataField="Name" SortExpression="Name"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AddressReference %>" DataField="Reference" SortExpression="Reference"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Description" SortExpression="Description"/>
    </Columns>
</qwc:ControlledGrid>