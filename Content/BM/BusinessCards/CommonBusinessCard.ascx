<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommonBusinessCard.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.BusinessCards.CommonBusinessCard" %>
<table  style="width:100%">
    <tr>
        <td class="businessCard <%= GetCardTypeCssClass() %>">            
            <table>
                <tr>
                    <td colspan="2" class="header">
                        <h1>
                            <qwc:Label Text="<%$ Resources:BM_SR,LocalAuthority%>" runat="server" ID="lblLocalAuthorityHeader" />
                            <qwc:Label Text="<%$ Resources:BM_SR,ServiceProvider%>" runat="server" ID="lblServiceProviderHeader" />
                            <qwc:Label Text="<%$ Resources:BM_SR,IndividualSupportTeam%>" runat="server" ID="lblStaffMemberHeader" />
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td class="organisation">
                        <h1>
                            <qwc:Label ID="lblNameContent" runat="server" />
                        </h1>
                    </td>
                    <td rowspan="8" valign="top" align="right">
                        <qwc:Image ID="imgLogo" runat="server" class="logo"/>
                    </td>
                </tr>
                <asp:TableRow runat="server" ID="trPosition">
                    <asp:TableCell runat="server">
                        <qwc:Label ID="lblPositionContent" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="trOrganisation">
                    <asp:TableCell class="organisation" runat="server">
                        <qwc:Label ID="lblOrganisationNameContent" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <tr>
                    <td>
                        <qwc:Label ID="lblAddressContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblContactNumberContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="strong">
                        <qwc:Label ID="lblEmail" Text="<%$ Resources:SR,Email%>" runat="server"/>:
                        <asp:HyperLink ID="hlEmail" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="strong">
                        <qwc:Label ID="lblWebSite" Text="<%$ Resources:SR,Website%>" runat="server"/>:
                        <asp:HyperLink ID="hlWebSite" runat="server" />
                    </td>
                </tr>
                <asp:TableRow runat="server" ID="trFreeText">
                    <asp:TableCell runat="server" ID="tdFreeText">
                        <qwc:Label ID="lblFreeText" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
            </table>
        </td>
    </tr>
</table>
