<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/Content/BM/BusinessCards/IndividualBusinessCard.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.BusinessCards.IndividualBusinessCard" %>
<table style="width:100%">
    <tr>
        <td class="individual businessCard">
            <table>
                <tr>
                    <td colspan="2" class="header">
                        <h1>
                            <qwc:Label Text="<%$ Resources:BM_SR,Individual%>" runat="server" ID="lblHeader" />
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td class="strong">
                        <h1>
                            <qwc:Label ID="lblNameContent" runat="server" />
                        </h1>
                    </td>
                    <td rowspan="4" valign="top" align="right">
                        <qwc:Image ID="imgLogo" runat="server" class="logo" />
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <qwc:Label ID="lblAddressContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="strong">
                        <qwc:Label ID="lblContactNumberContent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 8px">
                        <qwc:Label ID="lblEmail" runat="server" Text="<%$ Resources:SR,Email%>" class="strong" />:
                        <asp:HyperLink ID="hlEmail" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="businessCard">
            <table class="pbm">
                <tr>
                    <td rowspan="4" valign="top" align="left">
                        <qwc:Image runat="server" ID="imgPBMLogo" class="logo" />
                    </td>
                    <td>
                        <qwc:Label ID="lblPBM" Text="<%$ Resources:BM_SR,PersonalBudgetManager %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <h1>
                            <qwc:Label ID="lblPBMName" Text="lblPBMName" runat="server" />
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td class="strong">
                        <qwc:Label ID="lblPBMContactNumber" Text="lblPBMContactNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:Label ID="lblPBMEmail" runat="server" Text="<%$ Resources:SR,Email%>" class="strong" />:
                        <asp:HyperLink ID="hlPBMEmail" runat="server" class="email" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
