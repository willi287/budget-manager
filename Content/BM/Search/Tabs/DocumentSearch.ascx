<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentSearch.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Search.Tabs.DocumentSearch" %>
<%@ Register Assembly="Qulix.Web" Namespace="Qulix.Web.Controls" TagPrefix="qwc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3f72cbe8-9378-4c7f-ba75-518842dcfe9a" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption" colspan="2">
                        <qwc:Label ID="lblCreationDate" runat="server" Text="<%$ Resources:SR,CreationDate%>" />:
                    </td>
                </tr>
                <tr>
                    <td class="caption indent">
                        <qwc:Label ID="lblCreationDateFrom" runat="server" Text="<%$ Resources:SR,From%>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calCreationDateFrom" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption indent">
                        <qwc:Label ID="lblCreationDateTo" runat="server" Text="<%$ Resources:SR,To%>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calCreationDateTo" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblVWId" runat="server" Text="<%$ Resources:SR,VWId%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbVWId" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblSupplier" runat="server" Text="<%$ Resources:SR,Supplier%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSupplier" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblType" runat="server" Text="<%$ Resources:SR,Type%>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlType" runat="server" DataTextField="Value" DataValueField="Id"
                            AutoPostBack="True" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblDocumentNo" runat="server" Text="<%$ Resources:SR,DocumentNo%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbDocumentNo" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblCustomNumber" runat="server" Text="<%$ Resources:SR,CustomCode%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCustomNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblBuyer" runat="server" Text="<%$ Resources:SR,Buyer%>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbBuyer" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                    	<qwc:Label ID="lblIndividualsForename" runat="server" Text="<%$ Resources:BM_SR,IndividualsForename %>"/>:
                    </td>
                    <td>
                    	<qwc:TextBox ID="tbIndividualsForename" runat="server" />
                    </td>
				</tr>				
				<tr>
                    <td class="caption">
                    	<qwc:Label ID="lblIndividualsSurname" runat="server" Text="<%$ Resources:BM_SR,IndividualsSurname %>"/>:
                    </td>
                    <td>
                    	<qwc:TextBox ID="tbIndividualsSurname" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblState" runat="server" Text="<%$ Resources:SR,State%>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlState" runat="server" DataTextField="Value" DataValueField="Id"
                            ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="caption">
                        <qwc:RadioButton ID="rbOr" GroupName="Junction" Text="<%$ Resources:SR,Or %>" runat="server"
                            Checked="True" />
                        &nbsp;
                        <qwc:RadioButton ID="rbAnd" GroupName="Junction" Text="<%$ Resources:SR,And %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btSearch" Text="<%$ Resources:SR,Search %>" runat="server" />
                        <asp:Button ID="btReset" CausesValidation="false" Text="<%$ Resources:SR,Reset %>" runat="server" />
                    </td>
                </tr>
            </table>            
        </td>
    </tr>
</table>
