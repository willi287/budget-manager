<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaPersonalDetailContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.PbaPersonalDetailContent" %>

<script type="text/javascript" language="javascript">
    Sys.Application.add_load(WireEvents);
    function WireEvents() { $(document).ready(function() { $('input:checkbox[class=styled]').checkbox({ cls: 'styled-checkbox', empty: '../../../App_Themes/Default/Images/empty.png' }); }); }
</script>

<qwc:AccountInfoControl ID="pbaAccountControl" runat=server />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
