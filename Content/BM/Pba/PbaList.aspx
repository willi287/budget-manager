﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.PbaListPageBase" %>
<%@ Register src="PbaListContent.ascx" tagname="ListContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<script runat="server">
    /// <summary>
    /// Select item in main menu
    /// </summary>
    public override Guid SystemId
    {
        get { return Eproc2.Web.Content.BM.SystemIdentifier.PBA; }
    }
</script>

<asp:Content ID="ctnProvider" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:ListContent ID="ContentControl" runat="server" />
</asp:Content>
