﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaTreeView.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.PbaTreeView" %>
<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <qwc:PredictiveSearchTextBox runat="server" ID="ptbPba" PostBackOnSelect="true" OnValueSelected="OnPbaSelected" />
        <asp:TreeView ID="tvContent" runat="server" ExpandDepth="1" ShowLines="True" SelectedNodeStyle-CssClass="selected" />
    </ContentTemplate>
</asp:UpdatePanel>
