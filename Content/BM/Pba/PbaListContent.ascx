<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.PbaListContent" %>
<%@ Import Namespace="Eproc2.Web.Content.BM.Pba" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<% if (false){%>
 <script type="text/javascript">
     $(window).bind('beforeunload', function () {
         var table = $('.GridEx:first tr');
         for (var i = 0; i < table.length; i++) {
             if ($(table[i]).find("td:nth-child(7)").html() != $(table[i]).find(".columnControl").val()) {
                 return confirm('<%=Controller.PBAMessage() %>');
             }
         }
         return true;
     });
 </script>
<% }%>
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<uc:HelpBox runat="server" HelpBoxId="d99b3fcd-159e-4ea9-b0b5-edec19e480ca" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<%if (Controller.IsVisibleControl(PbaListController.SHOW_ARCHIVED_FILTER))
  {%>
<div id="pbaArchivedFilter">
    <qwc:QWCheckBox ID="chShowArchived" runat="server" AutoPostBack="true" OnCheckedChanged="ShowArchivedCheckedChanged" Text="<%$ Resources:BM_SR,ShowArchived %>" />
</div>
<%}%>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="tBM_Principal.Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" OnEditing="OnEditing">
    <Columns>
        <qwc:SelectColumn EnableHighlight="true" DataField="Selected" IsEditable="true"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="Name" SortExpression="tBM_Principal.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,RefNo %>" DataField="RefNumber" SortExpression="ReferenceNumber" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Address %>" DataField="Address" SortExpression="tAddress.Address" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,PersonalBudgetManager %>" SortExpression="PBMAlias.Name" DataField="Pbm" />
        <qwc:TextColumn  DataField="CurrentSpReferenceName" AllowSorting="False" IsEditable="False" Hidden="true" SortExpression="false"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,SpReferenceValue %>" SortExpression="pbareftypealias1.RefNumber" IsEditable="True" ColumnType="String" DataField="SpReferenceName" AllowSorting="True" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,CurrentBalance %>" SortExpression="tBM_Principal.CurrentBalance" DataField="CurrentBalance" />
    </Columns>
</qwc:ControlledGrid>
