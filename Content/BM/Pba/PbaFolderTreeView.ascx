﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaFolderTreeView.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.PbaFolderTreeView" EnableViewState="false" %>

<asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:TreeView ID="tvContent" runat="server" ExpandDepth="0" ShowLines="True"
            SelectedNodeStyle-CssClass="selected" />
    </ContentTemplate>
</asp:UpdatePanel>

