﻿<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.ManualTransaction.NotReconsiledListContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="E297D60F-1560-4B81-A713-5EA04A2556ED" />
<div id="pbaSearch">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="false" />
</div>
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo"
    runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="TransactionDate" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,TransactionDate %>" DataField="TransactionDate" SortExpression="TransactionDate"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" DataField="Type" AllowSorting="false" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Description" SortExpression="Description"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Status %>" DataField="Status" SortExpression="Status" AllowSorting="false"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Amount %>" DataField="Amount" DataFormatString="{0:£#,0.00}" SortExpression="Amount"/>
    </Columns>
</qwc:ControlledGrid>
