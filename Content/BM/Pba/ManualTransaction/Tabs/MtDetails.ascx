﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MtDetails.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.ManualTransaction.Tabs.MtDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<uc:HelpBox runat="server" HelpBoxId="e3764ba6-2643-46c4-9fdd-b03a755cdea1" />
<table class="form gray">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblTransactionDate" AssociatedControlID="rbIn" runat="server"
                            Text="<%$ Resources:BM_SR, TransactionDate %>" />:<span>*</span>
                    </td>
                    <td>
                        <e2c:calendar id="calTransactionDate" runat="server" EnableDayOfWeek="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <qwc:RadioButton ID="rbIn" runat="server" GroupName="FundingType" Text="<%$ Resources:BM_SR,MoneyIn  %>" />
                        <qwc:RadioButton ID="rbOut" runat="server" GroupName="FundingType" Text="<%$ Resources:BM_SR,MoneyOut  %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblDescription" AssociatedControlID="tbDescription" runat="server"
                            Text="<%$ Resources:BM_SR, TransactionDescription %>" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox MaxLength="50" ID="tbDescription" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblTransactionType" runat="server" AssociatedControlID="ddlTransactionType"
                            Text="<%$ Resources:BM_SR, TransactionType %>" />:<span>*</span>
                    </td>
                    <td>
                        <qwc:DropDownList SortOrder="None" ID="ddlTransactionType" runat="server" DataTextField="Value"
                            DataValueField="Id" AutoPostBack="true" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<table class="form gray" id="tItems" runat="server">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblItemDescription" AssociatedControlID="ptbItemDescription" runat="server"
                            Text="<%$ Resources:BM_SR, ItemDescription %>" />:
                    </td>
                    <td>
                        <qwc:PredictiveSearchTextBox MaxLength="30" ID="ptbItemDescription" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvItemDescription" ValidationGroup="addItem" runat="server"
                            Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" ControlToValidate="ptbItemDescription"
                            ErrorMessage="*" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAmount" AssociatedControlID="tbAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbAmount" runat="server" />
                        <qwc:PriceValidator ID="revAmount" ValidationGroup="addItem" Display="Dynamic" ErrorMessage="*"
                            ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" ControlToValidate="tbAmount"
                            EnableClientScript="true" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvAmount" ValidationGroup="addItem" runat="server" 
                            Display="Dynamic" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" ControlToValidate="tbAmount"
                            ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnAddItem" ValidationGroup="addItem" runat="server" Text="<%$ Resources:BM_SR, AddItem %>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<table class="form">
    <tr>
        <td colspan="3">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgItems"
                runat="server" AllowDeleting="true" AllowViewing="false" AllowEditing="false"  SortExpression="None"
                AllowAdding="false" KeyField="Id">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Description %>" DataField="Description"  SortExpression="None"/>
                    <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Amount %>" DataFormatString="{0:£#,0.00}"  SortExpression="None"
                        DataField="Amount" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
    <tr>
        <td style="width:70%">
            &nbsp;
        </td>
        <td class="gray bold">
            <qwc:InfoLabel ID="lblTotalNet" runat="server" Text="<%$ Resources:BM_SR,Total %>" />:
        </td>
        <td class="gray">
            <qwc:Label ID="lblTotalNetValue" Format="{0:£#,0.00}" runat="server" />
        </td>
    </tr>
</table>
