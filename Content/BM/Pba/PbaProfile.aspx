<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.PbaPageBase" %>
<%@ Register Src="PbaProfileContent.ascx" TagPrefix="uc" TagName="PbaProfileContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:PbaProfileContent ID="PbaProfileContent" runat="server" />
</asp:Content>
