<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamConfirmRelationship.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamConfirmRelationship" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="e6af079b-7e67-4d92-8f81-5e71f73e47c4" />
<table class="form">
    <tr>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblRelationship" runat="server" Text="<%$ Resources:BM_SR,Relationship %>"></qwc:Label>:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlRelationship" runat="server">
                        </qwc:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:Label ID="lblSelectStRole" runat="server" Text="<%$ Resources:BM_SR,SelectStRoleToDelegatePermissions %>"></qwc:Label>:
        </td>
    </tr>
    <tr>
        <td class="caption">
            <table class="subForm grey">
                <tr>
                    <td>
                        <qwc:RadioButton ID="rbSupportTeam" runat="server" GroupName="role" Text="<%$ Resources:BM_SR,SupportTeam %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:RadioButton ID="rbCareManager" runat="server" GroupName="role" Text="<%$ Resources:BM_SR,CareManager %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:RadioButton ID="rbSupportWorker" runat="server" GroupName="role" Text="<%$ Resources:BM_SR,SupportWorker %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ID="cgIcons" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AddLineText="<%$ Resources:BM_SR,AddLine %>" runat="server"
                KeyField="Id" AllowAdding="true" AllowDeleting="true" AllowEditing="false" AllowSelecting="false" SortExpression="None"
                AllowViewing="false">
                <Columns>
                    <qwc:ActionColumn />
                    <qwc:DropDownColumn ItemCellStyle-Width="100%" AutoPostBack="true" DataTextField="Name" DataField="AttributeIconId" DataValueField="Id"
                        HeaderTitle="<%$ Resources:BM_SR,Image %>">
                    </qwc:DropDownColumn>
                    <qwc:TextColumn ItemCellStyle-Width="100%" ColumnType="String" SortExpression="None"  DataField="AttributeDescription" HeaderTitle="<%$ Resources:BM_SR,AttributeDescription %>" MaxLength="200">
                    </qwc:TextColumn>
                    <qwc:TextColumn ItemCellStyle-Width="100%" ColumnType="Int" SortExpression="None" DataField="Order" HeaderTitle="<%$ Resources:BM_SR,Order %>">
                        <Validators>
                            <qwc:ColumnRegExpValidator BoundValidationExpression="Expression" ValidationExpression="^[1-5]$"
                                ToolTip="<%$ Resources:BM_ValidationSR, Msg_NumbersFrom1to5 %>" BoundToolTip="Example" />
                            <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:BM_ValidationSR, Msg_NumbersFrom1to5 %>" />
                        </Validators>
                    </qwc:TextColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
