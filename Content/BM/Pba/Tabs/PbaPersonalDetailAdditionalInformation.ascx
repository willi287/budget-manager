<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaPersonalDetailAdditionalInformation.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaPersonalDetailAdditionalInformation" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="4afecbe9-740b-46a9-81a5-0acc44f5273f" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAdditionalInformation" runat="server" Text="<%$ Resources:BM_SR, AdditionaInformation %>" />:
                    </td>
                </tr>
                <tr>
                    <td>
                        <qwc:TextArea ID="tbAdditionalInformation" runat="server" MaxLength="3000" TextMode="MultiLine" Height="100px" Width="100%"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="caption">
            <asp:Label ID="lblSecureDocumentManagementPortal" runat="server" Text="<%$ Resources: BM_SR, SecureDocumentManagementPortal%>" />:
        </td>
    </tr>
    <tr>
        <td class="comment">
            <asp:Label ID="lblExplanatoryText" runat="server" Text="<%$ Resources: BM_SR, ExplanatoryText%>" />
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnCreateAttachment" runat="server" OnCommand="attachmentAction_OnCommand"/>
        </td>
    </tr>
</table>

<qwc:ControlledGrid ID="cgAttachmentList" runat="server" KeyField="Id" AllowViewing="false" SortExpression="None"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" OnEditing="cgAttachmentList_OnEditing"  OnDeleting="cgAttachmentList_OnDeleting">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn DataField="UserProvidedAttachmentName" SortExpression="None" HeaderTitle="<%$ Resources:SR,FileName %>" />
        <qwc:TextColumn DataField="Description" SortExpression="None" HeaderTitle="<%$ Resources:SR,Description %>" />
        <qwc:TextColumn DataField="UpdateTime" SortExpression="UpdateTime" HeaderTitle="<%$ Resources:SR,DateTime %>" />
        <qwc:TextColumn DataField="UpdaterName" SortExpression="None" HeaderTitle="<%$ Resources:SR,User %>" />
        <qwc:ClientClickLinkColumn DataField="Url" HeaderTitle="<%$ Resources:SR,Download %>" Caption="<%$ Resources:SR,Download %>" ImgUrl="ImgUrl"/>   
    </Columns>
</qwc:ControlledGrid>

