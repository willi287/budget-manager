﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaSettings.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaSettings" %>
    <%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
    <uc:HelpBox runat="server" HelpBoxId="8BDB2ED0-B609-4580-AF38-9CCA00DBF75A" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblApprovalProcedure" runat="server" Text="<%$ Resources:SR,ApprovalProcedure %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlApprovalProcedure" DataTextField="Value" DataValueField="Id"
                            runat="server" ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:QWCheckBox ID="chAutoConfirmDn" runat="server" Text="<%$ Resources:SR,AutoConfirmDN %>"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
