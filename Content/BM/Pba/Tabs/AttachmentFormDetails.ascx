﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttachmentFormDetails.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.AttachmentFormDetails" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="0F8F0D70-4394-4642-A511-9F9B00F441AC" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lbFileName" runat="server" Text="<%$ Resources:SR,FileName%>" AssociatedControlID="tbFileNameValue" />:
                        <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFileNameValue" MaxLength="25" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lbDescription" runat="server" Text="<%$ Resources:SR,Description%>" />:
                    </td>
                    <td>
                        <qwc:TextArea ID="tbDescriptionValue" runat="server" MaxLength="300" TextMode="MultiLine" 
                            Height="100px" Width="80%" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc1:FileUpload SelectFileText="<%$ Resources:SR,FileToUpload%>" MaxFileLength="4194304" MaxLength="210"
                            ID="fileUpload" UploadedFileName="" runat="server" FormatMaxFileLength="<%$ Resources:BM_ValidationSR, MsgWrongFileSize %>" 
                            FileFilter="Excel (*.xlsx, *.xls)|*.xlsx;*.xls|PDF (*.pdf)|*.pdf|Word Document (*.docx, *.doc)|*.docx;*.doc" MsgFileFilterAccepted="<%$ Resources:BM_ValidationSR, MsgFileFilterAccepted %>" />
                    </td>
                </tr>
               
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
