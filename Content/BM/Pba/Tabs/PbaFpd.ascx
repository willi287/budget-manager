﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaFpd.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaFpd" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="8c857137-650e-41a8-a013-2d99fe71682a" />


<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblBirthDate" runat="server" Text="<%$ Resources:BM_SR, DateOfBirth %>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calBirthDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAge" runat="server" Text="<%$ Resources:BM_SR, Age %>" />:
                    </td>
                    <td>
                        <span id="lblAgeContent">
                            <%=Data.Age%></span>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNumberOfHoursSupported" runat="server" Text="<%$ Resources:BM_SR, NumberOfHoursSupported %>" CssClass="clearStyle" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbNumberOfHoursSupported" runat="server" />
                        <asp:RangeValidator ControlToValidate="tbNumberOfHoursSupported" EnableClientScript="true" Display="Dynamic"
                            ErrorMessage="*" MinimumValue="0" MaximumValue="2147483647" Type="Currency" runat="server"></asp:RangeValidator>
                    </td>
                </tr>
                <%if (IsSensitiveInformationVisible())
                  {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEthnicGroup" runat="server" Text="<%$ Resources:BM_SR, EthnicGroup %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlEthnicGroup" runat="server" DataTextField="Value" DataValueField="Id"
                            IsEmptyItemVisible="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblMaritalStatus" runat="server" Text="<%$ Resources:BM_SR, MaritalStatus %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlMaritalStatus" runat="server" DataTextField="Value" DataValueField="Id"
                            IsEmptyItemVisible="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblGender" runat="server" Text="<%$ Resources:BM_SR, Gender %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlGender" runat="server" DataTextField="Value" DataValueField="Id"
                            IsEmptyItemVisible="true" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDisability" runat="server" Text="<%$ Resources:BM_SR, Disability %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlDisability" runat="server" DataTextField="Value" DataValueField="Id"
                            IsEmptyItemVisible="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <qwc:QWCheckBox ID="cbOnAtRisk" runat="server" Text="<%$ Resources:BM_SR, OnAtRiskRegister %>" /></td>
                </tr>
                <%}
                  else if (!Controller.IsViewMode)
                  {%>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnShowSensitiveInformation" runat="server" Text="<%$ Resources:BM_SR, ShowSensitiveInformation %>"
                            OnClick="btnShowSensitiveInformation_Click" />
                    </td>
                </tr>
                <%}%>
            </table>
        </td>
        <td>
            <table class="subForm">
                <%if (Controller.IsCreateMode)
                  {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAdditionalInformation" runat="server" Text="<%$ Resources:SR, AdditionalInformation %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbAdditionalInformation" runat="server" TextMode="MultiLine" Rows="7"
                            MaxLength="5000" />
                    </td>
                </tr>
                <%}%>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AddLineText="<%$ Resources:BM_SR, AddReference %>"
    ID="cgReferences" runat="server" AllowDeleting="true" AllowViewing="false" AllowEditing="false" SortExpression="None"
    AllowAdding="true" KeyField="Id">
    <Columns>
        <qwc:GroupRadioButtonColumn GroupName="Select" DataField="Selected" EnableHighlight="true" IsEditableDataField="Editable" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:DropDownColumn HeaderTitle="<%$ Resources:SR, Organisation %>"
            DataTextField="Name"
            DataValueField="Id"
            DataField="OrganisationId"
            AutoPostBack="True"
            IsEditableDataField="Editable"
            DataSourceDataField="OrganisationsCollection">
        </qwc:DropDownColumn>
        <qwc:DropDownColumn HeaderTitle="<%$ Resources:BM_SR,ReferenceType %>" 
            DataTextField="Name"
            DataValueField="Id" 
            DataField="ReferenceTypeId" 
            AutoPostBack="true" 
            IsEditableDataField="Editable" 
            DataSourceDataField="ReferenceTypesCollection"
            />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ReferenceDetails %>" DataField="RefNumber" SortExpression="None"
            ColumnType="String" MaxLength="200" IsEditableDataField="Editable">
            <qwc:ColumnRegExpValidator BoundValidationExpression="Expression" ValidationExpression="\w+"
                ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" BoundToolTip="Example" />
            <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
        </qwc:TextColumn>
        <qwc:TextColumn DataField="Example" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, Format %>" IsEditable="false"></qwc:TextColumn>
    </Columns>
</qwc:ControlledGrid>
