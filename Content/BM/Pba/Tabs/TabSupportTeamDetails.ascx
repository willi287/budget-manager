﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamDetails" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="af2e3b6c-fa4d-4b6c-a3e1-9cbd00c17a43" />
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblForename" runat="server" Text="<%$ Resources:SR, Forename %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbForename" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblSurname" runat="server" Text="<%$ Resources:SR, Surname %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbSurname" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:Label ID="lblRelationship" runat="server" Text="<%$ Resources:BM_SR,Relationship %>"></qwc:Label>:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlRelationship" runat="server">
                        </qwc:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblOrganisation" runat="server" Text="<%$ Resources:SR, OrganisationName %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblOrganisationContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblAddressLine" runat="server" Text="<%$ Resources:SR, AddressLine %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:Label ID="qlblAddressLineValue" runat="server"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblTown" runat="server" Text="<%$ Resources:SR, Town %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:Label ID="qlblTownValue" runat="server"></qwc:Label>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="qlblCounty" runat="server" Text="<%$ Resources:SR, County %>"></qwc:InfoLabel>:
                    </td>
                    <td>
                        <qwc:Label ID="qlblCountyValue" runat="server"></qwc:Label>
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbPhone" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblMobile" runat="server" Text="<%$ Resources:SR, Mobile %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbMobile" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbFax" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="tbEmail" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
</table>
