<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamSetupPermissions.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamSetupPermissions" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="09b7035e-5d7d-4b1e-95b8-a08e2e46874b" />
<table class="form bordered">
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="ilblSelectedOrganisation" runat="server" Text="<%$ Resources:BM_SR, SelectedRole %>" />:
        </td>
        <td>
            <qwc:Label ID="lblSelectedOrganisationContent" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblS4sSupportPerm" runat="server" Text="<%$ Resources:BM_SR, Shop4supportPermissions %>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:QWCheckBox ID="cbAllowWorkAsIndividual" runat="server" AutoPostBack="false"
                Text="<%$ Resources:BM_SR, AllowWorkAsIndividual %>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:InfoLabel ID="lblSelectPermissions" runat="server" Text="<%$ Resources:BM_SR, SelectPermissions %>" />:
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid runat="server" ID="cgPbm" KeyField="ActionId" AllowPaging="false" SortExpression="None">
                <Columns>
                    <qwc:CheckBoxColumn EnableHiddeReadonlyRow="true" EnableHighlight="true" UseHeaderCheckBox="true" DataField="Selected" IsEditable="true" />
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:BM_SR,ActionName %>" DataField="ResourceId" IsEditable="false" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
