<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaST.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaST" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="e2w" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="25edea4e-2d9e-40ed-8963-fd09d575e5b6" />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl" TagPrefix="e2w" %>
<%@ Register Src="~/Content/BM/BusinessCards/CommonBusinessCard.ascx" TagName="CommonBusinessCard" TagPrefix="bm" %>
<table class="form">
    <tr>
        <td>
            <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="lbtnAdd_Click" Caption="<%$ Resources:SR,Add %>" />
        </td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgPrincipals" SortExpression="None"
    runat="server" KeyField="Id" CssClass="GridEx cellsCentered" >
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" >
            <CustomActions>
                <qwc:CustomAction Id="ViewBusinessCard" OnRenderCondition="ViewBusinessCardRenderCondition" OnClick="OnViewBusinessCardClick"
                    Name="View BC" ImageSkinId="viewbusinesscard" ></qwc:CustomAction>
            </CustomActions>
        </qwc:ActionColumn>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name %>" DataField="PrincipalIdObject.Name" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Relationship %>" DataField="RelationshipIdObject.Name" SortExpression="None" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Organisation %>" DataField="PrincipalIdObject.OrganisationIdObject.Name" SortExpression="None"/>
        <qwc:ResourceColumn  HeaderTitle="<%$ Resources:BM_SR,SystemRole %>" DataField="MemberRoleType" />
        <qwc:MultiImageColumn ImageWidth="25" HeaderTitle="<%$ Resources:BM_SR,AttributeIcons %>" DataField="Id" />
    </Columns>
</qwc:ControlledGrid>
<e2w:PopupTemplatedControl ID="supportTeamBusinessCardPopup" runat="server" CssClass="businessCardPopup" IsSubmitVisible="false" 
    CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <Title />
    <Content>
        <bm:CommonBusinessCard ID="supportTeamBusinessCard" runat="server" />
    </Content>
</e2w:PopupTemplatedControl>
