<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabPersonalDetailDeliveryAddress.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabPersonalDetailDeliveryAddress" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="adb92352-5542-4d5a-8c04-5b028ec42f0d" />
<qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" />
<qwc:ControlledGrid ID="cgAddress" SortExpression="None" AllowEditing=false AllowViewing=false runat="server" AllowSelecting="false" KeyField="Id" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>"/>
        <qwc:TextColumn DataField="Address" ColumnType="String" HeaderTitle="<%$ Resources:SR, AddressLine %>" SortExpression="None"/>
        <qwc:TextColumn DataField="City" ColumnType="String" HeaderTitle="<%$ Resources:SR, Town %>" SortExpression="None"/>
        <qwc:TextColumn DataField="Postcode" ColumnType="String" HeaderTitle="<%$ Resources:SR, Postcode %>" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
