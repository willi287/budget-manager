﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaCatalogue.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaCatalogue" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>    
<%@ Register Src="~/Controls/Core/PopupTemplatedControl.ascx" TagName="PopupTemplatedControl" TagPrefix="e2w" %>
<%@ Register Src="~/Content/BM/BusinessCards/CommonBusinessCard.ascx" TagName="CommonBusinessCard" TagPrefix="bm" %>

<uc:HelpBox runat="server" HelpBoxId="fe31ad38-59ce-482d-9154-c7749b9aa873" />
            <table class="grey">
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        <qwc:DropDownList ID="ddlFilter" SortOrder="None" DataTextField="Value" DataValueField="Id" runat="server"
                            AutoPostBack="true"/>
                    </td>
                    <td style="padding: 5px 5px 5px 5px;">
                    <qwc:DropDownList ID="ddlSpecificFilter" Visible = "false" DataTextField="Value" DataValueField="Id" runat="server" SortOrder="None"
                            AutoPostBack="true" NoSelectedValueText="<%$ Resources:BM_SR, SelectLABcgDropDown %>"  IsEmptyItemVisible="true" />
        
                    </td>
                </tr>
            </table>
<br />
        <qwc:ControlledGrid ID="cgSuppliers" runat="server" SortExpression="None" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
            KeyField="Id" AllowShowCustomActions="true">
            <Columns>
                <qwc:CheckBoxColumn DataField="Selected" UseHeaderCheckBox="true" EnableHighlight="true"
                    EnableCheckRow="true" IsEditable="true" />
                <qwc:CheckBoxColumn DataField="IsFavourite" HeaderTitle="<%$ Resources:BM_SR, AddToFavourites %>" IsEditable="true" >
                    <ControlStyle CssClass="styled"/>            
                </qwc:CheckBoxColumn>
                <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>">
                    <CustomActions>
                        <qwc:CustomAction Id="ViewBusinessCard" OnRenderCondition="ViewBusinessCardRenderCondition" OnClick="OnViewBusinessCardClick"
                            Name="View BC" ImageSkinId="viewbusinesscard" ></qwc:CustomAction>
                    </CustomActions>
                </qwc:ActionColumn>
                <qwc:TextColumn DataField="Name" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, ServiceProviderName %>" />
                <qwc:TextColumn DataField="Address" SortExpression="None" HeaderTitle="<%$ Resources:SR, Address %>" />
                <qwc:TextColumn DataField="Catalogues" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, Catalogues %>" />
                <qwc:TextColumn DataField="SupplierOf" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR, SupplierOf %>" />
                <qwc:MultiImageColumn ImageWidth="25" DataField="Id" HeaderTitle="<%$ Resources:BM_SR, Accreditation %>" />
            </Columns>
        </qwc:ControlledGrid>

<e2w:PopupTemplatedControl ID="businessCardPopup" runat="server" CssClass="businessCardPopup" IsSubmitVisible="false" 
    CancelText="<%$ Resources:SR,Ok %>" IsLogoVisible="false">
    <Title />
    <Content>
        <bm:CommonBusinessCard runat="server" />
    </Content>
</e2w:PopupTemplatedControl>
