<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamSelectUser.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamSelectUser" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3d6cbbb9-996c-451f-ba99-b3c4cc46e328" />
<table style="width: 100%;">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblSelectOrganisationType" runat="server" Text="<%$ Resources:BM_SR, SelectOrganisationType %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlOrganisationType" runat="server" AutoPostBack="true">
                        </qwc:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="ilblInputName" runat="server" Text="<%$ Resources:BM_SR, PleaseInputOrganisationName %>" />:
                    </td>
                    <td>
                        <qwc:PredictiveSearchTextBox runat="server" ID="ptbOrganisation" PostBackOnSelect="true"
                            OnValueSelected="OnOrganisationSelected" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="ilblSelectedOrganisation" runat="server" Text="<%$ Resources:BM_SR, SelectedOrganisation %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblSelectedOrganisationContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
        </td>
    </tr>
</table>
<table class="bordered" style="width: 100%;">
    <tr>
        <td>
            <table class="grey" style="width:auto;">
                <tr>
                    <td class="bold" style="vertical-align:middle">
                        <qwc:Label ID="qlblSelectRequiredUser" runat="server" Text="<%$ Resources:BM_SR, SelectRequiredUser %>"></qwc:Label>:
                    </td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">            
            <qwc:ImageActionButton ID="btnCreate" runat="server" OnCommand="CreateButtonClick" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" AllowPaging="true" SortExpression="None">
                <Columns>
                    <qwc:GroupRadioButtonColumn DataField="Selected" IsEditable="true" EnableHighlight="true"
                        GroupName="main" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Name%>" DataField="Name" SortExpression="None"/>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Address %>" DataField="Address" SortExpression="None"/>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
