<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamCreateUserDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamCreateUserDetails" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblPassword" runat="server" Text="<%$ Resources:SR, Password %>"
                            AssociatedControlID="tbPassword" />: <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPassword" runat="server" TextMode="Password" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvPassword" ControlToValidate="tbPassword" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                        <asp:RegularExpressionValidator ID="revPassword" runat="server" Text="*" ControlToValidate="tbPassword" ValidationExpression=".{0,50}" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblConfirmPassword" runat="server" Text="<%$ Resources:SR, ConfirmPassword %>"
                            AssociatedControlID="tbConfirmPassword" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbConfirmPassword" TextMode="Password" runat="server" /><br />
                        <asp:CompareValidator Display="Dynamic" ID="cvConfirmPassword" ControlToValidate="tbPassword"
                            ControlToCompare="tbConfirmPassword" runat="server" Text="<%$ Resources:SR, EnterTheSamePassword %>" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption" style="padding-top: 10px;">
                        <qwc:InfoLabel ID="lblEmail" runat="server" Text="<%$ Resources:SR, Email %>" AssociatedControlID="tbEmail" />:
                        <span>*</span>
                    </td>
                    <td style="padding-top: 10px;">
                        <qwc:TextBox ID="tbEmail" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvLogin" ControlToValidate="tbEmail" Display="Dynamic"
                            runat="server" ErrorMessage="*" />
                    </td>
                </tr>
                <tr id="IsContactOnlyLabelRow" visible="false" runat="server">
                    <td class="caption">
                        <qwc:InfoLabel ID="IsContactOnlyLabel" Visible="false" runat="server" Text="<%$ Resources:BM_SR, IsContactOnly %>" />:
                    </td>
                    <td class="gray">
                        <qwc:RadioButton class="gray" AutoPostBack="true" GroupName="IsContactOnly" Visible="false" ID="IsContactOnlyNoRadioButton"
                            Text="<%$ Resources:SR,No  %>" runat="server" />
                        <qwc:RadioButton class="gray" AutoPostBack="true" GroupName="IsContactOnly" Visible="false" ID="IsContactOnlyYesRadioButton"
                            Text="<%$ Resources:SR,Yes  %>" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblForename" runat="server" Text="<%$ Resources:SR, Forename %>"
                            AssociatedControlID="tbForename" />: <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbForename" runat="server" MaxLength="25" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="rfvName" ControlToValidate="tbForename" Display="Dynamic"
                            runat="server" ErrorMessage="*" Visible="<%# TabControl is Eproc2.Web.Framework.Tabs.Wizard %>" />
                    </td>
                </tr>
                <tr>
                    <td class="required caption">
                        <qwc:InfoLabel ID="lblSurname" runat="server" Text="<%$ Resources:SR, Surname %>"
                            AssociatedControlID="tbSurname" />: <span>*</span>
                    </td>
                    <td>
                        <qwc:TextBox ID="tbSurname" runat="server" MaxLength="25" />
                        <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            EnableClientScript="true" ID="RequiredFieldValidator1" ControlToValidate="tbSurname"
                            Display="Dynamic" runat="server" ErrorMessage="*" Visible="<%# TabControl is Eproc2.Web.Framework.Tabs.Wizard %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPosition" runat="server" Text="<%$ Resources:SR, Position %>"
                            AssociatedControlID="tbPosition" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPosition" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCulture" runat="server" Text="<%$ Resources:SR, Language %>"
                            AssociatedControlID="ddlCulture" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlCulture" runat="server" DataValueField="Id" DataTextField="Value" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm grey">
                <tr id="trOrgCaption" runat="server" visible="false">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblOrganisation" runat="server" Text="<%$ Resources:SR, Organisation %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblOrganisationContent" runat="server" />
                    </td>
                </tr>
                <tr id="trOrgType" runat="server" visible="false">
                    <td class="caption">
                        <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR, Type %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblTypeContent" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" />
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPhone" runat="server" Text="<%$ Resources:SR, Phone %>" AssociatedControlID="tbPhone" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPhone" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revPhone" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                            ControlToValidate="tbPhone" EnableClientScript="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblMobile" runat="server" Text="<%$ Resources:SR, Mobile%>" AssociatedControlID="tbMobile" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbMobile" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revMobile" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                            ControlToValidate="tbMobile" EnableClientScript="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFax" runat="server" Text="<%$ Resources:SR, Fax%>" AssociatedControlID="tbFax" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbFax" runat="server" MaxLength="30" />
                        <qwc:PhoneValidator ID="revFax" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectData %>"
                            ControlToValidate="tbFax" EnableClientScript="true" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td />
    </tr>
    <tr>
        <td colspan="2">
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ID="cgRoles"
                runat="server" KeyField="Id" SortExpression="None">
                <Columns>
                    <qwc:CheckBoxColumn DataField="HasUser" UseHeaderCheckBox="true" EnableHighlight="true" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,RoleName %>" DataField="Name" SortExpression="None" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
