﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaPersonalDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaPersonalDetails" %>
<%@ Register Src="~/Content/Core/Administration/Address/AddressControl.ascx" TagName="AddressControl"
    TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="9a4c0979-c8ad-4864-b25b-24df5c80a098" />
<table class="form">
    <tr style="height:100%;">
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblTitle" runat="server" AssociatedControlID="ddlTitle" Text="<%$ Resources:SR,Title %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlTitle" runat="server" DataTextField="Value" DataValueField="Id" IsEmptyItemVisible="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblForeName" runat="server" AssociatedControlID="tbForename" Text="<%$ Resources:SR,Forename %>" />:<span>*</span>
                    </td>
                    <td>
                        <table class="skinny">
                            <tr>
                                <td>
                                    <qwc:TextBox ID="tbForename" runat="server" MaxLength="25" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                        EnableClientScript="true" ID="rfvForename" ControlToValidate="tbForename" Display="Dynamic"
                                        runat="server" ErrorMessage="*" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblOtherNames" runat="server" AssociatedControlID="tbOtherNames"
                            Text="<%$ Resources:BM_SR,OtherNames %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbOtherNames" runat="server" MaxLength="25" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblSurname" runat="server" Text="<%$ Resources:BM_SR,Surname %>"
                            AssociatedControlID="tbSurname" />:<span>*</span>
                    </td>
                    <td>
                        <table class="skinny">
                            <tr>
                                <td>
                                    <qwc:TextBox ID="tbSurname" runat="server" MaxLength="25" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                        EnableClientScript="true" ID="rfvSurname" ControlToValidate="tbSurname" Display="Dynamic"
                                        runat="server" ErrorMessage="*" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblPreferredName" runat="server" AssociatedControlID="tbPreferredName"
                            Text="<%$ Resources:BM_SR,PreferredName %>" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbPreferredName" runat="server" MaxLength="50" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="form grey" style="height : 100%">
                <tr>
                    <td style="vertical-align:middle;width: auto; padding : 0px 5px 0px 5px;">
                        <qwc:Image Width="150px" Height="150px" ID="imgLogo" AllowPreview="true" runat="server" PanelID="panelId" />
                    </td>
                    <td align=center style="vertical-align:middle;width: 100%;">
                        <e2w:FileUpload MaxFileLength="4194304" ID="fuLogo" Width="300px" UploadedFileName="" FileFilter="Graphic (*.gif, *.jpg, *.eps, *.bmp, *.png)|*.gif;*.jpg;*.eps;*.bmp;*.png;" IsSubmitOnUpload="true"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <e2w:AddressControl ID="ucAddress" runat="server" LocalSearchEnabled="true" RegisterCustomAddressButtonEnabled="true" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td class="caption" colspan="2">
            <qwc:InfoLabel ID="InfoLabel1" runat="server" Text="<%$ Resources:BM_SR,ContactDetails %>" />
        </td>
    </tr>
</table>
<qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AddLineText="<%$ Resources:BM_SR, AddContact %>"
    ID="cgDetails" runat="server" AllowDeleting="true" AllowViewing="false" AllowEditing="false" SortExpression="None"
    AllowAdding="true" KeyField="Id">
    <Columns>
        <qwc:GroupRadioButtonColumn GroupName="Select" DataField="Selected" EnableHighlight="true" />
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:DropDownColumn HeaderTitle="<%$ Resources:BM_SR,ContactType %>" DataTextField="Name"
            DataValueField="Id" DataField="ContactDetailId" AutoPostBack="true" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ContactDetails %>" DataField="Detail" SortExpression="None"
            ColumnType="String" MaxLength="200">
            <Validators>
                <qwc:ColumnRegExpValidator BoundValidationExpression="Expression" ValidationExpression="\w+"
                    ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>" BoundToolTip="Example" />
                <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
            </Validators>
        </qwc:TextColumn>
        <qwc:TextColumn DataField="Example" HeaderTitle="<%$ Resources:BM_SR, Format %>" IsEditable="false" SortExpression="None"></qwc:TextColumn>
    </Columns>
</qwc:ControlledGrid>
