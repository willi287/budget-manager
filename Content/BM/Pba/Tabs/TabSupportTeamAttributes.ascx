﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabSupportTeamAttributes.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Tabs.TabSupportTeamAttributes" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>    
<uc:HelpBox runat="server" HelpBoxId="289E6627-A317-4143-9FDC-84074569934C" />
 
 <qwc:ControlledGrid ID="cgIcons" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AddLineText="<%$ Resources:BM_SR,AddLine %>" runat="server"
                KeyField="Id" AllowAdding="true" AllowDeleting="true" AllowEditing="false" AllowSelecting="false" SortExpression="None"
                AllowViewing="false">
                <Columns>
                    <qwc:ActionColumn />
                    <qwc:DropDownColumn ItemCellStyle-Width="100%" AutoPostBack="true" DataTextField="Name" DataField="AttributeIconId" DataValueField="Id"
                        HeaderTitle="<%$ Resources:BM_SR,Image %>">
                    </qwc:DropDownColumn>
                    <qwc:TextColumn ItemCellStyle-Width="100%" ColumnType="String"  DataField="AttributeDescription" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,AttributeDescription %>" MaxLength="200">
                    </qwc:TextColumn>
                    <qwc:TextColumn ItemCellStyle-Width="100%" ColumnType="Int" DataField="Order" SortExpression="None" HeaderTitle="<%$ Resources:BM_SR,Order %>">
                        <Validators>
                            <qwc:ColumnRegExpValidator BoundValidationExpression="Expression" ValidationExpression="^[1-5]$"
                                ToolTip="<%$ Resources:BM_ValidationSR, Msg_NumbersFrom1to5 %>" BoundToolTip="Example" />
                            <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:BM_ValidationSR, Msg_NumbersFrom1to5 %>" />
                        </Validators>
                    </qwc:TextColumn>
                </Columns>
            </qwc:ControlledGrid>
