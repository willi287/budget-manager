﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaIndividual.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaIndividual" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>    
<uc:HelpBox runat="server" HelpBoxId="d571fe5b-d6bf-42a5-bc59-a26fa9df90e4" />

<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblIndividual" runat="server" Text="<%$ Resources:BM_SR,SelectAnIndividualUser %>" AssociatedControlID="ddlIndividual"/>:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlIndividual" runat="server" DataTextField="Value" DataValueField="Id" />
                    </td>
                </tr>
            </table>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
