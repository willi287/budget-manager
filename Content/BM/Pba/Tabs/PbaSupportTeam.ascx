﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PbaSupportTeam.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Tabs.PbaSupportTeam" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>    
<uc:HelpBox runat="server" HelpBoxId="59f2dcd3-de82-455e-ab5b-f75a44af5bbf" />
<table class="form grey"  style="padding: 5px 5px 5px 5px;">
    <tr>
        <td>
            <qwc:QWCheckBox ID="cbSupportTeamWizard" runat="server" Checked="true"
                Text="<%$ Resources:BM_SR, IsSupportTeamWizardRequired %>" />
        </td>
    </tr>
</table>
