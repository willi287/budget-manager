﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.Controls.WizardContentControlBase"%>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<script type="text/javascript" language="javascript">
Sys.Application.add_load(WireEvents);
function WireEvents() { $(document).ready(function() { $('input:checkbox[class=styled]').checkbox({ cls: 'styled-checkbox', empty: '../../../App_Themes/Default/Images/empty.png' }); }); }
 </script>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" />
