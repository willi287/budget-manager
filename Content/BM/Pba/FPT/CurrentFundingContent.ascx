<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.Fpt.CurrentFundingContent" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="ccc64b31-75a5-4981-ad50-6dba152036b3" />
<div id="pbaSearch">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="false" />
</div>
<table width="100%">
    <tr>
        <td style="width: 50%">
            &nbsp;
        </td>
        <td class="caption">
            <%=BM_SR.ValueOfCurrentFunding %>:
        </td>
        <td>
            <qwc:Label ID="lblTotalValue" runat="server" />
        </td>
    </tr>
</table>
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo"
    runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="tBM_FS.Name,NextPayment" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id" CssClass="GridEx currentFunding">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Amount %>" DataField="Amount"
            AllowSorting="false" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,FundingSource %>" DataField="FundingSourceName"
            SortExpression="tBM_FS.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,FundingAuthority %>" DataField="FundingAuthorityName"
            SortExpression="tBM_FundingAuthority.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Repeating %>" DataField="Repeating"
            AllowSorting="false" SortExpression="None"/>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,NextPayment %>" DataField="NextPayment"
            SortExpression="NextPayment" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,LastPayment %>" DataField="EndDate"  SortExpression="None" />
        <qwc:BooleanTextColumn DataField="Flag" AllowSorting="false" >
            <ItemCellStyle CssClass="flag columnCell" />
        </qwc:BooleanTextColumn>
    </Columns>
</qwc:ControlledGrid>
