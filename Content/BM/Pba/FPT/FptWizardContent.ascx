﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.Fpt.FptWizardContent"%>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<script type="text/javascript">
    Sys.Application.add_load(WireEvents);
    function WireEvents() {
        $(document).ready(function() {
            $("#datepicker").datepicker({ showButtonPanel: false});
            $("#datepicker").datepicker('disable');
            var v=$("#datepicker").attr("current");
            var d = new Date(); if(v) var dv =Date.parseLocale(v, 'dd/MM/yyyy');
		    if(dv) d = new Date(dv);
            $("#datepicker").datepicker('setDate', d);
        });
    }
</script>
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo"  runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:PlaceHolder ID="plhTabs" runat="server" /> 
