<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptPaymentDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptPaymentDetails" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="abb758c6-86d2-489d-8d84-3fb659e9d1bc" />
<table class="form paymentDetails">
    <tr id="trDifferentPayment" runat="server" visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>">
        <td style="vertical-align: middle; height: 40px;" class="gray">
            <qwc:QWCheckBox ID="cbDifferentPayment" AutoPostBack="true" runat="server" Text="<%$ Resources:BM_SR, IsDifferentPayment %>" />&nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 60%">
            <asp:Panel runat="server" ID="pnlFirstPayment" GroupingText="<%$ Resources:BM_SR, FirstPayment %>">
                <table class="subForm">
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                                AssociatedControlID="tbFirstAmount" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:PriceTextBox ID="tbFirstAmount" runat="server" />
                            <qwc:PriceValidator ID="revFirstAmount" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbFirstAmount" EnableClientScript="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblFirstPaymentDate" AssociatedControlID="calFirstPaymentDate"
                                runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />: <span>*</span>
                        </td>
                        <td>
                            <e2c:calendar id="calFirstPaymentDate" EnableDayOfWeek="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblFirstPayDay" AssociatedControlID="lblFirstPayDayValue" runat="server"
                                Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblFirstPayDayValue" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlRegularPayment" Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>"
                runat="server" GroupingText="<%$ Resources:BM_SR, RegularPayment %>">
                <table class="subForm">
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblRegularAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                                AssociatedControlID="tbRegularAmount" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:PriceTextBox ID="tbRegularAmount" runat="server" />
                            <qwc:PriceValidator ID="pvRegularAmount" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbRegularAmount" EnableClientScript="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="ldlRegularPaymentDate" AssociatedControlID="calRegularPaymentDate"
                                runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />: <span>*</span>
                        </td>
                        <td>
                            <e2c:calendar id="calRegularPaymentDate" EnableDayOfWeek="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblRegularPayDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblRegularPayDayContent" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
        <td align="center">
            <div id="datepicker" current="<%=Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR ? ((Eproc2.Web.Controls.Core.Calendar)calRegularPaymentDate).Value : ((Eproc2.Web.Controls.Core.Calendar)calFirstPaymentDate).Value %>" />
        </td>
    </tr>
</table>
