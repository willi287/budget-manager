﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptSourceDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptSourceDetails" %>
<%@ Import Namespace="Eproc2.Core.Logic.Helpers" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="506642D2-E9EC-41B2-A854-B927A99F20F3" />

<script type="text/javascript">
    Sys.Application.add_load(WireEvents);
    function periodBehaviour() {
        var rbEndAfter = $('#<%=rbEndAfter.ClientID %>').get(0);
        var rbEdnDate = $('#<%=rbEndDate.ClientID %>').get(0);
        var tbEndPeriod = $('#<%=tbEndPeriod.ClientID %>').get(0);
        var cEndDate = $('#<%=EndDateCalendar.TextBoxClientId %>').get(0);
        if (rbEndAfter && tbEndPeriod) tbEndPeriod.disabled = !rbEndAfter.checked;
        if (rbEdnDate && cEndDate) {
            if (rbEdnDate.checked) { cEndDate.disabled = false; $('#tdEndDate input.btnCalendar').show(); }
            else { cEndDate.disabled = true; $('#tdEndDate input.btnCalendar').hide(); }
        }
    }
    function WireEvents() { $(document).ready(function() { periodBehaviour(); }); }
</script>

<table class="form" style="border-collapse: collapse">
    <tr class="gray">
        <td>
            <table class="subForm" style="vertical-align: middle" runat="server">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingAuthority" runat="server" AssociatedControlID="ddlFundingAuthority"
                            Text="<%$ Resources:BM_SR, FundingAuthority %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingAuthority" runat="server" DataTextField="Value" DataValueField="Id"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingSource" runat="server" Text="<%$ Resources:BM_SR, FundingSource %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingSource" runat="server" DataTextField="Value" DataValueField="Id"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingType" runat="server" Text="<%$ Resources:BM_SR, FundingType %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblFundingTypeValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm" style="vertical-align: middle" runat="server" visible="<%#Data.Id != Guid.Empty && Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR && (Data.EndDate >= DateTime.Today || Data.EndDate == null)%>">
                <tr>
                    <td colspan="2">
                        <qwc:Label ID="lblNextPayment" runat="server" Text="<%$ Resources:BM_SR, NextPayment %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNextPaymentAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblNextPaymentAmountValue" Format="<%#DataFormatter.FORMAT_MONEY %>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNextPaymentDate" runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblNextPaymentDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNextPaymentDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblNextPaymentDayValue" runat="server" />
                    </td>
                </tr>
            </table>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table class="subForm" runat="server" visible="<%#Data.SinglePaymentDate != null || (Data.Id == Guid.Empty && Data.IsDifferentPayment) %>">
                <tr>
                    <td colspan="2">
                        <qwc:Label ID="lblFirstPayment" runat="server" Text="<%#Data.FundingType==BM_FundingPaymentTransactionEntity.FT_REGULAR?BM_SR.FirstPayment:BM_SR.SinglePayment %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                            AssociatedControlID="ddlFundingAuthority" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbFirstAmount" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblFirstPaymentDate" AssociatedControlID="ddlFundingAuthority"
                            runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calFirstPaymentDate" EnableDayOfWeek="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFirstPayDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblFirstPayDayValue" runat="server" />
                    </td>
                </tr>
            </table>
            <br runat="server" visible="<%#Data.SinglePaymentDate != null || (Data.Id == Guid.Empty && Data.IsDifferentPayment) %>" />
            <table class="subForm" runat="server" visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>">
                <tr>
                    <td colspan="2">
                        <qwc:Label ID="lblRegularPayment" runat="server" Text="<%$ Resources:BM_SR, RegularPayment %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblRegularAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                            AssociatedControlID="ddlFundingAuthority" />:
                    </td>
                    <td>
                        <qwc:PriceTextBox ID="tbRegularAmount" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblRegularPaymentDate" AssociatedControlID="ddlFundingAuthority"
                            runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />:
                    </td>
                    <td>
                        <e2c:Calendar ID="calRegularPaymentDate" EnableDayOfWeek="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblRegularPayDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblRegularPayDayContent" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <table class="subForm" runat="server" visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>">
                <tr>
                    <td>
                        <qwc:Label ID="lblRepeatingPattern" Text="<%$ Resources:BM_SR, RepeatingPattern %>"
                            runat="server" />
                    </td>
                </tr>
                <tr class="gray">
                    <td style="vertical-align: middle">
                        <asp:Literal ID="litWeeklyPattern" runat="server" Text='<%#String.Format("<b>{0}</b> {1} <b>{2}</b>",BM_SR.RepeatEvery,Data.RepeatFrequency,BM_SR.WeekOn) %>'
                            Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_WEEKLY %>" />
                        <asp:Literal ID="litMonthlyPattern" runat="server" Text='<%#String.Format("<b>{0}</b> {1} <b>{2}</b> {3} <b>{4}</b>",BM_SR.RepeatOn,Data.RepeatPattern,BM_SR.DayOfEvery,Data.RepeatFrequency,BM_SR.months) %>'
                            Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_MONTHLY %>" />
                        <br />
                        <asp:CheckBoxList ID="cblWeeklyPatern" runat="server" RepeatDirection="Horizontal"
                            Enabled="false" Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_WEEKLY %>">
                            <asp:ListItem Text="<%$ Resources:BM_SR, Mo %>" Value="1" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, Tu %>" Value="2" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, We %>" Value="3" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, Th %>" Value="4" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, Fr %>" Value="5" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, Sa %>" Value="6" />
                            <asp:ListItem Text="<%$ Resources:BM_SR, Su %>" Value="0" />
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <asp:Panel runat="server" ID="pnlNewRegularPayment" GroupingText="<%$ Resources:BM_SR, NewRegularPayment %>"
                Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>">
                <table class="subForm" style="height: 125px">
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblNewRegularAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                                AssociatedControlID="tbNewRegularAmount" />:
                        </td>
                        <td>
                            <qwc:PriceTextBox ID="tbNewRegularAmount" runat="server" />
                            <qwc:PriceValidator ID="revNewRegularAmount" Display="Dynamic" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, IncorrectFormatMsg %>"
                                ControlToValidate="tbNewRegularAmount" EnableClientScript="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblNewRegularDate" AssociatedControlID="calNewRegularDate" runat="server"
                                Text="<%$ Resources:BM_SR, FromDate %>" />:
                        </td>
                        <td>
                            <e2c:calendar id="calNewRegularDate" enabledayofweek="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblNewRegularDay" AssociatedControlID="lblNewRegularDayValue"
                                runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblNewRegularDayValue" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel runat="server" ID="pnlEndDate" GroupingText="<%$ Resources:BM_SR, LastPayment %>"
                Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR %>">
                <table class="subForm">
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbNoEndDate" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, NoEndDate %>"
                                CssClass="bold" AutoPostBack="true" onclick="javascript:periodBehaviour()" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndAfter" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndAfter %>"
                                CssClass="bold" AutoPostBack="true" onclick="javascript:periodBehaviour()" />
                        </td>
                        <td>
                            <qwc:TextBox ID="tbEndPeriod" AutoPostBack="true" AllowLabelRendering="<%#Controller.IsViewMode %>"
                                runat="server" Enabled="<%#rbEndAfter.Checked %>" MaxLength="3" CssClass="shortest" />
                            <asp:Literal ID="litPeriod" runat="server" Text="<%$ Resources:BM_SR, Periods %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndDate" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndDate %>"
                                CssClass="bold" AutoPostBack="true" onclick="javascript:periodBehaviour()" />
                        </td>
                        <td id="tdEndDate">
                            <e2c:calendar id="calEndDate" autopostback="true" allowlabelrendering="<%#Controller.IsViewMode %>"
                                enabled="<%#rbEndDate.Checked %>" runat="server" enabledayofweek="true" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblLastPaymentDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblLastPaymentDayValue" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <table class="subForm bordered" runat="server" visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR%>">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblReviewDate" runat="server" Text="<%$ Resources:BM_SR, ReviewDate %>" />:
                    </td>
                    <td id="tdReviewDate">
                        <e2c:calendar id="calReviewDate" enabled="false" enabledayofweek="true" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblReviewDay" runat="server" Text="<%$ Resources:BM_SR, Date %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblReviewDayValue" runat="server" Text="<%$ Resources:BM_SR, Date %>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
