﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptPayments.ascx.cs" Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptPayments" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="3E02A2B4-31C5-4FC9-93B0-03ACCCD1DEF1" />

<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" AllowPaging="true" AllowSorting="true" ManualSource="true" SortExpression="PayDate">
    <Columns>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Amount %>" DataField="Amount" SortExpression="None"/>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,PaymentDate %>" DataField="PayDate" SortExpression="PayDate"/>
    </Columns>
</qwc:ControlledGrid>
