﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptFundingSource.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptFundingSource" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<uc:HelpBox runat="server" HelpBoxId="4f15a3d9-6a17-4706-971d-ae3cd34cc6e2" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td colspan="3" id="tdInitialBallance" class="gray" style="vertical-align:middle;height:40px;" runat="server">
                        <qwc:QWCheckBox ID="cbInitialBallance" AutoPostBack="true" runat="server" Text="<%$ Resources:BM_SR, ClickHereToInitialBalance %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingAuthority" runat="server" AssociatedControlID="ddlFundingAuthority"
                            Text="<%$ Resources:BM_SR, FundingAuthority %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingAuthority" LabelRender="false" runat="server" DataTextField="Value" DataValueField="Id" Enabled="<%#!Data.IsInitialBalance %>" />
                    </td>
                    <td>
                        <asp:Button ID="btnCreateFundingAuthority"  CausesValidation="false" runat="server" Text="<%$ Resources:SR, CreateNew %>" Enabled="<%#!Data.IsInitialBalance %>"  />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingSource" runat="server" Text="<%$ Resources:BM_SR, FundingSource %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingSource" LabelRender="false" runat="server" DataTextField="Value" DataValueField="Id" Enabled="<%#!Data.IsInitialBalance %>" />
                    </td>
                    <td>
                        <asp:Button ID="btnCreateFundingSource" CausesValidation="false" runat="server" Text="<%$ Resources:SR, CreateNew %>" Enabled="<%#!Data.IsInitialBalance %>" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingType" runat="server" Text="<%$ Resources:BM_SR, FundingType %>" />:
                    </td>
                    <td colspan="2">
                        <qwc:RadioButton ID="rbOneOff"  runat="server" GroupName="FundingType" Text="<%$ Resources:BM_SR,Single  %>" Enabled="<%#!Data.IsInitialBalance %>" />
                        <qwc:RadioButton ID="rbRegular" runat="server" GroupName="FundingType" Text="<%$ Resources:BM_SR,Regular  %>" Enabled="<%#!Data.IsInitialBalance %>" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
