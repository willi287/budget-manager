﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptRepeatingPattern.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptRepeatingPattern" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<uc:HelpBox runat="server" HelpBoxId="1a576b0d-e04b-4755-a98c-6c7b4233b336" />

<table id="fMain" class="form" runat="server">
    <tr>
        <td>
            <asp:Panel runat="server" ID="pnlRepeatingPattern" GroupingText="<%$ Resources:BM_SR, RepeatingPattern %>">
                <table class="subForm" style="height:100px">
                    <tr>
                        <td style="vertical-align:middle">
                            <qwc:RadioButton ID="rbWeekly" Text="<%$ Resources:BM_SR, Weekly %>" AutoPostBack="true"
                                GroupName="RepeatingPattern" runat="server" CausesValidation="false" /><br /><br />
                            <qwc:RadioButton ID="rbMonthly" Text="<%$ Resources:BM_SR, Monthly %>" AutoPostBack="true"
                                GroupName="RepeatingPattern" runat="server" CausesValidation="false" />
                        </td>
                        <td class="<%=(rbWeekly.Checked ||rbMonthly.Checked)?"gray":String.Empty %> bold" style="vertical-align:middle">
                            <asp:Literal ID="litRepeatEvery" runat="server" Text="<%$ Resources:BM_SR, RepeatEvery %>"
                                Visible="<%#rbWeekly.Checked %>" />
                            <asp:Literal ID="litRepeatOn" runat="server" Text="<%$ Resources:BM_SR, RepeatOn %>"
                                Visible="<%#rbMonthly.Checked %>" />   
                             <qwc:TextBox ID="tbRepeatPattern"  runat="server" Visible="<%#rbMonthly.Checked %>"
                                CssClass="shortest" MaxLength="2" />
                            <asp:RangeValidator ID="rvRepeatPattern" runat="server" Text="*" ControlToValidate="tbRepeatPattern"
                                MinimumValue="1" MaximumValue="31" Type="Integer" ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>"
                                Display="Dynamic" Visible="<%#rbMonthly.Checked %>" />
                            <asp:Literal ID="litDayOfEvery" runat="server" Text="<%$ Resources:BM_SR, DayOfEvery %>"
                                Visible="<%#rbMonthly.Checked %>" />
                            <qwc:TextBox ID="tbRepeatFrequency" runat="server" CssClass="shortest" MaxLength="2"
                                Visible="<%#rbMonthly.Checked || rbWeekly.Checked %>" />
                            <asp:RangeValidator ID="rvRepeatFrequency" runat="server" Text="*" ControlToValidate="tbRepeatFrequency"
                                Visible="<%#rbMonthly.Checked || rbWeekly.Checked %>" MinimumValue="1" MaximumValue="<%# rbMonthly.Checked ? 12 : 52 %>"
                                Type="Integer" ToolTip="<%$ Resources: ValidationSR, CommonRangeVEMsg %>" Display="Dynamic" />
                            <asp:Literal ID="litWeekOn" runat="server" Text="<%$ Resources:BM_SR, WeekOn %>"
                                Visible="<%#rbWeekly.Checked %>" />
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:BM_SR, months %>" Visible="<%#rbMonthly.Checked %>" />
                           
                            <br />
                            <asp:CheckBoxList ID="cblWeeklyPatern" CssClass="skinny" CellSpacing="3" runat="server" RepeatDirection="Horizontal"
                                Visible="<%#rbWeekly.Checked %>">
                                <asp:ListItem Text="<%$ Resources:BM_SR, Mo %>" Value="1"  />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Tu %>" Value="2"/>
                                <asp:ListItem Text="<%$ Resources:BM_SR, We %>" Value="3"/>
                                <asp:ListItem Text="<%$ Resources:BM_SR, Th %>" Value="4"/>
                                <asp:ListItem Text="<%$ Resources:BM_SR, Fr %>" Value="5"/>
                                <asp:ListItem Text="<%$ Resources:BM_SR, Sa %>" Value="6"/>
                                <asp:ListItem Text="<%$ Resources:BM_SR, Su %>" Value="0"/>
                            </asp:CheckBoxList>
                             <asp:Label ID="lblMonthlyAlert" CssClass="red" runat="server" Visible="<%#rbMonthly.Checked %>" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblReviewDate" runat="server" Text="<%$ Resources:BM_SR, NextReviewDate %>" />:
                    </td>
                    <td id="tdReviewDate">
                        <e2c:calendar id="calReviewDate" enabledayofweek="true" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <asp:Panel runat="server" ID="pnlEndDate" GroupingText="<%$ Resources:BM_SR, EndDate %>">
                <table class="subForm" style="height:100px;width:300px">
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbNoEndDate" AutoPostBack="true" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, NoEndDate %>"
                                Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndAfter" AutoPostBack="true" GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndAfter %>" />
                        </td>
                        <td>
                            <qwc:TextBox ID="tbEndPeriod" AutoPostBack="true"  AllowLabelRendering="<%#Controller.IsViewMode %>" runat="server" Enabled="<%#rbEndAfter.Checked %>" MaxLength="3" CssClass="shortest" />
                            <asp:Literal ID="litPeriod" runat="server" Text="<%$ Resources:BM_SR, Periods %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndDate" AutoPostBack="true"  GroupName="EndDate" runat="server" Text="<%$ Resources:BM_SR, EndDate %>" />
                        </td>
                        <td>
                            <e2c:calendar id="calEndDate" AutoPostBack="true" AllowLabelRendering="<%#Controller.IsViewMode %>" Enabled="<%#rbEndDate.Checked %>" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
        <td align="center">
            <div id="datepicker" current="<%=((Eproc2.Web.Controls.Core.Calendar)calEndDate).Value %>" />
        </td>
    </tr>
</table>
