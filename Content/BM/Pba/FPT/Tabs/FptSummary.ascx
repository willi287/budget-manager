<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FptSummary.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.Fpt.Tabs.FptSummary" %>
<%@ Import Namespace="Eproc2.Core.Logic.Helpers" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers" %>
<%@ Import Namespace="Eproc2.BM.Entities" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:helpbox runat="server" helpboxid="deac54d6-31fc-452f-a44f-16dbd5a2a656" />
<table class="form">
    <tr>
        <td>
            <table class="subForm gray bold" runat="server" visible="<%# Data.IsInitialBalance %>">
                <tr>
                    <td style="vertical-align: middle">
                        <qwc:Label ID="lblInitialBalance" runat="server" Text="<%$ Resources:BM_SR, InitialBalance %>"
                            Visible="<%# Data.IsInitialBalance %>" />
                    </td>
                </tr>
            </table>
            <table class="subForm gray" style="vertical-align: middle" runat="server" visible="<%# !Data.IsInitialBalance %>">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingAuthority" runat="server" AssociatedControlID="ddlFundingAuthority"
                            Text="<%$ Resources:BM_SR, FundingAuthority %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingAuthority" runat="server" DataTextField="Value" DataValueField="Id"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingSource" runat="server" Text="<%$ Resources:BM_SR, FundingSource %>" />:
                    </td>
                    <td>
                        <qwc:DropDownList ID="ddlFundingSource" runat="server" DataTextField="Value" DataValueField="Id"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFundingType" runat="server" Text="<%$ Resources:BM_SR, FundingType %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblFundingTypeValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td style="vertical-align: middle">
            <table class="subForm" runat="server" visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR%>">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblReviewDate" runat="server" Text="<%$ Resources:BM_SR, NextReviewDate %>" />:
                    </td>
                    <td id="tdReviewDate">
                        <e2c:calendar id="calReviewDate" enabledayofweek="true" enabled="false" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel runat="server" ID="pnlFirstPayment" GroupingText="<%$ Resources:BM_SR, FirstPayment %>">
                <table class="subForm">
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                                AssociatedControlID="ddlFundingAuthority" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:Label ID="lblFirstAmountValue" Format="<%#DataFormatter.FORMAT_MONEY %>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblFirstPaymentDate" AssociatedControlID="ddlFundingAuthority"
                                runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:Label ID="lblFirstPaymentDateValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblFirstPayDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblFirstPayDayValue" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
        <td>
            <asp:Panel runat="server" ID="pnlEndDate" GroupingText="<%$ Resources:BM_SR, EndDate %>"
                Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR%>">
                <table class="subForm" disabled="disabled">
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbNoEndDate" Enabled="false" GroupName="EndDate" runat="server"
                                Text="<%$ Resources:BM_SR, NoEndDate %>" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndAfter" Enabled="false" GroupName="EndDate" runat="server"
                                Text="<%$ Resources:BM_SR, EndAfter %>" />
                        </td>
                        <td>
                            <qwc:TextBox ID="tbEndPeriod" runat="server" Enabled="false" AllowLabelRendering="false"
                                MaxLength="3" CssClass="shortest" />
                            <asp:Literal ID="litPeriod" runat="server" Text="<%$ Resources:BM_SR, Periods %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <qwc:RadioButton ID="rbEndDate" Enabled="false" GroupName="EndDate" runat="server"
                                Text="<%$ Resources:BM_SR, EndDate %>" />
                        </td>
                        <td>
                            <qwc:TextBox Enabled="false" AllowLabelRendering="false" ID="tbEndDate" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="pnlRegularPayment" runat="server" GroupingText="<%$ Resources:BM_SR, RegularPayment %>"
                Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR%>">
                <table class="subForm" style="height: 100px">
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="lblRegularAmount" runat="server" Text="<%$ Resources:BM_SR, Amount %>"
                                AssociatedControlID="ddlFundingAuthority" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:Label ID="lblRegularAmountValue" Format="<%#DataFormatter.FORMAT_MONEY %>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption required">
                            <qwc:InfoLabel ID="ldlRegularPaymentDate" AssociatedControlID="ddlFundingAuthority"
                                runat="server" Text="<%$ Resources:BM_SR, PaymentDate %>" />: <span>*</span>
                        </td>
                        <td>
                            <qwc:Label ID="lblRegularPaymentDateValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            <qwc:InfoLabel ID="lblRegularPayDay" runat="server" Text="<%$ Resources:BM_SR, Payday %>" />:
                        </td>
                        <td>
                            <qwc:Label ID="lblRegularPayDayContent" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
        </td>
        <td>
            <asp:Panel runat="server" ID="pnlRepeatingPattern" GroupingText="<%$ Resources:BM_SR, RepeatingPattern %>"
                Visible="<%#Data.FundingType == BM_FundingPaymentTransactionEntity.FT_REGULAR%>">
                <table class="subForm bold" style="height: 100px">
                    <tr>
                        <td style="vertical-align: middle">
                                
                            <asp:Literal ID="litRepeatEvery" runat="server" Text="<%$ Resources:BM_SR, RepeatEvery %>"
                                Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_WEEKLY %>" />
                            <asp:Literal ID="litRepeatOn" runat="server" Text="<%$ Resources:BM_SR, RepeatOn %>"
                                Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_MONTHLY%>" />
                            <qwc:TextBox Enabled="false" AllowLabelRendering="false" ID="tbRepeatPattern" runat="server"
                                Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_MONTHLY %>"
                                CssClass="shortest" MaxLength="2" />
                            <asp:Literal ID="litDayOfEvery" runat="server" Text="<%$ Resources:BM_SR, DayOfEvery %>"
                                Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_MONTHLY%>" />
                            <qwc:TextBox Enabled="false" AllowLabelRendering="false" ID="tbRepeatFrequency" runat="server"
                                CssClass="shortest" MaxLength="2" />
                            <asp:Literal ID="litWeekOn" runat="server" Text="<%$ Resources:BM_SR, WeekOn %>"
                                Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_WEEKLY %>" />
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:BM_SR, months %>" Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_MONTHLY %>" />
                            <br />
                            <asp:CheckBoxList ID="cblWeeklyPatern" runat="server" RepeatDirection="Horizontal"
                                Enabled="false" Visible="<%#Data.RepeatType == BM_FundingPaymentTransactionEntity.RT_WEEKLY %>">
                                <asp:ListItem Text="<%$ Resources:BM_SR, Mo %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Tu %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, We %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Th %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Fr %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Sa %>" Value="6" />
                                <asp:ListItem Text="<%$ Resources:BM_SR, Su %>" Value="0" />
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
        </td>
    </tr>
</table>
