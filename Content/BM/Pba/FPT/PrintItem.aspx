﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" EnableEventValidation="false"
    Inherits="Eproc2.Web.Content.Core.Process.PrintPageBase"
    Title="Untitled Page" %>
<%@ Register TagPrefix="ctrl" TagName="PrintItem" Src="~/Content/BM/Pba/Fpt/PrintItemContent.ascx" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <ctrl:PrintItem ID="ContentControl" runat="server" />
</asp:Content>
