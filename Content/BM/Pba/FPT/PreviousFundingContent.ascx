﻿<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.Fpt.FptListContentBase" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Assembly="Eproc2.Web" Namespace="Eproc2.Web.Controls.Core" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="12a0C6e2-dbe4-436d-a4ca-45529b5cfb7a" />
<div id="pbaSearch">
    <e2w:Search ID="ucSearch" runat="server" AdvancedEnabled="false" />
</div>
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:SR, CantDelete %>" runat="server" />
<qwc:ControlledGrid ID="cgList" SortExpression="tBM_FS.Name,RegularPaymentDate" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,Amount %>" DataFormatString="{0:£#,0.00}" DataField="Amount" AllowSorting="false" SortExpression="None" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,FundingSource %>" DataField="FundingSourceName" SortExpression="tBM_FS.Name" />
          <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,FundingAuthority %>" DataField="FundingAuthorityName" SortExpression="tBM_FundingAuthority.Name" />
          <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Repeating %>" DataField="Repeating" AllowSorting="false" SortExpression="None"/>
          <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,FirstPayment %>" DataField="FirstPayment" SortExpression="RegularPaymentDate" />
          <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,LastPayment %>" DataField="EndDate" SortExpression="EndDate"/>
     </Columns>
</qwc:ControlledGrid>
