﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.PbaPageBase" %>
<%@ Register src="SupportTeamCreateUserContent.ascx" tagname="WizardContent" tagprefix="uc" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="cntSteps" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:WizardContent ID="ContentControl" runat="server" />
</asp:Content>

