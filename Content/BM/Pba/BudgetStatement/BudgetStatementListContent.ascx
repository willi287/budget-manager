<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BudgetStatementListContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.BudgetStatement.BudgetStatementListContent" %>
<%@ Import Namespace="Eproc2.Core.Logic.Helpers" %>
<%@ Import Namespace="Eproc2.Web.Framework.Helpers" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<script type="text/javascript">
    function filterBehaviour(isStatement) {
        $('#bsFilter input.btnCalendar').each(function () { this.disabled = isStatement; });
        $('#bsFilter .startDate input:text').each(function () { this.disabled = isStatement; });
        $('#bsFilter .endDate input:text').each(function () { this.disabled = isStatement; enableValidator(this, !isStatement); });
        $('#bsFilter select').each(function () { this.disabled = !isStatement; });
        $('#<%=ddlStatement.ClientID%>').each(function () { enableValidator(this, isStatement); });
        //if (!isStatement) { $('#tStatement').hide() } else { $('#tStatement').show(); }
        //if (isStatement) { $('#tTotal').hide() } else { $('#tTotal').show(); }
    }
    Sys.Application.add_load(WireEvents);
    function WireEvents() {
        $(document).ready(function () {
            filterBehaviour(document.getElementById('<%=rbStatement.ClientID%>').checked);
        });
    }
    function enableValidator(control, enable) {
        $($.grep(Page_Validators, function (e, i) {
            return e.controltovalidate == control.id;
        })).each(function (i, e) {
            ValidatorEnable(e, enable);
        })
    }
</script>
<uc:HelpBox runat="server" HelpBoxId="B886D774-7691-4779-BC35-EE6ABD01A0CB" />
<div id="bsFilter">
    <qwc:RadioButton ID="rbStatement" onclick="javascript: filterBehaviour(this.checked);" runat="server" GroupName="filter" />
    <asp:RequiredFieldValidator ID="ddlStatementRequiredFieldValidator" ControlToValidate="ddlStatement" EnableClientScript="true" ErrorMessage="*" runat="server" InitialValue="<%$ Resources:BM_SR, NoValues %>"></asp:RequiredFieldValidator>
    <qwc:DropDownList ID="ddlStatement" runat="server" DataTextField="Value" DataValueField="Id" LabelRender="false" SortOrder="None" />
    <qwc:RadioButton ID="rbDate" runat="server" GroupName="filter" onclick="javascript: filterBehaviour(!this.checked);" />
    <e2c:Calendar ID="calStartDate" runat="server" CssClass="startDate" />
    <e2c:Calendar ID="calEndDate" runat="server" Required="true" CssClass="endDate" />
    <asp:Button ID="btnApply" runat="server" Text="<%$ Resources:SR, Search %>" />
</div>
<qwc:AccountInfoControl ID="pbaAccountControl" CssClass="pbaPersonalInfo" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<asp:UpdatePanel ID="pnlTotals" runat="server">
    <ContentTemplate>
        <asp:Panel runat="server" id="divTotals" Style="width: 40%">
            <table class="form bold">
                <tr style="border-collapse: collapse">
                    <td style="width: 40%;" id="tTotal">
                        <asp:Table CssClass="subForm bsForm" runat="server" ID="tTotals">
                            <asp:TableRow CssClass="grey">
                                <asp:TableHeaderCell ColumnSpan="3">
                                    <asp:LinkButton ID="lbtnTotals" runat="server" Text="<%$ Resources:BM_SR, ShowHideTotals %>" CssClass='<%#AreTotalVisible ? "bst expanded" : "bst collapsed" %>' />
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableHeaderCell ColumnSpan="2" CssClass="caption">
                                    <qwc:Label ID="lblTotals" runat="server" Text="<%$ Resources:BM_SR, TransactionTotals %>" />
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow CssClass="alternatingItem">
                                <asp:TableCell Style="width: 40%">
                                          &nbsp;
                                </asp:TableCell><asp:TableCell>
                                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:BM_SR,MoneyIn %>" CssClass=" moneyIn" />
                                </asp:TableCell><asp:TableCell>
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:BM_SR,MoneyOut %>" CssClass="moneyOut" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <qwc:InfoLabel ID="lblPaid" runat="server" Text="<%$ Resources:BM_SR, Paid %>" />:
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblPaidIn" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblPaidOut" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow class="alternatingItem">
                                <asp:TableCell>
                                    <qwc:InfoLabel ID="lblDue" runat="server" Text="<%$ Resources:BM_SR, Due %>" />:
                                </asp:TableCell><asp:TableCell Style="color: Green">
                                    <qwc:Label ID="lblDueIn" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell><asp:TableCell Style="color: Red">
                                    <qwc:Label ID="lblDueOut" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <qwc:InfoLabel ID="lblTotal" runat="server" Text="<%$ Resources:BM_SR, Total %>" />:
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblTotalIn" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblTotalOut" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" id="divStatements">
            <table class="form bold">
                <tr style="border-collapse: collapse">
                    <td style="width: 40%;" id="tStatement">
                        <asp:Table CssClass="subForm bsForm" ID="tStatements" runat="server">
                            <asp:TableHeaderRow CssClass="grey">
                                <asp:TableHeaderCell CssClass="caption" ColumnSpan="2">
                                    <qwc:Label ID="lblPeriod" runat="server" Text="<%$ Resources:BM_SR, TransactionPeriod %>" />
                                </asp:TableHeaderCell><asp:TableHeaderCell>
                                    <qwc:Label ID="lblPeriodValue" runat="server" />
                                </asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell ColumnSpan="2" CssClass="caption">
                                    <qwc:Label ID="lblStatemnentSummary" runat="server" Text="<%$ Resources:BM_SR, StatementSummary%>" />
                                </asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow CssClass="alternatingItem">
                                <asp:TableCell ColumnSpan="2">
                                    <qwc:InfoLabel ID="lblPreviousBalance" runat="server" Text="<%$ Resources:BM_SR, OpeningBalance %>" />:
                                </asp:TableCell><asp:TableCell Style="width: 50%">
                                    <qwc:Label ID="lblPreviousBalanceValue" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <qwc:InfoLabel ID="lblMoneyIn" runat="server" Text="<%$ Resources:BM_SR, MoneyIn %>" />:
                                </asp:TableCell><asp:TableCell>
                                                          <span class="moneyIn">&nbsp</span>
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblMoneyInValue" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow CssClass="alternatingItem">
                                <asp:TableCell>
                                    <qwc:InfoLabel ID="lblMoneyOut" runat="server" Text="<%$ Resources:BM_SR, MoneyOut %>" />:
                                </asp:TableCell><asp:TableCell>
                                                          <span class="moneyOut">&nbsp</span>
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblMoneyOutValue" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <qwc:InfoLabel ID="lblCurrentBalance" runat="server" Text="<%$ Resources:BM_SR, ClosingBalance %>" />:
                                </asp:TableCell><asp:TableCell>
                                    <qwc:Label ID="lblCurrentBalanceValue" runat="server" Format="<%#DataFormatter.FORMAT_MONEY %>" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                    <td style="width: 20%;"></td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<qwc:ControlledGrid ID="cgList" SortExpression="DueDate" runat="server" KeyField="Id" ShowMessageIfNoData="true">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" AutoPosBack="true" />
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,Due %>" DataField="DueDate" SortExpression="DueDate">
            <ItemCellStyle CssClass="columnCell due" />
        </qwc:DateTimeColumn>
        <qwc:DateTimeColumn HeaderTitle="<%$ Resources:BM_SR,Actual %>" DataField="ActualDate" SortExpression="None" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,PaymentSourceDescription %>" DataField="PaymentSource" SortExpression="PaymentSource" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Type %>" DataField="TransactionType"
            AllowSorting="false" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,Status %>" DataField="Status" AllowSorting="false" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,MoneyIn %>" DataField="InAmount"
            SortExpression="Amount">
            <HeaderCellStyle CssClass="columnHeaderCell moneyIn" />
            <ItemCellStyle CssClass="columnCell moneyIn" />
        </qwc:DecimalColumn>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,MoneyOut %>" DataField="OutAmount"
            SortExpression="Amount">
            <HeaderCellStyle CssClass="columnHeaderCell moneyOut" />
            <ItemCellStyle CssClass="columnCell moneyOut" />
        </qwc:DecimalColumn>
    </Columns>
</qwc:ControlledGrid>
