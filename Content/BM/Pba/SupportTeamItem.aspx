﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Pba.PbaPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="SupportTeamItemContent.ascx" tagname="itemcontent" tagprefix="st" %>
<asp:Content ID="ctnProjectItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <st:itemcontent ID="ContentControl" runat="server" />
</asp:Content>
