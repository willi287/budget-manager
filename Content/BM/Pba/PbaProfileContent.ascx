<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/Content/BM/Pba/PbaProfileContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Pba.PbaProfileContent" %>
<%@ Import Namespace="Eproc2.BM.Entities.ListItems" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register src="~/Controls/Core/PopupControl.ascx" tagname="PopupControl" tagprefix="uc" %>
<%@ Register Src="~/Content/BM/BusinessCards/IndividualBusinessCard.ascx" TagName="IndividualBusinessCard"
    TagPrefix="bm" %>
<uc:HelpBox runat="server" HelpBoxId="234c3f3e-09c7-4be7-98ec-41749534b2ca" />
<qwc:AccountInfoControl ID="pbaAccountControl" runat="server" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<%if (IsProfilePageAllowed())
  {%>
  <uc:PopupControl ID="ucPopupControl" runat="server"/>
<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td>
                        <qwc:FolderControl runat="server" ID="fcCalendar" CssClass="pbaCalendar">
                            <Header>
                                <tr class="profileRowHeader">
                                    <td colspan="2">
                                        <div class="profileAreaHeader">
                                            <div class="calendarAreaIcon profileAreaIcon" align="center">
                                            </div>
                                            <div class="profileAreaName" align="center">
                                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:BM_SR,Calendar%>" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </Header>
                            <Content>
                                <asp:Repeater ID="calendarWeekDayRepeater" runat="server">
                                    <ItemTemplate>
                                        <tr class="calendarDayHeader">
                                            <td colspan="2">
                                                <%# Controller.GetLocalizedWeekDay(DataBinder.Eval(Container.DataItem, "Key.DayOfWeek")) %>
                                            </td>
                                        </tr>
                                        <asp:Repeater ID="calendarItemRepeater" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem, "Value") %>'>
                                            <ItemTemplate>
                                                <tr class="item">
                                                    <td class="time">
                                                        <%# GetTime((PbaCalendarListItem)Container.DataItem) %>
                                                    </td>
                                                    <td>
                                                        <%# DataBinder.Eval(Container.DataItem, "CalendarItemName") %>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tr class="alternatingItem">
                                                    <td width="25%">
                                                        <%# GetTime((PbaCalendarListItem)Container.DataItem) %>
                                                    </td>
                                                    <td>
                                                        <%# DataBinder.Eval(Container.DataItem, "CalendarItemName") %>
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </Content>
                        </qwc:FolderControl>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="subForm">
                <tr>
                    <td>
                        <qwc:FolderControl runat="server" ID="fcStatement" CssClass="pbaCalendar">
                            <Header>
                                <tr class="profileRowHeader">
                                    <td colspan="2">
                                        <div class="profileAreaHeader">
                                            <div class="statementAreaIcon profileAreaIcon" align="center">
                                            </div>
                                            <div class="profileAreaName" align="center">
                                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:BM_SR,Statement %>" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </Header>
                            <Content>
                                <asp:Repeater ID="statementRepeater" runat="server">
                                    <ItemTemplate>
                                        <tr class="item">
                                            <td class="caption" style="width: 50%">
                                                <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                            </td>
                                            <td>
                                                <%# DataBinder.Eval(Container.DataItem, "Value") %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="alternatingItem">
                                            <td class="caption">
                                                <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                            </td>
                                            <td>
                                                <%# DataBinder.Eval(Container.DataItem, "Value") %>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </Content>
                        </qwc:FolderControl>
                    </td>
                </tr>

                <%if(!IsSupplierBusinessView()){%>
                <tr>
                    <td>
                        <qwc:FolderControl runat="server" ID="fcStatutoryReviewDate" CssClass="pbaCalendar">
                            <Header>
                                <tr class="profileRowHeader">
                                    <td colspan="2">
                                        <div class="profileAreaHeader">
                                            <div class="statutoryAreaIcon profileAreaIcon" align="center">
                                            </div>
                                            <div class="profileAreaName" align="center">
                                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:BM_SR,StatutoryReviewDate %>" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </Header>
                            <Content>
                                <tr class="item">
                                    <td class="caption" style="width: 50%">
                                        <qwc:Label ID="lblNextStatutoryReview" runat="server" Text="<%$ Resources:BM_SR,SupportPlanReviewDate %>" />:
                                    </td>
                                    <td>
                                        <qwc:Label ID="lblNextStatutoryReviewContent" runat="server" />
                                    </td>
                                </tr>
                            </Content>
                        </qwc:FolderControl>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <qwc:FolderControl runat="server" ID="fcDocuments" CssClass="pbaCalendar">
                            <Header>
                                <tr class="profileRowHeader">
                                    <td colspan="2">
                                        <div class="profileAreaHeader">
                                            <div class="documentAreaIcon profileAreaIcon" align="center">
                                            </div>
                                            <div class="profileAreaName" align="center">
                                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:SR,Documents %>" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </Header>
                            <Content>
                                <asp:Repeater ID="documentsRepeater" runat="server" OnItemDataBound="documentsRepeater_OnDataBound">
                                    <ItemTemplate>
                                        <tr class="item">
                                            <td class="caption" style="width: 50%">
                                                <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                            </td>
                                            <td>
                                                <%# DataBinder.Eval(Container.DataItem, "Value") %>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="LinkButton1" runat="server" OnCommand="ItemsCount_OnCommand"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="alternatingItem">
                                            <td class="caption">
                                                <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                            </td>
                                            <td>
                                                <%# DataBinder.Eval(Container.DataItem, "Value") %>
                                            </td>
                                            <td>   
                                                <asp:LinkButton ID="LinkButton1" runat="server" OnCommand="ItemsCount_OnCommand"></asp:LinkButton>                                            
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </Content>
                        </qwc:FolderControl>
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td>
                        <bm:IndividualBusinessCard ID="individualBusinessCard" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<%}%>
