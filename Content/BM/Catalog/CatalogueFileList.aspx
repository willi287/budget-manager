﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="CatalogueFileListContent.ascx" tagname="CatalogueFileListContent" tagprefix="ctrl" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="ctnCatalogueFileList" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:CatalogueFileListContent ID="ContentControl" runat="server" />
</asp:Content>
