﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="CatalogueProductsPendingListContent.ascx" tagname="CatalogueProductsPendingListContent" tagprefix="ctrl" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="ctnCatalogueProductsList" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:CatalogueProductsPendingListContent ID="ContentControl" runat="server" />
</asp:Content>
