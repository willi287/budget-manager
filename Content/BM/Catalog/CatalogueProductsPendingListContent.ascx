<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueProductsPendingListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsPendingListContent" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register TagPrefix="e2w" TagName="Search" Src="~/Controls/Core/SearchWithFields.ascx" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="0d424294-6f86-487b-a71e-59abb85bc645" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />

<qwc:ControlledGrid ID="cgList" runat="server"
    SortExpression="Code"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    OnViewing="OnViewGridItem" OnEditing="OnEditGridItem"
    KeyField="Id">
    <Columns>
        <qwc:SelectColumn AlwaysVisible="true" IsEditable="true" DataField="Selected"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProductCode %>" DataField="Code" SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProductName %>" DataField="Name" SortExpression="Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, Price %>" DataField="Price" SortExpression="Price" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="Type" AllowSorting="False" SortExpression="None" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ChangeType %>" DataField="ChangeTypeString" AllowSorting="False" SortExpression="None" />
        <qwc:ImageColumn HeaderTitle="<%$ Resources:SR,IsApproved %>" DataField="StateLogo" AlternativeImageUrl="StateLogo" BlankImageUrl="StateLogo" AllowSorting="true" SortExpression="State" />
    </Columns>
</qwc:ControlledGrid>
