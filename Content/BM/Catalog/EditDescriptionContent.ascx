﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDescriptionContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.EditDescriptionContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/HtmlTextEditor.ascx" TagName="TextArea" TagPrefix="ta" %>
<uc:HelpBox runat="server" HelpBoxId="2257235e-c280-4105-b0f3-882c49851906" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <div>
                <ta:TextArea ID="txtEditor" runat="server" />
            </div>
        </td>
    </tr>
</table>
