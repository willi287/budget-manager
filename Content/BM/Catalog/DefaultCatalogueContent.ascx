<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.DefaultCatalogueContent"
    CodeBehind="DefaultCatalogueContent.ascx.cs" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="3d216d1f-e9a7-4e7f-b5e8-a4fb65a86520" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<div class="bordered" style="padding: 20px; float: left;">
    <table>
        <%if (IsPbmFilterVisible())
          {%>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litPbm" runat="server" Text="<%$ Resources:BM_SR,PersonalBudgetManager%>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlPbm" DataValueField="Id" DataTextField="Value" runat="server"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlPbm_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
        <%}%>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litIndividual" runat="server" Text="<%$ Resources:BM_SR,Individual%>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlPba" DataValueField="Id" DataTextField="Value" runat="server"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlPba_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="50%" class="caption">
                <qwc:InfoLabel ID="litBCG" runat="server" Text="<%$ Resources:SR,BuyerCatalogueGroup %>" />:
            </td>
            <td width="50%">
                <qwc:DropDownList ID="ddlBcg" DataValueField="Id" DataTextField="Value" runat="server"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlBcg_SelectedIndexChanged"
                    ComparerType="Eproc2.Web.Framework.Controls.EprocListItemComparer, Eproc2.Web.Framework">
                </qwc:DropDownList>
            </td>
        </tr>
    </table>
</div>
