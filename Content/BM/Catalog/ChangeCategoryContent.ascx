﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeCategoryContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.ChangeCategoryContent" %>
<%@ Register Src="~/Controls/Core/DynamicTreeView.ascx" TagName="DynamicTreeView" TagPrefix="dt" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="fcad4c90-ba92-429f-b0b8-2b537d13c8d6" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <qwc:InfoLabel ID="lblChooseCategory" runat="server" Text="<%$ Resources:SR,ChooseACategory%>"/>:
        </td>
    </tr>
    <tr>
        <td>
            <div runat="server">
                <dt:DynamicTreeView ID="treeView" runat="server" />
            </div>
        </td>
    </tr>
</table>
