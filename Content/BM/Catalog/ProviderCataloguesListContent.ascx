﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderCataloguesListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.ProviderCataloguesListContent" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="fb6870d7-edca-4e8e-bafc-a31f00ed8d0d" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDelete %>" />

<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Code" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/BM/Catalog/ProviderCatalogueItem.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProviderCataloguesListCode %>" DataField="Code" SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProviderCataloguesListName %>" DataField="Name" SortExpression="Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, ProviderCataloguesListOrganisationName %>" DataField="OrganisationName" SortExpression="None" AllowSorting="False" />
    </Columns>
</qwc:ControlledGrid>
