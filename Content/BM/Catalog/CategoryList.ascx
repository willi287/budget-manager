<%@ Import Namespace="Eproc2.Core.Logic.Catalog" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Import Namespace="Eproc2.Web.Framework.Routing" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.CategoryList" %>
<%@ Import Namespace="Eproc2.Web.Content.BM.Catalog"%>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="2cf63441-6736-42bd-9fcd-5b8cb61833c7" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<%if(Controller.IsVisibleControl(CategoryListController.SHOW_FAVOURITES_FILTER)){%>
   <div id="pbaArchivedFilter">
    <qwc:QWCheckBox ID="chShowFavourites" runat=server AutoPostBack=true OnCheckedChanged="ShowFavouritesCheckedChanged" Text="<%$ Resources:BM_SR,ShowFavourites %>" />
</div>
 <%}%>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<asp:DataList Width="100%" ID="dlCategories" runat="server" RepeatDirection="Horizontal"
    CaptionAlign="Top" BorderWidth="0px" CellPadding="0" CellSpacing="0" BorderStyle="None"
    RepeatColumns="4">
    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
    <ItemTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="100">
            <tr>
                <td align="center" valign="middle" style="width: 100px; height: 100px;">
                    <asp:HyperLink ID="CategoryLink" runat="server" OnDataBinding="CategoryLink_DataBinding">
                        <asp:Image ID="CategoryImage" runat="server" OnDataBinding="CategoryImage_DataBinding" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100px; height: 20px; text-align: center;">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="blue" Text='<%# Eval("CategoryName")%>' OnDataBinding="CategoryLink_DataBinding">                        
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>
