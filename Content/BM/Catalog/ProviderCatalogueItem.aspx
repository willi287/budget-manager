﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationPageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register src="ProviderCatalogueItemContent.ascx" tagname="ItemContent" tagprefix="ctrl" %>
<asp:Content ID="ctnProviderCatalogueItem" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ItemContent ID="ContentControl" runat="server" />
</asp:Content>