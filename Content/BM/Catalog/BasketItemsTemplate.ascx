<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasketItemsTemplate.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.BasketItemsTemplate" %>
<qwc:Label ID="lblServiceProviderName" runat=server CssClass="basket_sp_name"></qwc:Label>
<asp:HiddenField ID="hfServiceProviderId" runat="server" />
<br />
<qwc:ControlledGrid ID="cgListScheduled" SortExpression="None" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ShowHeaderIfNoData=false ShowMessageIfNoData=false ManualSource=true KeyField="Id" AllowDeleting="true" AllowViewing="true" AllowEditing="false" AllowPaging="false">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="Url" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ItemsThatCanBeScheduled %>" DataField="Name" SortExpression="None" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,PricePerUnit %>" DataField="PricePerUnitGross" SortExpression="None" />
        <qwc:TextColumn  ColumnType="IntNullable" HeaderTitle="<%$ Resources:BM_SR,HowManyAppointmentTimes %>" SortExpression="None" DataField="AppointmentTimes" IsEditable="true" MaxLength="3">
            <Validators>
                <qwc:ColumnRangeValidator Type="Integer" MinimumValue="1" MaximumValue="1000" ToolTip="<%$ Resources:BM_ValidationSR, ValueGreaterThanZero %>" />
                <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
            </Validators>
        </qwc:TextColumn>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:BM_SR,BasketItemStatus %>" DataField="Status"/>        
    </Columns>
</qwc:ControlledGrid>
<br />
<qwc:ControlledGrid ID="cgListOneOffOrder" SortExpression="None" runat="server" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ShowHeaderIfNoData=false ShowMessageIfNoData=false ManualSource=true KeyField="Id" AllowDeleting="true" AllowViewing="true" AllowEditing="false" AllowPaging="false" OnItemDataBound="cgListOneOffOrder_ItemDataBound">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="Url" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Item %>" DataField="Name" SortExpression="None" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,PricePerUnit %>" DataField="PricePerUnitGross" SortExpression="None" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, Quantity %>" DataField="Quantity" IsEditable="true" SortExpression="None" >
            <Validators>                
                <qwc:ColumnRangeValidator Type="Integer" MinimumValue="1" ID="Qty_Validator_ID" ToolTip="<%$ Resources:BM_ValidationSR, ValueGreaterThanZero %>" />                
                <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
            </Validators>
        </qwc:DecimalColumn>       
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:BM_SR,BasketItemStatus %>" DataField="Status"/>        
    </Columns>
</qwc:ControlledGrid>
<asp:Button ID="btnProcess" runat="server" Text="<%$ Resources:BM_SR,Process %>"
    OnClick="btnProcess_Click" />
