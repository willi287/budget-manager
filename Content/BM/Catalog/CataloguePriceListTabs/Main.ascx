﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CataloguePriceListTabs.Main" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="e2w" TagName="PopupControl" Src="~/Controls/Core/PopupControl.ascx" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="B431BBCD-0A45-47AE-9972-AD83A1FCC9A1" />

<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, ProcedureInContractMsg %>" runat="server" />
<table class="form">
    <tr>
        <td>
          <table class="subForm">
              <tr>
                  <td class="caption">
                      <qwc:InfoLabel ID="lblPriceListCode" runat="server" Text="<%$ Resources:SR, PriceListCode %>"
                            AssociatedControlID="lblPriceListCodeValue" />:
                  </td>
                  <td class="caption">
                       <qwc:Label ID="lblPriceListCodeValue" runat="server" />
                  </td>
              </tr>
              <tr>
                  <td class="caption">
                       <qwc:InfoLabel ID="lblCatalogueGroup" runat="server" Text="<%$ Resources:SR, AT_CM_BCG %>" />:
                  </td>
              </tr>
              <tr>
                  <td>
                      <qwc:ImageActionButton ID="btnAdd" runat="server" OnCommand="btnAdd_Click" Caption="<%$ Resources:SR,AddGroup %>" /> 
                  </td>
              </tr>
          </table>
        </td>
    </tr>
    <tr>
        <td>
             <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" 
                 OnDeleting="CgDeleting" KeyField="Id" >
                <Columns>         
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BCGName %>" DataField="BcgIdObject.Name" SortExpression="Name" AllowSorting="True"/>      
                </Columns>
             </qwc:ControlledGrid>
        </td>
    </tr>
</table>
