﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasketContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.BasketContent" %>
<%@ Register Src="~/Controls/Core/Search.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/BM/BasketItemsProcessedBox.ascx" TagName="BasketItemsProcessedBox" TagPrefix="uc" %>
<%@ Register Src="BasketItemsTemplate.ascx" TagName="BasketItemsGroupTemplate" TagPrefix="uc1" %>
<uc:HelpBox runat="server" HelpBoxId="0EDA4CD1-7039-4E21-A299-4AD8552E36BD" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
    
<uc:BasketItemsProcessedBox ID="basketItemsProcessedBox" runat=server />

<qwc:RepeatControl ID="SummaryRepeatControl" runat="server">
    <ItemTemplate>
        <uc1:BasketItemsGroupTemplate ID="BasketItemsGroupTemplate" runat="server" OnBasketProcess="OnBasketProcess" />
        <br />
        <br />
    </ItemTemplate>
</qwc:RepeatControl>
<br />
<qwc:QWCheckBox ID="chShowAllOrderedItems" runat=server AutoPostBack=true Text="<%$ Resources:BM_SR,ShowAllOrderedItems %>" />
<br />

<%if(chShowAllOrderedItems.Checked){%>
   <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="tBM_Product.Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" ManualSource=true KeyField="Id" AllowDeleting="true" AllowViewing="true" AllowEditing="false" AllowPaging="false">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrlBindField="Url" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Item %>" DataField="Name" SortExpression="tBM_Product.Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:BM_SR,PricePerUnit %>" DataField="PricePerUnitGross" SortExpression="tBM_Product.Price" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,ServiceProviderName %>" DataField="ServiceProviderName" SortExpression="tBM_Org.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AppointmentTimes %>" DataField="AppointmentTimes" SortExpression="None"/>
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, Quantity %>" DataField="Quantity" SortExpression="None"/>        
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:BM_SR,BasketItemStatus %>" DataField="Status"/>        
    </Columns>
</qwc:ControlledGrid>
 <%}%>
