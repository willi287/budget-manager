﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CataloguePriceListsListContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.CataloguePriceListsListContent" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="6f1c6092-aacc-4947-aa37-8e2d6f9db6e2" />
<% if ( Controller.GetFilterValues().Count > 2)
              { %>
    <div id="search">
        <table>
            <tr>
                <td style="vertical-align: top;">
                    <asp:DropDownList ID="ddlField" DataTextField="LocalizedName" AutoPostBack="true"
                        DataValueField="FieldName" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<% } %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand"
    runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDelete %>" />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Code" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" NavigateUrl="~/Content/BM/Catalog/CataloguePriceList.aspx" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, CataloguePriceListsCode %>" DataField="Code"
            SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, CataloguePriceListBcgName %>" DataField="CatalogueGroup"
            SortExpression="None" AllowSorting="false" />
    </Columns>
</qwc:ControlledGrid>
