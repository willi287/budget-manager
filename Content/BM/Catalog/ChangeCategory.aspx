﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.ChangeCategory" %>
<%@ Register Src="ChangeCategoryContent.ascx" TagPrefix="uc" TagName="ChangeCategoryContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:ChangeCategoryContent ID="changeCategoryContent" runat="server" />
</asp:Content>
