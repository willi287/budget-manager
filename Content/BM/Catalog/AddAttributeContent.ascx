﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddAttributeContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.AddAttributeContent" %>
<%@ Register Src="~/Controls/Core/DynamicTreeView.ascx" TagName="DynamicTreeView" TagPrefix="dt" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="fd9ffb60-d83c-4118-890c-3eb45e05df08" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<br/>
<% if(IsNotItems){%>
     <qwc:InfoLabel ID="lblNoItemsMsg" runat="server" Text="<%$ Resources:SR,Msg_NoItemsFound%>"/>
<% }else{ %>
<table class="form">
    <tr>
        <td>
            <qwc:InfoLabel ID="lblChooseAttributes" runat="server" Text="<%$ Resources:SR,ChooseAttributesLabel%>"/>:
        </td>
    </tr>
    <tr>
        <td>
            <div runat="server">
                <dt:DynamicTreeView ID="treeView" runat="server" />
            </div>
        </td>
    </tr>
</table>
<% }%>