<%@ Control Language="C#" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.ProductsListContent" %>
<%@ Import Namespace="Eproc2.Web.Content.BM.Catalog"%>
<%@ Register Src="~/Controls/Core/FreeSearch.ascx" TagName="Search" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<%if(Controller.IsVisibleControl(ProductListController.SHOW_FAVOURITES_FILTER)){%>
<div id="pbaArchivedFilter">
    <qwc:QWCheckBox ID="chShowFavourites" runat=server AutoPostBack=true OnCheckedChanged="ShowFavouritesCheckedChanged" Text="<%$ Resources:BM_SR,ShowFavourites %>" />
</div>
 <%}%>
<div>
    <div class="floatLeft">
        <uc:HelpBox runat="server" HelpBoxId="173b9090-4832-4f2d-9769-1bb46dc4947d" />
    </div>
    <%if(Controller.IsVisibleControl(ProductListController.BASKET_SUMMARY_CONTROL)){%>
   <table class="basketSummary">
    <tr class="Header">
        <td colspan="3">
        </td>
    </tr>
    <tr class="InfoRow">
        <td>
            <qwc:Label ID="lblMyBasketItems" runat="server" Text="<%$ Resources:BM_SR,MyBasketItems %>"/>            
        </td>
        <td>
            <qwc:Label ID="lblMyBasketItemsCount" runat="server"/>
        </td>
        <td>
        </td>
    </tr>
    <tr>        
        <td colspan="3" align=right>
            <asp:Button ID="btnCheckout" runat="server" OnCommand="btnCheckout_OnCommand" Text="<%$ Resources:BM_SR,Checkout %>" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
    </tr>
</table>
 <%}%>
</div>
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="ListItemId" AllowDeleting="false" AllowEditing="false" SortExpression="Name"
    AllowPaging="true">
    <Columns>
        <qwc:SelectColumn AlwaysVisible="false" IsEditable="true"  DataField="Selected"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:ImageColumn AllowPreview="true" HeaderTitle="<%$ Resources:SR,Image %>" DataField="ImageUrl"
            SortExpression="MfrCode" Width="75" AlternativeImageUrl="LogoUrl" BlankImageUrl="BlankUrl" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="tPriceListItem.ProductCode" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductName %>" DataField="Name" SortExpression="Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,AppointmentDuration %>" DataField="AppointmentsDuration" AllowSorting="false" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:BM_SR,Availability %>" DataField="Availability" AllowSorting="false" SortExpression="None"/>
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,SupplierName %>" DataField="SupplierName"
            SortExpression="tSupplierOrg.Name" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,Manufacturer %>" DataField="Mfr" SortExpression="Manufacturer" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR,NetInvoicePrice %>" DataField="NetInvoicePrice"
            SortExpression="tPriceListItem.NetInvoicePrice" />
    </Columns>
</qwc:ControlledGrid>
