﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributesListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.AttributesListContent" %>
<%@ Register TagPrefix="uc2" TagName="PopupControl" Src="~/Controls/Core/PopupControl.ascx" %>
<%@ Register TagPrefix="e2w" TagName="Search" Src="~/Controls/Core/SearchWithFields.ascx" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox2" runat="server" HelpBoxId="566a0c75-a098-4789-91a4-5bf3e1cec5c9" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />
<qwc:ControlledGrid ID="cgList" runat="server"
    SortExpression="Name"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProductName %>" DataField="Name" SortExpression="Name" />
    </Columns>
</qwc:ControlledGrid>