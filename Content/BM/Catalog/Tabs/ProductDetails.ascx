<%@ Import Namespace="Eproc2.Web.Content.BM.Catalog" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.Tabs.ProductDetails" %>
<%@ Register src="~/Controls/Core/HelpBox.ascx" tagname="HelpBox" tagprefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="5f347940-ce67-4c9d-b28a-3606c631991a" />
<table class="form">
    <tr>
        <td valign="top">
            <table class="subForm">
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblProductName" AssociatedControlID="txtProductName" runat="server"
                            Text="<%$ Resources:SR,ProductName %>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:Label ID="txtProductName" runat="server" />
                    </td>
                </tr>
                <%if (Controller.IsVisibleControl(ProductEntity.PROP_SUPPLIERCATALOGUEID))
                  {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblSupplier" runat="server" Text="<%$ Resources:SR,Supplier %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblValueSupplier" runat="server" />
                    </td>
                </tr>
                <%
                    }%>
                <tr>
                    <td class="caption required">
                        <qwc:InfoLabel ID="lblProductCode" AssociatedControlID="txtProductCode" runat="server"
                            Text="<%$ Resources:SR,SupplierCode %>" />:&nbsp;<span>*</span>
                    </td>
                    <td>
                        <qwc:Label ID="txtProductCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblUnits" runat="server" Text="<%$ Resources:SR,Units %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="ddlUnits" runat="server" />
                    </td>
                </tr>
                <%if (Controller.IsVisibleControl(PriceListItemEntity.PROP_NETINVOICEPRICE))
                  {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblNetInvoice" runat="server" Text="<%$ Resources:SR,NetInvoicePrice%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtNetInvoice" runat="server" />
                    </td>
                </tr>
                <%
                    }%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblVat" runat="server" Text="<%$ Resources:SR,VAT %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="ddlVat" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <%if (Controller.IsVisibleControl(BCGProductController.SHOW_SERVICE_DATA))
                  {%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAppointsmentDuration" runat="server" Text="<%$ Resources:BM_SR,AppointmentDuration%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtAppointsmentDuration" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblAvailability" runat="server" Text="<%$ Resources:BM_SR,Availability%>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtAvailability" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <%
                    }%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDescription" runat="server" Text="<%$ Resources:SR,Description %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtDescription" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCategory" runat="server" Text="<%$ Resources:SR,Category %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtCategory" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblLeadTime" runat="server" Text="<%$ Resources:SR,LeadTime %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtLeadTime" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblManufacturer" runat="server" Text="<%$ Resources:SR,Manufacturer %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtManufacturer" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblManufacturerCode" runat="server" Text="<%$ Resources:SR,ManufacturerCode %>" />:
                    </td>
                    <td>
                        <qwc:Label ID="txtManufacturerCode" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="subForm" >
                <tr>
                    <td colspan="2">
                        <qwc:Image ID="imgProductLogo" AllowPreview=true runat="server" PanelID="panelId" />
                    </td>
                </tr>
                 <asp:Repeater ID="rptDocuments" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="ProductAttachmentsIcon">
                                <asp:HyperLink ID="lnkPdf" runat="server" NavigateUrl="<%# Container.DataItem %>">
                                    <asp:Image ID="imgPdf" runat="server" ImageUrl="<%# GetPdfImageUrl() %>" />
                                </asp:HyperLink>    
                            </td>
                            <td class="ProductAttachments">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:SR,ProductDescription%>" />&nbsp;<%# Container.ItemIndex + 1 %>                                                            
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function CheckProducts() {

        var http = getHTTPObject(); // We create the HTTP Object

        function checkFilesExists() {
        var links = $('.ProductAttachmentsIcon').find('a')

            for (i = 0; i < links.length; i++) {
                if (!checkFileExists(links[i].href)) {
                    links[i].parentNode.parentNode.style.visibility = 'hidden';
                }
            }
        }

        checkFilesExists();

        function checkFileExists(handleRequest) {
            /*http.onreadystatechange = handleHttpReceiveNewPwd;*/
            /*http.setRequestHeader('Content-Type', 'application/pdf');*/
            try {
                http.open('HEAD', handleRequest, false);
                http.send(null);
                return http.status == 200;
            } catch (e) {
                return false;
            }
        }

        function getHTTPObject() {
            if (window.XMLHttpRequest) { // Mozilla, Safari,...
                var obj = new XMLHttpRequest();
                if (obj.overrideMimeType) {
                    obj.overrideMimeType('application/pdf');
                }
            }
            else if (window.ActiveXObject) { // IE
                try {
                    var obj = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    try {
                        var obj = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) { }
                }
            }
            if (obj)
                return obj;
        }

    }
    </script>
