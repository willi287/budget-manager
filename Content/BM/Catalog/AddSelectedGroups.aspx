﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.AddSelectedGroups" %>
<%@ Register Src="AddSelectedGroupsContent.ascx" TagPrefix="uc" TagName="AddSelectedGroupsContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:AddSelectedGroupsContent ID="addSelectedGroupsContentContent" runat="server" />
</asp:Content>