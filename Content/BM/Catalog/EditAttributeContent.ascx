﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditAttributeContent.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.EditAttributeContent" %>
<%@ Register Src="~/Controls/Core/DynamicTreeView.ascx" TagName="DynamicTreeView" TagPrefix="dt" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox runat="server" HelpBoxId="aa45b4f6-c9e8-4bfb-a9d6-414620ffdddc" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<br/>
 <asp:Table runat="server" class="subForm" Width="60%">
                <asp:TableRow>
                    <asp:TableCell class="caption">
                      <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, AttributeName %>"/>:
                   </asp:TableCell>
                    <asp:TableCell>
                       <qwc:Label ID="lblNameValue" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell>
                     </asp:TableCell>
                    <asp:TableCell>
                      <qwc:QWCheckBox runat="server" ID="cbMultipleValueAllow" Text="<%$ Resources:SR, MultipleValueAllowed %>" Enabled="False"/> 
                    </asp:TableCell>
                </asp:TableRow>
         </asp:Table>
<br/>
<% if(IsNotItems){%>
     <%--<qwc:InfoLabel ID="lblNoItemsMsg" runat="server" Text="<%$ Resources:SR,Msg_NoItemsFound%>"/>--%>
<% }else{ %>
<table class="form">
    <tr>
        <td>
            <qwc:InfoLabel ID="lblChooseAttributeValues" runat="server" Text="<%$ Resources:SR,ChooseAttributeValueToThisCatalogueItemLabel%>"/>:
        </td>
    </tr>
    <tr>
        <td>
            <div runat="server">
                <dt:DynamicTreeView ID="treeView" runat="server" />
            </div>
        </td>
    </tr>
</table>
<% }%>
