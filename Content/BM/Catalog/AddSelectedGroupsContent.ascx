﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddSelectedGroupsContent.ascx.cs" 
    Inherits="Eproc2.Web.Content.BM.Catalog.AddSelectedGroupsContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="24e42499-368a-4706-a50c-62df778510b8" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<table class="form">
    <tr>
        <td>
            <div runat="server">
             <% if (IsAnyBcgsAvaible)
              { %>
                    <qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" KeyField="Id">
                    <Columns>
                        <qwc:SelectColumn AlwaysVisible="true" IsEditable="true"  DataField="Selected" />         
                        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,BuyerCatalogueName %>" DataField="Name" SortExpression="Name" AllowSorting="True"/>      
                    </Columns>
                 </qwc:ControlledGrid>
            <% }
            else
            { %>
                <qwc:Label ID="lblNotBcgs" runat="server" Text="<%$ Resources:SR, Msg_NoItemsFound%>" />
                <br />
            <% } %>

            </div>
        </td>
    </tr>
</table>
