﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.ProviderCatalogueTabs.Main" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="A9164366-3F9E-4C75-B8AA-A323009BA55D" />

<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <%if (Controller.IsVisibleControl(SupplierCatalogueEntity.PROP_VW_SUPPLIERCATALOGUE_ID))
                  {%>
                <tr>                    
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCatalogueCode" runat="server" Text="<%$ Resources:SR, ProviderCatalogueCode %>"
                            AssociatedControlID="lblCatalogueCodeValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblCatalogueCodeValue" runat="server" />
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblCatalogueName" runat="server" Text="<%$ Resources:SR, ProviderCatalogueName %>"
                            AssociatedControlID="tbCatalogueName" />:
                    </td>
                    <td>
                        <qwc:TextBox ID="tbCatalogueName" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblOrganisationName" runat="server" Text="<%$ Resources:SR, ProviderCatalogueOrganisationName %>"
                            AssociatedControlID="lblOrganisationNameValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblOrganisationNameValue" runat="server" />
                    </td>
                </tr>              
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblDisableCatalogue" runat="server" Text="<%$ Resources:SR, ProviderCatalogueDisabled %>"
                            AssociatedControlID="cbDisableCatalogue" />:
                    </td>
                    <td>
                        <qwc:QWCheckBox ID="cbDisableCatalogue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
