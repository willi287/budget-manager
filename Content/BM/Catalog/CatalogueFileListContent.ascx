﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueFileListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueFileListContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="901033ef-0400-4d5c-9cb2-a34501132662" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<qwc:ControlledGrid ID="cgList" runat="server" KeyField="Id" SortExpression="CreateTime" SortDirection="False" OnItemDataBound="cgList_ItemDataBound">
    <Columns>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CatalogueFileListFileName%>" DataField="FileName" SortExpression="tFileInfo.FileName" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CatalogueFileListUploadTime%>" DataField="CreateTime" SortExpression="CreateTime" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CatalogueFileListUploadedBy%>" DataField="CreatorName" SortExpression="tBM_Principal.Name" />
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,CatalogueFileListStatus%>" DataField="StatusString" SortExpression="None" AllowSorting="False"/>
    </Columns>
</qwc:ControlledGrid>
