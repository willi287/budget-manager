﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true"
    Inherits="Eproc2.Web.Content.Core.Catalog.CatalogueListPageBase" %>

<%@ Register Src="~/Content/BM/Catalog/ProductsListContent.ascx" TagName="ProductsList"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc1:ProductsList ID="ContentControl" runat="server" />
</asp:Content>
