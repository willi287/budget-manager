﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.EditAttribute" %>
<%@ Register Src="EditAttributeContent.ascx" TagPrefix="uc" TagName="EditAttributeContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:EditAttributeContent ID="editAttributeContent" runat="server" />
</asp:Content>
