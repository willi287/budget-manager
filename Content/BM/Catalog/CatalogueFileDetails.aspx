﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="CatalogueFileDetailsContent.ascx" TagName="CatalogueFileDetailsContent" TagPrefix="uc" %>

<asp:Content ID="CatalogueFileDetailsContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:CatalogueFileDetailsContent ID="Content" runat="server" />
</asp:Content>
