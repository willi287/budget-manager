﻿<%@ Page Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Framework.UI.PageBase" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>
<%@ Register Src="CatalogueFileUploadContent.ascx" TagName="CatalogueFileUploadContent" TagPrefix="uc" %>

<asp:Content ID="CatalogueFileUploadContent" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <uc:CatalogueFileUploadContent ID="Content" runat="server" />
</asp:Content>
