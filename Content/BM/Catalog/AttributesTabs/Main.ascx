﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.AttributesTabs.Main" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="e2w" TagName="PopupControl" Src="~/Controls/Core/PopupControl.ascx" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="6B052830-F9B2-478B-A0B6-BCA3F67ACF82" />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />

<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, NotPossibleToDelete %>" runat="server" />

<table width="50%">
    <tr>
        <td>
           <asp:Table ID="Table1" runat="server" class="subForm">
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                      <qwc:InfoLabel ID="lblName" runat="server" Text="<%$ Resources:SR, AttributeName %>"
                            AssociatedControlID="txtNameValue" />:&nbsp;<span>*</span>
                   </asp:TableCell>
                    <asp:TableCell>
                       <qwc:TextBox ID="txtNameValue" runat="server" MaxLength="255" />
                       <asp:RequiredFieldValidator ID="tbNameValueEmptyValidator" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="txtNameValue" EnableClientScript="true" runat="server">
                       </asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell>
                     </asp:TableCell>
                    <asp:TableCell>
                      <qwc:QWCheckBox runat="server" ID="cbMultipleValueAllow" Text="<%$ Resources:SR, MultipleValueAllowed %>"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell class="required caption">
                          <qwc:InfoLabel ID="lblAttributeValues" runat="server" Text="<%$ Resources:SR, AttributeValues %>"
                            AssociatedControlID="txtNameValue"/>:&nbsp;<span>*</span>
                     </asp:TableCell>
                </asp:TableRow>
         </asp:Table>
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" AddLineText="<%$ Resources:SR, AddValue %>" 
                ID="cgList" runat="server" AllowDeleting="true" AllowViewing="false" AllowEditing="false" SortExpression="None" AllowAdding="true" KeyField="Id" Width="70%">
                <Columns>
                    <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Value %>" DataField="Name" SortExpression="None"
                        ColumnType="String" MaxLength="255" IsEditableDataField="Editable">
                        <qwc:ColumnRequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>" />
                    </qwc:TextColumn>
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
