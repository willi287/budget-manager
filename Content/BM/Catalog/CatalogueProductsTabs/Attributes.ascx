﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attributes.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsTabs.Attributes" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="e2w" TagName="PopupControl" Src="~/Controls/Core/PopupControl.ascx" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="0D21557F-0FC9-432F-BCA5-B5E14DED81A0" />
<br />
<asp:Button runat="server" ID="btnAdd" Text="Add" OnCommand="OnAddAttribute" CssClass="js-needSave"></asp:Button>
<br />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, NotPossibleToDelete %>" runat="server" />
<br />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="Name" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
 KeyField="Id" AllowViewing="false" AllowDeleting="False" AllowEditing="False" OnDeleting="CgDeleting">
   <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, AttributeName %>" DataField="Name" SortExpression="None" />    
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Value %>" DataField="Value" SortExpression="None" />        
    </Columns>
</qwc:ControlledGrid>

<input runat="server" type="hidden" class="js-isChange" id="isChange"/>
