﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceLists.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsTabs.PriceLists" %>
<%@ Import Namespace="Eproc2.BM.S4S.Dtos.Immutable" %>
<%@ Import Namespace="Eproc2.Core.Entities.Interfaces" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="58edc026-2b4c-41fb-870d-6dba36ac5912" />
<br/>
<%if(((IProductEntity)Data).Type != ProductTypes.TYPE_EVENT){%>
<table>
    <tr>
        <td>
             <qwc:DropDownList ID="ddlCatalogues" runat="server" AutoPostBack="true" DataValueField="Id"
                    DataTextField="Value" SortOrder="None" OnSelectedIndexChanged="ddlCatalogue_SelectedIndexChanged"/>
        </td>
        <td>
             <qwc:DropDownList ID="ddlPriceLists" runat="server" AutoPostBack="true" DataValueField="Id"
                    DataTextField="Value" SortOrder="None" OnSelectedIndexChanged="ddlPriceList_SelectedIndexChanged"/>
        </td>
        <td>
           <asp:Button ID="btnCreate" runat="server" Text="<%$ Resources:SR, CreateNew %>"
                    OnClick="btnCreate_Click" /> 
        </td>
   </tr>
</table>
<br/>
 <asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:SR, AddItem %>" CssClass="js-needSave"
                    OnClick="btnAdd_Click" />
<asp:Button ID="btnAddPending" runat="server" Text="<%$ Resources:SR, AddPendingItem %>" CssClass="js-needSave"
                    OnClick="btnAddPending_Click" />
<br />
<asp:Panel ID="pnlErrors" runat="server" CssClass="error" />
<e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, ProcedureInContractMsg %>" runat="server" />
<br />
<qwc:InfoLabel ID="LiveLabel" runat="server" Text="<%$ Resources:SR, AT_CM_PriceListProductsLive%>"  AssociatedControlID="cgProductList" />:
<qwc:ControlledGrid ID="cgProductList" runat="server" SortExpression="tBM_Product.Name" OnDeleting="CgProductDeleting" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id" >
   <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PriceListCode %>" DataField="PriceListCode" SortExpression="tPriceList.PriceListCode"/>      
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, AT_CM_BCG %>" DataField="Name" SortExpression="tBM_Product.Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="TypeName" AllowSorting="false" SortExpression="None" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, InvoicePrice %>" DataField="InvoicePrice" SortExpression="NetInvoicePrice" />
    </Columns>
</qwc:ControlledGrid>
<br />
<qwc:InfoLabel ID="PendingLabel" runat="server" Text="<%$ Resources:SR, AT_CM_PriceListProductsPending%>" AssociatedControlID="cgPendingProductList" />:
<qwc:ControlledGrid ID="cgPendingProductList" runat="server" SortExpression="tBM_Product.Name" OnDeleting="CgPendingProductDeleting" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
    <Columns>
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR,PriceListCode %>" DataField="PriceListCode" SortExpression="tPriceList.PriceListCode" />      
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, AT_CM_BCG %>" DataField="Name" SortExpression="tBM_Product.Name" />
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="TypeName" AllowSorting="false" SortExpression="None" />
         <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, InvoicePrice %>" DataField="InvoicePrice" SortExpression="NetInvoicePrice" />
    </Columns>
</qwc:ControlledGrid>

<input runat="server" type="hidden" class="js-isChange" id="isChange"/>
<%} else {%>
    <qwc:Label ID="TabNotAvaibleLabel" runat="server" Text="<%$ Resources:SR, ThePriceListsTabNotAvailableMsg%>" />
<% } %>

