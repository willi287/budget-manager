﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsTabs.Main" %>
<%@ Import Namespace="Eproc2.Core.Entities" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/Calendar.ascx" TagName="Calendar" TagPrefix="e2c" %>
<uc:HelpBox runat="server" HelpBoxId="7006fc0a-9cc8-43ae-be3f-8af6154c8d10" />
<table class="form">
    <tr>
        <td>
            <asp:Table runat="server" class="subForm">
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblProviderCatalogue" runat="server" Text="<%$ Resources:SR, ProviderCatalogue %>"
                            AssociatedControlID="ddlProviderCatalogue" />:&nbsp;<span>*</span>
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:DropDownList ID="ddlProviderCatalogue" runat="server" DataValueField="ID" DataTextField="Value" />
                        <asp:RequiredFieldValidator ID="ddlProviderCatalogueRequiredFieldValidator" ControlToValidate="ddlProviderCatalogue" EnableClientScript="true" ErrorMessage="*" runat="server" InitialValue="<%$ Resources:BM_SR, NoValues %>" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"></asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel ID="lblCustomFieldName" runat="server" Text="<%$ Resources:SR, QuotedCustomFieldName %>"
                            AssociatedControlID="tbCustomFieldName" />:
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:TextBox ID="tbCustomFieldName" runat="server" MaxLength="255" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblProductCode" runat="server" Text="<%$ Resources:SR, ProductCode %>"
                            AssociatedControlID="tbProductCode" />:&nbsp;<span>*</span>
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:TextBox ID="tbProductCode" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="tbProductCodeEmptyValidator" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="tbProductCode" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblProductName" runat="server" Text="<%$ Resources:SR, ProductName %>"
                            AssociatedControlID="tbProductName" />:&nbsp;<span>*</span>
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:TextBox ID="tbProductName" runat="server" MaxLength="255" />
                        <asp:RequiredFieldValidator ID="tbProductNameEmptyValidator" ErrorMessage="*" ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                            ControlToValidate="tbProductName" EnableClientScript="true" runat="server">
                        </asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblType" runat="server" Text="<%$ Resources:SR, Type %>" AssociatedControlID="ddlType" />:&nbsp;<span>*</span>
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:DropDownList ID="ddlType" runat="server" DataValueField="Id" DataTextField="Value"
                            IsEmptyItemVisible="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblCategory" runat="server" Text="<%$ Resources:SR, Category %>"
                            AssociatedControlID="lblCategoryDesc" />:&nbsp;<span>*</span>
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:Label ID="lblCategoryDesc" runat="server" /><br />
                        <asp:Button runat="server" ID="btnChangeCategory" Text="Change" OnCommand="OnChangeCategory"></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel ID="lblProductDescription" runat="server" Text="<%$ Resources:SR, ProductDescription %>"
                            AssociatedControlID="ltDescription" />:
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                            <asp:Literal ID="ltDescription" runat="server" />
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Button runat="server" ID="btEditDesc" Text="<%$ Resources:SR, EditDescription %>" OnCommand="OnEditDescription"></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </td>
        <td>
            <asp:Table runat="server" class="subForm grey">
                <asp:TableRow ID="CreationDateBlock">
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel ID="lblCreationDate" runat="server" Text="<%$ Resources:SR, CreationDate %>"
                            AssociatedControlID="lbCreationDate" />:
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:Label ID="lbCreationDate" class="state_field" runat="server" AllowLabelRendering="True"
                            ReadOnly="true" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow ID="PublishDateBlock">
                    <asp:TableCell class="required caption">
                        <qwc:InfoLabel ID="lblPublishDate" runat="server" Text="<%$ Resources:SR, PublishDate %>"
                            AssociatedControlID="calPublishDate" />:
                    </asp:TableCell>
                    <asp:TableCell>
                        <e2c:Calendar ID="calPublishDate" class="state_field" runat="server" Required="True" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel ID="lblDisableProduct" runat="server" Text="<%$ Resources:SR, DisableProductInAllCats %>"
                            AssociatedControlID="chkDisableProduct" />:
                    </asp:TableCell>
                    <asp:TableCell>
                        <qwc:QWCheckBox ID="chkDisableProduct" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
             <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <br/>
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR, UploadProductImageList %>"
                            AssociatedControlID="fuList" ID="lblListImage"/>:
                    </asp:TableCell>
                 </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell>
                        <uc1:FileUpload ID="fuList" runat="server" FileFilter="Image File |*.jpg" MaxFileLength="21000" IsSubmitOnUpload="true"/>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell class="imgStoreHeader image-scale-container">
                        <qwc:Image ID="listImage" runat="server" AllowPreview="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell class="caption">
                        <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR, UploadProductImageDetails %>"
                            AssociatedControlID="fuDetails" ID="lblDetailsImage"/>:
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                         <uc1:FileUpload ID="fuDetails" runat="server" FileFilter="Image File |*.jpg" MaxFileLength="1048576" IsSubmitOnUpload="true" 
                         FormatMaxFileLength="<%$ Resources:BM_ValidationSR, MaximumFileSize %>"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="imgStoreHeader image-scale-container">
                        <qwc:Image ID="detailsImage" runat="server" AllowPreview="true" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </td>
    </tr>
</table>

