﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlannedChange.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsTabs.PlannedChange" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="e2w" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="c1cb9540-83b6-41b5-bb5b-de98c832e6e4" />
<br/>
 <% if (Controller.IsExist(Data.Id))
 { %>
 <e2w:PopupControl ID="pcDelete" Text="<%$ Resources:ValidationSR, ProcedureInContractMsg %>" runat="server" />
<qwc:InfoLabel ID="CatalogChangesLabel" runat="server" Text="<%$ Resources:SR, CatalogChangesMsg%>"  AssociatedControlID="cgList" />:
<br />
<qwc:ControlledGrid ID="cgList" runat="server" SortExpression="tBM_Product.Name" OnDeleting="CgDeleting" ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>" KeyField="Id">
   <Columns>         
         <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
         <qwc:DateTimeColumn HeaderTitle="<%$ Resources:SR,PublishDate %>" DataField="PublishDate" AllowSorting="false" SortExpression="None" />      
         <qwc:TextColumn HeaderTitle="<%$ Resources:SR, AddedBy %>" DataField="AddedBy" AllowSorting="false" SortExpression="None" />
    </Columns>
</qwc:ControlledGrid>
<% }
   else
   { %>
    <qwc:InfoLabel ID="NotCatalogChangesLabel" runat="server" Text="<%$ Resources:SR, NotCatalogChangesMsg%>" />:
    <br />
    <asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:SR, AddNew %>" CssClass="js-needSave"
                    OnClick="btnAdd_Click" /> 
<% } %>

<input runat="server" type="hidden" class="js-isChange" id="isChange"/>
