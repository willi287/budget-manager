﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalDetails.ascx.cs"
    Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsTabs.AdditionalDetails" %>
<%@ Import Namespace="Eproc2.BM.S4S.Dtos.Immutable" %>
<%@ Register Src="~/Controls/Core/SetupAvailability.ascx" TagName="SetupAvailability"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/SetupDuration.ascx" TagName="SetupDuration" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/SetupMinIncrements.ascx" TagName="SetupMinIncrements"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="ad3d0bdc-b8e4-4601-bde6-546e23d0c613" />
<table class="form">
    <tbody>
        <tr>
            <td>
                <table class="subForm">
                    <tbody>
                        <%if (Data.Type != ProductTypes.TYPE_EVENT)
                          {%>
                        <asp:Panel runat="server" ID="ManufacturerDetailsPanel">
                            <tr>
                                <td class="caption">
                                    <qwc:InfoLabel ID="lblManufacturer" runat="server" Text="<%$ Resources:SR, Manufacturer %>"
                                        AssociatedControlID="tbManufacturer" />:
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbManufacturer" runat="server" MaxLength="255" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption">
                                    <qwc:InfoLabel ID="InfoLabel2" runat="server" Text="<%$ Resources:SR, ManufacturerCode %>"
                                        AssociatedControlID="tbManufacturerCode" />:
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbManufacturerCode" runat="server" MaxLength="50" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <%}%>
                        <%if (Data.Type != ProductTypes.TYPE_EVENT)
                          {%>
                        <asp:Panel runat="server" ID="VAT_UOM_StandardPricePanel">
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel ID="lblVatPercent" runat="server" Text="<%$ Resources:SR, VatPercent %>"
                                        AssociatedControlID="ddlVatPercent" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:DropDownList ID="ddlVatPercent" runat="server" DataValueField="ID" DataTextField="Value"
                                        SortOrder="None" />
                                </td>
                            </tr>
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel ID="lblUnitOfMeasure" runat="server" Text="<%$ Resources:SR, UnitOfMeasure %>"
                                        AssociatedControlID="ddlUnitOfMeasure" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:DropDownList ID="ddlUnitOfMeasure" runat="server" DataValueField="ID" DataTextField="Value"
                                        SortOrder="None" />
                                </td>
                            </tr>
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel ID="lblStandardPrice" runat="server" Text="<%$ Resources:SR, StandardPrice %>"
                                        AssociatedControlID="tbStandardPrice" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:PriceTextBox ID="tbStandardPrice" runat="server" Width="218px" />
                                    <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                        EnableClientScript="true" ID="RequiredStandardPriceValidator" ControlToValidate="tbStandardPrice"
                                        Display="Dynamic" runat="server" ErrorMessage="*" />
                                    <asp:RangeValidator ID="rvStandardPriceRange" MinimumValue="0.00" MaximumValue="999999.99"
                                        Type="Currency" runat="server" Text="*" ControlToValidate="tbStandardPrice" ToolTip="<%$ Resources:ValidationSR,CurrencyVEMsg %>"
                                        Display="Dynamic" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <%}%>
                        <%if (Data.Type != ProductTypes.TYPE_PRODUCT)
                          {%>
                        <asp:Panel runat="server" ID="PostcodePanel">
                            <tr>
                                <td class="caption">
                                    <qwc:InfoLabel ID="lblPostcode" runat="server" Text="<%$ Resources:SR, Postcode %>"
                                        AssociatedControlID="tbPostcode" />:
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbPostcode" runat="server" MaxLength="10" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <%}%>
                    </tbody>
                </table>
            </td>
            <td>
                <table class="subForm">
                    <tbody>
                        <%if (Data.Type == ProductTypes.TYPE_PRODUCT)
                          {%>
                        <asp:Panel runat="server" ID="UOMDetailsPanel">
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel ID="lblUnitQuantity" runat="server" Text="<%$ Resources:SR, UnitQuantity %>"
                                        AssociatedControlID="tbUnitQuantity" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:TextBox ID="tbUnitQuantity" runat="server" />
                                    <asp:RequiredFieldValidator ToolTip="<%$ Resources:ValidationSR, RequiredMsgInj %>"
                                        EnableClientScript="true" ID="RequiredFieldValidatorUnitQuantity" ControlToValidate="tbUnitQuantity"
                                        Display="Dynamic" runat="server" ErrorMessage="*" />
                                    <asp:RangeValidator ID="tbUnitQuantityRange" MinimumValue="1" MaximumValue="100000"
                                        Type="Integer" runat="server" Text="*" ControlToValidate="tbUnitQuantity" ToolTip="<%$ Resources:ValidationSR,UnitQuantityRangeVEMsg %>"
                                        Display="Dynamic" />
                                </td>
                            </tr>
                            <tr>
                                <td class="required caption">
                                    <qwc:InfoLabel ID="lblOrderingQuantity" runat="server" Text="<%$ Resources:SR, OrderingQuantity %>"
                                        AssociatedControlID="ddlOrderingQuantity" />:&nbsp;<span>*</span>
                                </td>
                                <td>
                                    <qwc:DropDownList ID="ddlOrderingQuantity" runat="server" DataValueField="ID" DataTextField="Value"
                                        SortOrder="None" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <%}%>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<br />
<%if (Data.Type == ProductTypes.TYPE_SERVICE)
  {%>
<asp:Panel runat="server" ID="DurationPanel">
    <uc:SetupDuration ID="ctrlSetupDuration" runat="server" />
</asp:Panel>
<br />
<asp:Panel runat="server" ID="MinIncrementsPanel">
    <uc:SetupMinIncrements ID="ctrlSetupMinIncrements" runat="server" />
</asp:Panel>
<br />
<%}%>
<% if (Data.Type == ProductTypes.TYPE_SERVICE || Data.Type == ProductTypes.TYPE_EVENT)
   {%>
<asp:Panel runat="server" ID="AvailabilityPanel">
    <uc:SetupAvailability ID="ctrlSetupAvailability" runat="server" />
</asp:Panel>
<%}%>
