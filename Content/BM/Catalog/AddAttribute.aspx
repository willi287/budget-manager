﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.AddAttribute" %>
<%@ Register Src="AddAttributeContent.ascx" TagPrefix="uc" TagName="AddAttributeContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:AddAttributeContent ID="addAttributeContent" runat="server" />
</asp:Content>