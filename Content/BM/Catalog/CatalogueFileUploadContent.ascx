﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueFileUploadContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueFileUploadContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Silverlight/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="72e1a2ce-caba-431b-91cd-a349012caf02" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<table class="form">
    <tr>
        <td>
            <table class="subForm" style="width: 500px;">
                <tbody>
                    <tr>
                        <td>
                            <qwc:InfoLabel  runat="server" Text="<%$ Resources:SR, UploadCatalogueFile %>" AssociatedControlID="fuCatalogueFile" ID="lblUploadCatalogueFile"/>:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:FileUpload ID="fuCatalogueFile" runat="server" FileFilter="Excel file (*.xls, *.xlsx) |*.xls;*.xlsx" MaxFileLength="10485760" IsSubmitOnUpload="false"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
