﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Main.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.Core.Administration.AdministrationListPageBase" %>
<%@ Register src="ProviderCataloguesListContent.ascx" tagname="ProviderCataloguesListContent" tagprefix="ctrl" %>
<%@ Register TagPrefix="spring" Namespace="Spring.Web.UI.Controls" Assembly="Spring.Web" %>

<asp:Content ID="ctnProviderCataloguesList" ContentPlaceHolderID="cphMainContent" runat="server">
    <spring:ValidationSummary ID="ItemValidationSummary" runat="server" />
    <ctrl:ProviderCataloguesListContent ID="ContentControl" runat="server" />
</asp:Content>
