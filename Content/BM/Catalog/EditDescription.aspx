﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FullScreen.Master" AutoEventWireup="true" Inherits="Eproc2.Web.Content.BM.Catalog.EditDescription" %>
<%@ Register Src="EditDescriptionContent.ascx" TagPrefix="uc" TagName="EditDescriptionContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc:EditDescriptionContent ID="editDescriptionContent" runat="server" />
</asp:Content>
