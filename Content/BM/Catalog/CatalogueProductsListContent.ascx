<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueProductsListContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueProductsListContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>
<%@ Register TagPrefix="e2w" TagName="Search" Src="~/Controls/Core/SearchWithFields.ascx" %>
<%@ Register Src="~/Controls/Core/PopupControl.ascx" TagName="PopupControl" TagPrefix="uc2" %>
<uc:HelpBox runat="server" HelpBoxId="f8d1663a-bebb-4516-b8d7-4caba164f051" />
<div id="search">
    <e2w:Search ID="ucSearch" runat="server" />
</div>
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />
<uc2:PopupControl ID="pcDelete" runat="server" Text="<%$ Resources:SR, CantDeleteBcg %>" />

<qwc:ControlledGrid ID="cgList" runat="server"
    SortExpression="Code"
    ConfirmationMsg="<%$ Resources:SR, ConfirmationDelete %>"
    KeyField="Id"
    OnViewing="OnViewGridItem" OnEditing="OnEditGridItem"
    >
    <Columns>
        <qwc:SelectColumn AlwaysVisible="true" IsEditable="true"  DataField="Selected"/>
        <qwc:ActionColumn HeaderTitle="<%$ Resources:SR,Actions %>" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR,ProductCode %>" DataField="Code" SortExpression="Code" />
        <qwc:TextColumn HeaderTitle="<%$ Resources:SR, ProductName %>" DataField="Name" SortExpression="Name" />
        <qwc:DecimalColumn HeaderTitle="<%$ Resources:SR, Price %>" DataField="Price" SortExpression="Price"/>
        <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR, Type %>" DataField="Type" AllowSorting="False" SortExpression="None"/>
    </Columns>
</qwc:ControlledGrid>
