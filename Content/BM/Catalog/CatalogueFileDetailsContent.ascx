﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueFileDetailsContent.ascx.cs" Inherits="Eproc2.Web.Content.BM.Catalog.CatalogueFileDetailsContent" %>
<%@ Register Src="~/Controls/Core/HelpBox.ascx" TagName="HelpBox" TagPrefix="uc" %>

<uc:HelpBox ID="HelpBox1" runat="server" HelpBoxId="59d182ac-d2e3-4caf-8a75-a34c0134733b" />
<qwc:ImageActionRepeater ID="ImageActionRepeater" OnCommand="ActionRepeater_OnCommand" runat="server" />

<table class="form">
    <tr>
        <td>
            <table class="subForm">
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblFileName" runat="server" Text="<%$ Resources:SR, CatalogueFileDetailsFileName %>"
                            AssociatedControlID="lblFileNameValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblFileNameValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblUploadDate" runat="server" Text="<%$ Resources:SR, CatalogueFileDetailsUploadDate %>"
                            AssociatedControlID="lblUploadDateValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblUploadDateValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblUploadedBy" runat="server" Text="<%$ Resources:SR, CatalogueFileDetailsUploadedBy %>"
                            AssociatedControlID="lblUploadedByValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblUploadedByValue" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="caption">
                        <qwc:InfoLabel ID="lblStatus" runat="server" Text="<%$ Resources:SR, CatalogueFileDetailsStatus %>"
                            AssociatedControlID="lblStatusValue" />:
                    </td>
                    <td>
                        <qwc:Label ID="lblStatusValue" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="caption">
            <qwc:InfoLabel ID="lblResults" runat="server" Text="<%$ Resources:SR, CatalogueFileDetailsResults %>" />:
        </td>
    </tr>
    <tr>
        <td>
            <qwc:ControlledGrid ID="cgFileItems" runat="server" KeyField="Id" AllowSorting="False">
                <Columns>
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CatalogueFileDetailsRow%>" DataField="RowId" AllowSorting="False" SortExpression="None" />
                    <qwc:ResourceColumn HeaderTitle="<%$ Resources:SR,CatalogueFileDetailsItemStatus%>" DataField="StatusString" AllowSorting="False" SortExpression="None" />
                    <qwc:TextColumn HeaderTitle="<%$ Resources:SR,CatalogueFileDetailsErrorMessages%>" DataField="StatusMessage" AllowSorting="False" SortExpression="None" />
                </Columns>
            </qwc:ControlledGrid>
        </td>
    </tr>
</table>
