﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BackgroundTaskStarter.aspx.cs" Inherits="Eproc2.Web.Technical.BackgroundTaskStarter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 20px">
        <table align="center">            
            
                <asp:Repeater ID="ProviderList" runat="server" OnItemCommand="PokeButton_Click">
                    <HeaderTemplate>
                            <th></th>
                            <th><asp:Label ID="BackgroundTaskLabel" runat="server" >Background Task Provider</asp:Label></th>
                            <th><asp:Label ID="PokeLabel" runat="server" Visible="false"></asp:Label></th>
                            <th><asp:Label ID="LastStartHeader" runat="server">Last Start</asp:Label></th>
                            <th><asp:Label ID="IsActiveHeader" runat="server">Is Active</asp:Label></th>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><asp:Label ID="TaskIdLabel" runat="server" Visible="false" Text='<%# Eval("Id")%>'></asp:Label> </td>
                            <td><asp:Label ID="ProviderLabel" runat="server"><%# Eval("ProviderId")%></asp:Label></td>                    
                            <td><asp:Button ID="PokeButtom" runat="server" Text="Poke"/></td>
                            <td><asp:Label ID="LastStartLabel" runat="server"><%# Eval("LastStart")%></asp:Label></td>                    
                            <td align="center"><asp:CheckBox ID="IsActiveCheckBox" runat="server" Checked='<%# Eval ("IsActive") %>' Enabled="false" /></td>
                        </tr>
                    </ItemTemplate>                    
                </asp:Repeater>
             
        </table>
    </div>    
    </form>
</body>
</html>
